package com.example.binaryFaceTracker;

import android.content.Context;
import java.io.File;

public class binaryFaceTracker {

    static {
        System.loadLibrary("binary");
    }

    public static class TrackerConfig {
        public TrackerConfig() { }
        public int imageWidth;
        public int imageHeight;
        public int isFrontFacing;
        public int cameraOrientation;
        public float horizontalFov;
        public float framePerSecond;
        public float detectionFps;
        public int maxNumFaces;
        public int asyncFaceDetection;
    };

//    private native static void getLog();
//    private native static void setSetting(String bundleDir, TrackerConfig tracker_config, byte[] img, int cameraFacing, int orientation);

    private native static void initialize(String bundleDir);
    private native static int setConfig(TrackerConfig tracker_config);
    private native static void setCameraInfo(int facing, int width, int height, int orientation);
    private native static boolean setRawData(byte[] data, int orientation);

    private native static float[] getFace198Landmarks(int idx);
    private native static float[] getFace3D207Landmarks(int idx);
    private native static int getTrigger(int idx);
    private native static float[] getRigidMotionMatrix(int idx);
    private native static float[] getCameraMatrix(int idx);
    private native static int getBinaryFoundFaceNum();

    private native static float[] getFaceLandmarkFromImage(String modelPath, String inputPath);

    private native static void  getFaceSwap(String inputPath, String referencePath, float[] face_landmarks);


    public static void initializeTracker(String ItemsBundlePath) {

        initialize(ItemsBundlePath);
    }

    public static void setCameraInformation(boolean isCameraFront, int width, int height, int orientation) {

        int facing = 0;

        if(isCameraFront)
            facing = 1;

        setCameraInfo(facing, width, height, orientation);
    }


    public static void setTrackerConfig(int previewWidth,
                                   int previewHeight,
                                   float horizontalFov,
                                   float detectionFps,
                                   int orientation,
                                   boolean isFrontFacing,
                                   float fps){

        TrackerConfig config = new TrackerConfig();

        config.imageWidth = previewWidth;
        config.imageHeight = previewHeight;
        config.isFrontFacing = isFrontFacing ? 1 : 0;
        config.cameraOrientation = orientation;
        config.horizontalFov = horizontalFov;
        config.framePerSecond = fps;
        config.detectionFps = detectionFps;
        config.maxNumFaces = 5;
        config.asyncFaceDetection = 1;

        setConfig(config);
    }

    public static boolean processFrame(byte[] data, int orientation) {

        boolean ret = setRawData(data, orientation);
        return ret;
    }


    public static float[] getFaceLandmarksInfo(int idx) {
        return getFace198Landmarks(idx);
    }

    public static float[] getFace3DLandmarksInfo(int idx) {
        return getFace3D207Landmarks(idx);
    }

    public static float[] getRigidMotionMatrixInfo(int idx) {
        return getRigidMotionMatrix(idx);
    }

    public static float[] getCameraMatrixInfo(int idx) {
        return getCameraMatrix(idx);
    }

    public static int getTriggerInfo(int idx) {
        return getTrigger(idx);
    }

    public static int getBinaryFoundFaceNumInfo() {
        return getBinaryFoundFaceNum();
    }

    public static float[] getFaceLandmark198FromImage(String model_path, String path) {return getFaceLandmarkFromImage(model_path, path); }

    public static void faceSwapFromWarping(String inputPath, String referencePath, float[] face_landmarks) {getFaceSwap(inputPath, referencePath, face_landmarks);}


}