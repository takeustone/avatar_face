#include <jni.h>

#include "binaryTracker/binaryTracker.hpp"

#ifdef __cplusplus
extern "C" {
#endif


binaryTracker* ft = nullptr;

JNIEXPORT void JNICALL Java_com_example_binaryFaceTracker_binaryFaceTracker_getFaceSwap
        (JNIEnv *env, jobject obj, jstring jInputDirPath,jstring jReferDirPath, jfloatArray landmark_198) {

    int pointSize = NUMFEATUREPOINTS*2;
    const char* inputPath = (char*) env->GetStringUTFChars(jInputDirPath, JNI_FALSE);
    const char* referPath = (char*) env->GetStringUTFChars(jReferDirPath, JNI_FALSE);
    jfloat *landmarks_org = (float*) env->GetFloatArrayElements(landmark_198, JNI_FALSE);

    ft->getFaceSwapResult(inputPath, referPath, landmarks_org);

}



JNIEXPORT jfloatArray JNICALL Java_com_example_binaryFaceTracker_binaryFaceTracker_getFaceLandmarkFromImage
        (JNIEnv *env, jobject obj, jstring jModelDirPath,jstring jContentDirPath) {

    int pointSize = NUMFEATUREPOINTS*2;
    const char* path = (char*) env->GetStringUTFChars(jContentDirPath, JNI_FALSE);
    const char* model_path = (char*) env->GetStringUTFChars(jModelDirPath, JNI_FALSE);
    jfloatArray result = env->NewFloatArray(pointSize);

    jfloat landmarks_from_image[pointSize];
    const float *nativeLandmarksFromImage = ft->getFaceLandmarkFromImage(model_path, path);

    if(nativeLandmarksFromImage != NULL) {
        for(int i=0; i< pointSize; ++i)
            landmarks_from_image[i] = nativeLandmarksFromImage[i];

        env->SetFloatArrayRegion(result, 0, pointSize, landmarks_from_image);
    }

    return result;
}




JNIEXPORT void JNICALL Java_com_example_binaryFaceTracker_binaryFaceTracker_initialize
        (JNIEnv *env, jobject obj, jstring jContentDirPath) {
    //logger->init(LOG_LEVEL, FILE_LOG_LEVEL, DEFAULT_LOG_FILENAME);
    ft = nullptr;//SAFE_DELETE(ft);

    ft = new binaryTracker();
    if (!ft) {
        return;
    }
    const char* path = (char*) env->GetStringUTFChars(jContentDirPath, JNI_FALSE);

    ft->initialize(path);
    env->ReleaseStringUTFChars(jContentDirPath, path);
}

JNIEXPORT void JNICALL Java_com_example_binaryFaceTracker_binaryFaceTracker_setCameraInfo
        (JNIEnv *env, jobject obj, jint facing, jint width, jint height, jint orientation) {
    if (ft) {
        ft->setCameraInfo(facing, width, height, orientation);
    }
}

JNIEXPORT jint JNICALL Java_com_example_binaryFaceTracker_binaryFaceTracker_setConfig
        (JNIEnv *env, jobject obj, jobject tracker_config) {

    int ret = 0;

    jclass config_class = env->GetObjectClass(tracker_config);

    binary_config_t config;

    memset(&config, 0, sizeof(config));

    config.image_width = env->GetIntField(
            tracker_config,
            env->GetFieldID(config_class, "imageWidth", "I"));

    config.image_height = env->GetIntField(
            tracker_config,
            env->GetFieldID(config_class, "imageHeight", "I"));

    config.isFrontFacing = env->GetIntField(
            tracker_config,
            env->GetFieldID(config_class, "isFrontFacing", "I"));

    config.camera_orientation = env->GetIntField(
            tracker_config,
            env->GetFieldID(config_class, "cameraOrientation", "I"));

    config.horizontal_fov = env->GetFloatField(
            tracker_config,
            env->GetFieldID(config_class, "horizontalFov", "F"));

    config.frame_per_second = env->GetFloatField(
            tracker_config,
            env->GetFieldID(config_class, "framePerSecond", "F"));

    config.detection_fps = env->GetFloatField(
            tracker_config,
            env->GetFieldID(config_class, "detectionFps", "F"));

    config.max_num_faces = env->GetIntField(
            tracker_config,
            env->GetFieldID(config_class, "maxNumFaces", "I"));

    config.async_face_detection = env->GetIntField(
            tracker_config,
            env->GetFieldID(config_class, "asyncFaceDetection", "I"));

    if (ft != NULL)
        ret = ft->setConfig(&config);

    return ret;
}

// 카메라에서 제공하는 이미지 프로세싱용 raw data feeding.
JNIEXPORT jboolean JNICALL Java_com_example_binaryFaceTracker_binaryFaceTracker_setRawData
        (JNIEnv *env, jobject obj, jbyteArray img, jint orientation) {

    if (ft) {
        jbyte *yuvPixels = (jbyte *) env->GetByteArrayElements(img, JNI_FALSE);
        bool retVal = ft->setImageBuffer((unsigned char *) yuvPixels, orientation);
        env->ReleaseByteArrayElements(img, yuvPixels, JNI_FALSE);
        return (jboolean)retVal;
    }
    return (jboolean) false;
}




JNIEXPORT jfloatArray JNICALL Java_com_example_binaryFaceTracker_binaryFaceTracker_getFace198Landmarks
        (JNIEnv *env, jobject obj, jint idx) {
    int pointSize = NUMFEATUREPOINTS*2;
    jfloatArray result = env->NewFloatArray(pointSize);

    //if (renderers != NULL) {
        jfloat llandmarks[pointSize];
        const float *nativeLandmarks = ft->getFace198Landmarks(idx);
        if (nativeLandmarks != NULL) {
            for (int i = 0; i < pointSize; ++i)
                llandmarks[i] = nativeLandmarks[i];
            env->SetFloatArrayRegion(result, 0, pointSize, llandmarks);
        }
    //}

    return result;
}

JNIEXPORT jfloatArray JNICALL Java_com_example_binaryFaceTracker_binaryFaceTracker_getFace3D207Landmarks
        (JNIEnv *env, jobject obj, jint idx) {
    int pointSize = (NUMFEATUREPOINTS + NUMTRACKINGPOINTS)*3;
    jfloatArray result = env->NewFloatArray(pointSize);

    //if (renderers != NULL) {
    jfloat llandmarks[pointSize];
    const float *nativeLandmarks = ft->getFace3D207Landmarks(idx);
    if (nativeLandmarks != NULL) {
        for (int i = 0; i < pointSize; ++i)
            llandmarks[i] = nativeLandmarks[i];
        env->SetFloatArrayRegion(result, 0, pointSize, llandmarks);
    }
    //}

    return result;
}

JNIEXPORT jfloatArray JNICALL Java_com_example_binaryFaceTracker_binaryFaceTracker_getRigidMotionMatrix
        (JNIEnv *env, jobject obj, jint idx) {

    int row = 4;
    int col = 4;
    jfloatArray result = env->NewFloatArray(row * col);

    //if (renderers != NULL) {
    jfloat llandmarks[row * col];
    const float *nativeLandmarks = ft->getRigidMotionMatrix(idx);
    if (nativeLandmarks != NULL) {
        for (int i = 0; i < row; ++i) {
            for (int j = 0; j < col; ++j) {
                llandmarks[i * 4 + j] = nativeLandmarks[i * 4 + j];
            }
        }
        env->SetFloatArrayRegion(result, 0, row * col, llandmarks);
    }
    //}

    return result;
}


JNIEXPORT jfloatArray JNICALL Java_com_example_binaryFaceTracker_binaryFaceTracker_getCameraMatrix
        (JNIEnv *env, jobject obj, jint idx) {
    int row = 4;
    int col = 4;
    jfloatArray result = env->NewFloatArray(row * col);

    //if (renderers != NULL) {
    jfloat llandmarks[row * col];
    const float *nativeLandmarks = ft->getCameraMatrix(idx);
    if (nativeLandmarks != NULL) {
        for (int i = 0; i < row; ++i) {
            for (int j = 0; j < col; ++j) {
                llandmarks[i * 4 + j] = nativeLandmarks[i * 4 + j];
            }
        }
        env->SetFloatArrayRegion(result, 0, row * col, llandmarks);
    }
    //}

    return result;
}

JNIEXPORT jint JNICALL Java_com_example_binaryFaceTracker_binaryFaceTracker_getTrigger
        (JNIEnv *env, jobject obj, jint idx) {
    return ft->getTrigger(idx);
}


JNIEXPORT jint JNICALL Java_com_example_binaryFaceTracker_binaryFaceTracker_getBinaryFoundFaceNum
        (JNIEnv *env, jobject obj) {
    return ft->getBinaryFoundFaceNum();
}


#ifdef __cplusplus
}
#endif

