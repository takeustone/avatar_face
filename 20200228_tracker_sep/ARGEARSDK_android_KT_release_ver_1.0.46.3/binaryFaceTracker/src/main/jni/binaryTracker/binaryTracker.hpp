//
// Created by JJ on 2019-12-02.
//
#pragma once

#include <BinaryFaceSDK/sdk/include/binaryface.h>

#define NUMFEATUREPOINTS 198
#define NUMTRACKINGPOINTS 9
#define NUM_FACES_TO_TRACK_DEFAULT 5


#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#define BINARYVR_API_KEY    "ktcorp-reg-narel-a031b4";
#define BINARYVR_MODEL_FILE     "model-ktcorp.bf2";

#define BINARYVR_TRIGGER_NO 9
#define BINARYVR_TRIGGER_DURATION 5 // Trigger 지속 프레임


typedef struct _binary_config_t {
    bool isSet;
    int32_t image_width;
    int32_t image_height;
    int32_t isFrontFacing;
    int32_t camera_orientation;
    float   horizontal_fov;
    float   frame_per_second;
    float   detection_fps;
    int32_t max_num_faces;
    int32_t async_face_detection;
} binary_config_t;


typedef enum {
    CAMERA_REAR = 0,
    CAMERA_FRONT = 1,
    CAMERA_UNKNOWN
} camera_position;

struct bItem {
    //int num_faces_value = 0;
    int trigger = 0;
    float* mFeature2D198Points;
    float* mFeature3D207Points;
    float rigid_motion_matrix[4][4];
    float camera_matrix[4][4];
};


class binaryTracker {

private:


public:
    binaryTracker();
    ~binaryTracker();


    std::string basePath;

    bool mInitialized = false;
    static const float foreheadPoints[];

    binaryface_context_t mContext;
    binaryface_session_t mSession;

    binary_config_t camera_config;

    int mScreenOrientation;
    int mCameraFacing;

    bItem *bi;
    int num_faces_value;


    void initialize(const char *bundleDir);
    bool isInitialized();
    int setConfig(binary_config_t *config);
    void setCameraInfo(int facing, int width, int height, int orientation);

    bool setImageBuffer(unsigned char *imageBuffer, int orientation, int bytesPerRow = 0);
    void getFaceObjectList();
    bool getOrientedLandmark(int idx);

    int openSession(binaryface_context_t context, binaryface_session_t &session, binaryface_session_config_t *config);
    int closeSession(binaryface_session_t &session);

    std::vector<float> featurePoints198{std::vector<float>(NUMFEATUREPOINTS*2,0.0f)};

    float* getFace198Landmarks(int idx);
    float* getFace3D207Landmarks(int idx);
    float* getRigidMotionMatrix(int idx);
    float* getCameraMatrix(int idx);
    int getBinaryFoundFaceNum();
    int getTrigger(int idx);

    float* getFaceLandmarkFromImage(const char *model_path, const char *path);

    void getFaceSwapResult(const char *inputPath, const char *referPath, float* landmarks_org);
    //std::vector<cv::Point2f> binaryTracker::Landmark_converter(float* landmarks_org);
};