//
// Created by JJ on 2019-12-06.
//

#include "binaryTracker.hpp"

#undef LOG_TAG
#define LOG_TAG "FaceTrack"
#include <android/log.h>
#define LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)


const float binaryTracker::foreheadPoints[] = {
        -68.1771f, 34.937f, 35.2433f,
        -61.765f, 63.2215f, 39.6908f,
        -45.3772f, 77.3563f, 54.3227f,
        -22.0381f, 85.7282f, 64.6345f,
        0.0201591f, 88.0688f, 66.2788f,
        22.0832f, 85.8522f, 64.6458f,
        45.466f, 77.5607f, 54.4098f,
        61.9087f, 63.4583f, 39.8341f,
        68.2485f, 35.0346f, 35.1627f
};



binaryTracker::binaryTracker(){
    mInitialized = false;

    mContext = 0;
    mSession = 0;

    mScreenOrientation = 0;
    mCameraFacing = CAMERA_FRONT;

    num_faces_value = 0;

    bi = new bItem[NUM_FACES_TO_TRACK_DEFAULT];
    memset(bi, 0, sizeof(struct bItem) * NUM_FACES_TO_TRACK_DEFAULT);

    for(int i=0; i<NUM_FACES_TO_TRACK_DEFAULT; i++){
        bi[i].mFeature3D207Points = new float[(NUMFEATUREPOINTS+NUMTRACKINGPOINTS) * 3];
        memset(bi[i].mFeature3D207Points, 0, sizeof(float)*(NUMFEATUREPOINTS+NUMTRACKINGPOINTS) * 3);

        bi[i].mFeature2D198Points = new float[(NUMFEATUREPOINTS) * 2];
        memset(bi[i].mFeature2D198Points, 0, sizeof(float)*(NUMFEATUREPOINTS) * 2);
    }
}

binaryTracker::~binaryTracker(){
    mInitialized = false;

    int fps = camera_config.frame_per_second > 0 ? camera_config.frame_per_second : 30;
    long milisec = 1000/fps * 2;
    struct timespec req = {0};
    req.tv_sec = 0;
    req.tv_nsec = milisec * 1000000L;

    binaryface_close_session(mSession);
    nanosleep(&req, (struct timespec *)NULL);
    binaryface_close_context(mContext);

    mContext = 0;
    mSession = 0;

    if(bi != nullptr) {
        for (int i = 0; i < NUM_FACES_TO_TRACK_DEFAULT; i++) {
            if(bi[i].mFeature3D207Points){
                delete(bi[i].mFeature3D207Points);
                bi[i].mFeature3D207Points = nullptr;
            }
            if(bi[i].mFeature2D198Points) {
                delete (bi[i].mFeature2D198Points);
                bi[i].mFeature2D198Points = nullptr;
            }
        }
    }

}

bool binaryTracker::isInitialized(){
    return mInitialized;
}


int binaryTracker::openSession(binaryface_context_t context, binaryface_session_t &session, binaryface_session_config_t *config) {

    binaryface_ret ret;
    binaryface_session_info_t session_info;

    ret = binaryface_open_session(
            context,
            config,
            &session,
            &session_info);

    return (int)ret;
}

int binaryTracker::closeSession(binaryface_session_t &session) {

    binaryface_ret ret;

    if(session != 0){
        ret = binaryface_close_session(session);
        if(ret != BINARYFACE_OK) {
            LOGE("Binary VR binaryface_close_session Failed - Code(%d)", ret);
            return ret;
        }
        session = 0;
    }

    return (int)ret;
}



void binaryTracker::initialize(const char *bundleDir){

    basePath = std::string(bundleDir);

    basePath.append("/.bundles/");


    std::string data_file_path = basePath + BINARYVR_MODEL_FILE;
    std::string app_id = BINARYVR_API_KEY;

    LOGE("jws BinaryVR : file %s", data_file_path.c_str());

    try
    {
        LOGE("Binary VR : init");
        binaryface_context_info_t info;

        binaryface_ret ret = binaryface_open_context(
                data_file_path.c_str(),
                app_id.c_str(),
                BINARYFACE_SDK_VERSION,
                &mContext,
                &info);

        if(ret == BINARYFACE_OK){
            mInitialized = true;
            LOGE("Binary VR initialized : sdk_version(%d), data_file_version(%d), num_feature_points(%d)", info.sdk_version, info.data_file_version, info.num_feature_points);
        }else {
            LOGE("Binary VR initialized failed - Error Code(%d)", ret);
        }

//        int num_action_triggers;
//        binaryface_get_num_action_triggers(mContext, &num_action_triggers);
//        //  Enumerate and save all action trigger names
//        for (int i = 0; i < num_action_triggers; i++) {
//            const char *name;
//            binaryface_get_action_trigger_name(mContext, i, &name);
//            ////LOGE("Action Trigger %d: %s", i, name);
//        }


    }catch (const std::exception& e)
    {
        LOGE("Error initialize tracker : %s", e.what());
        return;
    }

}


int binaryTracker::setConfig(binary_config_t *config) {

    binaryface_ret ret;

    if(!mInitialized || !mContext) return BINARYFACE_INVALID_PARAMETER;

    config->isSet = true;
    if(config->isSet){
        memcpy(&camera_config, config, sizeof(*config));
        camera_config.isSet = true;
        config->isSet = false;
    }
    else
        return BINARYFACE_INVALID_PARAMETER;


    if(mSession != 0){
        ret = (binaryface_ret)closeSession(mSession);
        if(ret != BINARYFACE_OK) {
            LOGE("Binary VR binaryface_close_session Failed - Code(%d)", ret);
            return ret;
        }
    }

    if(mSession == 0 && camera_config.isSet){

        binaryface_session_config_t config;

        config.image_width = camera_config.image_width;
        config.image_height = camera_config.image_height;
        config.async_face_detection = camera_config.async_face_detection;
        config.frame_per_second = camera_config.frame_per_second;
        config.max_num_faces = camera_config.max_num_faces;
        config.horizontal_fov = camera_config.horizontal_fov;
        config.detection_fps = camera_config.detection_fps;
        config.min_face_size = 0;

        ret = (binaryface_ret)openSession(mContext, mSession, &config);

        binaryface_session_register_tracking_3d_points(mSession, NUMTRACKINGPOINTS, foreheadPoints);

        if(ret != BINARYFACE_OK) {
            LOGE("Binary VR binaryface_open session Failed - Code(%d)", ret);
            return ret;
        }

    }

    return ret;
}



bool binaryTracker::setImageBuffer(unsigned char *imageBuffer, int orientation, int bytesPerRow) {

    if (!mInitialized || !(camera_config.image_width*camera_config.image_height) || !mContext) {
        LOGE("FaceTrack is not Initialized or frameWidth(%d)*frameHeight(%d) = 0  ======> return;", camera_config.image_width, camera_config.image_height);
        return false; //escape when tracker is not initialized
    }

    binaryface_ret ret;
    if(mSession == 0 && camera_config.isSet){

        binaryface_session_config_t config;

        config.image_width = camera_config.image_width;
        config.image_height = camera_config.image_height;
        config.async_face_detection = camera_config.async_face_detection;
        config.frame_per_second = camera_config.frame_per_second;
        config.max_num_faces = camera_config.max_num_faces;
        config.horizontal_fov = camera_config.horizontal_fov;
        config.detection_fps = camera_config.detection_fps;
        config.min_face_size = 0;

        ret = (binaryface_ret)openSession(mContext, mSession, &config);

        binaryface_session_register_tracking_3d_points(mSession, NUMTRACKINGPOINTS, foreheadPoints);

        if(ret != BINARYFACE_OK) {
            LOGE("Binary VR binaryface_open session Failed - Code(%d)", ret);
            return false;
        }

    }

    if(mSession){
        int ori = orientation;

        switch (ori) {
            case 0:
                ori = BINARYFACE_IMAGE_ORIENTATION_FIRST_ROW_UP;
                break;
            case 90:
                ori = BINARYFACE_IMAGE_ORIENTATION_FIRST_COLUMN_UP;
                break;
            case 180:
                ori = BINARYFACE_IMAGE_ORIENTATION_LAST_ROW_UP;
                break;
            case 270:
                ori = BINARYFACE_IMAGE_ORIENTATION_LAST_COLUMN_UP;
                break;
        }

        try {
            ret = binaryface_run_tracking(mSession, imageBuffer, 0, BINARYFACE_PIXEL_FORMAT_GRAY, ori);

        }catch (const std::exception& e)
        {
            LOGE("Error binaryface_run_tracking : %s", e.what());
            return false;
        }

        if(ret != BINARYFACE_OK) {
            LOGE("Binary VR binaryface_run_tracking ERROR - Code(%d)", ret);
        }

    }

    if(isInitialized()&&(camera_config.image_width * camera_config.image_height)) {
        getFaceObjectList();
    }

    return true;
}




void binaryTracker::setCameraInfo(int facing, int width, int height, int orientation) {

    if (! (width * height)) {
        LOGE(" ------ setCameraInfo : width * height = 0 ---> return;");
        return;
    }


    if(mScreenOrientation != orientation || mCameraFacing != facing) {
        mCameraFacing = facing;
        mScreenOrientation = orientation;
    }

}



void binaryTracker::getFaceObjectList(){

    if(!mInitialized || !mContext || !mSession) {
        LOGE("getFaceObjectList null");
        return;
    }

    try
    {
        binaryface_ret ret;
        binaryface_session_info_t sessionInfo;

        num_faces_value = 0;
        ret = binaryface_get_num_faces(mSession, &num_faces_value);

        binaryface_get_session_info(mSession, &sessionInfo);


        for (int i = 0; i < NUM_FACES_TO_TRACK_DEFAULT; ++i) {
            if (i<num_faces_value) {
                if(getOrientedLandmark(i)) {

                    //Check Trigger Condition
                    for (int idx=0; idx < BINARYVR_TRIGGER_NO; idx++) {
                        binaryface_check_action_trigger(mSession, idx, i,&bi[i].trigger);
                    }
                }


            }

        }

    }catch (const std::exception& e)
    {
        LOGE("Facetrack Exception Occurred! : %s", e.what());
        return;
    }

}



bool binaryTracker::getOrientedLandmark(int idx) {

    if(!mContext || !mSession)
        return false;

    binaryface_ret ret;
    binaryface_session_info_t sessionInfo;
    binaryface_face_info_t face_info;
    binaryface_face_3d_info_t face_3d_info;

    ret = binaryface_get_session_info(mSession, &sessionInfo);
    if(ret != BINARYFACE_OK){
        return false;
    }

    ret = binaryface_get_face_info(mSession, idx, &face_info, featurePoints198.data());
    for(int i=0; i<NUMFEATUREPOINTS * 2; i++){
        bi[idx].mFeature2D198Points[i] = featurePoints198[i];
    }

    if(ret != BINARYFACE_OK){
        return false;
    }

    ret = binaryface_get_face_3d_info(mSession, idx, &face_3d_info, bi[idx].mFeature3D207Points, &bi[idx].mFeature3D207Points[NUMFEATUREPOINTS * 3]);
    if(ret != BINARYFACE_OK){
        return false;
    }

    for(int row=0; row<4; row++){
        for(int col=0; col<4; col++){
            bi[idx].rigid_motion_matrix[row][col] = face_3d_info.rigid_motion_matrix[row][col];
            bi[idx].camera_matrix[row][col] = sessionInfo.camera_matrix[row][col];
        }
    }


    return true;
}

void binaryTracker::getFaceSwapResult(const char *inputPath, const char *referPath, float* landmarks_org) {
   // std::string file_input = inputPath;
    //std::string file_refer = referPath;

    cv::Mat img1 = cv::imread(inputPath);
    cv::Mat img2 = cv::imread(referPath);
    cv::Mat img1Warped = img2.clone();




}

/*std::vector<cv::Point2f> binaryTracker::Landmark_converter(float* landmarks_org) {
    int points[68] ={0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, // 얼굴외곽 (0~16)
                     33, 35, 37, 39, 41, // 왼쪽 눈썹 (17~21)
                     58, 56, 54, 52, 50, // 오른쪽 눈썹 (22~26)
                     67, 69, 71, 73, // 코 (27~30)
                     81, 82, 83, 84, 85, // 코 밑 (31~35)
                     93, 95, 97, 99, 101, 103, // 왼쪽 눈 (36~41)
                     111, 109, 107, 105, 115, 113, // 오른쪽 눈 (42~47)
                     117, 119, 121, 123, 125, 127, 129, // 윗입 바깥쪽 (48~54)
                     131, 133, 135, 137, 139, // 아랫입 바깥쪽 (55~59)
                     //(118+140)/2, 142, 144, 146, (128+130)/2, 149, 151, 153 // 입 안쪽 (60~67)

    };
}*/

float* binaryTracker::getFaceLandmarkFromImage(const char *model_path, const char *path) {
    std::string model_file;
    model_file = std::string(model_path);
    model_file.append("/.bundles/");

    std::string model_file_path = model_file + BINARYVR_MODEL_FILE;
    std::string app_id = BINARYVR_API_KEY;

    LOGE("jws model : file %s", model_file_path.c_str());
    LOGE("jws input : file %s", path);

    cv::Mat face;
    face = cv::imread(path);

    if (face.rows % 2 != 0)
    {
        cv::resize(face, face, cv::Size(face.cols, face.rows + 1));
    }

    if (face.cols % 2 != 0)
    {
        cv::resize(face, face, cv::Size(face.cols + 1, face.rows));
    }

    cv::Mat video_frame_yuv;
    cv::Mat video_frame_y(face.rows, face.cols, 0);

    cv::cvtColor(face, video_frame_yuv, CV_BGR2YUV_I420);
    video_frame_yuv(cv::Rect(0, 0, face.cols, face.rows)).copyTo(video_frame_y);
    //cv::Mat frame_y(face.rows);

    binaryface_context_t bf_context = 0;
    binaryface_context_info_t bf_context_info;

    int ret = 0;
    ret = binaryface_open_context(model_file_path.c_str(), app_id.c_str(), BINARYFACE_SDK_VERSION, &bf_context, &bf_context_info);

    binaryface_session_config_t bf_sess_config;

    bf_sess_config.image_width = face.cols;
    bf_sess_config.image_height = face.rows;
    bf_sess_config.max_num_faces = 1;

    bf_sess_config.frame_per_second = 1;
    bf_sess_config.async_face_detection = 0;
    bf_sess_config.horizontal_fov = 60.0f;
    bf_sess_config.detection_fps = 0.0f;
    bf_sess_config.min_face_size = 0;

    binaryface_session_t bf_sess = 0;
    binaryface_session_info_t bf_sess_info;

    ret = binaryface_open_session(bf_context, &bf_sess_config, &bf_sess, &bf_sess_info);

    ///----------------------------------------------------------------------------------------------

    binaryface_face_info_t face_info_out;

    float coords_out[198 * 2];

    int32_t face_num;

    binaryface_run_tracking(bf_sess, video_frame_y.data, 0, BINARYFACE_PIXEL_FORMAT_GRAY, BINARYFACE_IMAGE_ORIENTATION_FIRST_ROW_UP);

    binaryface_get_num_faces(bf_sess, &face_num);

    binaryface_get_face_info(bf_sess, 0, &face_info_out, coords_out);

    binaryface_close_session(bf_sess);
    binaryface_close_context(bf_context);

    return coords_out;
}

float* binaryTracker::getFace198Landmarks(int idx){
    if(&bi[idx] && idx<NUM_FACES_TO_TRACK_DEFAULT){
        return bi[idx].mFeature2D198Points;
    }
    LOGE("binaryItem[%d].mFeature3D207Points NULL",idx);
    return nullptr;
}

float* binaryTracker::getFace3D207Landmarks(int idx){
    if(&bi[idx] && idx<NUM_FACES_TO_TRACK_DEFAULT) {
        return bi[idx].mFeature3D207Points;
    }
    LOGE("binaryItem[%d].mFeature3D207Points NULL",idx);
    return nullptr;
}

float* binaryTracker::getRigidMotionMatrix(int idx){

    if(&bi[idx] && idx<NUM_FACES_TO_TRACK_DEFAULT) {
        return *bi[idx].rigid_motion_matrix;
    }
    LOGE("binaryItem[%d].rigid_motion_matrix NULL",idx);
    return nullptr;
}



float* binaryTracker::getCameraMatrix(int idx){

    if(&bi[idx] && idx<NUM_FACES_TO_TRACK_DEFAULT) {
        return *bi[idx].camera_matrix;
    }
    LOGE("binaryItem[%d].camera_matrix NULL",idx);
    return nullptr;
}


int binaryTracker::getBinaryFoundFaceNum(){
    return num_faces_value;
}

int binaryTracker::getTrigger(int idx){

    if(&bi[idx] && idx<NUM_FACES_TO_TRACK_DEFAULT) {
        return bi[idx].trigger;
    }
    return 0;
}