package com.argear.sdksample;

import android.annotation.SuppressLint;
import android.app.KeyguardManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.argear.sdksample.adapter.Beauty2ListAdapter;
import com.argear.sdksample.adapter.FilterListAdapter;
import com.argear.sdksample.adapter.StickerCategoryListAdapter;
import com.argear.sdksample.adapter.StickerListAdapter;
import com.argear.sdksample.data.Beauty2ItemData;
import com.argear.sdksample.model.CategoryModel;
import com.argear.sdksample.model.CategoryRespModel;
import com.argear.sdksample.model.FilterModel;
import com.argear.sdksample.model.ItemModel;
import com.argear.sdksample.network.ItemService;
import com.seerslab.argearsdk.ARGearSDK;
import com.seerslab.eos.Seerslab3DMM;
import com.example.binaryFaceTracker.binaryFaceTracker;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.argear.sdksample.SLThreadPool.postOnUiThread;

public class CameraActivity extends AppCompatActivity implements
        StickerCategoryListAdapter.Listener, StickerListAdapter.Listener,
        FilterListAdapter.Listener, RadioGroup.OnCheckedChangeListener, Beauty2ListAdapter.Listener {

    private static final String TAG = CameraActivity.class.getSimpleName();

    // camera
    private FrameLayout mCameraLayout;
    private View mViewTopRatio;
    private View mViewBottomRatio;
    private LinearLayout mViewFunctions;
    private RadioGroup mRadioGroupBitrate;
    private RadioButton mOneMBpsGroupButton;
    private RadioButton mTwoMBpsGroupButton;
    private RadioButton mFourMBpsGroupButton;
    private RadioGroup mRadioGroupCameraRatio;
    private CheckBox mCheckboxVideoOnly;
    private TextView mTextViewFaceStatus;
    private TextView mTextViewTriggerStatus;

    // sticker
    private RecyclerView mRecyclerViewStickerCategory;
    private RecyclerView mRecyclerViewSticker;
    private StickerCategoryListAdapter mStickerCategoryListAdapter;
    private StickerListAdapter mStickerListAdapter;
    private RelativeLayout mViewStickers;

    // filter
    private RecyclerView mRecyclerViewFilter;
    private FilterListAdapter mFilterListAdapter;
    private RelativeLayout mViewFilters;
    private TextView mTextViewFilterValue;

    // beauty
    private RelativeLayout mViewBeauty;
    private View mViewFaceType;
    private View mViewSkinLevel;
    private View mViewSlimLevel;
    private View mViewEyeLevel;
    private SeekBar mSeekBarSlimLevel;
    private SeekBar mSeekBarNoseLevel;
    private SeekBar mSeekBarEyeLevel;
    private SeekBar mSeekBarSkinLevel;
    private TextView mTextSlimLevel;
    private TextView mTextNoseLevel;
    private TextView mTextEyeLevel;
    private TextView mTextSkinLevel;

    // beauty2
    private RelativeLayout mViewBeauty2;
    private RecyclerView mRecyclerViewBeauty2;
    private Beauty2ListAdapter mBeauty2ListAdapter;
    private TextView mBeauty2LevelInfo;
    private SeekBar mBeauty2SeekBar;
    private Button mComparisonButton;

    // bulge
    private RelativeLayout mViewBulge;

    private LinearLayout mMoreLayout;
    private ToggleButton mButtonVideoRecording;
    private Button mButtonVideoPlaying;
    private CheckBox mCheckDebugLandmark;
    private CheckBox mCheckDebugHWRect;


    private GLView mGlView;
    private ReferenceCamera mCamera;

    private boolean mIsRecording = false;
    private File mRecordingFile;

    private boolean mFilterVignette = false;
    private boolean mFilterBlur = false;
    private int mFilterLevel = 100;
    private String mCurrentFilteritemID = null;
    private String mCurrentStickeritemID = null;
    private boolean mHasTrigger = false;

    private int mDeviceWidth = 0;
    private int mDeviceHeight = 0;
    private int mTargetWidth = 0;
    private int mTargetHeight = 0;

	private boolean mIsInitialize = false;

    private BroadcastReceiver unlockReceiver;
    private IntentFilter unlockFilter;
    private Toast mTriggerToast = null;

    private Beauty2ItemData mBeauty2ItemData;
    private int mCurrentBeautyType = AppConfig.BEAUTY_TYPE_SKIN_FACE;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.e(TAG, " ******** protected void onCreate(Bundle savedInstanceState)  ");

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.activity_camera);

        Point realSize = new Point();
        Display display= ((WindowManager)this.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        display.getRealSize(realSize);
        mDeviceWidth = realSize.x;
        mDeviceHeight = realSize.y;
        mTargetWidth = realSize.x;
        mTargetHeight = realSize.y;

        mBeauty2ItemData = new Beauty2ItemData();

        initUI();

        Realm.init(this);
        RealmConfiguration realmConfig = new RealmConfiguration.Builder().build();
        Realm.setDefaultConfiguration(realmConfig);

        requestStickerAPI();

        Seerslab3DMM.initialize(this);
    }

    private void runResume() {
        ARGearSDK.createEngine(this, AppConfig.API_KEY, AppConfig.SECRET_KEY);
        binaryFaceTracker.initializeTracker(this.getFilesDir().getAbsolutePath());

        initialize();

        if (mIsInitialize) {
            ARGearSDK.resumeEngine();
        }

        updateUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, " ******** protected void onResume() ");

        KeyguardManager km = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
        if(km.inKeyguardRestrictedInputMode()) {
            if (unlockReceiver == null) {
                Log.e(TAG, " ******** onResume() at lockscreen");
                // 전원버튼의 의해 lockscreen 상태로 갔을때, lockscreen 밑에 떠 있는 activity의 resume이 호출되어
                // crash가 발생하는 경우가 있음. lockscreen 상태일때는 unlock event를 등록하고, 실제 unlock event가
                // 호출 되었을때 resume을 타게함.
                unlockFilter = new IntentFilter(Intent.ACTION_USER_PRESENT);
                unlockReceiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        if (intent.getAction().equals(Intent.ACTION_USER_PRESENT)) {
                            Log.e(TAG, " ******** receive unlock event ");
                            if (mIsInitialize == false) {
                                runResume();
                            }

                            if (unlockReceiver != null) {
                                unregisterReceiver(unlockReceiver);
                                unlockReceiver = null;
                                unlockFilter = null;
                            }
                        }
                    }
                };
                registerReceiver(unlockReceiver, unlockFilter);
            }
        } else {
            if (mIsInitialize == false) {
                runResume();
            }
        }

        initBeautyDefault();
    }

    @Override
    protected void onPause() {
        super.onPause();

        Log.e(TAG, " ******** protected void onPause() ");
        if (mIsInitialize) {
            mIsInitialize = false;

            mCamera.stopCamera();
            mCamera.destroy();

            ARGearSDK.pauseEngine();

            mCameraLayout.removeView(mGlView);
            mGlView = null;

            ARGearSDK.destoryEngine();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG, " ******** protected void onDestroy() ");
    }

    private void initialize() {
        mCameraLayout = findViewById(R.id.camera_layout);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        mGlView = new GLView(this);
        mGlView.setZOrderMediaOverlay(true);
        initGLView();

        mCameraLayout.addView(mGlView, params);

        initCamera();

        mIsInitialize = true;
    }

    private void initGLView() {
        mGlView.setGLViewListener(new GLView.GLViewListener() {
            @Override
            public void onGLSurfaceCreated() {
                mCamera.startCamera();
            }
        });
    }

    private void initCamera() {

        mCamera = new ReferenceCamera1(this, getWindowManager().getDefaultDisplay().getRotation());

        if (mCamera.getCameraFacingFrontValue() != -1) {
            mCamera.setFacing(mCamera.getCameraFacingFrontValue());
        } else if (mCamera.getCameraFacingBackValue() != -1) {
            mCamera.setFacing(mCamera.getCameraFacingBackValue());
        } else {
            return;
        }

        mCamera.setCameraStateListener(new ReferenceCamera.CameraStateListener() {
            @Override
            public void onReadyCamera(boolean success, boolean availableFlash) {
                Log.d(TAG, " ###### onReadyCamera " + mCamera.isCameraFacingFront());

                if (success) {
                    ARGearSDK.isCameraFacingFront = mCamera.isCameraFacingFront();
                    ARGearSDK.CameraPreviewSize = mCamera.getPreviewSize();

                    binaryFaceTracker.setCameraInformation(mCamera.isCameraFacingFront(),mCamera.getPreviewSize()[1],mCamera.getPreviewSize()[0],90);
                    if (!ARGearSDK.isResumed()) {
                        ARGearSDK.resumeEngine();
                    }

                    if (mCurrentFilteritemID != null) {
                        ARGearSDK.setFilter(mCurrentFilteritemID);
                    }
                    if (mCurrentStickeritemID != null) {
                        ARGearSDK.setItem(mCurrentStickeritemID);
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            initRatioUI();
                        }
                    });
                }
            }

            @Override
            public void onCloseCamera(boolean stopRender) {

            }

            @Override
            public void onUpdateFaceFoundState(final boolean foundFace) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(ARGearSDK.isFaceRequired()) {
                            if (foundFace) {
                                mTextViewFaceStatus.setText("Face Found");
                            } else {
                                mTextViewFaceStatus.setText("Face REQUIRED!!");
                            }
                        } else {
                            mTextViewFaceStatus.setText("NO REQUIRED");
                        }

                        // TRIGGER_MOUTH_FLAG   = (1 << 0)
                        // TRIGGER_HEAD_FLAG    = (1 << 1)
                        // TRIGGER_DELAY_FLAG   = (1 << 2)
                        int triggerstatus;
                        triggerstatus = ARGearSDK.getTriggerFlag();
                        mTextViewTriggerStatus.setText(String.format("%d", triggerstatus));
                        onGetTriggerFlag(triggerstatus);
                    }
                });
            }
        });

        mRecordingFile = new File(Environment.getExternalStorageDirectory(), AppConfig.RECORDING_FILE_NAMW);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initUI() {
        mViewTopRatio = findViewById(R.id.top_ratio_view);
        mViewBottomRatio = findViewById(R.id.bottom_ratio_view);

        mViewFunctions = findViewById(R.id.functions_layout);
        mViewStickers = (RelativeLayout) findViewById(R.id.stickers_layout);
        mViewFilters = (RelativeLayout) findViewById(R.id.filters_layout);
        mViewBeauty = (RelativeLayout) findViewById(R.id.beauty_layout);
        mViewBeauty2 = (RelativeLayout) findViewById(R.id.beauty_layout2);

        mCheckboxVideoOnly = (CheckBox) findViewById(R.id.video_only_checkbox);
        mRadioGroupBitrate = (RadioGroup) findViewById(R.id.bitrate_radiogroup);
        mRadioGroupBitrate.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.onembps_radiobutton:
                        AppPreference.getInstance(CameraActivity.this).setBitrate(1);
                        break;
                    case R.id.twombps_radiobutton:
                        AppPreference.getInstance(CameraActivity.this).setBitrate(2);
                        break;
                    case R.id.fourmbps_radiobutton:
                        AppPreference.getInstance(CameraActivity.this).setBitrate(3);
                        break;
                }
            }
        });

        mOneMBpsGroupButton = findViewById(R.id.onembps_radiobutton);
        mTwoMBpsGroupButton = findViewById(R.id.twombps_radiobutton);;
        mFourMBpsGroupButton = findViewById(R.id.fourmbps_radiobutton);;

        mRadioGroupCameraRatio = (RadioGroup) findViewById(R.id.camera_ratio_radiogroup);
        mRadioGroupCameraRatio.setOnCheckedChangeListener(this);
        // init category list
        mRecyclerViewStickerCategory = (RecyclerView) findViewById(R.id.sticker_category_recyclerview);

        mTextViewFaceStatus = (TextView) findViewById(R.id.face_status_textview);
        mTextViewTriggerStatus = (TextView) findViewById(R.id.trigger_status_textview);

        mRecyclerViewStickerCategory.setHasFixedSize(true);
        LinearLayoutManager categoryLayoutManager = new LinearLayoutManager(this);
        categoryLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mRecyclerViewStickerCategory.setLayoutManager(categoryLayoutManager);

        mStickerCategoryListAdapter = new StickerCategoryListAdapter(this);
        mRecyclerViewStickerCategory.setAdapter(mStickerCategoryListAdapter);


        // init sticker list
        mRecyclerViewSticker = (RecyclerView) findViewById(R.id.sticker_recyclerview);

        mRecyclerViewSticker.setHasFixedSize(true);
        LinearLayoutManager itemsLayoutManager = new LinearLayoutManager(this);
        itemsLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mRecyclerViewSticker.setLayoutManager(itemsLayoutManager);

        mStickerListAdapter = new StickerListAdapter(this, this);
        mRecyclerViewSticker.setAdapter(mStickerListAdapter);

        // init filter list
        mRecyclerViewFilter = (RecyclerView) findViewById(R.id.filter_recyclerview);

        mRecyclerViewFilter.setHasFixedSize(true);
        LinearLayoutManager filterLayoutManager = new LinearLayoutManager(this);
        filterLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mRecyclerViewFilter.setLayoutManager(filterLayoutManager);

        mFilterListAdapter = new FilterListAdapter(this, this);
        mRecyclerViewFilter.setAdapter(mFilterListAdapter);

        mTextViewFilterValue = (TextView) findViewById(R.id.filter_value_textview);


        mViewBulge = (RelativeLayout) findViewById(R.id.bulge_layout);
        mViewFaceType = findViewById(R.id.facetype_layout);
        mViewSkinLevel = findViewById(R.id.skin_level_layout);
        mViewSlimLevel = findViewById(R.id.slim_Level_layout);
        mViewEyeLevel = findViewById(R.id.eyelevel_layout);

        mTextEyeLevel = (TextView) findViewById(R.id.eyelevel_textview);
        mSeekBarEyeLevel = (SeekBar) findViewById(R.id.eyelevel_seekbar);
        mSeekBarEyeLevel.setProgress(0);

        mTextSlimLevel = (TextView) findViewById(R.id.slimlevel_textview);
        mSeekBarSlimLevel = (SeekBar) findViewById(R.id.slimlevel_seekbar);
        mSeekBarSlimLevel.setProgress(0);

        mTextNoseLevel = (TextView) findViewById(R.id.noselevel_textview);
        mSeekBarNoseLevel = (SeekBar) findViewById(R.id.noselevel_seekbar);
        mSeekBarNoseLevel.setProgress(0);

        mTextSkinLevel = (TextView) findViewById(R.id.skin_level_textview);
        mSeekBarSkinLevel = (SeekBar) findViewById(R.id.skin_level_seekbar);
        mSeekBarSkinLevel.setProgress(0);

        mMoreLayout = findViewById(R.id.more_layout);
        mButtonVideoRecording = findViewById(R.id.video_recording_button);
        mButtonVideoPlaying = findViewById(R.id.video_playing_button);
        mCheckDebugLandmark = findViewById(R.id.debug_landmark_checkbox);
        mCheckDebugHWRect = findViewById(R.id.debug_rect_checkbox);

        mRecyclerViewBeauty2 = findViewById(R.id.beauty2_items_layout);
        mRecyclerViewBeauty2.setHasFixedSize(true);
        LinearLayoutManager beauty2LayoutManager = new LinearLayoutManager(this);
        beauty2LayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mRecyclerViewBeauty2.setLayoutManager(beauty2LayoutManager);

        mBeauty2ListAdapter = new Beauty2ListAdapter(mBeauty2ItemData.getItemInfoData(), this);
        mRecyclerViewBeauty2.setAdapter(mBeauty2ListAdapter);

        mBeauty2LevelInfo = findViewById(R.id.beauty2_level_info);
        mBeauty2SeekBar = findViewById(R.id.beauty2_seekbar);
        mBeauty2SeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    updateBeautyInfoPosition(mBeauty2LevelInfo);
                    updateBeautyLevel(mCurrentBeautyType, progress);
                    mBeauty2ItemData.updateSeekBarInfo(mCurrentBeautyType, progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                mBeauty2LevelInfo.setVisibility(View.VISIBLE);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mBeauty2LevelInfo.setVisibility(View.GONE);
                int progress = seekBar.getProgress();
            }
        });

        mComparisonButton = findViewById(R.id.bulge_comparison_button);
        mComparisonButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if(MotionEvent.ACTION_DOWN == event.getAction()) {
                    zeroBulge();
                } else if(MotionEvent.ACTION_UP == event.getAction()) {
                    reloadBulge();
                }
                return true;
            }
        });
    }

    private void updateUI() {
        boolean landmark = AppPreference.getInstance(this).isCheckedLandmark();
        boolean faceRect = AppPreference.getInstance(this).isCheckedFaceRect();
        boolean videoOnly = AppPreference.getInstance(this).isCheckedVideoOnly();
        int bitrate = AppPreference.getInstance(this).getBitrate();

        setDrawLandmark(landmark, faceRect);
        if (landmark) {
            mCheckDebugLandmark.setChecked(true);
        }
        if (faceRect) {
            mCheckDebugHWRect.setChecked(true);
        }

        if (videoOnly) {
            mCheckboxVideoOnly.setChecked(true);
        }

        if (bitrate == 2) {
            mTwoMBpsGroupButton.setChecked(true);
        } else if (bitrate == 3) {
            mFourMBpsGroupButton.setChecked(true);
        } else {
            mOneMBpsGroupButton.setChecked(true);
        }
    }

    private void initRatioUI() {
        int previewWidth = mCamera.getPreviewSize()[1];
        int previewHeight = mCamera.getPreviewSize()[0];

        int cameraRatioType = mCamera.getCameraRatio();
        if (cameraRatioType == ReferenceCamera.CAMERA_RATIO_FULL) {
            mTargetHeight = mDeviceHeight;
            mTargetWidth = (int) ((float) mDeviceHeight * previewWidth / previewHeight );
        } else {
            mTargetWidth = mDeviceWidth;
            mTargetHeight = (int) ((float) mDeviceWidth * previewHeight / previewWidth);
        }

        if (mGlView != null) {
            mGlView.getHolder().setFixedSize(mTargetWidth, mTargetHeight);
        }

        if (cameraRatioType == ReferenceCamera.CAMERA_RATIO_FULL) {
            mViewTopRatio.setVisibility(View.GONE);
            mViewBottomRatio.setVisibility(View.GONE);
        } else {
            if (is1to1CameraRatio()) {
                int viewTopHight = (mTargetHeight - mTargetWidth) / 2;
                int viewBottomHeight = mDeviceHeight - (viewTopHight + mTargetWidth);
                mViewTopRatio.getLayoutParams().height = viewTopHight;
                mViewBottomRatio.getLayoutParams().height = viewBottomHeight;
                mViewTopRatio.setVisibility(View.VISIBLE);
                mViewBottomRatio.setVisibility(View.VISIBLE);
            } else {
                int viewBottomHeight = mDeviceHeight - mTargetHeight;
                mViewBottomRatio.getLayoutParams().height = viewBottomHeight;
                mViewTopRatio.setVisibility(View.GONE);
                mViewBottomRatio.setVisibility(View.VISIBLE);
            }
        }

        updateUIStyle(cameraRatioType);
    }

    private void updateUIStyle(int ratio) {
        switch (ratio){
            case ReferenceCamera.CAMERA_RATIO_FULL:
                mBeauty2SeekBar.setActivated(false);
                mBeauty2LevelInfo.setActivated(false);
                mBeauty2LevelInfo.setTextColor(Color.BLACK);
                break;
            case ReferenceCamera.CAMERA_RATIO_4_3:
                mBeauty2SeekBar.setActivated(true);
                mBeauty2LevelInfo.setActivated(true);
                mBeauty2LevelInfo.setTextColor(Color.WHITE);
                break;
        }
    }

    private void updateBeautyInfoPosition(TextView view) {
        if (view != null) {
            int progress = mBeauty2SeekBar.getProgress();
            int max = mBeauty2SeekBar.getMax();
            //int percent = (int) (progress * 100.0f / max);
            if (max <= 100) {
                view.setText(String.format(Locale.getDefault(), "%d", progress));
            } else {
                view.setText(String.format(Locale.getDefault(), "%d", (progress - 100)));
            }

            int paddingLeft = 0;
            int paddingRight = 0;
            int offset = -5;
            int viewWidth = view.getWidth();
            int x = (int) ((float) (mBeauty2SeekBar.getRight() - mBeauty2SeekBar.getLeft() - paddingLeft - paddingRight - viewWidth - 2 * offset)
                    * progress / max)
                    + mBeauty2SeekBar.getLeft() + paddingLeft + offset;
            view.setX(x);
        }
    }

    private void updateBeautyLevel(int beautyType, int progress) {
        float progress_parameter = getBulgeLevelValue(beautyType, progress);
        ARGearSDK.setBeautyParam(beautyType, progress_parameter);
    }

    private float getBulgeLevelValue(int beautyType, int progress) {
        float progress_parameter = progress;

        switch (beautyType) {
            case -1:        // bulge skin
                break;
            case AppConfig.BEAUTY_TYPE_VLINE: {
                progress_parameter = (float)(progress - AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_VLINE]);
                break;
            }
            case AppConfig.BEAUTY_TYPE_FACE_SLIM: {
                progress_parameter = (float)(progress - AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_FACE_SLIM]);
                break;
            }
            case AppConfig.BEAUTY_TYPE_JAW: {
                progress_parameter = (float)(progress - AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_JAW]);
                break;
            }
            case AppConfig.BEAUTY_TYPE_CHIN: {
                progress_parameter = (float)(progress - AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_CHIN]);
                break;
            }
            case AppConfig.BEAUTY_TYPE_EYE: {
                progress_parameter = (float)(progress - AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_EYE]);
                break;
            }
            case AppConfig.BEAUTY_TYPE_EYE_GAP: {
                progress_parameter = (float)(progress - AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_EYE_GAP]);
                break;
            }
            case AppConfig.BEAUTY_TYPE_NOSE_LINE: {
                progress_parameter = (float)(progress - AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_NOSE_LINE]);
                break;
            }
            case AppConfig.BEAUTY_TYPE_NOSE_SIDE: {
                progress_parameter = (float)(progress - AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_NOSE_SIDE]);
                break;
            }
            case AppConfig.BEAUTY_TYPE_NOSE_LENGTH: {
                progress_parameter = (float)(progress - AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_NOSE_LENGTH]);
                break;
            }
            case AppConfig.BEAUTY_TYPE_MOUTH_SIZE: {
                progress_parameter = (float)(progress - AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_MOUTH_SIZE]);
                break;
            }
            case AppConfig.BEAUTY_TYPE_EYE_BACK: {
                progress_parameter = (float)(progress - AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_EYE_BACK]);
                break;
            }
            case AppConfig.BEAUTY_TYPE_LIP_SIZE: {
                progress_parameter = (float)(progress - AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_LIP_SIZE]);
                break;
            }
            case AppConfig.BEAUTY_TYPE_EYE_CORNER: {
                progress_parameter = (float)(progress - AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_EYE_CORNER]);
                break;
            }
            case AppConfig.BEAUTY_TYPE_SKIN_FACE: {
                progress_parameter = (float) (progress - AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_SKIN_FACE]);
                break;
            }
            case AppConfig.BEAUTY_TYPE_SKIN_DARK_CIRCLE: {
                progress_parameter = (float) (progress - AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_SKIN_DARK_CIRCLE]);
                break;
            }
            case AppConfig.BEAUTY_TYPE_SKIN_MOUTH_WRINKLE: {
                progress_parameter = (float)(progress - AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_SKIN_MOUTH_WRINKLE]);
                break;
            }
        }
        return progress_parameter;
    }

    private void initBulge() {
        float bulgeBeauty[] = new float[AppConfig.BEAUTY_TYPE_NUM];

        // default init setting
        bulgeBeauty[AppConfig.BEAUTY_TYPE_VLINE             ] = getBulgeLevelValue(AppConfig.BEAUTY_TYPE_VLINE             ,AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_VLINE             ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_VLINE             ]);
        bulgeBeauty[AppConfig.BEAUTY_TYPE_FACE_SLIM         ] = getBulgeLevelValue(AppConfig.BEAUTY_TYPE_FACE_SLIM         ,AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_FACE_SLIM         ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_FACE_SLIM         ]);
        bulgeBeauty[AppConfig.BEAUTY_TYPE_JAW               ] = getBulgeLevelValue(AppConfig.BEAUTY_TYPE_JAW               ,AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_JAW               ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_JAW               ]);
        bulgeBeauty[AppConfig.BEAUTY_TYPE_CHIN              ] = getBulgeLevelValue(AppConfig.BEAUTY_TYPE_CHIN              ,AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_CHIN              ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_CHIN              ]);
        bulgeBeauty[AppConfig.BEAUTY_TYPE_EYE               ] = getBulgeLevelValue(AppConfig.BEAUTY_TYPE_EYE               ,AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_EYE               ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_EYE               ]);
        bulgeBeauty[AppConfig.BEAUTY_TYPE_EYE_GAP           ] = getBulgeLevelValue(AppConfig.BEAUTY_TYPE_EYE_GAP           ,AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_EYE_GAP           ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_EYE_GAP           ]);
        bulgeBeauty[AppConfig.BEAUTY_TYPE_NOSE_LINE         ] = getBulgeLevelValue(AppConfig.BEAUTY_TYPE_NOSE_LINE         ,AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_NOSE_LINE         ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_NOSE_LINE         ]);
        bulgeBeauty[AppConfig.BEAUTY_TYPE_NOSE_SIDE         ] = getBulgeLevelValue(AppConfig.BEAUTY_TYPE_NOSE_SIDE         ,AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_NOSE_SIDE         ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_NOSE_SIDE         ]);
        bulgeBeauty[AppConfig.BEAUTY_TYPE_NOSE_LENGTH       ] = getBulgeLevelValue(AppConfig.BEAUTY_TYPE_NOSE_LENGTH       ,AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_NOSE_LENGTH       ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_NOSE_LENGTH       ]);
        bulgeBeauty[AppConfig.BEAUTY_TYPE_MOUTH_SIZE        ] = getBulgeLevelValue(AppConfig.BEAUTY_TYPE_MOUTH_SIZE        ,AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_MOUTH_SIZE        ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_MOUTH_SIZE        ]);
        bulgeBeauty[AppConfig.BEAUTY_TYPE_EYE_BACK          ] = getBulgeLevelValue(AppConfig.BEAUTY_TYPE_EYE_BACK          ,AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_EYE_BACK          ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_EYE_BACK          ]);
        bulgeBeauty[AppConfig.BEAUTY_TYPE_EYE_CORNER        ] = getBulgeLevelValue(AppConfig.BEAUTY_TYPE_EYE_CORNER        ,AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_EYE_CORNER        ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_EYE_CORNER        ]);
        bulgeBeauty[AppConfig.BEAUTY_TYPE_LIP_SIZE          ] = getBulgeLevelValue(AppConfig.BEAUTY_TYPE_LIP_SIZE          ,AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_LIP_SIZE          ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_LIP_SIZE          ]);
        bulgeBeauty[AppConfig.BEAUTY_TYPE_SKIN_FACE         ] = getBulgeLevelValue(AppConfig.BEAUTY_TYPE_SKIN_FACE         ,AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_SKIN_FACE         ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_SKIN_FACE         ]);
        bulgeBeauty[AppConfig.BEAUTY_TYPE_SKIN_DARK_CIRCLE  ] = getBulgeLevelValue(AppConfig.BEAUTY_TYPE_SKIN_DARK_CIRCLE  ,AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_SKIN_DARK_CIRCLE  ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_SKIN_DARK_CIRCLE  ]);
        bulgeBeauty[AppConfig.BEAUTY_TYPE_SKIN_MOUTH_WRINKLE] = getBulgeLevelValue(AppConfig.BEAUTY_TYPE_SKIN_MOUTH_WRINKLE,AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_SKIN_MOUTH_WRINKLE] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_SKIN_MOUTH_WRINKLE]);

        ARGearSDK.setBeautyAllParam(bulgeBeauty);
    }

    private void zeroBulge() {
        float bulgeBeauty[] = new float[AppConfig.BEAUTY_TYPE_NUM];

        for(int i=0; i<AppConfig.BEAUTY_TYPE_NUM; i++)
            bulgeBeauty[i] = 0;

        ARGearSDK.setBeautyAllParam(bulgeBeauty);
    }

    private void initBeautyDefault(){
        float[] bulgeBeauty = new float[AppConfig.BEAUTY_TYPE_NUM];

        for(int i=0; i<AppConfig.BEAUTY_TYPE_NUM; i++){
            bulgeBeauty[i] = ARGearSDK.getBeautyParam(i);
        }
        ARGearSDK.setBeautyAllParam(bulgeBeauty);
    }

    private void reloadBulge() {
        float bulgeBeauty[] = new float[AppConfig.BEAUTY_TYPE_NUM];
        for (int i = 0; i< AppConfig.BEAUTY_TYPE_NUM; i++) {
            Beauty2ItemData.Beauty2ItemSeekBarInfo seekBarInfo = mBeauty2ItemData.getSeekBarInfo(i);
            bulgeBeauty[i] = getBulgeLevelValue(i, seekBarInfo.mProgress);
        }
        ARGearSDK.setBeautyAllParam(bulgeBeauty);
    }

    public void onClickButtons(View v) {
        switch (v.getId()){
            case R.id.more_button: {
                if (mMoreLayout.getVisibility() == View.GONE) {
                    mMoreLayout.setVisibility(View.VISIBLE);
                } else {
                    mMoreLayout.setVisibility(View.GONE);
                }
                break;
            }
            case R.id.debug_landmark_checkbox:
            case R.id.debug_rect_checkbox:
                setDrawLandmark(mCheckDebugLandmark.isChecked(), mCheckDebugHWRect.isChecked());
                break;
            case R.id.video_only_checkbox:
                if (mCheckboxVideoOnly.isChecked()) {
                    AppPreference.getInstance(this).setVideoOnly(true);
                } else {
                    AppPreference.getInstance(this).setVideoOnly(false);
                }
                break;
            case R.id.sdk_version_button: {
                Toast toast = Toast.makeText(CameraActivity.this, "SDK Version : " + ARGearSDK.getVersion(), Toast.LENGTH_SHORT);
                toast.show();
            }
                break;
            case R.id.sticker_button:
                showStickers();
                break;
            case R.id.close_sticker_button:
                closeStickers();
                break;
            case R.id.clear_sticker_button: {
                clearStickers();
                mCurrentStickeritemID = null;
                mHasTrigger = false;
                break;
            }
            case R.id.filter_button:
                showFilters();
                break;
            case R.id.close_filter_button:
                closeFilters();
                break;
            case R.id.clear_filter_button: {
                clearFilter();
                mCurrentFilteritemID = null;
                break;
            }
            case R.id.vignett_button: {
                mFilterVignette = !mFilterVignette;
                ARGearSDK.setVignette(mFilterVignette);
                break;
            }
            case R.id.blur_button: {
                mFilterBlur = !mFilterBlur;
                ARGearSDK.setBlurVignette(mFilterBlur);
                break;
            }
            case R.id.filter_plus_button: {
                mFilterLevel = ARGearSDK.getFilterStrength();
                if (mFilterLevel < 100) mFilterLevel += 10;
                ARGearSDK.setFilterStrength(mFilterLevel);
                mTextViewFilterValue.setText("filter value : " + mFilterLevel);
                break;
            }
            case R.id.filter_minus_button: {
                mFilterLevel = ARGearSDK.getFilterStrength();
                if (mFilterLevel > 0) mFilterLevel -= 10;
                ARGearSDK.setFilterStrength(mFilterLevel);
                mTextViewFilterValue.setText("filter value : " + mFilterLevel);
                break;
            }
            case R.id.video_recording_button:
                if (mIsRecording || !mButtonVideoRecording.isChecked()) {
                    mIsRecording = false;
                    stopRecording();
                    mButtonVideoPlaying.setEnabled(true);
                } else {
                    mIsRecording = true;
                    int targetBitrate = getTargetBitrate();
                    boolean ratio1to1 = is1to1CameraRatio();
                    Log.i(TAG, "startRecording with videoOnly : " + mCheckboxVideoOnly.isChecked() + ", 1to1 : " + ratio1to1 + ", bitrate : " + targetBitrate);
                    startRecording(mRecordingFile, targetBitrate, mCheckboxVideoOnly.isChecked(), ratio1to1);
                    mButtonVideoPlaying.setEnabled(false);
                }
                break;
            case R.id.video_playing_button: {
                Intent intent = new Intent(this, PlayerActivity.class);
                Bundle b = new Bundle();
                b.putString(PlayerActivity.INTENT_URI, mRecordingFile.getAbsolutePath()); //Your id
                intent.putExtras(b);
                startActivity(intent);
                break;
            }
            case R.id.take_picture_button: {
                ARGearSDK.takePicture(AppConfig.SAVE_PICTURE_PATH);
                Toast toast = Toast.makeText(CameraActivity.this, "Picture saved at " + AppConfig.SAVE_PICTURE_PATH, Toast.LENGTH_SHORT);
                toast.show();
            }
            break;

            case R.id.eos_button: {

                //String inputImgPath = AppConfig.SAVE_PICTURE_PATH;
                String inputImgPath = "/mnt/sdcard/input_image.jpg";
                String model_path = this.getFilesDir().getAbsolutePath();
                //String model_path = "/storage/emulated/0/Download/";

                String referencePath = "/mnt/sdcard/toImage.jpg";
                String referenceLandmark = "/mnt/sdcard/input_landmark.txt";

                Log.d(TAG, "jws modelPath: " + model_path + " inputPath: " +  inputImgPath);

                float[] landmarkFromImage = new float[75*2];

                try {
                    FileReader tab = new FileReader(referenceLandmark);
                    char[] in = new char[1000];
                    Scanner z = new Scanner(System.in);
                    tab.read(in);
                    String fileRead = new String(in, 0, in.length);
                    String lineRead[] = fileRead.split("\n");

                    Log.d(TAG, "jws lineRead length: " + lineRead.length);

                    for(int i=0; i<lineRead.length; i++)
                    {
                        String k[] = lineRead[i].split("\\s+");
                        //Log.d(TAG, "jws k length: " + k.length );
                        landmarkFromImage[2*i] = Float.parseFloat(k[0]);
                        //Log.d(TAG, "jws k[0]: " + k[0]);
                        landmarkFromImage[2*i+1] = Float.parseFloat(k[1]);
                        //Log.d(TAG, "jws k[1]: " + k[1]);
                        //Log.d(TAG, "jws k[2]: " + k[2]);
                    }

                } catch(IOException ioe) {
                    System.out.println("IOException : " + ioe);
                }

                /*for(int i=0; i<75; i++)
                {
                    Log.d(TAG, "jws landmarkFromImage(" + i + "): " + landmarkFromImage[2*i] + " " + landmarkFromImage[2*i+1]);
                }*/

                float[] landmarkFromRefer = {13, 251,
                        19,     322,
                        34,      390,
                        53,        441,
                        93,        479,
                        134,      505,
                        173,      523,
                        223,      543,
                        281,      549,
                        339,      543,
                        390,      523,
                        428,      505,
                        470,      479,
                        509,      441,
                        527,      390,
                        543,      322,
                        549,      251,
                        70 ,       193,
                        93 ,       168,
                        131,      156,
                        174,      156,
                        225,      175,
                        338,      175,
                        388,      156,
                        432,      156,
                        468,      168,
                        493,      193,
                        281,      210,
                        281,      242,
                        281,      275,
                        281,      306,
                        237,      335,
                        261,      343,
                        281,      345,
                        302,      343,
                        324,      335,
                        115,      227,
                        134,      218,
                        181,      213,
                        212,      227,
                        177,      238,
                        132,      237,
                        350,      227,
                        382,      213,
                        428,      218,
                        448,      227,
                        429,      237,
                        385,      238,
                        196,      412,
                        216,      397,
                        247,      389,
                        281,      389,
                        315,      389,
                        346,      397,
                        366,      412,
                        360,      429,
                        324,      442,
                        281,      446,
                        238,      442,
                        202,      429,
                        239,      404,
                        281,      403,
                        323,      404,
                        323,      424,
                        281,      431,
                        239,      424,
                        26 ,       180,
                        57 ,       87,
                        113,      33,
                        182,      20,
                        281,      14,
                        381,      20,
                        449,      33,
                        507,      87,
                        536,      180};

                /*for(int i=0; i<75; i++)
                {
                    Log.d(TAG, "jws landmarkFromRefer(" + i + "): " + landmarkFromRefer[2*i] + " " + landmarkFromRefer[2*i+1]);
                }*/


                //landmarkFromImage = binaryFaceTracker.getFaceLandmark198FromImage(model_path, inputImgPath);
                int[] triangle_index;

                binaryFaceTracker.faceSwapFromWarping(inputImgPath, referencePath, landmarkFromImage);

/*                for(int i=0; i<198; i++)
                {
                    Log.d(TAG, "landmarkFromImage(" + i + "): " + landmarkFromImage[2*i] + " " + landmarkFromImage[2*i+1]);
                }*/



               /* String cropImgPath = "/mnt/sdcard/input_crop.jpg";
                String outPath = "mnt/sdcard/out";

                ARGearSDK.cropFaceImg(inputImgPath, cropImgPath);
                if(Seerslab3DMM.SetPath(cropImgPath, outPath)) {
                    float[] landmark = ARGearSDK.getFaceLandmarkFromImage(cropImgPath);
                    float[] faceinfo = ARGearSDK.getFaceInfoFromImage(cropImgPath);
                    Seerslab3DMM.setFaceInfo(1);
                    if (Seerslab3DMM.Run3DMM(landmark, faceinfo)) {
                        Toast toast = Toast.makeText(CameraActivity.this, "3DMM success! : saved (/mnt/sdcard/out.obj)", Toast.LENGTH_SHORT);
                        toast.show();
                        Seerslab3DMM.getSkinColor();
                        Seerslab3DMM.getEyebrowColor();
                        Seerslab3DMM.getLipColor();
                        Seerslab3DMM.getScale();

                        Seerslab3DMM.getFaceEyeglasses();
                        Seerslab3DMM.getFaceBeardType();
                    } else {
                        Toast toast = Toast.makeText(CameraActivity.this, "3DMM failed! : change Input Image", Toast.LENGTH_SHORT);
                        toast.show();
                    }
                }
                else {
                    Toast toast = Toast.makeText(CameraActivity.this, "Input Image is not exist", Toast.LENGTH_SHORT);
                    toast.show();
                }*/
                break;
            }
            case R.id.beauty_button:
                showBeauty();
                break;
            case R.id.close_beauty_button:
                closeBeauty();
                break;
            case R.id.beauty2_button:
                showBeauty2();
                break;
            case R.id.close_beauty2_button:
                closeBeauty2();
                break;
            case R.id.open_facetype_button:
                mViewFaceType.setVisibility(View.VISIBLE);
                mViewSkinLevel.setVisibility(View.INVISIBLE);
                mViewSlimLevel.setVisibility(View.INVISIBLE);
                mViewEyeLevel.setVisibility(View.INVISIBLE);
                break;
            case R.id.open_skinlevel_button:
                mViewFaceType.setVisibility(View.INVISIBLE);
                mViewSkinLevel.setVisibility(View.VISIBLE);
                mViewSlimLevel.setVisibility(View.INVISIBLE);
                mViewEyeLevel.setVisibility(View.INVISIBLE);

                mSeekBarSkinLevel.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    public void onStopTrackingTouch(SeekBar seekBar) {
                    }

                    public void onStartTrackingTouch(SeekBar seekBar) {
                    }

                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        mTextSkinLevel.setText("SkinLevel : " + String.format("%d", progress));
                        ARGearSDK.setSkinEffectLevel(progress);
                    }
                });
                break;
            case R.id.open_slimlevel_button:
                mViewFaceType.setVisibility(View.INVISIBLE);
                mViewSkinLevel.setVisibility(View.INVISIBLE);
                mViewSlimLevel.setVisibility(View.VISIBLE);
                mViewEyeLevel.setVisibility(View.INVISIBLE);

                mSeekBarSlimLevel.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    public void onStopTrackingTouch(SeekBar seekBar) {
                    }

                    public void onStartTrackingTouch(SeekBar seekBar) {
                    }

                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        int progress_parameter = (progress);
                        mTextSlimLevel.setText("SlimLevel : " + String.format("%d", progress_parameter));
                        ARGearSDK.setFaceEffectLevel(progress_parameter);
                    }
                });

                mSeekBarNoseLevel.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    public void onStopTrackingTouch(SeekBar seekBar) {
                    }

                    public void onStartTrackingTouch(SeekBar seekBar) {
                    }

                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        int progress_parameter = (progress);
                        mTextNoseLevel.setText("NoseLevel : " + String.format("%d", progress_parameter));
                        ARGearSDK.setFaceNoseEffectLevel(progress_parameter);
                    }
                });
                break;
            case R.id.open_eyelevel_button:
                mViewFaceType.setVisibility(View.INVISIBLE);
                mViewSkinLevel.setVisibility(View.INVISIBLE);
                mViewSlimLevel.setVisibility(View.INVISIBLE);
                mViewEyeLevel.setVisibility(View.VISIBLE);

                mSeekBarEyeLevel.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    public void onStopTrackingTouch(SeekBar seekBar) { }

                    public void onStartTrackingTouch(SeekBar seekBar) { }

                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        int progress_parameter = (progress);
                        mTextEyeLevel.setText("EyeLevel : " + String.format("%d", progress_parameter));
                        ARGearSDK.setEyeEffectLevel(progress_parameter);
                    }
                });
                break;
            case R.id.facetype_normal_button:
                ARGearSDK.setFaceEffectType(ARGearSDK.FACE_TYPE_NORMAL);
                break;
            case R.id.facetype_oval_button:
                ARGearSDK.setFaceEffectType(ARGearSDK.FACE_TYPE_OVAL);
                break;
            case R.id.facetype_round_button:
                ARGearSDK.setFaceEffectType(ARGearSDK.FACE_TYPE_ROUND);
                break;
            case R.id.facetype_square_button:
                ARGearSDK.setFaceEffectType(ARGearSDK.FACE_TYPE_SQUARE);
                break;
            case R.id.facetype_heart_button:
                ARGearSDK.setFaceEffectType(ARGearSDK.FACE_TYPE_HEART);
                break;
            case R.id.facetype_diamond_button:
                ARGearSDK.setFaceEffectType(ARGearSDK.FACE_TYPE_DIAMOND);
                break;
            case R.id.camera_switch_button:
                // 아래와 같이 카메라 전환중에는 pauseEngine() 를 호출하셔야 전환중 이전 화면을 reset 하실수 있습니다
                ARGearSDK.pauseEngine();
                mCamera.changeCameraFacing();
                break;
            case R.id.bulge_button:
                initOriginalBeauty();
                showBulge();
                break;
            case R.id.close_bulge_button:
                closeBulge();
                break;
            case R.id.bulge_fun1_button :
                ARGearSDK.setBulgeFunType(1);
                break;
            case R.id.bulge_fun2_button :
                ARGearSDK.setBulgeFunType(2);
                break;
            case R.id.bulge_fun3_button :
                ARGearSDK.setBulgeFunType(3);
                break;
            case R.id.bulge_fun4_button :
                ARGearSDK.setBulgeFunType(4);
                break;
            case R.id.bulge_fun5_button :
                ARGearSDK.setBulgeFunType(5);
                break;
            case R.id.bulge_fun6_button :
                ARGearSDK.setBulgeFunType(6);
                break;

            case R.id.bulge_beauty_init_button :
                initBulge();

                mBeauty2ItemData.initSeekBarInfo();

                Beauty2ItemData.Beauty2ItemSeekBarInfo seekBarInfo = mBeauty2ItemData.getSeekBarInfo(mCurrentBeautyType);
                mBeauty2SeekBar.setMax(seekBarInfo.mMax);
                mBeauty2SeekBar.setProgress(seekBarInfo.mProgress);
                mBeauty2SeekBar.setSecondaryProgress(seekBarInfo.mSecondaryProgress);
                updateBeautyLevel(mCurrentBeautyType, seekBarInfo.mProgress);


                break;
        }
    }

    private void setDrawLandmark(boolean landmark, boolean faceRect) {

        int flag = 0;

        if(landmark){
            flag |= AppConfig.FACE_BINARY_LANDMARK;
            AppPreference.getInstance(this).setLandmark(true);
        }else
            AppPreference.getInstance(this).setLandmark(false);

        if(faceRect){
            flag |= AppConfig.FACE_RECT_SW;
            AppPreference.getInstance(this).setFaceRect(true);
        }else
            AppPreference.getInstance(this).setFaceRect(false);


        ARGearSDK.setDrawLandmarkInfo(flag);
    }

    public void setMeasureSurfaceView(View view) {
        if (view.getParent() instanceof FrameLayout) {
            view.setLayoutParams(new FrameLayout.LayoutParams(mTargetWidth, mTargetHeight));
        }else if(view.getParent() instanceof RelativeLayout) {
            view.setLayoutParams(new RelativeLayout.LayoutParams(mTargetWidth, mTargetHeight));
        }

        /* to align center */
        int cameraRatioType = mCamera.getCameraRatio();
        if ((cameraRatioType == ReferenceCamera.CAMERA_RATIO_FULL) && (mTargetWidth > mDeviceWidth)) {
            view.setX((mDeviceWidth - mTargetWidth) / 2);
        } else {
            view.setX(0);
        }
    }

    public int getTargetWidth() {
        return mTargetWidth;
    }

    public int getTargetHeight() {
        return mTargetHeight;
    }

    private boolean is1to1CameraRatio(){
        return mRadioGroupCameraRatio.getCheckedRadioButtonId() == R.id.ratio11_radiobutton;
    }

    private int getTargetBitrate(){
        int bitRate = 2 * 1000 * 1000;
        switch (mRadioGroupBitrate.getCheckedRadioButtonId()){
            case R.id.onembps_radiobutton:
                bitRate = 1 * 1000 * 1000;
                break;
            case R.id.twombps_radiobutton:
                bitRate = 2 * 1000 * 1000;
                break;
            case R.id.fourmbps_radiobutton:
                bitRate = 4 * 1000 * 1000;
                break;
        }

        return bitRate;
    }

    private void setLastUpdatedAt(long lastUpdatedAt){
        SharedPreferences prefs = getSharedPreferences("sample", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong("lastUpdatedAt", lastUpdatedAt);
        editor.apply();
    }

    private long getLastUpdatedAt(){
        SharedPreferences prefs = getSharedPreferences("sample", MODE_PRIVATE);
        return prefs.getLong("lastUpdatedAt", 0);
    }

    private void requestStickerAPI() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ItemService itemService = retrofit.create(ItemService.class);
        Map<String, String> params = new HashMap<>();
        itemService.requestItem(params).enqueue(itemCallBack);
    }

    private Callback<CategoryRespModel> itemCallBack = new Callback<CategoryRespModel>() {
        @Override
        public void onResponse(Call<CategoryRespModel> call, Response<CategoryRespModel> response) {
            if (response.isSuccessful()) {
                final CategoryRespModel respModel = response.body();
                Log.d(TAG, "onResponse : " + respModel.toString());
                if (respModel == null || respModel.categories == null) {
                    Log.w(TAG, "invalid root model.");
                    return;
                }

                Realm rm = Realm.getDefaultInstance();
                rm.executeTransactionAsync(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        // 응답의 아이템이 변경 되었는지 last_updated_at 을 통해 확인 합니다
                        if(getLastUpdatedAt() !=  respModel.last_updated_at) {
                            for( CategoryModel category : respModel.categories) {
                                for(ItemModel item : category.items) {
                                    ItemModel existItem = realm.where(ItemModel.class).equalTo("uuid", item.uuid).findFirst();
                                    if(existItem != null){
                                        // 각 아이템의 updated at 값을 비교 하여 아이템을 재 다운로드 해야 하는지 확인 합니다
                                        if(existItem.updated_at != item.updated_at){
                                            Log.d(TAG, "update item status " + item.uuid + ":" + existItem.downloadStatus + "=>" + ItemModel.STATUS_NEED_DOWNLOAD);
                                            item.downloadStatus = ItemModel.STATUS_NEED_DOWNLOAD;
                                        }else{
                                            // updated at 이 변경 되지 않았으면 기존의 아이템의 상태를 유지 합니다
                                            Log.d(TAG, "exist item status " + item.uuid + ":" + existItem.downloadStatus);
                                            item.downloadStatus = existItem.downloadStatus;
                                        }
                                    }else{
                                        // 새로운 아이템이면 다운로드 필요 상태로 초기화 합니다.
                                        Log.d(TAG, "new item status " + item.uuid + ":" + ItemModel.STATUS_NEED_DOWNLOAD);
                                        item.downloadStatus = ItemModel.STATUS_NEED_DOWNLOAD;
                                    }
                                }
                            }

                            for( FilterModel fiter : respModel.filters) {
                                for(ItemModel item : fiter.items) {
                                    ItemModel existItem = realm.where(ItemModel.class).equalTo("uuid", item.uuid).findFirst();
                                    if(existItem != null){
                                        // 각 아이템의 updated at 값을 비교 하여 아이템을 재 다운로드 해야 하는지 확인 합니다
                                        if(existItem.updated_at != item.updated_at){
                                            Log.d(TAG, "update item status " + item.uuid + ":" + existItem.downloadStatus + "=>" + ItemModel.STATUS_NEED_DOWNLOAD);
                                            item.downloadStatus = ItemModel.STATUS_NEED_DOWNLOAD;
                                        }else{
                                            // updated at 이 변경 되지 않았으면 기존의 아이템의 상태를 유지 합니다
                                            Log.d(TAG, "exist item status " + item.uuid + ":" + existItem.downloadStatus);
                                            item.downloadStatus = existItem.downloadStatus;
                                        }
                                    }else{
                                        // 새로운 아이템이면 다운로드 필요 상태로 초기화 합니다.
                                        Log.d(TAG, "new item status " + item.uuid + ":" + ItemModel.STATUS_NEED_DOWNLOAD);
                                        item.downloadStatus = ItemModel.STATUS_NEED_DOWNLOAD;
                                    }
                                }
                            }

                            // database 를 업데이트 합니다.
                            realm.delete(CategoryModel.class);
                            realm.delete(FilterModel.class);
                            realm.delete(ItemModel.class);
                            realm.copyToRealmOrUpdate(respModel.categories);
                            realm.copyToRealmOrUpdate(respModel.filters);

                            // last updated at 을 업데이트 합니다.
                            setLastUpdatedAt(respModel.last_updated_at);
                        } else {
                            Log.e(TAG, "last sync at is not changed!");
                        }
                    }
                });
            } else {
                Log.e(TAG, "onError : response.isSuccessful() = " + response.isSuccessful());
            }
        }

        @Override
        public void onFailure(Call<CategoryRespModel> call, Throwable t) {
            Log.e(TAG, "onFailure " + t);
        }
    };

    public List<CategoryModel> getStickerCategories(){
        return Realm.getDefaultInstance().where(CategoryModel.class).findAll();
    }

    public List<FilterModel> getFilters(){
        return Realm.getDefaultInstance().where(FilterModel.class).findAll();
    }

    private void showStickers(){
        List<CategoryModel> categories = getStickerCategories();
        mStickerCategoryListAdapter.setData(categories);
        mViewStickers.setVisibility(View.VISIBLE);
        mViewFunctions.setVisibility(View.GONE);
    }

    private void closeStickers(){
        mViewStickers.setVisibility(View.GONE);
        mViewFunctions.setVisibility(View.VISIBLE);
    }

    private void clearStickers(){
        ARGearSDK.setItem(null);
    }

    private void showFilters(){
        List<FilterModel> filters = getFilters();
        mFilterListAdapter.setData(filters);
        mViewFilters.setVisibility(View.VISIBLE);
        mViewFunctions.setVisibility(View.GONE);
        mTextViewFilterValue.setText("filter value : " + mFilterLevel);
    }

    private void closeFilters(){
        mViewFilters.setVisibility(View.GONE);
        mViewFunctions.setVisibility(View.VISIBLE);
    }

    private void clearFilter(){
        ARGearSDK.setFilter(null);
        mFilterLevel = 100;
        mTextViewFilterValue.setText("filter value : " + mFilterLevel);
    }

    private void showBeauty(){
        mViewBeauty.setVisibility(View.VISIBLE);
        mViewFunctions.setVisibility(View.GONE);
        ARGearSDK.setItem(null);
        reloadOriginalBeauty();
    }

    private void showBeauty2(){
        initOriginalBeauty();
        mViewBeauty2.setVisibility(View.VISIBLE);
        mViewFunctions.setVisibility(View.GONE);
        ARGearSDK.setItem(null);
        ARGearSDK.setStartBulge(false);

        Beauty2ItemData.Beauty2ItemSeekBarInfo seekBarInfo;
        float[] tempProgress = new float[AppConfig.BEAUTY_TYPE_NUM];

        for(int i=0; i<AppConfig.BEAUTY_TYPE_NUM; i++){
            mBeauty2ItemData.updateSeekBarInfo(i, ARGearSDK.getBeautyParam(i) + AppConfig.BEAUTY_TYPE_MAX_VALUE[i]);
            seekBarInfo = mBeauty2ItemData.getSeekBarInfo(i);
            tempProgress[i] = (float)(seekBarInfo.mProgress - AppConfig.BEAUTY_TYPE_MAX_VALUE[i]);
        }
        ARGearSDK.setBeautyAllParam(tempProgress);

        seekBarInfo = mBeauty2ItemData.getSeekBarInfo(mCurrentBeautyType);
        mBeauty2SeekBar.setMax(seekBarInfo.mMax);
        mBeauty2SeekBar.setProgress(seekBarInfo.mProgress);
        mBeauty2SeekBar.setSecondaryProgress(seekBarInfo.mSecondaryProgress);
        updateBeautyLevel(mCurrentBeautyType, seekBarInfo.mProgress);

        if (mBeauty2ListAdapter != null) {
            mBeauty2ListAdapter.selectItem(mCurrentBeautyType);
        }
    }

    private void closeBeauty(){
        mViewBeauty.setVisibility(View.GONE);
        mViewFunctions.setVisibility(View.VISIBLE);
        ARGearSDK.setItem(mCurrentStickeritemID);
    }

    private void closeBeauty2(){
        mViewBeauty2.setVisibility(View.GONE);
        mViewFunctions.setVisibility(View.VISIBLE);
        ARGearSDK.setStartBulge(false);
        ARGearSDK.setItem(mCurrentStickeritemID);
    }

    private void showBulge(){
        mViewBulge.setVisibility(View.VISIBLE);
        mViewFunctions.setVisibility(View.GONE);
        //ARGearSDK.setItem(null);
        ARGearSDK.setItem(mCurrentStickeritemID);
        ARGearSDK.setStartBulge(true);
        ARGearSDK.setBulgeMode(AppConfig.BULGE_MODE_FUN);
    }

    private void closeBulge(){
        mViewBulge.setVisibility(View.GONE);
        mViewFunctions.setVisibility(View.VISIBLE);
        ARGearSDK.setStartBulge(false);
        ARGearSDK.setItem(mCurrentStickeritemID);
    }

    private void initOriginalBeauty(){
        ARGearSDK.setEyeEffectLevel(0);
        ARGearSDK.setFaceNoseEffectLevel(0);
        ARGearSDK.setFaceEffectLevel(0);
        ARGearSDK.setSkinEffectLevel(0);
    }

    private void reloadOriginalBeauty(){
        ARGearSDK.setEyeEffectLevel(mSeekBarEyeLevel.getProgress());
        ARGearSDK.setFaceNoseEffectLevel(mSeekBarNoseLevel.getProgress());
        ARGearSDK.setFaceEffectLevel(mSeekBarEyeLevel.getProgress());
    }

    private void startRecording(File recordingFile, int bitrate, boolean videoOnly, boolean ratio1to1){
        ARGearSDK.startRecordVideo(recordingFile, bitrate, videoOnly, ratio1to1);
    }

    private void stopRecording(){
        ARGearSDK.stopRecordVideo();
    }

    @Override
    public void onCategorySelected(CategoryModel category) {
        mStickerListAdapter.setData(category.items);
    }

    @Override
    public void onStickerSelected(int position, ItemModel item) {
        mCurrentStickeritemID = item.uuid;
        mHasTrigger = item.has_trigger;

        if (TextUtils.equals(item.downloadStatus, ItemModel.STATUS_OK)) {
                ARGearSDK.setItem(item.uuid);
        } else {
            new DownloadItemProgressTask(getFilesDir().getAbsolutePath() + "/" + item.uuid, item.zip_file, item.uuid, position, true).execute();
        }
    }

    @Override
    public void onFilterSelected(int position, ItemModel item) {
        mCurrentFilteritemID = item.uuid;

        if (TextUtils.equals(item.downloadStatus, ItemModel.STATUS_OK)) {
                ARGearSDK.setFilter(item.uuid);
        } else {
            new DownloadItemProgressTask(getFilesDir().getAbsolutePath() + "/" + item.uuid, item.zip_file, item.uuid, position, false).execute();
        }
    }

    @Override
    public void onBeautyItemSelected(int position, int beautyType) {
        Beauty2ItemData.Beauty2ItemSeekBarInfo seekBarInfo = mBeauty2ItemData.getSeekBarInfo(beautyType);

        if (seekBarInfo == null) {
            return;
        }

        mCurrentBeautyType = beautyType;
        mBeauty2SeekBar.setMax(seekBarInfo.mMax);
        mBeauty2SeekBar.setProgress(seekBarInfo.mProgress);
        mBeauty2SeekBar.setSecondaryProgress(seekBarInfo.mSecondaryProgress);

        updateBeautyLevel(beautyType, seekBarInfo.mProgress);
    }

    public void onGetTriggerFlag(final int flag) {
        postOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mCurrentStickeritemID != null && mHasTrigger) {
                    // TRIGGER_MOUTH_FLAG   = (1 << 0)
                    // TRIGGER_HEAD_FLAG    = (1 << 1)
                    // TRIGGER_DELAY_FLAG   = (1 << 2)
                    String strTrigger = null;
                    if ((flag & 1) != 0) {
                        strTrigger = "Open your mouth.";
                    } else if ((flag & 2) != 0) {
                        strTrigger = "Move your head side to side.";
                    } else {
                        if (mTriggerToast != null) {
                            mTriggerToast.cancel();
                            mTriggerToast = null;
                        }
                    }

                    if (strTrigger != null) {
                        mTriggerToast = Toast.makeText(CameraActivity.this, strTrigger, Toast.LENGTH_SHORT);
                        mTriggerToast.setGravity(Gravity.CENTER, 0, 0);
                        mTriggerToast.show();
                        mHasTrigger = false;
                    }
                }
            }
        });
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {

        switch (checkedId) {
            case R.id.ratio_full_radiobutton:
                mCamera.changeCameraRatio(ReferenceCamera.CAMERA_RATIO_FULL);
                break;
            case R.id.ratio43_radiobugtton:
                mCamera.changeCameraRatio(ReferenceCamera.CAMERA_RATIO_4_3);
                break;
            case R.id.ratio11_radiobutton:
                mCamera.changeCameraRatio(ReferenceCamera.CAMERA_RATIO_4_3);
                break;
        }

        updateUIStyle(mCamera.getCameraRatio());
    }

    public class DownloadItemProgressTask extends AsyncTask<Integer, Integer, Boolean> {
        private ProgressDialog progressDialog;
        private String targetPath;
        private String achiveUrl;
        private String itemId;
        private int itemPosition;
        private boolean isSticker;

        public DownloadItemProgressTask(String targetPath, String url, String itemId, int itemPosition, boolean isSticker) {
            this.targetPath = targetPath;
            this.achiveUrl = url;
            this.itemId = itemId;
            this.itemPosition = itemPosition;
            this.isSticker = isSticker;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(CameraActivity.this);
            progressDialog.show();

            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Integer... params) {
            boolean success = false;
            FileOutputStream fileOutput = null;
            InputStream inputStream = null;

            try {
                URL url = new URL(achiveUrl);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.setDoOutput(false);
                urlConnection.connect();

                File file = new File(targetPath);
                file.createNewFile();

                fileOutput = new FileOutputStream(file);

                inputStream = urlConnection.getInputStream();

                Log.d(TAG, "read " + achiveUrl + "=>" + targetPath + " " + urlConnection.getContentLength());

                byte[] buffer = new byte[1024];
                int bufferLength;
                int read = 0;
                int total = urlConnection.getContentLength();
                while ((bufferLength = inputStream.read(buffer)) > 0) {
                    fileOutput.write(buffer, 0, bufferLength);
                    read += bufferLength;
                    if (total > 0) {
                        int progress = (int) (100 * (read / (float) total));
                        publishProgress(progress);
                    }
                }

                success = true;
            } catch (IOException e) {
                Log.e(TAG, "" + e);
            } finally {
                if (fileOutput != null) {
                    try {
                        fileOutput.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            if (success) {
                boolean unzipSuccess = ARGearSDK.unzip(targetPath, itemId);
                success = unzipSuccess;
            }

            return success;
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            progressDialog.setProgress(progress[0]);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            progressDialog.dismiss();
            if (result) {
                Realm.getDefaultInstance().executeTransactionAsync(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        // 다운로드가 성공하면 item 상태를 사용 가능한 상태로 변경합니다
                        ItemModel item = realm.where(ItemModel.class).equalTo("uuid", itemId).findFirst();
                        item.downloadStatus = ItemModel.STATUS_OK;
                    }
                }, new Realm.Transaction.OnSuccess(){
                    @Override
                    public void onSuccess() {
                        // 다운로드가 성공하고 item 상태변경이 끝났으면 ui 를 업데이트 합니다.
                        if(isSticker) {
                            mStickerListAdapter.notifyItemChanged(itemPosition);
                        }else{
                            mFilterListAdapter.notifyItemChanged(itemPosition);
                        }
                    }
                });
                Log.d(TAG, "download success!");
            } else {
                Log.e(TAG, "download failed!");
            }
        }
    }
}
