package com.argear.sdksample.network;

import com.argear.sdksample.AppConfig;
import com.argear.sdksample.model.CategoryRespModel;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface ItemService {
    @GET("/api/v3/" + AppConfig.API_KEY)
    Call<CategoryRespModel> requestItem(@QueryMap Map<String, String> params);
}
