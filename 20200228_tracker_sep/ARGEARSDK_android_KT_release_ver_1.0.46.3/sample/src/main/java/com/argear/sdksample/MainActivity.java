package com.argear.sdksample;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.argear.sdksample.R;

import java.util.ArrayList;
import java.util.List;

import static android.os.Build.VERSION_CODES.M;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    private static final int REQUEST_MUST_PERMISSION = 10;

    private boolean mIsPermissionGranted = false;

    private final String[] mPermissions = {
            Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mIsPermissionGranted = requestPermissions();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void onClick(View v) {
        if (!mIsPermissionGranted) {
            Toast.makeText(this, "Please check your permission!", Toast.LENGTH_SHORT).show();
            return;
        }

        switch (v.getId()) {
            case R.id.camera_active_button: {
                Intent intent = new Intent(this, CameraActivity.class);
                startActivity(intent);
            }
                break;
        }
    }

    private boolean requestPermissions() {
        Log.d(TAG, "requestPermissions " + Build.VERSION.SDK_INT);

        if (Build.VERSION.SDK_INT >= M) {
            List<String> deniedPermission = new ArrayList<>();

            for (String permission : mPermissions) {
                if (ActivityCompat.checkSelfPermission(this, permission)
                        != PackageManager.PERMISSION_GRANTED) {

                    Log.e(TAG, "permission " + permission + " is not granted");

                    deniedPermission.add(permission);

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                        Log.e(TAG, "shouldShowRequestPermissionRationale");
                        return false;
                    }
                }
            }

            if (deniedPermission.size() > 0) {
                String[] targetList = new String[deniedPermission.size()];
                targetList = deniedPermission.toArray(targetList);
                ActivityCompat.requestPermissions(this, targetList, REQUEST_MUST_PERMISSION);
                return false;
            }
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.e(TAG, "onRequestPermissionsResult " + requestCode);
        if (requestCode == REQUEST_MUST_PERMISSION) {
            for (int result : grantResults) {
                if (result != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
            }
            mIsPermissionGranted = true;
        }
    }

}
