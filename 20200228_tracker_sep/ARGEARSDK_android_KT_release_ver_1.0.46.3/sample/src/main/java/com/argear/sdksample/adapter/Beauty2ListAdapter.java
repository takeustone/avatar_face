package com.argear.sdksample.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.argear.sdksample.R;
import com.argear.sdksample.data.Beauty2ItemData;

import java.util.ArrayList;

public class Beauty2ListAdapter extends RecyclerView.Adapter<Beauty2ListAdapter.ViewHolder> {
    
    private static final String TAG = Beauty2ListAdapter.class.getSimpleName();

    private ArrayList<Beauty2ItemData.Beauty2ItemInfo> mData;
    private int mSelectedIndex = -1;

    public interface Listener {
        void onBeautyItemSelected(int position, int beautyType);
    }

    private Beauty2ListAdapter.Listener mListener;

    public Beauty2ListAdapter(ArrayList<Beauty2ItemData.Beauty2ItemInfo> data, Beauty2ListAdapter.Listener listener) {
        mData = data;
        mListener = listener;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public void onBindViewHolder(final Beauty2ListAdapter.ViewHolder holder, final int position) {
        holder.bind(position);
    }

    @Override
    public Beauty2ListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_beauty2, parent, false);
        return new Beauty2ListAdapter.BeautyItemViewHolder(v);
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    public void selectItem(int beautyType) {
        for (int i = 0; i < mData.size(); i++) {
            Beauty2ItemData.Beauty2ItemInfo itemInfo = mData.get(i);
            if (beautyType == itemInfo.mBeautyType) {
                mSelectedIndex = i;
                break;
            }
        }

        if (mSelectedIndex != -1) {
            notifyItemChanged(mSelectedIndex);
        }
    }


    public abstract class ViewHolder extends RecyclerView.ViewHolder {
        abstract void bind(int position);
        public ViewHolder(View v) {
            super(v);
        }
    }

    public class BeautyItemViewHolder extends Beauty2ListAdapter.ViewHolder implements View.OnClickListener {
        public Button mItemButton = null;
        public Beauty2ItemData.Beauty2ItemInfo mInfo;

        public BeautyItemViewHolder(View v) {
            super(v);
            mItemButton = (Button) v.findViewById(R.id.beauty2_item_button);
        }

        @Override
        void bind(int position) {
            mInfo = mData.get(position);

            if (mSelectedIndex == position) {
                mItemButton.setBackgroundResource(mInfo.mResource2);
            } else {
                mItemButton.setBackgroundResource(mInfo.mResource1);
            }
            mItemButton.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mItemButton.setBackgroundResource(mInfo.mResource2);

            notifyItemChanged(mSelectedIndex);
            mSelectedIndex = getLayoutPosition();

            if (mListener != null) {
                mListener.onBeautyItemSelected(mSelectedIndex, mInfo.mBeautyType);
            }
        }
    }
}
