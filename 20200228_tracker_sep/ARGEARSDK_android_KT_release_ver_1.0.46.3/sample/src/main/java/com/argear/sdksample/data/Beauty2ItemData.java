package com.argear.sdksample.data;

import com.argear.sdksample.AppConfig;
import com.argear.sdksample.R;

import java.util.ArrayList;
import java.util.HashMap;


public class Beauty2ItemData {

    ArrayList<Beauty2ItemInfo> mItemInfo = new ArrayList<>();
    HashMap<Integer, Beauty2ItemSeekBarInfo> mSeekBarInfo = new HashMap<>();

    public class Beauty2ItemInfo {
        public int mBeautyType;
        public int mResource1;  // default
        public int mResource2;  // checked
        public Beauty2ItemInfo(int type, int res1, int res2) {
            mBeautyType = type;
            mResource1 = res1;
            mResource2 = res2;
        }
    }

    public class Beauty2ItemSeekBarInfo {
        public int mBeautyType;
        public int mMax;
        public int mProgress;
        public int mSecondaryProgress;
        public Beauty2ItemSeekBarInfo(int type, int max, int progress, int secondaryProgress) {
            mBeautyType = type;
            mMax = max;
            mProgress = progress;
            mSecondaryProgress = secondaryProgress;
        }
    }

    public Beauty2ItemData() {
        mItemInfo.add(new Beauty2ItemInfo(AppConfig.BEAUTY_TYPE_SKIN_FACE, R.mipmap.bulge_skin_btn_default, R.mipmap.bulge_skin_btn_checked));
        mItemInfo.add(new Beauty2ItemInfo(AppConfig.BEAUTY_TYPE_VLINE, R.mipmap.bulge_vline_btn_default, R.mipmap.bulge_vline_btn_checked));
        mItemInfo.add(new Beauty2ItemInfo(AppConfig.BEAUTY_TYPE_FACE_SLIM, R.mipmap.bulge_face_slim_btn_default, R.mipmap.bulge_face_slim_btn_checked));
        mItemInfo.add(new Beauty2ItemInfo(AppConfig.BEAUTY_TYPE_JAW, R.mipmap.bulge_jaw_btn_default, R.mipmap.bulge_jaw_btn_checked));
        mItemInfo.add(new Beauty2ItemInfo(AppConfig.BEAUTY_TYPE_CHIN, R.mipmap.bulge_chin_btn_default, R.mipmap.bulge_chin_btn_checked));
        mItemInfo.add(new Beauty2ItemInfo(AppConfig.BEAUTY_TYPE_EYE, R.mipmap.bulge_eye_btn_default, R.mipmap.bulge_eye_btn_checked));
        mItemInfo.add(new Beauty2ItemInfo(AppConfig.BEAUTY_TYPE_EYE_BACK, R.mipmap.bulge_eyeback_btn_default, R.mipmap.bulge_eyeback_btn_checked));
        mItemInfo.add(new Beauty2ItemInfo(AppConfig.BEAUTY_TYPE_EYE_GAP, R.mipmap.bulge_eyegap_btn_default, R.mipmap.bulge_eyegap_btn_checked));
        mItemInfo.add(new Beauty2ItemInfo(AppConfig.BEAUTY_TYPE_EYE_CORNER, R.mipmap.bulge_eyecorner_btn_default, R.mipmap.bulge_eyecorner_btn_checked));
        mItemInfo.add(new Beauty2ItemInfo(AppConfig.BEAUTY_TYPE_NOSE_LINE, R.mipmap.bulge_nose_line_btn_default, R.mipmap.bulge_nose_line_btn_checked));
        mItemInfo.add(new Beauty2ItemInfo(AppConfig.BEAUTY_TYPE_NOSE_SIDE, R.mipmap.bulge_nose_side_btn_default, R.mipmap.bulge_nose_side_btn_checked));
        mItemInfo.add(new Beauty2ItemInfo(AppConfig.BEAUTY_TYPE_NOSE_LENGTH, R.mipmap.bulge_nose_length_btn_default, R.mipmap.bulge_nose_length_btn_checked));
        mItemInfo.add(new Beauty2ItemInfo(AppConfig.BEAUTY_TYPE_MOUTH_SIZE, R.mipmap.bulge_mouth_size_btn_default, R.mipmap.bulge_mouth_size_btn_checked));
        mItemInfo.add(new Beauty2ItemInfo(AppConfig.BEAUTY_TYPE_LIP_SIZE, R.mipmap.bulge_lip_size_btn_default, R.mipmap.bulge_lip_size_btn_checked));
        mItemInfo.add(new Beauty2ItemInfo(AppConfig.BEAUTY_TYPE_SKIN_DARK_CIRCLE, R.mipmap.bulge_dark_circle_btn_default, R.mipmap.bulge_dark_circle_btn_checked));
        mItemInfo.add(new Beauty2ItemInfo(AppConfig.BEAUTY_TYPE_SKIN_MOUTH_WRINKLE, R.mipmap.bulge_mouth_wrinkle_btn_default, R.mipmap.bulge_mouth_wrinkle_btn_checked));

        initSeekBarInfo();
    }

    public ArrayList<Beauty2ItemInfo> getItemInfoData() {
        return mItemInfo;
    }

    public void initSeekBarInfo() {
        mSeekBarInfo.clear();
        mSeekBarInfo.put(AppConfig.BEAUTY_TYPE_VLINE             , new Beauty2ItemSeekBarInfo(AppConfig.BEAUTY_TYPE_VLINE             , 100 + AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_VLINE             ], AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_VLINE             ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_VLINE             ] , AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_VLINE             ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_VLINE             ]));
        mSeekBarInfo.put(AppConfig.BEAUTY_TYPE_FACE_SLIM         , new Beauty2ItemSeekBarInfo(AppConfig.BEAUTY_TYPE_FACE_SLIM         , 100 + AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_FACE_SLIM         ], AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_FACE_SLIM         ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_FACE_SLIM         ] , AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_FACE_SLIM         ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_FACE_SLIM         ]));
        mSeekBarInfo.put(AppConfig.BEAUTY_TYPE_JAW               , new Beauty2ItemSeekBarInfo(AppConfig.BEAUTY_TYPE_JAW               , 100 + AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_JAW               ], AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_JAW               ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_JAW               ] , AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_JAW               ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_JAW               ]));
        mSeekBarInfo.put(AppConfig.BEAUTY_TYPE_CHIN              , new Beauty2ItemSeekBarInfo(AppConfig.BEAUTY_TYPE_CHIN              , 100 + AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_CHIN              ], AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_CHIN              ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_CHIN              ] , AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_CHIN              ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_CHIN              ]));
        mSeekBarInfo.put(AppConfig.BEAUTY_TYPE_EYE               , new Beauty2ItemSeekBarInfo(AppConfig.BEAUTY_TYPE_EYE               , 100 + AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_EYE               ], AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_EYE               ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_EYE               ] , AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_EYE               ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_EYE               ]));
        mSeekBarInfo.put(AppConfig.BEAUTY_TYPE_EYE_GAP           , new Beauty2ItemSeekBarInfo(AppConfig.BEAUTY_TYPE_EYE_GAP           , 100 + AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_EYE_GAP           ], AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_EYE_GAP           ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_EYE_GAP           ] , AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_EYE_GAP           ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_EYE_GAP           ]));
        mSeekBarInfo.put(AppConfig.BEAUTY_TYPE_EYE_CORNER        , new Beauty2ItemSeekBarInfo(AppConfig.BEAUTY_TYPE_EYE_CORNER        , 100 + AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_EYE_CORNER        ], AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_EYE_CORNER        ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_EYE_CORNER        ] , AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_EYE_CORNER        ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_EYE_CORNER        ]));
        mSeekBarInfo.put(AppConfig.BEAUTY_TYPE_NOSE_LINE         , new Beauty2ItemSeekBarInfo(AppConfig.BEAUTY_TYPE_NOSE_LINE         , 100 + AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_NOSE_LINE         ], AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_NOSE_LINE         ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_NOSE_LINE         ] , AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_NOSE_LINE         ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_NOSE_LINE         ]));
        mSeekBarInfo.put(AppConfig.BEAUTY_TYPE_NOSE_SIDE         , new Beauty2ItemSeekBarInfo(AppConfig.BEAUTY_TYPE_NOSE_SIDE         , 100 + AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_NOSE_SIDE         ], AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_NOSE_SIDE         ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_NOSE_SIDE         ] , AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_NOSE_SIDE         ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_NOSE_SIDE         ]));
        mSeekBarInfo.put(AppConfig.BEAUTY_TYPE_NOSE_LENGTH       , new Beauty2ItemSeekBarInfo(AppConfig.BEAUTY_TYPE_NOSE_LENGTH       , 100 + AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_NOSE_LENGTH       ], AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_NOSE_LENGTH       ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_NOSE_LENGTH       ] , AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_NOSE_LENGTH       ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_NOSE_LENGTH       ]));
        mSeekBarInfo.put(AppConfig.BEAUTY_TYPE_MOUTH_SIZE        , new Beauty2ItemSeekBarInfo(AppConfig.BEAUTY_TYPE_MOUTH_SIZE        , 100 + AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_MOUTH_SIZE        ], AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_MOUTH_SIZE        ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_MOUTH_SIZE        ] , AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_MOUTH_SIZE        ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_MOUTH_SIZE        ]));
        mSeekBarInfo.put(AppConfig.BEAUTY_TYPE_EYE_BACK          , new Beauty2ItemSeekBarInfo(AppConfig.BEAUTY_TYPE_EYE_BACK          , 100 + AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_EYE_BACK          ], AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_EYE_BACK          ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_EYE_BACK          ] , AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_EYE_BACK          ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_EYE_BACK          ]));
        mSeekBarInfo.put(AppConfig.BEAUTY_TYPE_LIP_SIZE          , new Beauty2ItemSeekBarInfo(AppConfig.BEAUTY_TYPE_LIP_SIZE          , 100 + AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_LIP_SIZE          ], AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_LIP_SIZE          ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_LIP_SIZE          ] , AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_LIP_SIZE          ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_LIP_SIZE          ]));
        mSeekBarInfo.put(AppConfig.BEAUTY_TYPE_SKIN_FACE         , new Beauty2ItemSeekBarInfo(AppConfig.BEAUTY_TYPE_SKIN_FACE         , 100 + AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_SKIN_FACE         ], AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_SKIN_FACE         ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_SKIN_FACE         ] , AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_SKIN_FACE         ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_SKIN_FACE         ]));
        mSeekBarInfo.put(AppConfig.BEAUTY_TYPE_SKIN_DARK_CIRCLE  , new Beauty2ItemSeekBarInfo(AppConfig.BEAUTY_TYPE_SKIN_DARK_CIRCLE  , 100 + AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_SKIN_DARK_CIRCLE  ], AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_SKIN_DARK_CIRCLE  ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_SKIN_DARK_CIRCLE  ] , AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_SKIN_DARK_CIRCLE  ] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_SKIN_DARK_CIRCLE  ]));
        mSeekBarInfo.put(AppConfig.BEAUTY_TYPE_SKIN_MOUTH_WRINKLE, new Beauty2ItemSeekBarInfo(AppConfig.BEAUTY_TYPE_SKIN_MOUTH_WRINKLE, 100 + AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_SKIN_MOUTH_WRINKLE], AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_SKIN_MOUTH_WRINKLE] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_SKIN_MOUTH_WRINKLE] , AppConfig.BEAUTY_TYPE_MAX_VALUE[AppConfig.BEAUTY_TYPE_SKIN_MOUTH_WRINKLE] + AppConfig.BEAUTY_TYPE_INIT_VALUE[AppConfig.BEAUTY_TYPE_SKIN_MOUTH_WRINKLE]));
    }

    public void updateSeekBarInfo(int beautyType, int progress) {
        if (mSeekBarInfo.containsKey(beautyType)) {
            mSeekBarInfo.get(beautyType).mProgress = progress;
        }
    }

    public Beauty2ItemSeekBarInfo getSeekBarInfo(int beautyType) {
        if (mSeekBarInfo.containsKey(beautyType)) {
            return mSeekBarInfo.get(beautyType);
        }
        return null;
    }
}
