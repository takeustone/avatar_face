package io.realm;


public interface CategoryModelRealmProxyInterface {
    public String realmGet$uuid();
    public void realmSet$uuid(String value);
    public String realmGet$title();
    public void realmSet$title(String value);
    public boolean realmGet$is_bundle();
    public void realmSet$is_bundle(boolean value);
    public long realmGet$updated_at();
    public void realmSet$updated_at(long value);
    public String realmGet$description();
    public void realmSet$description(String value);
    public String realmGet$status();
    public void realmSet$status(String value);
    public RealmList<com.argear.sdksample.model.ItemModel> realmGet$items();
    public void realmSet$items(RealmList<com.argear.sdksample.model.ItemModel> value);
}
