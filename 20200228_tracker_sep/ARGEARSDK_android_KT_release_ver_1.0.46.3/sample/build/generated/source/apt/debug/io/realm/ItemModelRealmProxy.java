package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.ProxyUtils;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class ItemModelRealmProxy extends com.argear.sdksample.model.ItemModel
    implements RealmObjectProxy, ItemModelRealmProxyInterface {

    static final class ItemModelColumnInfo extends ColumnInfo {
        long uuidIndex;
        long titleIndex;
        long descriptionIndex;
        long thumbnailIndex;
        long zip_fileIndex;
        long num_stickersIndex;
        long num_effectsIndex;
        long num_bgmsIndex;
        long num_filtersIndex;
        long num_masksIndex;
        long has_triggerIndex;
        long statusIndex;
        long updated_atIndex;
        long downloadStatusIndex;

        ItemModelColumnInfo(OsSchemaInfo schemaInfo) {
            super(14);
            OsObjectSchemaInfo objectSchemaInfo = schemaInfo.getObjectSchemaInfo("ItemModel");
            this.uuidIndex = addColumnDetails("uuid", objectSchemaInfo);
            this.titleIndex = addColumnDetails("title", objectSchemaInfo);
            this.descriptionIndex = addColumnDetails("description", objectSchemaInfo);
            this.thumbnailIndex = addColumnDetails("thumbnail", objectSchemaInfo);
            this.zip_fileIndex = addColumnDetails("zip_file", objectSchemaInfo);
            this.num_stickersIndex = addColumnDetails("num_stickers", objectSchemaInfo);
            this.num_effectsIndex = addColumnDetails("num_effects", objectSchemaInfo);
            this.num_bgmsIndex = addColumnDetails("num_bgms", objectSchemaInfo);
            this.num_filtersIndex = addColumnDetails("num_filters", objectSchemaInfo);
            this.num_masksIndex = addColumnDetails("num_masks", objectSchemaInfo);
            this.has_triggerIndex = addColumnDetails("has_trigger", objectSchemaInfo);
            this.statusIndex = addColumnDetails("status", objectSchemaInfo);
            this.updated_atIndex = addColumnDetails("updated_at", objectSchemaInfo);
            this.downloadStatusIndex = addColumnDetails("downloadStatus", objectSchemaInfo);
        }

        ItemModelColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new ItemModelColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final ItemModelColumnInfo src = (ItemModelColumnInfo) rawSrc;
            final ItemModelColumnInfo dst = (ItemModelColumnInfo) rawDst;
            dst.uuidIndex = src.uuidIndex;
            dst.titleIndex = src.titleIndex;
            dst.descriptionIndex = src.descriptionIndex;
            dst.thumbnailIndex = src.thumbnailIndex;
            dst.zip_fileIndex = src.zip_fileIndex;
            dst.num_stickersIndex = src.num_stickersIndex;
            dst.num_effectsIndex = src.num_effectsIndex;
            dst.num_bgmsIndex = src.num_bgmsIndex;
            dst.num_filtersIndex = src.num_filtersIndex;
            dst.num_masksIndex = src.num_masksIndex;
            dst.has_triggerIndex = src.has_triggerIndex;
            dst.statusIndex = src.statusIndex;
            dst.updated_atIndex = src.updated_atIndex;
            dst.downloadStatusIndex = src.downloadStatusIndex;
        }
    }

    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>(14);
        fieldNames.add("uuid");
        fieldNames.add("title");
        fieldNames.add("description");
        fieldNames.add("thumbnail");
        fieldNames.add("zip_file");
        fieldNames.add("num_stickers");
        fieldNames.add("num_effects");
        fieldNames.add("num_bgms");
        fieldNames.add("num_filters");
        fieldNames.add("num_masks");
        fieldNames.add("has_trigger");
        fieldNames.add("status");
        fieldNames.add("updated_at");
        fieldNames.add("downloadStatus");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    private ItemModelColumnInfo columnInfo;
    private ProxyState<com.argear.sdksample.model.ItemModel> proxyState;

    ItemModelRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (ItemModelColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<com.argear.sdksample.model.ItemModel>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$uuid() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.uuidIndex);
    }

    @Override
    public void realmSet$uuid(String value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'uuid' cannot be changed after object was created.");
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$title() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.titleIndex);
    }

    @Override
    public void realmSet$title(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.titleIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.titleIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.titleIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.titleIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$description() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.descriptionIndex);
    }

    @Override
    public void realmSet$description(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.descriptionIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.descriptionIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.descriptionIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.descriptionIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$thumbnail() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.thumbnailIndex);
    }

    @Override
    public void realmSet$thumbnail(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.thumbnailIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.thumbnailIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.thumbnailIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.thumbnailIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$zip_file() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.zip_fileIndex);
    }

    @Override
    public void realmSet$zip_file(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.zip_fileIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.zip_fileIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.zip_fileIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.zip_fileIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$num_stickers() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.num_stickersIndex);
    }

    @Override
    public void realmSet$num_stickers(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.num_stickersIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.num_stickersIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$num_effects() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.num_effectsIndex);
    }

    @Override
    public void realmSet$num_effects(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.num_effectsIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.num_effectsIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$num_bgms() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.num_bgmsIndex);
    }

    @Override
    public void realmSet$num_bgms(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.num_bgmsIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.num_bgmsIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$num_filters() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.num_filtersIndex);
    }

    @Override
    public void realmSet$num_filters(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.num_filtersIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.num_filtersIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$num_masks() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.num_masksIndex);
    }

    @Override
    public void realmSet$num_masks(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.num_masksIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.num_masksIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public boolean realmGet$has_trigger() {
        proxyState.getRealm$realm().checkIfValid();
        return (boolean) proxyState.getRow$realm().getBoolean(columnInfo.has_triggerIndex);
    }

    @Override
    public void realmSet$has_trigger(boolean value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setBoolean(columnInfo.has_triggerIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setBoolean(columnInfo.has_triggerIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$status() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.statusIndex);
    }

    @Override
    public void realmSet$status(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.statusIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.statusIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.statusIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.statusIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public long realmGet$updated_at() {
        proxyState.getRealm$realm().checkIfValid();
        return (long) proxyState.getRow$realm().getLong(columnInfo.updated_atIndex);
    }

    @Override
    public void realmSet$updated_at(long value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.updated_atIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.updated_atIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$downloadStatus() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.downloadStatusIndex);
    }

    @Override
    public void realmSet$downloadStatus(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.downloadStatusIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.downloadStatusIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.downloadStatusIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.downloadStatusIndex, value);
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("ItemModel", 14, 0);
        builder.addPersistedProperty("uuid", RealmFieldType.STRING, Property.PRIMARY_KEY, Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("title", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("description", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("thumbnail", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("zip_file", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("num_stickers", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("num_effects", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("num_bgms", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("num_filters", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("num_masks", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("has_trigger", RealmFieldType.BOOLEAN, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("status", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("updated_at", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("downloadStatus", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static ItemModelColumnInfo createColumnInfo(OsSchemaInfo schemaInfo) {
        return new ItemModelColumnInfo(schemaInfo);
    }

    public static String getSimpleClassName() {
        return "ItemModel";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static com.argear.sdksample.model.ItemModel createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        com.argear.sdksample.model.ItemModel obj = null;
        if (update) {
            Table table = realm.getTable(com.argear.sdksample.model.ItemModel.class);
            ItemModelColumnInfo columnInfo = (ItemModelColumnInfo) realm.getSchema().getColumnInfo(com.argear.sdksample.model.ItemModel.class);
            long pkColumnIndex = columnInfo.uuidIndex;
            long rowIndex = Table.NO_MATCH;
            if (json.isNull("uuid")) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstString(pkColumnIndex, json.getString("uuid"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(com.argear.sdksample.model.ItemModel.class), false, Collections.<String> emptyList());
                    obj = new io.realm.ItemModelRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("uuid")) {
                if (json.isNull("uuid")) {
                    obj = (io.realm.ItemModelRealmProxy) realm.createObjectInternal(com.argear.sdksample.model.ItemModel.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.ItemModelRealmProxy) realm.createObjectInternal(com.argear.sdksample.model.ItemModel.class, json.getString("uuid"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'uuid'.");
            }
        }

        final ItemModelRealmProxyInterface objProxy = (ItemModelRealmProxyInterface) obj;
        if (json.has("title")) {
            if (json.isNull("title")) {
                objProxy.realmSet$title(null);
            } else {
                objProxy.realmSet$title((String) json.getString("title"));
            }
        }
        if (json.has("description")) {
            if (json.isNull("description")) {
                objProxy.realmSet$description(null);
            } else {
                objProxy.realmSet$description((String) json.getString("description"));
            }
        }
        if (json.has("thumbnail")) {
            if (json.isNull("thumbnail")) {
                objProxy.realmSet$thumbnail(null);
            } else {
                objProxy.realmSet$thumbnail((String) json.getString("thumbnail"));
            }
        }
        if (json.has("zip_file")) {
            if (json.isNull("zip_file")) {
                objProxy.realmSet$zip_file(null);
            } else {
                objProxy.realmSet$zip_file((String) json.getString("zip_file"));
            }
        }
        if (json.has("num_stickers")) {
            if (json.isNull("num_stickers")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'num_stickers' to null.");
            } else {
                objProxy.realmSet$num_stickers((int) json.getInt("num_stickers"));
            }
        }
        if (json.has("num_effects")) {
            if (json.isNull("num_effects")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'num_effects' to null.");
            } else {
                objProxy.realmSet$num_effects((int) json.getInt("num_effects"));
            }
        }
        if (json.has("num_bgms")) {
            if (json.isNull("num_bgms")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'num_bgms' to null.");
            } else {
                objProxy.realmSet$num_bgms((int) json.getInt("num_bgms"));
            }
        }
        if (json.has("num_filters")) {
            if (json.isNull("num_filters")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'num_filters' to null.");
            } else {
                objProxy.realmSet$num_filters((int) json.getInt("num_filters"));
            }
        }
        if (json.has("num_masks")) {
            if (json.isNull("num_masks")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'num_masks' to null.");
            } else {
                objProxy.realmSet$num_masks((int) json.getInt("num_masks"));
            }
        }
        if (json.has("has_trigger")) {
            if (json.isNull("has_trigger")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'has_trigger' to null.");
            } else {
                objProxy.realmSet$has_trigger((boolean) json.getBoolean("has_trigger"));
            }
        }
        if (json.has("status")) {
            if (json.isNull("status")) {
                objProxy.realmSet$status(null);
            } else {
                objProxy.realmSet$status((String) json.getString("status"));
            }
        }
        if (json.has("updated_at")) {
            if (json.isNull("updated_at")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'updated_at' to null.");
            } else {
                objProxy.realmSet$updated_at((long) json.getLong("updated_at"));
            }
        }
        if (json.has("downloadStatus")) {
            if (json.isNull("downloadStatus")) {
                objProxy.realmSet$downloadStatus(null);
            } else {
                objProxy.realmSet$downloadStatus((String) json.getString("downloadStatus"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.argear.sdksample.model.ItemModel createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        final com.argear.sdksample.model.ItemModel obj = new com.argear.sdksample.model.ItemModel();
        final ItemModelRealmProxyInterface objProxy = (ItemModelRealmProxyInterface) obj;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("uuid")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$uuid((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$uuid(null);
                }
                jsonHasPrimaryKey = true;
            } else if (name.equals("title")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$title((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$title(null);
                }
            } else if (name.equals("description")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$description((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$description(null);
                }
            } else if (name.equals("thumbnail")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$thumbnail((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$thumbnail(null);
                }
            } else if (name.equals("zip_file")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$zip_file((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$zip_file(null);
                }
            } else if (name.equals("num_stickers")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$num_stickers((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'num_stickers' to null.");
                }
            } else if (name.equals("num_effects")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$num_effects((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'num_effects' to null.");
                }
            } else if (name.equals("num_bgms")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$num_bgms((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'num_bgms' to null.");
                }
            } else if (name.equals("num_filters")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$num_filters((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'num_filters' to null.");
                }
            } else if (name.equals("num_masks")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$num_masks((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'num_masks' to null.");
                }
            } else if (name.equals("has_trigger")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$has_trigger((boolean) reader.nextBoolean());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'has_trigger' to null.");
                }
            } else if (name.equals("status")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$status((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$status(null);
                }
            } else if (name.equals("updated_at")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$updated_at((long) reader.nextLong());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'updated_at' to null.");
                }
            } else if (name.equals("downloadStatus")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$downloadStatus((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$downloadStatus(null);
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'uuid'.");
        }
        return realm.copyToRealm(obj);
    }

    public static com.argear.sdksample.model.ItemModel copyOrUpdate(Realm realm, com.argear.sdksample.model.ItemModel object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null) {
            final BaseRealm otherRealm = ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm();
            if (otherRealm.threadId != realm.threadId) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            }
            if (otherRealm.getPath().equals(realm.getPath())) {
                return object;
            }
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.argear.sdksample.model.ItemModel) cachedRealmObject;
        }

        com.argear.sdksample.model.ItemModel realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(com.argear.sdksample.model.ItemModel.class);
            ItemModelColumnInfo columnInfo = (ItemModelColumnInfo) realm.getSchema().getColumnInfo(com.argear.sdksample.model.ItemModel.class);
            long pkColumnIndex = columnInfo.uuidIndex;
            String value = ((ItemModelRealmProxyInterface) object).realmGet$uuid();
            long rowIndex = Table.NO_MATCH;
            if (value == null) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstString(pkColumnIndex, value);
            }
            if (rowIndex == Table.NO_MATCH) {
                canUpdate = false;
            } else {
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(com.argear.sdksample.model.ItemModel.class), false, Collections.<String> emptyList());
                    realmObject = new io.realm.ItemModelRealmProxy();
                    cache.put(object, (RealmObjectProxy) realmObject);
                } finally {
                    objectContext.clear();
                }
            }
        }

        return (canUpdate) ? update(realm, realmObject, object, cache) : copy(realm, object, update, cache);
    }

    public static com.argear.sdksample.model.ItemModel copy(Realm realm, com.argear.sdksample.model.ItemModel newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.argear.sdksample.model.ItemModel) cachedRealmObject;
        }

        // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
        com.argear.sdksample.model.ItemModel realmObject = realm.createObjectInternal(com.argear.sdksample.model.ItemModel.class, ((ItemModelRealmProxyInterface) newObject).realmGet$uuid(), false, Collections.<String>emptyList());
        cache.put(newObject, (RealmObjectProxy) realmObject);

        ItemModelRealmProxyInterface realmObjectSource = (ItemModelRealmProxyInterface) newObject;
        ItemModelRealmProxyInterface realmObjectCopy = (ItemModelRealmProxyInterface) realmObject;

        realmObjectCopy.realmSet$title(realmObjectSource.realmGet$title());
        realmObjectCopy.realmSet$description(realmObjectSource.realmGet$description());
        realmObjectCopy.realmSet$thumbnail(realmObjectSource.realmGet$thumbnail());
        realmObjectCopy.realmSet$zip_file(realmObjectSource.realmGet$zip_file());
        realmObjectCopy.realmSet$num_stickers(realmObjectSource.realmGet$num_stickers());
        realmObjectCopy.realmSet$num_effects(realmObjectSource.realmGet$num_effects());
        realmObjectCopy.realmSet$num_bgms(realmObjectSource.realmGet$num_bgms());
        realmObjectCopy.realmSet$num_filters(realmObjectSource.realmGet$num_filters());
        realmObjectCopy.realmSet$num_masks(realmObjectSource.realmGet$num_masks());
        realmObjectCopy.realmSet$has_trigger(realmObjectSource.realmGet$has_trigger());
        realmObjectCopy.realmSet$status(realmObjectSource.realmGet$status());
        realmObjectCopy.realmSet$updated_at(realmObjectSource.realmGet$updated_at());
        realmObjectCopy.realmSet$downloadStatus(realmObjectSource.realmGet$downloadStatus());
        return realmObject;
    }

    public static long insert(Realm realm, com.argear.sdksample.model.ItemModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.argear.sdksample.model.ItemModel.class);
        long tableNativePtr = table.getNativePtr();
        ItemModelColumnInfo columnInfo = (ItemModelColumnInfo) realm.getSchema().getColumnInfo(com.argear.sdksample.model.ItemModel.class);
        long pkColumnIndex = columnInfo.uuidIndex;
        String primaryKeyValue = ((ItemModelRealmProxyInterface) object).realmGet$uuid();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$title = ((ItemModelRealmProxyInterface) object).realmGet$title();
        if (realmGet$title != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.titleIndex, rowIndex, realmGet$title, false);
        }
        String realmGet$description = ((ItemModelRealmProxyInterface) object).realmGet$description();
        if (realmGet$description != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.descriptionIndex, rowIndex, realmGet$description, false);
        }
        String realmGet$thumbnail = ((ItemModelRealmProxyInterface) object).realmGet$thumbnail();
        if (realmGet$thumbnail != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.thumbnailIndex, rowIndex, realmGet$thumbnail, false);
        }
        String realmGet$zip_file = ((ItemModelRealmProxyInterface) object).realmGet$zip_file();
        if (realmGet$zip_file != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.zip_fileIndex, rowIndex, realmGet$zip_file, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.num_stickersIndex, rowIndex, ((ItemModelRealmProxyInterface) object).realmGet$num_stickers(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.num_effectsIndex, rowIndex, ((ItemModelRealmProxyInterface) object).realmGet$num_effects(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.num_bgmsIndex, rowIndex, ((ItemModelRealmProxyInterface) object).realmGet$num_bgms(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.num_filtersIndex, rowIndex, ((ItemModelRealmProxyInterface) object).realmGet$num_filters(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.num_masksIndex, rowIndex, ((ItemModelRealmProxyInterface) object).realmGet$num_masks(), false);
        Table.nativeSetBoolean(tableNativePtr, columnInfo.has_triggerIndex, rowIndex, ((ItemModelRealmProxyInterface) object).realmGet$has_trigger(), false);
        String realmGet$status = ((ItemModelRealmProxyInterface) object).realmGet$status();
        if (realmGet$status != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.statusIndex, rowIndex, realmGet$status, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.updated_atIndex, rowIndex, ((ItemModelRealmProxyInterface) object).realmGet$updated_at(), false);
        String realmGet$downloadStatus = ((ItemModelRealmProxyInterface) object).realmGet$downloadStatus();
        if (realmGet$downloadStatus != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.downloadStatusIndex, rowIndex, realmGet$downloadStatus, false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.argear.sdksample.model.ItemModel.class);
        long tableNativePtr = table.getNativePtr();
        ItemModelColumnInfo columnInfo = (ItemModelColumnInfo) realm.getSchema().getColumnInfo(com.argear.sdksample.model.ItemModel.class);
        long pkColumnIndex = columnInfo.uuidIndex;
        com.argear.sdksample.model.ItemModel object = null;
        while (objects.hasNext()) {
            object = (com.argear.sdksample.model.ItemModel) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            String primaryKeyValue = ((ItemModelRealmProxyInterface) object).realmGet$uuid();
            long rowIndex = Table.NO_MATCH;
            if (primaryKeyValue == null) {
                rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
            } else {
                rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
            } else {
                Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
            }
            cache.put(object, rowIndex);
            String realmGet$title = ((ItemModelRealmProxyInterface) object).realmGet$title();
            if (realmGet$title != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.titleIndex, rowIndex, realmGet$title, false);
            }
            String realmGet$description = ((ItemModelRealmProxyInterface) object).realmGet$description();
            if (realmGet$description != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.descriptionIndex, rowIndex, realmGet$description, false);
            }
            String realmGet$thumbnail = ((ItemModelRealmProxyInterface) object).realmGet$thumbnail();
            if (realmGet$thumbnail != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.thumbnailIndex, rowIndex, realmGet$thumbnail, false);
            }
            String realmGet$zip_file = ((ItemModelRealmProxyInterface) object).realmGet$zip_file();
            if (realmGet$zip_file != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.zip_fileIndex, rowIndex, realmGet$zip_file, false);
            }
            Table.nativeSetLong(tableNativePtr, columnInfo.num_stickersIndex, rowIndex, ((ItemModelRealmProxyInterface) object).realmGet$num_stickers(), false);
            Table.nativeSetLong(tableNativePtr, columnInfo.num_effectsIndex, rowIndex, ((ItemModelRealmProxyInterface) object).realmGet$num_effects(), false);
            Table.nativeSetLong(tableNativePtr, columnInfo.num_bgmsIndex, rowIndex, ((ItemModelRealmProxyInterface) object).realmGet$num_bgms(), false);
            Table.nativeSetLong(tableNativePtr, columnInfo.num_filtersIndex, rowIndex, ((ItemModelRealmProxyInterface) object).realmGet$num_filters(), false);
            Table.nativeSetLong(tableNativePtr, columnInfo.num_masksIndex, rowIndex, ((ItemModelRealmProxyInterface) object).realmGet$num_masks(), false);
            Table.nativeSetBoolean(tableNativePtr, columnInfo.has_triggerIndex, rowIndex, ((ItemModelRealmProxyInterface) object).realmGet$has_trigger(), false);
            String realmGet$status = ((ItemModelRealmProxyInterface) object).realmGet$status();
            if (realmGet$status != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.statusIndex, rowIndex, realmGet$status, false);
            }
            Table.nativeSetLong(tableNativePtr, columnInfo.updated_atIndex, rowIndex, ((ItemModelRealmProxyInterface) object).realmGet$updated_at(), false);
            String realmGet$downloadStatus = ((ItemModelRealmProxyInterface) object).realmGet$downloadStatus();
            if (realmGet$downloadStatus != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.downloadStatusIndex, rowIndex, realmGet$downloadStatus, false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, com.argear.sdksample.model.ItemModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.argear.sdksample.model.ItemModel.class);
        long tableNativePtr = table.getNativePtr();
        ItemModelColumnInfo columnInfo = (ItemModelColumnInfo) realm.getSchema().getColumnInfo(com.argear.sdksample.model.ItemModel.class);
        long pkColumnIndex = columnInfo.uuidIndex;
        String primaryKeyValue = ((ItemModelRealmProxyInterface) object).realmGet$uuid();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$title = ((ItemModelRealmProxyInterface) object).realmGet$title();
        if (realmGet$title != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.titleIndex, rowIndex, realmGet$title, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.titleIndex, rowIndex, false);
        }
        String realmGet$description = ((ItemModelRealmProxyInterface) object).realmGet$description();
        if (realmGet$description != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.descriptionIndex, rowIndex, realmGet$description, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.descriptionIndex, rowIndex, false);
        }
        String realmGet$thumbnail = ((ItemModelRealmProxyInterface) object).realmGet$thumbnail();
        if (realmGet$thumbnail != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.thumbnailIndex, rowIndex, realmGet$thumbnail, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.thumbnailIndex, rowIndex, false);
        }
        String realmGet$zip_file = ((ItemModelRealmProxyInterface) object).realmGet$zip_file();
        if (realmGet$zip_file != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.zip_fileIndex, rowIndex, realmGet$zip_file, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.zip_fileIndex, rowIndex, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.num_stickersIndex, rowIndex, ((ItemModelRealmProxyInterface) object).realmGet$num_stickers(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.num_effectsIndex, rowIndex, ((ItemModelRealmProxyInterface) object).realmGet$num_effects(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.num_bgmsIndex, rowIndex, ((ItemModelRealmProxyInterface) object).realmGet$num_bgms(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.num_filtersIndex, rowIndex, ((ItemModelRealmProxyInterface) object).realmGet$num_filters(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.num_masksIndex, rowIndex, ((ItemModelRealmProxyInterface) object).realmGet$num_masks(), false);
        Table.nativeSetBoolean(tableNativePtr, columnInfo.has_triggerIndex, rowIndex, ((ItemModelRealmProxyInterface) object).realmGet$has_trigger(), false);
        String realmGet$status = ((ItemModelRealmProxyInterface) object).realmGet$status();
        if (realmGet$status != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.statusIndex, rowIndex, realmGet$status, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.statusIndex, rowIndex, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.updated_atIndex, rowIndex, ((ItemModelRealmProxyInterface) object).realmGet$updated_at(), false);
        String realmGet$downloadStatus = ((ItemModelRealmProxyInterface) object).realmGet$downloadStatus();
        if (realmGet$downloadStatus != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.downloadStatusIndex, rowIndex, realmGet$downloadStatus, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.downloadStatusIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.argear.sdksample.model.ItemModel.class);
        long tableNativePtr = table.getNativePtr();
        ItemModelColumnInfo columnInfo = (ItemModelColumnInfo) realm.getSchema().getColumnInfo(com.argear.sdksample.model.ItemModel.class);
        long pkColumnIndex = columnInfo.uuidIndex;
        com.argear.sdksample.model.ItemModel object = null;
        while (objects.hasNext()) {
            object = (com.argear.sdksample.model.ItemModel) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            String primaryKeyValue = ((ItemModelRealmProxyInterface) object).realmGet$uuid();
            long rowIndex = Table.NO_MATCH;
            if (primaryKeyValue == null) {
                rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
            } else {
                rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
            }
            cache.put(object, rowIndex);
            String realmGet$title = ((ItemModelRealmProxyInterface) object).realmGet$title();
            if (realmGet$title != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.titleIndex, rowIndex, realmGet$title, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.titleIndex, rowIndex, false);
            }
            String realmGet$description = ((ItemModelRealmProxyInterface) object).realmGet$description();
            if (realmGet$description != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.descriptionIndex, rowIndex, realmGet$description, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.descriptionIndex, rowIndex, false);
            }
            String realmGet$thumbnail = ((ItemModelRealmProxyInterface) object).realmGet$thumbnail();
            if (realmGet$thumbnail != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.thumbnailIndex, rowIndex, realmGet$thumbnail, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.thumbnailIndex, rowIndex, false);
            }
            String realmGet$zip_file = ((ItemModelRealmProxyInterface) object).realmGet$zip_file();
            if (realmGet$zip_file != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.zip_fileIndex, rowIndex, realmGet$zip_file, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.zip_fileIndex, rowIndex, false);
            }
            Table.nativeSetLong(tableNativePtr, columnInfo.num_stickersIndex, rowIndex, ((ItemModelRealmProxyInterface) object).realmGet$num_stickers(), false);
            Table.nativeSetLong(tableNativePtr, columnInfo.num_effectsIndex, rowIndex, ((ItemModelRealmProxyInterface) object).realmGet$num_effects(), false);
            Table.nativeSetLong(tableNativePtr, columnInfo.num_bgmsIndex, rowIndex, ((ItemModelRealmProxyInterface) object).realmGet$num_bgms(), false);
            Table.nativeSetLong(tableNativePtr, columnInfo.num_filtersIndex, rowIndex, ((ItemModelRealmProxyInterface) object).realmGet$num_filters(), false);
            Table.nativeSetLong(tableNativePtr, columnInfo.num_masksIndex, rowIndex, ((ItemModelRealmProxyInterface) object).realmGet$num_masks(), false);
            Table.nativeSetBoolean(tableNativePtr, columnInfo.has_triggerIndex, rowIndex, ((ItemModelRealmProxyInterface) object).realmGet$has_trigger(), false);
            String realmGet$status = ((ItemModelRealmProxyInterface) object).realmGet$status();
            if (realmGet$status != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.statusIndex, rowIndex, realmGet$status, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.statusIndex, rowIndex, false);
            }
            Table.nativeSetLong(tableNativePtr, columnInfo.updated_atIndex, rowIndex, ((ItemModelRealmProxyInterface) object).realmGet$updated_at(), false);
            String realmGet$downloadStatus = ((ItemModelRealmProxyInterface) object).realmGet$downloadStatus();
            if (realmGet$downloadStatus != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.downloadStatusIndex, rowIndex, realmGet$downloadStatus, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.downloadStatusIndex, rowIndex, false);
            }
        }
    }

    public static com.argear.sdksample.model.ItemModel createDetachedCopy(com.argear.sdksample.model.ItemModel realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.argear.sdksample.model.ItemModel unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new com.argear.sdksample.model.ItemModel();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.argear.sdksample.model.ItemModel) cachedObject.object;
            }
            unmanagedObject = (com.argear.sdksample.model.ItemModel) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        ItemModelRealmProxyInterface unmanagedCopy = (ItemModelRealmProxyInterface) unmanagedObject;
        ItemModelRealmProxyInterface realmSource = (ItemModelRealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$uuid(realmSource.realmGet$uuid());
        unmanagedCopy.realmSet$title(realmSource.realmGet$title());
        unmanagedCopy.realmSet$description(realmSource.realmGet$description());
        unmanagedCopy.realmSet$thumbnail(realmSource.realmGet$thumbnail());
        unmanagedCopy.realmSet$zip_file(realmSource.realmGet$zip_file());
        unmanagedCopy.realmSet$num_stickers(realmSource.realmGet$num_stickers());
        unmanagedCopy.realmSet$num_effects(realmSource.realmGet$num_effects());
        unmanagedCopy.realmSet$num_bgms(realmSource.realmGet$num_bgms());
        unmanagedCopy.realmSet$num_filters(realmSource.realmGet$num_filters());
        unmanagedCopy.realmSet$num_masks(realmSource.realmGet$num_masks());
        unmanagedCopy.realmSet$has_trigger(realmSource.realmGet$has_trigger());
        unmanagedCopy.realmSet$status(realmSource.realmGet$status());
        unmanagedCopy.realmSet$updated_at(realmSource.realmGet$updated_at());
        unmanagedCopy.realmSet$downloadStatus(realmSource.realmGet$downloadStatus());

        return unmanagedObject;
    }

    static com.argear.sdksample.model.ItemModel update(Realm realm, com.argear.sdksample.model.ItemModel realmObject, com.argear.sdksample.model.ItemModel newObject, Map<RealmModel, RealmObjectProxy> cache) {
        ItemModelRealmProxyInterface realmObjectTarget = (ItemModelRealmProxyInterface) realmObject;
        ItemModelRealmProxyInterface realmObjectSource = (ItemModelRealmProxyInterface) newObject;
        realmObjectTarget.realmSet$title(realmObjectSource.realmGet$title());
        realmObjectTarget.realmSet$description(realmObjectSource.realmGet$description());
        realmObjectTarget.realmSet$thumbnail(realmObjectSource.realmGet$thumbnail());
        realmObjectTarget.realmSet$zip_file(realmObjectSource.realmGet$zip_file());
        realmObjectTarget.realmSet$num_stickers(realmObjectSource.realmGet$num_stickers());
        realmObjectTarget.realmSet$num_effects(realmObjectSource.realmGet$num_effects());
        realmObjectTarget.realmSet$num_bgms(realmObjectSource.realmGet$num_bgms());
        realmObjectTarget.realmSet$num_filters(realmObjectSource.realmGet$num_filters());
        realmObjectTarget.realmSet$num_masks(realmObjectSource.realmGet$num_masks());
        realmObjectTarget.realmSet$has_trigger(realmObjectSource.realmGet$has_trigger());
        realmObjectTarget.realmSet$status(realmObjectSource.realmGet$status());
        realmObjectTarget.realmSet$updated_at(realmObjectSource.realmGet$updated_at());
        realmObjectTarget.realmSet$downloadStatus(realmObjectSource.realmGet$downloadStatus());
        return realmObject;
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ItemModelRealmProxy aItemModel = (ItemModelRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aItemModel.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aItemModel.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aItemModel.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }
}
