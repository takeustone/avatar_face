package io.realm;


import android.util.JsonReader;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.RealmProxyMediator;
import io.realm.internal.Row;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

@io.realm.annotations.RealmModule
class DefaultRealmModuleMediator extends RealmProxyMediator {

    private static final Set<Class<? extends RealmModel>> MODEL_CLASSES;
    static {
        Set<Class<? extends RealmModel>> modelClasses = new HashSet<Class<? extends RealmModel>>(3);
        modelClasses.add(com.argear.sdksample.model.CategoryModel.class);
        modelClasses.add(com.argear.sdksample.model.FilterModel.class);
        modelClasses.add(com.argear.sdksample.model.ItemModel.class);
        MODEL_CLASSES = Collections.unmodifiableSet(modelClasses);
    }

    @Override
    public Map<Class<? extends RealmModel>, OsObjectSchemaInfo> getExpectedObjectSchemaInfoMap() {
        Map<Class<? extends RealmModel>, OsObjectSchemaInfo> infoMap = new HashMap<Class<? extends RealmModel>, OsObjectSchemaInfo>(3);
        infoMap.put(com.argear.sdksample.model.CategoryModel.class, io.realm.CategoryModelRealmProxy.getExpectedObjectSchemaInfo());
        infoMap.put(com.argear.sdksample.model.FilterModel.class, io.realm.FilterModelRealmProxy.getExpectedObjectSchemaInfo());
        infoMap.put(com.argear.sdksample.model.ItemModel.class, io.realm.ItemModelRealmProxy.getExpectedObjectSchemaInfo());
        return infoMap;
    }

    @Override
    public ColumnInfo createColumnInfo(Class<? extends RealmModel> clazz, OsSchemaInfo schemaInfo) {
        checkClass(clazz);

        if (clazz.equals(com.argear.sdksample.model.CategoryModel.class)) {
            return io.realm.CategoryModelRealmProxy.createColumnInfo(schemaInfo);
        }
        if (clazz.equals(com.argear.sdksample.model.FilterModel.class)) {
            return io.realm.FilterModelRealmProxy.createColumnInfo(schemaInfo);
        }
        if (clazz.equals(com.argear.sdksample.model.ItemModel.class)) {
            return io.realm.ItemModelRealmProxy.createColumnInfo(schemaInfo);
        }
        throw getMissingProxyClassException(clazz);
    }

    @Override
    public List<String> getFieldNames(Class<? extends RealmModel> clazz) {
        checkClass(clazz);

        if (clazz.equals(com.argear.sdksample.model.CategoryModel.class)) {
            return io.realm.CategoryModelRealmProxy.getFieldNames();
        }
        if (clazz.equals(com.argear.sdksample.model.FilterModel.class)) {
            return io.realm.FilterModelRealmProxy.getFieldNames();
        }
        if (clazz.equals(com.argear.sdksample.model.ItemModel.class)) {
            return io.realm.ItemModelRealmProxy.getFieldNames();
        }
        throw getMissingProxyClassException(clazz);
    }

    @Override
    public String getSimpleClassNameImpl(Class<? extends RealmModel> clazz) {
        checkClass(clazz);

        if (clazz.equals(com.argear.sdksample.model.CategoryModel.class)) {
            return io.realm.CategoryModelRealmProxy.getSimpleClassName();
        }
        if (clazz.equals(com.argear.sdksample.model.FilterModel.class)) {
            return io.realm.FilterModelRealmProxy.getSimpleClassName();
        }
        if (clazz.equals(com.argear.sdksample.model.ItemModel.class)) {
            return io.realm.ItemModelRealmProxy.getSimpleClassName();
        }
        throw getMissingProxyClassException(clazz);
    }

    @Override
    public <E extends RealmModel> E newInstance(Class<E> clazz, Object baseRealm, Row row, ColumnInfo columnInfo, boolean acceptDefaultValue, List<String> excludeFields) {
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        try {
            objectContext.set((BaseRealm) baseRealm, row, columnInfo, acceptDefaultValue, excludeFields);
            checkClass(clazz);

            if (clazz.equals(com.argear.sdksample.model.CategoryModel.class)) {
                return clazz.cast(new io.realm.CategoryModelRealmProxy());
            }
            if (clazz.equals(com.argear.sdksample.model.FilterModel.class)) {
                return clazz.cast(new io.realm.FilterModelRealmProxy());
            }
            if (clazz.equals(com.argear.sdksample.model.ItemModel.class)) {
                return clazz.cast(new io.realm.ItemModelRealmProxy());
            }
            throw getMissingProxyClassException(clazz);
        } finally {
            objectContext.clear();
        }
    }

    @Override
    public Set<Class<? extends RealmModel>> getModelClasses() {
        return MODEL_CLASSES;
    }

    @Override
    public <E extends RealmModel> E copyOrUpdate(Realm realm, E obj, boolean update, Map<RealmModel, RealmObjectProxy> cache) {
        // This cast is correct because obj is either
        // generated by RealmProxy or the original type extending directly from RealmObject
        @SuppressWarnings("unchecked") Class<E> clazz = (Class<E>) ((obj instanceof RealmObjectProxy) ? obj.getClass().getSuperclass() : obj.getClass());

        if (clazz.equals(com.argear.sdksample.model.CategoryModel.class)) {
            return clazz.cast(io.realm.CategoryModelRealmProxy.copyOrUpdate(realm, (com.argear.sdksample.model.CategoryModel) obj, update, cache));
        }
        if (clazz.equals(com.argear.sdksample.model.FilterModel.class)) {
            return clazz.cast(io.realm.FilterModelRealmProxy.copyOrUpdate(realm, (com.argear.sdksample.model.FilterModel) obj, update, cache));
        }
        if (clazz.equals(com.argear.sdksample.model.ItemModel.class)) {
            return clazz.cast(io.realm.ItemModelRealmProxy.copyOrUpdate(realm, (com.argear.sdksample.model.ItemModel) obj, update, cache));
        }
        throw getMissingProxyClassException(clazz);
    }

    @Override
    public void insert(Realm realm, RealmModel object, Map<RealmModel, Long> cache) {
        // This cast is correct because obj is either
        // generated by RealmProxy or the original type extending directly from RealmObject
        @SuppressWarnings("unchecked") Class<RealmModel> clazz = (Class<RealmModel>) ((object instanceof RealmObjectProxy) ? object.getClass().getSuperclass() : object.getClass());

        if (clazz.equals(com.argear.sdksample.model.CategoryModel.class)) {
            io.realm.CategoryModelRealmProxy.insert(realm, (com.argear.sdksample.model.CategoryModel) object, cache);
        } else if (clazz.equals(com.argear.sdksample.model.FilterModel.class)) {
            io.realm.FilterModelRealmProxy.insert(realm, (com.argear.sdksample.model.FilterModel) object, cache);
        } else if (clazz.equals(com.argear.sdksample.model.ItemModel.class)) {
            io.realm.ItemModelRealmProxy.insert(realm, (com.argear.sdksample.model.ItemModel) object, cache);
        } else {
            throw getMissingProxyClassException(clazz);
        }
    }

    @Override
    public void insert(Realm realm, Collection<? extends RealmModel> objects) {
        Iterator<? extends RealmModel> iterator = objects.iterator();
        RealmModel object = null;
        Map<RealmModel, Long> cache = new HashMap<RealmModel, Long>(objects.size());
        if (iterator.hasNext()) {
            //  access the first element to figure out the clazz for the routing below
            object = iterator.next();
            // This cast is correct because obj is either
            // generated by RealmProxy or the original type extending directly from RealmObject
            @SuppressWarnings("unchecked") Class<RealmModel> clazz = (Class<RealmModel>) ((object instanceof RealmObjectProxy) ? object.getClass().getSuperclass() : object.getClass());

            if (clazz.equals(com.argear.sdksample.model.CategoryModel.class)) {
                io.realm.CategoryModelRealmProxy.insert(realm, (com.argear.sdksample.model.CategoryModel) object, cache);
            } else if (clazz.equals(com.argear.sdksample.model.FilterModel.class)) {
                io.realm.FilterModelRealmProxy.insert(realm, (com.argear.sdksample.model.FilterModel) object, cache);
            } else if (clazz.equals(com.argear.sdksample.model.ItemModel.class)) {
                io.realm.ItemModelRealmProxy.insert(realm, (com.argear.sdksample.model.ItemModel) object, cache);
            } else {
                throw getMissingProxyClassException(clazz);
            }
            if (iterator.hasNext()) {
                if (clazz.equals(com.argear.sdksample.model.CategoryModel.class)) {
                    io.realm.CategoryModelRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(com.argear.sdksample.model.FilterModel.class)) {
                    io.realm.FilterModelRealmProxy.insert(realm, iterator, cache);
                } else if (clazz.equals(com.argear.sdksample.model.ItemModel.class)) {
                    io.realm.ItemModelRealmProxy.insert(realm, iterator, cache);
                } else {
                    throw getMissingProxyClassException(clazz);
                }
            }
        }
    }

    @Override
    public void insertOrUpdate(Realm realm, RealmModel obj, Map<RealmModel, Long> cache) {
        // This cast is correct because obj is either
        // generated by RealmProxy or the original type extending directly from RealmObject
        @SuppressWarnings("unchecked") Class<RealmModel> clazz = (Class<RealmModel>) ((obj instanceof RealmObjectProxy) ? obj.getClass().getSuperclass() : obj.getClass());

        if (clazz.equals(com.argear.sdksample.model.CategoryModel.class)) {
            io.realm.CategoryModelRealmProxy.insertOrUpdate(realm, (com.argear.sdksample.model.CategoryModel) obj, cache);
        } else if (clazz.equals(com.argear.sdksample.model.FilterModel.class)) {
            io.realm.FilterModelRealmProxy.insertOrUpdate(realm, (com.argear.sdksample.model.FilterModel) obj, cache);
        } else if (clazz.equals(com.argear.sdksample.model.ItemModel.class)) {
            io.realm.ItemModelRealmProxy.insertOrUpdate(realm, (com.argear.sdksample.model.ItemModel) obj, cache);
        } else {
            throw getMissingProxyClassException(clazz);
        }
    }

    @Override
    public void insertOrUpdate(Realm realm, Collection<? extends RealmModel> objects) {
        Iterator<? extends RealmModel> iterator = objects.iterator();
        RealmModel object = null;
        Map<RealmModel, Long> cache = new HashMap<RealmModel, Long>(objects.size());
        if (iterator.hasNext()) {
            //  access the first element to figure out the clazz for the routing below
            object = iterator.next();
            // This cast is correct because obj is either
            // generated by RealmProxy or the original type extending directly from RealmObject
            @SuppressWarnings("unchecked") Class<RealmModel> clazz = (Class<RealmModel>) ((object instanceof RealmObjectProxy) ? object.getClass().getSuperclass() : object.getClass());

            if (clazz.equals(com.argear.sdksample.model.CategoryModel.class)) {
                io.realm.CategoryModelRealmProxy.insertOrUpdate(realm, (com.argear.sdksample.model.CategoryModel) object, cache);
            } else if (clazz.equals(com.argear.sdksample.model.FilterModel.class)) {
                io.realm.FilterModelRealmProxy.insertOrUpdate(realm, (com.argear.sdksample.model.FilterModel) object, cache);
            } else if (clazz.equals(com.argear.sdksample.model.ItemModel.class)) {
                io.realm.ItemModelRealmProxy.insertOrUpdate(realm, (com.argear.sdksample.model.ItemModel) object, cache);
            } else {
                throw getMissingProxyClassException(clazz);
            }
            if (iterator.hasNext()) {
                if (clazz.equals(com.argear.sdksample.model.CategoryModel.class)) {
                    io.realm.CategoryModelRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(com.argear.sdksample.model.FilterModel.class)) {
                    io.realm.FilterModelRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else if (clazz.equals(com.argear.sdksample.model.ItemModel.class)) {
                    io.realm.ItemModelRealmProxy.insertOrUpdate(realm, iterator, cache);
                } else {
                    throw getMissingProxyClassException(clazz);
                }
            }
        }
    }

    @Override
    public <E extends RealmModel> E createOrUpdateUsingJsonObject(Class<E> clazz, Realm realm, JSONObject json, boolean update)
        throws JSONException {
        checkClass(clazz);

        if (clazz.equals(com.argear.sdksample.model.CategoryModel.class)) {
            return clazz.cast(io.realm.CategoryModelRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        }
        if (clazz.equals(com.argear.sdksample.model.FilterModel.class)) {
            return clazz.cast(io.realm.FilterModelRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        }
        if (clazz.equals(com.argear.sdksample.model.ItemModel.class)) {
            return clazz.cast(io.realm.ItemModelRealmProxy.createOrUpdateUsingJsonObject(realm, json, update));
        }
        throw getMissingProxyClassException(clazz);
    }

    @Override
    public <E extends RealmModel> E createUsingJsonStream(Class<E> clazz, Realm realm, JsonReader reader)
        throws IOException {
        checkClass(clazz);

        if (clazz.equals(com.argear.sdksample.model.CategoryModel.class)) {
            return clazz.cast(io.realm.CategoryModelRealmProxy.createUsingJsonStream(realm, reader));
        }
        if (clazz.equals(com.argear.sdksample.model.FilterModel.class)) {
            return clazz.cast(io.realm.FilterModelRealmProxy.createUsingJsonStream(realm, reader));
        }
        if (clazz.equals(com.argear.sdksample.model.ItemModel.class)) {
            return clazz.cast(io.realm.ItemModelRealmProxy.createUsingJsonStream(realm, reader));
        }
        throw getMissingProxyClassException(clazz);
    }

    @Override
    public <E extends RealmModel> E createDetachedCopy(E realmObject, int maxDepth, Map<RealmModel, RealmObjectProxy.CacheData<RealmModel>> cache) {
        // This cast is correct because obj is either
        // generated by RealmProxy or the original type extending directly from RealmObject
        @SuppressWarnings("unchecked") Class<E> clazz = (Class<E>) realmObject.getClass().getSuperclass();

        if (clazz.equals(com.argear.sdksample.model.CategoryModel.class)) {
            return clazz.cast(io.realm.CategoryModelRealmProxy.createDetachedCopy((com.argear.sdksample.model.CategoryModel) realmObject, 0, maxDepth, cache));
        }
        if (clazz.equals(com.argear.sdksample.model.FilterModel.class)) {
            return clazz.cast(io.realm.FilterModelRealmProxy.createDetachedCopy((com.argear.sdksample.model.FilterModel) realmObject, 0, maxDepth, cache));
        }
        if (clazz.equals(com.argear.sdksample.model.ItemModel.class)) {
            return clazz.cast(io.realm.ItemModelRealmProxy.createDetachedCopy((com.argear.sdksample.model.ItemModel) realmObject, 0, maxDepth, cache));
        }
        throw getMissingProxyClassException(clazz);
    }

}
