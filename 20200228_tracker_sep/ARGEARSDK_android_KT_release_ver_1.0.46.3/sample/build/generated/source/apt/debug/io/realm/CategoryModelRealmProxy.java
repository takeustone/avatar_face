package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.ProxyUtils;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class CategoryModelRealmProxy extends com.argear.sdksample.model.CategoryModel
    implements RealmObjectProxy, CategoryModelRealmProxyInterface {

    static final class CategoryModelColumnInfo extends ColumnInfo {
        long uuidIndex;
        long titleIndex;
        long is_bundleIndex;
        long updated_atIndex;
        long descriptionIndex;
        long statusIndex;
        long itemsIndex;

        CategoryModelColumnInfo(OsSchemaInfo schemaInfo) {
            super(7);
            OsObjectSchemaInfo objectSchemaInfo = schemaInfo.getObjectSchemaInfo("CategoryModel");
            this.uuidIndex = addColumnDetails("uuid", objectSchemaInfo);
            this.titleIndex = addColumnDetails("title", objectSchemaInfo);
            this.is_bundleIndex = addColumnDetails("is_bundle", objectSchemaInfo);
            this.updated_atIndex = addColumnDetails("updated_at", objectSchemaInfo);
            this.descriptionIndex = addColumnDetails("description", objectSchemaInfo);
            this.statusIndex = addColumnDetails("status", objectSchemaInfo);
            this.itemsIndex = addColumnDetails("items", objectSchemaInfo);
        }

        CategoryModelColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new CategoryModelColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final CategoryModelColumnInfo src = (CategoryModelColumnInfo) rawSrc;
            final CategoryModelColumnInfo dst = (CategoryModelColumnInfo) rawDst;
            dst.uuidIndex = src.uuidIndex;
            dst.titleIndex = src.titleIndex;
            dst.is_bundleIndex = src.is_bundleIndex;
            dst.updated_atIndex = src.updated_atIndex;
            dst.descriptionIndex = src.descriptionIndex;
            dst.statusIndex = src.statusIndex;
            dst.itemsIndex = src.itemsIndex;
        }
    }

    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>(7);
        fieldNames.add("uuid");
        fieldNames.add("title");
        fieldNames.add("is_bundle");
        fieldNames.add("updated_at");
        fieldNames.add("description");
        fieldNames.add("status");
        fieldNames.add("items");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    private CategoryModelColumnInfo columnInfo;
    private ProxyState<com.argear.sdksample.model.CategoryModel> proxyState;
    private RealmList<com.argear.sdksample.model.ItemModel> itemsRealmList;

    CategoryModelRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (CategoryModelColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<com.argear.sdksample.model.CategoryModel>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$uuid() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.uuidIndex);
    }

    @Override
    public void realmSet$uuid(String value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'uuid' cannot be changed after object was created.");
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$title() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.titleIndex);
    }

    @Override
    public void realmSet$title(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.titleIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.titleIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.titleIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.titleIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public boolean realmGet$is_bundle() {
        proxyState.getRealm$realm().checkIfValid();
        return (boolean) proxyState.getRow$realm().getBoolean(columnInfo.is_bundleIndex);
    }

    @Override
    public void realmSet$is_bundle(boolean value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setBoolean(columnInfo.is_bundleIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setBoolean(columnInfo.is_bundleIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public long realmGet$updated_at() {
        proxyState.getRealm$realm().checkIfValid();
        return (long) proxyState.getRow$realm().getLong(columnInfo.updated_atIndex);
    }

    @Override
    public void realmSet$updated_at(long value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.updated_atIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.updated_atIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$description() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.descriptionIndex);
    }

    @Override
    public void realmSet$description(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.descriptionIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.descriptionIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.descriptionIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.descriptionIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$status() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.statusIndex);
    }

    @Override
    public void realmSet$status(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.statusIndex, row.getIndex(), true);
                return;
            }
            row.getTable().setString(columnInfo.statusIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.statusIndex);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.statusIndex, value);
    }

    @Override
    public RealmList<com.argear.sdksample.model.ItemModel> realmGet$items() {
        proxyState.getRealm$realm().checkIfValid();
        // use the cached value if available
        if (itemsRealmList != null) {
            return itemsRealmList;
        } else {
            OsList osList = proxyState.getRow$realm().getModelList(columnInfo.itemsIndex);
            itemsRealmList = new RealmList<com.argear.sdksample.model.ItemModel>(com.argear.sdksample.model.ItemModel.class, osList, proxyState.getRealm$realm());
            return itemsRealmList;
        }
    }

    @Override
    public void realmSet$items(RealmList<com.argear.sdksample.model.ItemModel> value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            if (proxyState.getExcludeFields$realm().contains("items")) {
                return;
            }
            // if the list contains unmanaged RealmObjects, convert them to managed.
            if (value != null && !value.isManaged()) {
                final Realm realm = (Realm) proxyState.getRealm$realm();
                final RealmList<com.argear.sdksample.model.ItemModel> original = value;
                value = new RealmList<com.argear.sdksample.model.ItemModel>();
                for (com.argear.sdksample.model.ItemModel item : original) {
                    if (item == null || RealmObject.isManaged(item)) {
                        value.add(item);
                    } else {
                        value.add(realm.copyToRealm(item));
                    }
                }
            }
        }

        proxyState.getRealm$realm().checkIfValid();
        OsList osList = proxyState.getRow$realm().getModelList(columnInfo.itemsIndex);
        // For lists of equal lengths, we need to set each element directly as clearing the receiver list can be wrong if the input and target list are the same.
        if (value != null && value.size() == osList.size()) {
            int objects = value.size();
            for (int i = 0; i < objects; i++) {
                com.argear.sdksample.model.ItemModel linkedObject = value.get(i);
                proxyState.checkValidObject(linkedObject);
                osList.setRow(i, ((RealmObjectProxy) linkedObject).realmGet$proxyState().getRow$realm().getIndex());
            }
        } else {
            osList.removeAll();
            if (value == null) {
                return;
            }
            int objects = value.size();
            for (int i = 0; i < objects; i++) {
                com.argear.sdksample.model.ItemModel linkedObject = value.get(i);
                proxyState.checkValidObject(linkedObject);
                osList.addRow(((RealmObjectProxy) linkedObject).realmGet$proxyState().getRow$realm().getIndex());
            }
        }
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("CategoryModel", 7, 0);
        builder.addPersistedProperty("uuid", RealmFieldType.STRING, Property.PRIMARY_KEY, Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("title", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("is_bundle", RealmFieldType.BOOLEAN, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("updated_at", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("description", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("status", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedLinkProperty("items", RealmFieldType.LIST, "ItemModel");
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static CategoryModelColumnInfo createColumnInfo(OsSchemaInfo schemaInfo) {
        return new CategoryModelColumnInfo(schemaInfo);
    }

    public static String getSimpleClassName() {
        return "CategoryModel";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static com.argear.sdksample.model.CategoryModel createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = new ArrayList<String>(1);
        com.argear.sdksample.model.CategoryModel obj = null;
        if (update) {
            Table table = realm.getTable(com.argear.sdksample.model.CategoryModel.class);
            CategoryModelColumnInfo columnInfo = (CategoryModelColumnInfo) realm.getSchema().getColumnInfo(com.argear.sdksample.model.CategoryModel.class);
            long pkColumnIndex = columnInfo.uuidIndex;
            long rowIndex = Table.NO_MATCH;
            if (json.isNull("uuid")) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstString(pkColumnIndex, json.getString("uuid"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(com.argear.sdksample.model.CategoryModel.class), false, Collections.<String> emptyList());
                    obj = new io.realm.CategoryModelRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("items")) {
                excludeFields.add("items");
            }
            if (json.has("uuid")) {
                if (json.isNull("uuid")) {
                    obj = (io.realm.CategoryModelRealmProxy) realm.createObjectInternal(com.argear.sdksample.model.CategoryModel.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.CategoryModelRealmProxy) realm.createObjectInternal(com.argear.sdksample.model.CategoryModel.class, json.getString("uuid"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'uuid'.");
            }
        }

        final CategoryModelRealmProxyInterface objProxy = (CategoryModelRealmProxyInterface) obj;
        if (json.has("title")) {
            if (json.isNull("title")) {
                objProxy.realmSet$title(null);
            } else {
                objProxy.realmSet$title((String) json.getString("title"));
            }
        }
        if (json.has("is_bundle")) {
            if (json.isNull("is_bundle")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'is_bundle' to null.");
            } else {
                objProxy.realmSet$is_bundle((boolean) json.getBoolean("is_bundle"));
            }
        }
        if (json.has("updated_at")) {
            if (json.isNull("updated_at")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'updated_at' to null.");
            } else {
                objProxy.realmSet$updated_at((long) json.getLong("updated_at"));
            }
        }
        if (json.has("description")) {
            if (json.isNull("description")) {
                objProxy.realmSet$description(null);
            } else {
                objProxy.realmSet$description((String) json.getString("description"));
            }
        }
        if (json.has("status")) {
            if (json.isNull("status")) {
                objProxy.realmSet$status(null);
            } else {
                objProxy.realmSet$status((String) json.getString("status"));
            }
        }
        if (json.has("items")) {
            if (json.isNull("items")) {
                objProxy.realmSet$items(null);
            } else {
                objProxy.realmGet$items().clear();
                JSONArray array = json.getJSONArray("items");
                for (int i = 0; i < array.length(); i++) {
                    com.argear.sdksample.model.ItemModel item = ItemModelRealmProxy.createOrUpdateUsingJsonObject(realm, array.getJSONObject(i), update);
                    objProxy.realmGet$items().add(item);
                }
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.argear.sdksample.model.CategoryModel createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        final com.argear.sdksample.model.CategoryModel obj = new com.argear.sdksample.model.CategoryModel();
        final CategoryModelRealmProxyInterface objProxy = (CategoryModelRealmProxyInterface) obj;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("uuid")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$uuid((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$uuid(null);
                }
                jsonHasPrimaryKey = true;
            } else if (name.equals("title")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$title((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$title(null);
                }
            } else if (name.equals("is_bundle")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$is_bundle((boolean) reader.nextBoolean());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'is_bundle' to null.");
                }
            } else if (name.equals("updated_at")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$updated_at((long) reader.nextLong());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'updated_at' to null.");
                }
            } else if (name.equals("description")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$description((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$description(null);
                }
            } else if (name.equals("status")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$status((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$status(null);
                }
            } else if (name.equals("items")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    objProxy.realmSet$items(null);
                } else {
                    objProxy.realmSet$items(new RealmList<com.argear.sdksample.model.ItemModel>());
                    reader.beginArray();
                    while (reader.hasNext()) {
                        com.argear.sdksample.model.ItemModel item = ItemModelRealmProxy.createUsingJsonStream(realm, reader);
                        objProxy.realmGet$items().add(item);
                    }
                    reader.endArray();
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'uuid'.");
        }
        return realm.copyToRealm(obj);
    }

    public static com.argear.sdksample.model.CategoryModel copyOrUpdate(Realm realm, com.argear.sdksample.model.CategoryModel object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null) {
            final BaseRealm otherRealm = ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm();
            if (otherRealm.threadId != realm.threadId) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            }
            if (otherRealm.getPath().equals(realm.getPath())) {
                return object;
            }
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.argear.sdksample.model.CategoryModel) cachedRealmObject;
        }

        com.argear.sdksample.model.CategoryModel realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(com.argear.sdksample.model.CategoryModel.class);
            CategoryModelColumnInfo columnInfo = (CategoryModelColumnInfo) realm.getSchema().getColumnInfo(com.argear.sdksample.model.CategoryModel.class);
            long pkColumnIndex = columnInfo.uuidIndex;
            String value = ((CategoryModelRealmProxyInterface) object).realmGet$uuid();
            long rowIndex = Table.NO_MATCH;
            if (value == null) {
                rowIndex = table.findFirstNull(pkColumnIndex);
            } else {
                rowIndex = table.findFirstString(pkColumnIndex, value);
            }
            if (rowIndex == Table.NO_MATCH) {
                canUpdate = false;
            } else {
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(com.argear.sdksample.model.CategoryModel.class), false, Collections.<String> emptyList());
                    realmObject = new io.realm.CategoryModelRealmProxy();
                    cache.put(object, (RealmObjectProxy) realmObject);
                } finally {
                    objectContext.clear();
                }
            }
        }

        return (canUpdate) ? update(realm, realmObject, object, cache) : copy(realm, object, update, cache);
    }

    public static com.argear.sdksample.model.CategoryModel copy(Realm realm, com.argear.sdksample.model.CategoryModel newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.argear.sdksample.model.CategoryModel) cachedRealmObject;
        }

        // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
        com.argear.sdksample.model.CategoryModel realmObject = realm.createObjectInternal(com.argear.sdksample.model.CategoryModel.class, ((CategoryModelRealmProxyInterface) newObject).realmGet$uuid(), false, Collections.<String>emptyList());
        cache.put(newObject, (RealmObjectProxy) realmObject);

        CategoryModelRealmProxyInterface realmObjectSource = (CategoryModelRealmProxyInterface) newObject;
        CategoryModelRealmProxyInterface realmObjectCopy = (CategoryModelRealmProxyInterface) realmObject;

        realmObjectCopy.realmSet$title(realmObjectSource.realmGet$title());
        realmObjectCopy.realmSet$is_bundle(realmObjectSource.realmGet$is_bundle());
        realmObjectCopy.realmSet$updated_at(realmObjectSource.realmGet$updated_at());
        realmObjectCopy.realmSet$description(realmObjectSource.realmGet$description());
        realmObjectCopy.realmSet$status(realmObjectSource.realmGet$status());

        RealmList<com.argear.sdksample.model.ItemModel> itemsList = realmObjectSource.realmGet$items();
        if (itemsList != null) {
            RealmList<com.argear.sdksample.model.ItemModel> itemsRealmList = realmObjectCopy.realmGet$items();
            itemsRealmList.clear();
            for (int i = 0; i < itemsList.size(); i++) {
                com.argear.sdksample.model.ItemModel itemsItem = itemsList.get(i);
                com.argear.sdksample.model.ItemModel cacheitems = (com.argear.sdksample.model.ItemModel) cache.get(itemsItem);
                if (cacheitems != null) {
                    itemsRealmList.add(cacheitems);
                } else {
                    itemsRealmList.add(ItemModelRealmProxy.copyOrUpdate(realm, itemsItem, update, cache));
                }
            }
        }

        return realmObject;
    }

    public static long insert(Realm realm, com.argear.sdksample.model.CategoryModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.argear.sdksample.model.CategoryModel.class);
        long tableNativePtr = table.getNativePtr();
        CategoryModelColumnInfo columnInfo = (CategoryModelColumnInfo) realm.getSchema().getColumnInfo(com.argear.sdksample.model.CategoryModel.class);
        long pkColumnIndex = columnInfo.uuidIndex;
        String primaryKeyValue = ((CategoryModelRealmProxyInterface) object).realmGet$uuid();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$title = ((CategoryModelRealmProxyInterface) object).realmGet$title();
        if (realmGet$title != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.titleIndex, rowIndex, realmGet$title, false);
        }
        Table.nativeSetBoolean(tableNativePtr, columnInfo.is_bundleIndex, rowIndex, ((CategoryModelRealmProxyInterface) object).realmGet$is_bundle(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.updated_atIndex, rowIndex, ((CategoryModelRealmProxyInterface) object).realmGet$updated_at(), false);
        String realmGet$description = ((CategoryModelRealmProxyInterface) object).realmGet$description();
        if (realmGet$description != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.descriptionIndex, rowIndex, realmGet$description, false);
        }
        String realmGet$status = ((CategoryModelRealmProxyInterface) object).realmGet$status();
        if (realmGet$status != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.statusIndex, rowIndex, realmGet$status, false);
        }

        RealmList<com.argear.sdksample.model.ItemModel> itemsList = ((CategoryModelRealmProxyInterface) object).realmGet$items();
        if (itemsList != null) {
            OsList itemsOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.itemsIndex);
            for (com.argear.sdksample.model.ItemModel itemsItem : itemsList) {
                Long cacheItemIndexitems = cache.get(itemsItem);
                if (cacheItemIndexitems == null) {
                    cacheItemIndexitems = ItemModelRealmProxy.insert(realm, itemsItem, cache);
                }
                itemsOsList.addRow(cacheItemIndexitems);
            }
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.argear.sdksample.model.CategoryModel.class);
        long tableNativePtr = table.getNativePtr();
        CategoryModelColumnInfo columnInfo = (CategoryModelColumnInfo) realm.getSchema().getColumnInfo(com.argear.sdksample.model.CategoryModel.class);
        long pkColumnIndex = columnInfo.uuidIndex;
        com.argear.sdksample.model.CategoryModel object = null;
        while (objects.hasNext()) {
            object = (com.argear.sdksample.model.CategoryModel) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            String primaryKeyValue = ((CategoryModelRealmProxyInterface) object).realmGet$uuid();
            long rowIndex = Table.NO_MATCH;
            if (primaryKeyValue == null) {
                rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
            } else {
                rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
            } else {
                Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
            }
            cache.put(object, rowIndex);
            String realmGet$title = ((CategoryModelRealmProxyInterface) object).realmGet$title();
            if (realmGet$title != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.titleIndex, rowIndex, realmGet$title, false);
            }
            Table.nativeSetBoolean(tableNativePtr, columnInfo.is_bundleIndex, rowIndex, ((CategoryModelRealmProxyInterface) object).realmGet$is_bundle(), false);
            Table.nativeSetLong(tableNativePtr, columnInfo.updated_atIndex, rowIndex, ((CategoryModelRealmProxyInterface) object).realmGet$updated_at(), false);
            String realmGet$description = ((CategoryModelRealmProxyInterface) object).realmGet$description();
            if (realmGet$description != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.descriptionIndex, rowIndex, realmGet$description, false);
            }
            String realmGet$status = ((CategoryModelRealmProxyInterface) object).realmGet$status();
            if (realmGet$status != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.statusIndex, rowIndex, realmGet$status, false);
            }

            RealmList<com.argear.sdksample.model.ItemModel> itemsList = ((CategoryModelRealmProxyInterface) object).realmGet$items();
            if (itemsList != null) {
                OsList itemsOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.itemsIndex);
                for (com.argear.sdksample.model.ItemModel itemsItem : itemsList) {
                    Long cacheItemIndexitems = cache.get(itemsItem);
                    if (cacheItemIndexitems == null) {
                        cacheItemIndexitems = ItemModelRealmProxy.insert(realm, itemsItem, cache);
                    }
                    itemsOsList.addRow(cacheItemIndexitems);
                }
            }
        }
    }

    public static long insertOrUpdate(Realm realm, com.argear.sdksample.model.CategoryModel object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.argear.sdksample.model.CategoryModel.class);
        long tableNativePtr = table.getNativePtr();
        CategoryModelColumnInfo columnInfo = (CategoryModelColumnInfo) realm.getSchema().getColumnInfo(com.argear.sdksample.model.CategoryModel.class);
        long pkColumnIndex = columnInfo.uuidIndex;
        String primaryKeyValue = ((CategoryModelRealmProxyInterface) object).realmGet$uuid();
        long rowIndex = Table.NO_MATCH;
        if (primaryKeyValue == null) {
            rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
        } else {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$title = ((CategoryModelRealmProxyInterface) object).realmGet$title();
        if (realmGet$title != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.titleIndex, rowIndex, realmGet$title, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.titleIndex, rowIndex, false);
        }
        Table.nativeSetBoolean(tableNativePtr, columnInfo.is_bundleIndex, rowIndex, ((CategoryModelRealmProxyInterface) object).realmGet$is_bundle(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.updated_atIndex, rowIndex, ((CategoryModelRealmProxyInterface) object).realmGet$updated_at(), false);
        String realmGet$description = ((CategoryModelRealmProxyInterface) object).realmGet$description();
        if (realmGet$description != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.descriptionIndex, rowIndex, realmGet$description, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.descriptionIndex, rowIndex, false);
        }
        String realmGet$status = ((CategoryModelRealmProxyInterface) object).realmGet$status();
        if (realmGet$status != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.statusIndex, rowIndex, realmGet$status, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.statusIndex, rowIndex, false);
        }

        OsList itemsOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.itemsIndex);
        RealmList<com.argear.sdksample.model.ItemModel> itemsList = ((CategoryModelRealmProxyInterface) object).realmGet$items();
        if (itemsList != null && itemsList.size() == itemsOsList.size()) {
            // For lists of equal lengths, we need to set each element directly as clearing the receiver list can be wrong if the input and target list are the same.
            int objects = itemsList.size();
            for (int i = 0; i < objects; i++) {
                com.argear.sdksample.model.ItemModel itemsItem = itemsList.get(i);
                Long cacheItemIndexitems = cache.get(itemsItem);
                if (cacheItemIndexitems == null) {
                    cacheItemIndexitems = ItemModelRealmProxy.insertOrUpdate(realm, itemsItem, cache);
                }
                itemsOsList.setRow(i, cacheItemIndexitems);
            }
        } else {
            itemsOsList.removeAll();
            if (itemsList != null) {
                for (com.argear.sdksample.model.ItemModel itemsItem : itemsList) {
                    Long cacheItemIndexitems = cache.get(itemsItem);
                    if (cacheItemIndexitems == null) {
                        cacheItemIndexitems = ItemModelRealmProxy.insertOrUpdate(realm, itemsItem, cache);
                    }
                    itemsOsList.addRow(cacheItemIndexitems);
                }
            }
        }

        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.argear.sdksample.model.CategoryModel.class);
        long tableNativePtr = table.getNativePtr();
        CategoryModelColumnInfo columnInfo = (CategoryModelColumnInfo) realm.getSchema().getColumnInfo(com.argear.sdksample.model.CategoryModel.class);
        long pkColumnIndex = columnInfo.uuidIndex;
        com.argear.sdksample.model.CategoryModel object = null;
        while (objects.hasNext()) {
            object = (com.argear.sdksample.model.CategoryModel) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            String primaryKeyValue = ((CategoryModelRealmProxyInterface) object).realmGet$uuid();
            long rowIndex = Table.NO_MATCH;
            if (primaryKeyValue == null) {
                rowIndex = Table.nativeFindFirstNull(tableNativePtr, pkColumnIndex);
            } else {
                rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, primaryKeyValue);
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
            }
            cache.put(object, rowIndex);
            String realmGet$title = ((CategoryModelRealmProxyInterface) object).realmGet$title();
            if (realmGet$title != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.titleIndex, rowIndex, realmGet$title, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.titleIndex, rowIndex, false);
            }
            Table.nativeSetBoolean(tableNativePtr, columnInfo.is_bundleIndex, rowIndex, ((CategoryModelRealmProxyInterface) object).realmGet$is_bundle(), false);
            Table.nativeSetLong(tableNativePtr, columnInfo.updated_atIndex, rowIndex, ((CategoryModelRealmProxyInterface) object).realmGet$updated_at(), false);
            String realmGet$description = ((CategoryModelRealmProxyInterface) object).realmGet$description();
            if (realmGet$description != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.descriptionIndex, rowIndex, realmGet$description, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.descriptionIndex, rowIndex, false);
            }
            String realmGet$status = ((CategoryModelRealmProxyInterface) object).realmGet$status();
            if (realmGet$status != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.statusIndex, rowIndex, realmGet$status, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.statusIndex, rowIndex, false);
            }

            OsList itemsOsList = new OsList(table.getUncheckedRow(rowIndex), columnInfo.itemsIndex);
            RealmList<com.argear.sdksample.model.ItemModel> itemsList = ((CategoryModelRealmProxyInterface) object).realmGet$items();
            if (itemsList != null && itemsList.size() == itemsOsList.size()) {
                // For lists of equal lengths, we need to set each element directly as clearing the receiver list can be wrong if the input and target list are the same.
                int objectCount = itemsList.size();
                for (int i = 0; i < objectCount; i++) {
                    com.argear.sdksample.model.ItemModel itemsItem = itemsList.get(i);
                    Long cacheItemIndexitems = cache.get(itemsItem);
                    if (cacheItemIndexitems == null) {
                        cacheItemIndexitems = ItemModelRealmProxy.insertOrUpdate(realm, itemsItem, cache);
                    }
                    itemsOsList.setRow(i, cacheItemIndexitems);
                }
            } else {
                itemsOsList.removeAll();
                if (itemsList != null) {
                    for (com.argear.sdksample.model.ItemModel itemsItem : itemsList) {
                        Long cacheItemIndexitems = cache.get(itemsItem);
                        if (cacheItemIndexitems == null) {
                            cacheItemIndexitems = ItemModelRealmProxy.insertOrUpdate(realm, itemsItem, cache);
                        }
                        itemsOsList.addRow(cacheItemIndexitems);
                    }
                }
            }

        }
    }

    public static com.argear.sdksample.model.CategoryModel createDetachedCopy(com.argear.sdksample.model.CategoryModel realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.argear.sdksample.model.CategoryModel unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new com.argear.sdksample.model.CategoryModel();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.argear.sdksample.model.CategoryModel) cachedObject.object;
            }
            unmanagedObject = (com.argear.sdksample.model.CategoryModel) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        CategoryModelRealmProxyInterface unmanagedCopy = (CategoryModelRealmProxyInterface) unmanagedObject;
        CategoryModelRealmProxyInterface realmSource = (CategoryModelRealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$uuid(realmSource.realmGet$uuid());
        unmanagedCopy.realmSet$title(realmSource.realmGet$title());
        unmanagedCopy.realmSet$is_bundle(realmSource.realmGet$is_bundle());
        unmanagedCopy.realmSet$updated_at(realmSource.realmGet$updated_at());
        unmanagedCopy.realmSet$description(realmSource.realmGet$description());
        unmanagedCopy.realmSet$status(realmSource.realmGet$status());

        // Deep copy of items
        if (currentDepth == maxDepth) {
            unmanagedCopy.realmSet$items(null);
        } else {
            RealmList<com.argear.sdksample.model.ItemModel> manageditemsList = realmSource.realmGet$items();
            RealmList<com.argear.sdksample.model.ItemModel> unmanageditemsList = new RealmList<com.argear.sdksample.model.ItemModel>();
            unmanagedCopy.realmSet$items(unmanageditemsList);
            int nextDepth = currentDepth + 1;
            int size = manageditemsList.size();
            for (int i = 0; i < size; i++) {
                com.argear.sdksample.model.ItemModel item = ItemModelRealmProxy.createDetachedCopy(manageditemsList.get(i), nextDepth, maxDepth, cache);
                unmanageditemsList.add(item);
            }
        }

        return unmanagedObject;
    }

    static com.argear.sdksample.model.CategoryModel update(Realm realm, com.argear.sdksample.model.CategoryModel realmObject, com.argear.sdksample.model.CategoryModel newObject, Map<RealmModel, RealmObjectProxy> cache) {
        CategoryModelRealmProxyInterface realmObjectTarget = (CategoryModelRealmProxyInterface) realmObject;
        CategoryModelRealmProxyInterface realmObjectSource = (CategoryModelRealmProxyInterface) newObject;
        realmObjectTarget.realmSet$title(realmObjectSource.realmGet$title());
        realmObjectTarget.realmSet$is_bundle(realmObjectSource.realmGet$is_bundle());
        realmObjectTarget.realmSet$updated_at(realmObjectSource.realmGet$updated_at());
        realmObjectTarget.realmSet$description(realmObjectSource.realmGet$description());
        realmObjectTarget.realmSet$status(realmObjectSource.realmGet$status());
        RealmList<com.argear.sdksample.model.ItemModel> itemsList = realmObjectSource.realmGet$items();
        RealmList<com.argear.sdksample.model.ItemModel> itemsRealmList = realmObjectTarget.realmGet$items();
        if (itemsList != null && itemsList.size() == itemsRealmList.size()) {
            // For lists of equal lengths, we need to set each element directly as clearing the receiver list can be wrong if the input and target list are the same.
            int objects = itemsList.size();
            for (int i = 0; i < objects; i++) {
                com.argear.sdksample.model.ItemModel itemsItem = itemsList.get(i);
                com.argear.sdksample.model.ItemModel cacheitems = (com.argear.sdksample.model.ItemModel) cache.get(itemsItem);
                if (cacheitems != null) {
                    itemsRealmList.set(i, cacheitems);
                } else {
                    itemsRealmList.set(i, ItemModelRealmProxy.copyOrUpdate(realm, itemsItem, true, cache));
                }
            }
        } else {
            itemsRealmList.clear();
            if (itemsList != null) {
                for (int i = 0; i < itemsList.size(); i++) {
                    com.argear.sdksample.model.ItemModel itemsItem = itemsList.get(i);
                    com.argear.sdksample.model.ItemModel cacheitems = (com.argear.sdksample.model.ItemModel) cache.get(itemsItem);
                    if (cacheitems != null) {
                        itemsRealmList.add(cacheitems);
                    } else {
                        itemsRealmList.add(ItemModelRealmProxy.copyOrUpdate(realm, itemsItem, true, cache));
                    }
                }
            }
        }
        return realmObject;
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoryModelRealmProxy aCategoryModel = (CategoryModelRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aCategoryModel.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aCategoryModel.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aCategoryModel.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }
}
