package io.realm;


public interface ItemModelRealmProxyInterface {
    public String realmGet$uuid();
    public void realmSet$uuid(String value);
    public String realmGet$title();
    public void realmSet$title(String value);
    public String realmGet$description();
    public void realmSet$description(String value);
    public String realmGet$thumbnail();
    public void realmSet$thumbnail(String value);
    public String realmGet$zip_file();
    public void realmSet$zip_file(String value);
    public int realmGet$num_stickers();
    public void realmSet$num_stickers(int value);
    public int realmGet$num_effects();
    public void realmSet$num_effects(int value);
    public int realmGet$num_bgms();
    public void realmSet$num_bgms(int value);
    public int realmGet$num_filters();
    public void realmSet$num_filters(int value);
    public int realmGet$num_masks();
    public void realmSet$num_masks(int value);
    public boolean realmGet$has_trigger();
    public void realmSet$has_trigger(boolean value);
    public String realmGet$status();
    public void realmSet$status(String value);
    public long realmGet$updated_at();
    public void realmSet$updated_at(long value);
    public String realmGet$downloadStatus();
    public void realmSet$downloadStatus(String value);
}
