package com.argear.sdkplugin;


public class AppConfig {

    public static final String API_URL = "https://api.argear.io";

    // KT API
    public static final String API_KEY = "b4b465490fa0ff640e44d735";
    public static final String SECRET_KEY = "a87a56620043b41bbbc092d5ebe4818e0d8e6a0ae8f1b50ad240e5f51befb528";

    public static final String SAVE_PICTURE_PATH = "/mnt/sdcard/SDK_Sample_Pic.jpg";
    public static final String RECORDING_FILE_NAMW = "argear-sdk-recording.mp4";

    // beauty type ID
    // param : [0]vLineParam, [1]faceSlimPram, [2]jawPram, [3]chinParam, [4]eyeParam,
    //         [5]eyeGapParam, [6]noseLineParam, [7]noseSideParam, [8]noseLengthParam, [9]mouthParam
    public static final int BEAUTY_TYPE_VLINE = 0;
    public static final int BEAUTY_TYPE_FACE_SLIM = 1;
    public static final int BEAUTY_TYPE_JAW = 2;
    public static final int BEAUTY_TYPE_CHIN = 3;
    public static final int BEAUTY_TYPE_EYE = 4;
    public static final int BEAUTY_TYPE_EYE_GAP = 5;
    public static final int BEAUTY_TYPE_NOSE_LINE = 6;
    public static final int BEAUTY_TYPE_NOSE_SIDE = 7;
    public static final int BEAUTY_TYPE_NOSE_LENGTH = 8;
    public static final int BEAUTY_TYPE_MOUTH_SIZE = 9;
    public static final int BEAUTY_TYPE_EYE_BACK = 10;
    public static final int BEAUTY_TYPE_EYE_CORNER = 11;
    public static final int BEAUTY_TYPE_LIP_SIZE = 12;
    public static final int BEAUTY_TYPE_NUM = 13;         // BEAUTY_TYPE 총 갯수

    public static final boolean BULGE_MODE_BEAUTY = true;    // 벌지 타입 : Beauty
    public static final boolean BULGE_MODE_FUN = false;      // 벌지 타입 : Fun

    // debug landmark
    public static final int FACE_RECT_HW    = (1 << 0);  // HW Rect 그리기
    public static final int FACE_RECT_SW    = (1 << 1);  // 결과 Rect 그리기
    public static final int DLIB_LANDMARK   = (1 << 2);  // 68 pt 표시

}
