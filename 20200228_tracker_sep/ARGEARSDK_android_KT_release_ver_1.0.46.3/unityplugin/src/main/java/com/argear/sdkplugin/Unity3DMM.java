package com.argear.sdkplugin;

import android.app.Activity;

import com.seerslab.argearsdk.ARGearSDK;
import com.seerslab.eos.Seerslab3DMM;

import java.io.File;

public class Unity3DMM {

    private static final String TAG = Unity3DMM.class.getName();

    private Activity mActivity;

    private String InputImagePath;
    private String OutputPath;
    private static boolean mEnableDebug = false;

    public Unity3DMM(Activity activity, boolean enableDebug) {
        mActivity = activity;
        mEnableDebug = enableDebug;
        //Log(TAG, "UnityAndroid3DMM");

        Seerslab3DMM.initialize(mActivity);
    }

    public void SetDebugLog(Boolean bOn)
    {
        mEnableDebug = bOn;
    }

    public String GetVersion()
    {
        return Seerslab3DMM.GetVersion();
    }

    static void Log(String tag, String message)
    {
        if (mEnableDebug) {
            android.util.Log.d(tag, message);
        }
    }

    static void LogE(String tag, String message)
    {
        android.util.Log.e(tag, message);
    }

    public void SetPath(String ImagePath, String OutPath)
    {

        Log(TAG, "SetPath: Input Image Path: "+ImagePath+", OutputPath"+OutPath);

        File file = new File(ImagePath);
        if(file.exists()){
            Log(TAG, "Input Image is exist : " + InputImagePath);

            OutputPath = OutPath;
            String cropImgPath = OutPath + "_crop.jpg";

            if(ARGearSDK.cropFaceImg(ImagePath, cropImgPath)){
                InputImagePath = cropImgPath;
            }else {
                InputImagePath = ImagePath;
            }

            Seerslab3DMM.SetPath(InputImagePath, OutputPath);
        }
        else{
            LogE(TAG, "Input Image is not exist : " + InputImagePath);
        }

    }

    public boolean Run3DMM()
    {
        //Log(TAG, "Run3DMM");
        float[] landmark = ARGearSDK.getFaceLandmarkFromImage(InputImagePath);
        float[] faceinfo = ARGearSDK.getFaceInfoFromImage(InputImagePath);
        return Seerslab3DMM.Run3DMM(landmark, faceinfo);
    }


    /**
     *  Face 정보 입력
     * @param info
     *          0 : 여자
     *          1 : 남자
     */
    public void setFaceInfo(int info){

        LogE(TAG, "FaceInfo : " + info);
        Seerslab3DMM.setFaceInfo(info);

    }

    public static int getSkinType(){
        return Seerslab3DMM.getSkinType();
    }

    public static int getEyebrowType(){
        return Seerslab3DMM.getEyebrowType();
    }

    public static int getLipType(){
        return Seerslab3DMM.getLipType();
    }

    public static int[] getSkinColor(){
        int color[] = Seerslab3DMM.getSkinColor();
        LogE(TAG, "getSkinColor : (R:" + color[0]+", G:" + color[1]+", B:" + color[2]+")");
        return color;
    }

    public static int[] getEyebrowColor(){
        int color[] = Seerslab3DMM.getEyebrowColor();
        LogE(TAG, "getEyebrowColor : (R:" + color[0]+", G:" + color[1]+", B:" + color[2]+")");
        return color;
    }

    public static int[] getLipColor(){
        int color[] = Seerslab3DMM.getLipColor();
        LogE(TAG, "getLipColor : (R:" + color[0]+", G:" + color[1]+", B:" + color[2]+")");
        return color;
    }

    public static void stopBGMPlayer(){
        ARGearSDK.stopBGMPlayer();
    }

    public static float[] getScale(){
        float scale[] = Seerslab3DMM.getScale();
        LogE(TAG, "getScale : (X:" + scale[0]+", Y:" + scale[1]+", Z:" + scale[2]+")");
        return scale;
    }

}