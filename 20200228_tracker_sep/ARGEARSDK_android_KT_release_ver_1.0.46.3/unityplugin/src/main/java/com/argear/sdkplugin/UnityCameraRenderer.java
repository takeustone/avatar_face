package com.argear.sdkplugin;

import android.graphics.Bitmap;
import android.opengl.GLES11Ext;
import android.opengl.GLES20;
import android.util.Log;

import com.seerslab.argearsdk.ARGearSDK;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

import static android.opengl.GLES20.GL_FRAMEBUFFER_BINDING;

public class UnityCameraRenderer {

    private static final String TAG = UnityCameraRenderer.class.getSimpleName();

    private int outputTextures[] = new int[2];

    private FloatBuffer pVertex;
    private FloatBuffer pTexCoord;
    private int hProgram;

    private int mDisplayWidth;
    private int mDisplayHeight;
    private int mPreviewWidth;
    private int mPreviewHeight;
    private int mViewportX;
    private int mViewportY;
    private int mViewportWidth;
    private int mViewportHeight;

    private boolean USE_RENDERED_TEXTURE = true;

    private boolean CAMERA_RENDER_FLAG = true;

    private final String vss =
            "attribute vec2 vPosition;\n" +
                    "attribute vec2 vTexCoord;\n" +
                    "varying vec2 texCoord;\n" +
                    "void main() {\n" +
                    "  texCoord = vTexCoord;\n" +
                    "  gl_Position = vec4 ( vPosition.x, vPosition.y, 0.0, 1.0 );\n" +
                    "}";

    private final String fss =
            "precision mediump float;\n" +
                    "uniform sampler2D sTexture;\n" +
                    "varying vec2 texCoord;\n" +
                    "void main() {\n" +
                    "  gl_FragColor = texture2D(sTexture,texCoord);\n" +
                    "}";

    private final String fss2 =
            "precision mediump float;\n" +
                    "uniform samplerExternalOES sTexture;\n" +
                    "varying vec2 texCoord;\n" +
                    "void main() {\n" +
                    "  gl_FragColor = texture2D(sTexture,texCoord);\n" +
                    "}";

    public UnityCameraRenderer(int displayWidth, int displayHeight, int previewWidth, int previewHeight) {

        mDisplayWidth = displayWidth;
        mDisplayHeight = displayHeight;
        mPreviewWidth = previewWidth;
        mPreviewHeight = previewHeight;

        ARGearSDK.onSurfaceCreated(null, null);
        ARGearSDK.onSurfaceChanged(null, mDisplayWidth, mDisplayHeight);

        if(USE_RENDERED_TEXTURE)
            initGL();
        else
            initGL2();

        initViewportSize();
    }

    public UnityCameraRenderer(int displayWidth, int displayHeight, int previewWidth, int previewHeight, boolean cameraRender) {

        mDisplayWidth = displayWidth;
        mDisplayHeight = displayHeight;
        mPreviewWidth = previewWidth;
        mPreviewHeight = previewHeight;

        CAMERA_RENDER_FLAG = cameraRender;
        ARGearSDK.UseRender(CAMERA_RENDER_FLAG);
        ARGearSDK.onSurfaceCreated(null, null);
        ARGearSDK.onSurfaceChanged(null, mDisplayWidth, mDisplayHeight);

        if(USE_RENDERED_TEXTURE)
            initGL();
        else {
            initGL2();
            ARGearSDK.UseRender(false);
        }

        initViewportSize();
    }

    private void initViewportSize() {
        mViewportHeight = mDisplayHeight;
        mViewportWidth = (int) ((float) mViewportHeight * mPreviewWidth / mPreviewHeight );

        if (mViewportWidth < mDisplayWidth) {
            mViewportWidth = mDisplayWidth;
            mViewportHeight = (int) ((float) mViewportWidth * mPreviewHeight / mPreviewWidth );
        }

        if (mViewportWidth > mDisplayWidth) {
            mViewportX = (mDisplayWidth - mViewportWidth) / 2;
            mViewportY = 0;
        } else if (mViewportHeight > mDisplayHeight) {
            mViewportX = 0;
            mViewportY = (mDisplayHeight - mViewportHeight) / 2;
        } else {
            mViewportX = 0;
            mViewportY = 0;
        }
    }

    int[] default_fbo = new int[1];
    public void onDrawFrame(GL10 gl) {

        GLES20.glGetIntegerv(GL_FRAMEBUFFER_BINDING, default_fbo, 0); // System frame buffer의 id 획득

        ARGearSDK.onDrawFrame(gl, outputTextures);


        if(CAMERA_RENDER_FLAG){
            if(USE_RENDERED_TEXTURE)
                drawCameraTexture(outputTextures[0]);
            else
                drawCameraTexture2(ARGearSDK.getCameraTexID());
        }
        
		GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, default_fbo[0]);
    }

    private void initGL() {

        float[] vtmp = { 1.0f, -1.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f };
        //float[] ttmp = { 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f };
        //float[] ttmp = { 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f };
        float[] ttmp = { 0.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f };

        pVertex = ByteBuffer.allocateDirect(8*4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        pVertex.put ( vtmp );
        pVertex.position(0);
        pTexCoord = ByteBuffer.allocateDirect(8*4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        pTexCoord.put ( ttmp );
        pTexCoord.position(0);

        hProgram = loadShader ( vss, fss );
    }

    private void drawCameraTexture(int taxID) {
        if(ARGearSDK.DEBUGMODE) {
            Log.d(TAG, " --- out_textureID = " + taxID);
            Log.d(TAG, " --- viewWidth = " + mViewportWidth + ", " + mViewportHeight);
        }

        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, default_fbo[0]);

        GLES20.glViewport(mViewportX, mViewportY, mViewportWidth, mViewportHeight);
        GLES20.glUseProgram(hProgram);

        int ph = GLES20.glGetAttribLocation(hProgram, "vPosition");
        int tch = GLES20.glGetAttribLocation ( hProgram, "vTexCoord" );
        int th = GLES20.glGetUniformLocation ( hProgram, "sTexture" );

        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, taxID);
        GLES20.glUniform1i(th, 0);

        GLES20.glVertexAttribPointer(ph, 2, GLES20.GL_FLOAT, false, 4*2, pVertex);
        GLES20.glVertexAttribPointer(tch, 2, GLES20.GL_FLOAT, false, 4*2, pTexCoord );
        GLES20.glEnableVertexAttribArray(ph);
        GLES20.glEnableVertexAttribArray(tch);

        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
        GLES20.glFlush();
    }

    private  int loadShader ( String vss, String fss ) {
        int vshader = GLES20.glCreateShader(GLES20.GL_VERTEX_SHADER);
        GLES20.glShaderSource(vshader, vss);
        GLES20.glCompileShader(vshader);
        int[] compiled = new int[1];
        GLES20.glGetShaderiv(vshader, GLES20.GL_COMPILE_STATUS, compiled, 0);
        if (compiled[0] == 0) {
            if(ARGearSDK.DEBUGMODE) Log.e("Shader", "Could not compile vshader");
            if(ARGearSDK.DEBUGMODE) Log.v("Shader", "Could not compile vshader:"+GLES20.glGetShaderInfoLog(vshader));
            GLES20.glDeleteShader(vshader);
            vshader = 0;
        }

        int fshader = GLES20.glCreateShader(GLES20.GL_FRAGMENT_SHADER);
        GLES20.glShaderSource(fshader, fss);
        GLES20.glCompileShader(fshader);
        GLES20.glGetShaderiv(fshader, GLES20.GL_COMPILE_STATUS, compiled, 0);
        if (compiled[0] == 0) {
            if(ARGearSDK.DEBUGMODE) Log.e("Shader", "Could not compile fshader");
            if(ARGearSDK.DEBUGMODE) Log.v("Shader", "Could not compile fshader:"+GLES20.glGetShaderInfoLog(fshader));
            GLES20.glDeleteShader(fshader);
            fshader = 0;
        }

        int program = GLES20.glCreateProgram();
        GLES20.glAttachShader(program, vshader);
        GLES20.glAttachShader(program, fshader);
        GLES20.glLinkProgram(program);

        return program;
    }


    private void initGL2() {

        float[] vtmp = { 1.0f, -1.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f };
        float[] ttmp = { 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f };
//        float[] ttmp = { 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f };

        pVertex = ByteBuffer.allocateDirect(8*4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        pVertex.put ( vtmp );
        pVertex.position(0);
        pTexCoord = ByteBuffer.allocateDirect(8*4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        pTexCoord.put ( ttmp );
        pTexCoord.position(0);

        hProgram = loadShader ( vss, fss2 );
    }

    private void drawCameraTexture2(int taxID) {
        if(ARGearSDK.DEBUGMODE) {
            Log.d(TAG, " --- out_textureID = " + taxID);
            Log.d(TAG, " --- viewWidth = " + mViewportWidth + ", " + mViewportHeight);
        }

        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, default_fbo[0]);

        GLES20.glViewport(mViewportX, mViewportY, mViewportWidth, mViewportHeight);
        GLES20.glUseProgram(hProgram);

        int ph = GLES20.glGetAttribLocation(hProgram, "vPosition");
        int tch = GLES20.glGetAttribLocation ( hProgram, "vTexCoord" );
        int th = GLES20.glGetUniformLocation ( hProgram, "sTexture" );

        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, taxID);
        GLES20.glUniform1i(th, 0);

        GLES20.glVertexAttribPointer(ph, 2, GLES20.GL_FLOAT, false, 4*2, pVertex);
        GLES20.glVertexAttribPointer(tch, 2, GLES20.GL_FLOAT, false, 4*2, pTexCoord );
        GLES20.glEnableVertexAttribArray(ph);
        GLES20.glEnableVertexAttribArray(tch);

        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
        GLES20.glFlush();
    }

    public void setCameraRenderFlag(boolean flag){
        CAMERA_RENDER_FLAG = flag;
    }

    public int getOutFboID(){
        return ARGearSDK.renderingFboID();
    }

    public int getOutTexID(){
        return ARGearSDK.renderingTexID();
    }

    // Framebuffer에서 RawData 가져오는 방법 예시 함수
    public void getRawData() {

        int width = ARGearSDK.CameraPreviewSize[0];
        int height = ARGearSDK.CameraPreviewSize[1];
        // 1. Framebuffer의 RawData 가져옴
        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, getOutFboID());

        ByteBuffer buffer = ByteBuffer.allocateDirect(width * height * 4);
        GLES20.glReadPixels(0, 0, width, height, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, buffer);


        // 2. RawData를 이용하여 jpg파일로 저장 (확인용)
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        bitmap.copyPixelsFromBuffer(buffer);

        Bitmap resize = Bitmap.createScaledBitmap(bitmap, height, width, false);

        File fileCacheItem = new File("/mnt/sdcard/result.jpg");
        OutputStream out = null;

        try {
            fileCacheItem.createNewFile();
            out = new FileOutputStream(fileCacheItem);
        } catch (IOException e) {
            e.printStackTrace();
        }
        resize.compress(Bitmap.CompressFormat.JPEG, 100, out);

        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, default_fbo[0]);
    }

}
