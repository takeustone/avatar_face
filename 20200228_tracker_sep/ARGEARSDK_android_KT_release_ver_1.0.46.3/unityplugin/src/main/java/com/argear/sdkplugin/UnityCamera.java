package com.argear.sdkplugin;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;


import com.seerslab.argearsdk.ARGearSDK;

public class UnityCamera {

    private static final String TAG = UnityCamera.class.getName();

    private Activity mActivity;
    private ReferenceCamera mCamera;
    private UnityCameraRenderer mRenderer;

    private int mDisplayWidth;
    private int mDisplayHeight;

    private int mFaceTypeIndex = 0;

    private static boolean mEnableDebug = false;
    TrakerThread          _trakerThread = null;
    private int nFPS    = 30;

    public UnityCamera(Activity activity) {
        mActivity = activity;

        ARGearSDK.createEngine(activity, AppConfig.API_KEY, AppConfig.SECRET_KEY);
        initCamera();
    }

    public void SetDebugLog(Boolean bOn)
    {
        mEnableDebug = bOn;
    }

    public String GetVersion()
    {
        return ARGearSDK.getVersion();
    }

    private void initCamera() {

        Point realSize = new Point();
        Display display = ((WindowManager) mActivity.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        display.getRealSize(realSize);
        mDisplayWidth = realSize.x;
        mDisplayHeight = realSize.y;

        mCamera = new ReferenceCamera1(mActivity, mActivity.getWindowManager().getDefaultDisplay().getRotation());

        if (mCamera.getCameraFacingFrontValue() != -1) {
            mCamera.setFacing(mCamera.getCameraFacingFrontValue());
        } else if (mCamera.getCameraFacingBackValue() != -1) {
            mCamera.setFacing(mCamera.getCameraFacingBackValue());
        } else {
            return;
        }
        mCamera.initCameraPreview();

        ARGearSDK.isCameraFacingFront = mCamera.isCameraFacingFront();
        ARGearSDK.CameraPreviewSize = mCamera.getPreviewSize();
    }

    public void drawCameraFrame(int width, int height) {
        if (mRenderer == null) {
            int [] previewSize = mCamera.getPreviewSize();
            mRenderer = new UnityCameraRenderer(mDisplayWidth, mDisplayHeight, previewSize[1], previewSize[0]);
            ARGearSDK.resumeEngine();
            mCamera.startCamera();
        }
        mRenderer.onDrawFrame(null);
    }

    public void drawCameraFrame(int width, int height, boolean cameraRender) {
        if (mRenderer == null) {
            int [] previewSize = mCamera.getPreviewSize();
            mRenderer = new UnityCameraRenderer(mDisplayWidth, mDisplayHeight, previewSize[1], previewSize[0], cameraRender);
            ARGearSDK.resumeEngine();
            mCamera.startCamera();
        }
        mRenderer.setCameraRenderFlag(cameraRender);
        mRenderer.onDrawFrame(null);

    }

    public void onDestroy() {
        if(mCamera != null){
            mCamera.stopCamera();
            mCamera.destroy();
            mCamera = null;
        }

        ARGearSDK.pauseEngine();
        ARGearSDK.destoryEngine();

        if (mRenderer != null) {
            mRenderer = null;
        }
    }

    /**
     * 현재 프레임의 Face Tracking을 진행함
     * startFaceTrack() 완료해야 현재 프레임의 Landmark를 가져올수 있다.
     */
    public void startFaceTrack() {
        ARGearSDK.TrackFace();
    }


    public void FaceTrackThreadStart() {
        if(_trakerThread == null)
            _trakerThread = new TrakerThread();
        _trakerThread.setSleepTime(nFPS);
        _trakerThread.start();
    }

    public void FaceTrackThreadStop() {
        if (_trakerThread != null) {
            _trakerThread.setStop();
            try {
                _trakerThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            _trakerThread = null;
        }
    }

    public void setDrawLandmark(boolean flag)
    {
        int LandmarkInfoFlag = (1 << 2);

        if (flag)
            ARGearSDK.setDrawLandmarkInfo(LandmarkInfoFlag);
        else
            ARGearSDK.setDrawLandmarkInfo(0);
    }

    public float[] getLandmark68(int idx){
        return ARGearSDK.getLandmark68(idx);
    }

    public float[] get3DLandmark68(int idx){
        return ARGearSDK.get3DLandmark68(idx);
    }

    public float[] getAxesProjected(int idx){
        return ARGearSDK.getAxesProjected(idx);
    }

    public float[] getRectProjected(int idx){
        return ARGearSDK.getRectProjected(idx);
    }


    public int detectedFaceNum(){
        return ARGearSDK.detectedFaceNum();
    }

    public int getCameraTexID(){
        return ARGearSDK.renderingTexID();
    }

    public int getCameraWidth() {return ARGearSDK.CameraPreviewSize[0];}
    public int getCameraHeight() {return ARGearSDK.CameraPreviewSize[1];}

    // ARGearSDK에서 렌더링 진행 후 최종 텍스쳐 ID ( 카메라 영상 + 필터 + 스티커 + 뷰티)
    public int getOutTexID(){
        if(mRenderer != null)
            return mRenderer.getOutTexID();
        else
            return -1;
    }

    /**
     *
     * @param idx : Detecting 된 Face Index
     * @return Angle 정보
     *          [0]roll , [1]yaw, [3]pitch
     */
    public float[] getAngle(int idx){
        return ARGearSDK.getAngle(idx);
    }

    /**
     *
     * @param idx : Detecting 된 Face Index
     * @return 3x3 Rotation Matrix
     *          |[0],[1],[2]|
     *          |[3],[4],[5]|
     *          |[6],[7],[8]|
     */
    public double[] getRotationMatrix(int idx){
        return ARGearSDK.getRotationMatrix(idx);
    }

    /**
     *
     * @param idx : Detecting 된 Face Index
     * @return vector[3]
     *     [0] : x
     *     [1] : y
     *     [2] : z
     */
    public double[] getTranslationVector(int idx){
        return ARGearSDK.getTranslationVector(idx);
    }

    public void savePicture(String mSavePath){
        ARGearSDK.takePicture(mSavePath);
    }

    public void changeFaceType() {

        int index = mFaceTypeIndex % 6;

        ARGearSDK.setSkinEffectLevel(10);

        switch (index) {
            case 0:
                ARGearSDK.setFaceEffectType(ARGearSDK.FACE_TYPE_NORMAL);
                break;
            case 1:
                ARGearSDK.setFaceEffectType(ARGearSDK.FACE_TYPE_OVAL);
                break;
            case 2:
                ARGearSDK.setFaceEffectType(ARGearSDK.FACE_TYPE_ROUND);
                break;
            case 3:
                ARGearSDK.setFaceEffectType(ARGearSDK.FACE_TYPE_SQUARE);
                break;
            case 4:
                ARGearSDK.setFaceEffectType(ARGearSDK.FACE_TYPE_HEART);
                break;
            case 5:
                ARGearSDK.setFaceEffectType(ARGearSDK.FACE_TYPE_DIAMOND);
                break;
        }

        mFaceTypeIndex++;

    }

    public void changeCameraFacing(){
        ARGearSDK.pauseEngine();
        mCamera.changeCameraFacing();

        ARGearSDK.isCameraFacingFront = mCamera.isCameraFacingFront();
        ARGearSDK.CameraPreviewSize = mCamera.getPreviewSize();
        if (!ARGearSDK.isResumed()) {
            ARGearSDK.resumeEngine();
        }
    }



    private class TrakerThread extends Thread {
        private boolean mRun = true;
        private long mSleepTime = 0;

        public void setSleepTime(int fps) {
            mSleepTime = 1000 / fps;
        }

        public void setStop() {
            mRun = false;
        }

        @Override
        public void run() {
            while (mRun) {
                try {
                    startFaceTrack();
                    Thread.sleep(mSleepTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }



}