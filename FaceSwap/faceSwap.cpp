#include <opencv2/opencv.hpp>
#include <iostream>
#include <fstream>
#include <string> 
#include <time.h>
#include <chrono>
#include <ctime>

using namespace cv;
using namespace std;

typedef unsigned char byte;

//Read points from text file
vector<Point2f> readPoints(string pointsFileName) {
	vector<Point2f> points;
	ifstream ifs(pointsFileName.c_str());
	float x, y;
	int count = 0;
	while (ifs >> x >> y)
	{
		points.push_back(Point2f(x, y));

	}

	return points;
}

//Read points from text file
vector< vector<int> > readStructure(string pointsFileName) {
	//vector<Point2f> points;

	vector< vector<int> > points;
	ifstream ifs(pointsFileName.c_str());
	float index, v1, v2, v3;
	int count = 0;
	while (ifs >> index >> v1 >> v2 >> v3)
	{
		vector<int> point_one;
		point_one.push_back(v1);
		point_one.push_back(v2);
		point_one.push_back(v3);
		points.push_back(point_one);

	}

	return points;
}

// Apply affine transform calculated using srcTri and dstTri to src
void applyAffineTransform(Mat &warpImage, Mat &src, vector<Point2f> &srcTri, vector<Point2f> &dstTri)
{
	// Given a pair of triangles, find the affine transform.
	Mat warpMat = getAffineTransform(srcTri, dstTri);

	// Apply the Affine Transform just found to the src image
	warpAffine(src, warpImage, warpMat, warpImage.size(), INTER_LINEAR, BORDER_REFLECT_101);
}


// Calculate Delaunay triangles for set of points
// Returns the vector of indices of 3 points for each triangle
static void calculateDelaunayTriangles(Rect rect, vector<Point2f> &points, vector< vector<int> > &delaunayTri) {

	// Create an instance of Subdiv2D
	Subdiv2D subdiv(rect);

	// Insert points into subdiv
	for (vector<Point2f>::iterator it = points.begin(); it != points.end(); it++)
		subdiv.insert(*it);

	vector<Vec6f> triangleList;
	subdiv.getTriangleList(triangleList);
	vector<Point2f> pt(3);
	vector<int> ind(3);

	for (size_t i = 0; i < triangleList.size(); i++)
	{
		Vec6f t = triangleList[i];
		pt[0] = Point2f(t[0], t[1]);
		pt[1] = Point2f(t[2], t[3]);
		pt[2] = Point2f(t[4], t[5]);

		if (rect.contains(pt[0]) && rect.contains(pt[1]) && rect.contains(pt[2])) {
			for (int j = 0; j < 3; j++)
				for (size_t k = 0; k < points.size(); k++)
					if (abs(pt[j].x - points[k].x) < 1.0 && abs(pt[j].y - points[k].y) < 1)
						ind[j] = k;

			delaunayTri.push_back(ind);
		}
	}

}


// Warps and alpha blends triangular regions from img1 and img2 to img
void warpTriangle(Mat &img1, Mat &img2, vector<Point2f> &t1, vector<Point2f> &t2)
{

	Rect r1 = boundingRect(t1);
	Rect r2 = boundingRect(t2);

	// Offset points by left top corner of the respective rectangles
	vector<Point2f> t1Rect, t2Rect;
	vector<Point> t2RectInt;
	for (int i = 0; i < 3; i++)
	{

		t1Rect.push_back(Point2f(t1[i].x - r1.x, t1[i].y - r1.y));
		t2Rect.push_back(Point2f(t2[i].x - r2.x, t2[i].y - r2.y));
		t2RectInt.push_back(Point(t2[i].x - r2.x, t2[i].y - r2.y)); // for fillConvexPoly

	}

	// Get mask by filling triangle
	Mat mask = Mat::zeros(r2.height, r2.width, CV_32FC3);
	fillConvexPoly(mask, t2RectInt, Scalar(1.0, 1.0, 1.0), 16, 0);

	// Apply warpImage to small rectangular patches
	Mat img1Rect;
	img1(r1).copyTo(img1Rect);

	Mat img2Rect = Mat::zeros(r2.height, r2.width, img1Rect.type());

	applyAffineTransform(img2Rect, img1Rect, t1Rect, t2Rect);

	multiply(img2Rect, mask, img2Rect);
	multiply(img2(r2), Scalar(1.0, 1.0, 1.0) - mask, img2(r2));
	img2(r2) = img2(r2) + img2Rect;


}

byte *matToBytes(Mat image)
{
	int size = image.rows*image.cols * 3;
	byte *bytes = new byte[size];
	std::memcpy(bytes, image.data, size * sizeof(byte));


	return bytes;
}

Mat bytesToMat(byte * bytes, int width, int height)
{
	//Mat image = Mat(height, width, CV_8UC3, bytes).clone(); // make a copy
	Mat image(height, width, CV_8UC3);
	image.data = bytes;
	return image;
}


int main(int argc, char** argv)
{
	clock_t start_main, end_main;
	double result;

	//start = clock();
	std::chrono::time_point<std::chrono::system_clock> start, start_convex, start_triangle, start_warp, start_cloning, end, end_convex, end_triangle, end_warp, end_cloning;


	//Read input images
	string filename1 = "target_wui.png"; // 800_face1.jpg
	string filename2 = "toImage.jpg"; //download.png"

	Mat img1 = imread(filename1);
	Mat img2 = imread(filename2);
	Mat img1Warped = img2.clone();

	//Read points
	vector<Point2f> points1, points2;
	points1 = readPoints(filename1 + ".txt");
	points2 = readPoints(filename2 + ".txt");

	//Mat landmark_image = img2.clone();
	//for (int i = 0; i < points2.size(); i++)
	//{
	//	cv::rectangle(landmark_image, cv::Point2f(points2[i].x - 2.0f, points2[i].y - 2.0f), cv::Point2f(points2[i].x + 2.0f, points2[i].y + 2.0f), { 0,0,255 });
	//		
	//}
	//imwrite("target_lee_landmark.png", landmark_image);


	//start2 = std::chrono::system_clock::now();
	start = std::chrono::system_clock::now();
	start_main = clock();

	//convert Mat to float data type
	img1.convertTo(img1, CV_32F);
	img1Warped.convertTo(img1Warped, CV_32F);


	// Find convex hull
	vector<Point2f> hull1;
	vector<Point2f> hull2;
	vector<int> hullIndex;

	/////convex hull
	start_convex = std::chrono::system_clock::now();
	//convexHull(points2, hullIndex, false, false);
	for (int i = 0; i < points2.size(); i++)   //jws
	{
		hullIndex.push_back(i);
	}
	end_convex = std::chrono::system_clock::now();


	for (int i = 0; i < hullIndex.size(); i++)
	{
		hull1.push_back(points1[hullIndex[i]]);
		hull2.push_back(points2[hullIndex[i]]);
	}


	// Find delaunay triangulation for points on the convex hull
	vector< vector<int> > dt;
	Rect rect(0, 0, img1Warped.cols, img1Warped.rows);

	dt = readStructure("dt_temp.txt");

	start_triangle = std::chrono::system_clock::now();
	//calculateDelaunayTriangles(rect, hull2, dt);
	end_triangle = std::chrono::system_clock::now();



	// Apply affine transformation to Delaunay triangles

	start_warp = std::chrono::system_clock::now();

	for (size_t i = 0; i < dt.size(); i++)
	{
		vector<Point2f> t1, t2;
		// Get points for img1, img2 corresponding to the triangles
		for (size_t j = 0; j < 3; j++)
		{
			t1.push_back(hull1[dt[i][j]]);
			t2.push_back(hull2[dt[i][j]]);
		}

		warpTriangle(img1, img1Warped, t1, t2);

	}

	//cv::line(img1Warped,);

	imwrite("warped_image.jpg", img1Warped);

	printf("size: %d\n", img1Warped.data[0]);
	
	byte *byteArrayFromMat = matToBytes(img1Warped);

	cv::Mat byteToImage = bytesToMat(byteArrayFromMat, img1Warped.rows, img1Warped.cols);

	imwrite("byteImage.jpg", byteToImage);


	//Bitmap bitmap = cv::OpenCvSharp.Extensions.BitmapConverter.ToBitmap(src);

	// Calculate mask
	vector<Point> hull8U;
	for (int i = 0; i < hull2.size(); i++)
	{
		Point pt(hull2[i].x, hull2[i].y);
		hull8U.push_back(pt);
	}

	Mat mask = Mat::zeros(img2.rows, img2.cols, img2.depth());
	fillConvexPoly(mask, &hull8U[0], hull8U.size(), Scalar(255, 255, 255));

	end_warp = std::chrono::system_clock::now();



	// Clone seamlessly.

	start_cloning = std::chrono::system_clock::now();

	Rect r = boundingRect(hull2);
	Point center = (r.tl() + r.br()) / 2;

	Mat output;
	img1Warped.convertTo(img1Warped, CV_8UC3);
	seamlessClone(img1Warped, img2, mask, center, output, NORMAL_CLONE);

	end_cloning = std::chrono::system_clock::now();

	//end = std::chrono::system_clock::now();
	//std::chrono::duration<double> elapsed_seconds2 = end2 - start2;
	//std::cout << "elapsed time: " << elapsed_seconds2.count() << "s\n\n";

	end_main = clock();
	result = (double)(end_main - start_main);
	cout << "result : " << ((result) / CLOCKS_PER_SEC) << " seconds" << endl;

	end = std::chrono::system_clock::now();

	//////time output
	std::chrono::duration<double> elapsed_seconds = end - start;
	std::cout << "elapsed time: " << elapsed_seconds.count() << "s\n\n";

	std::chrono::duration<double> elapsed_seconds_convex = end_convex - start_convex;
	std::cout << "convex hull time: " << elapsed_seconds_convex.count() << "s\n\n";
	std::cout << "convex hull time(ratio): " << elapsed_seconds_convex / elapsed_seconds * 100 << "%\n\n";


	std::chrono::duration<double> elapsed_seconds_triangle = end_triangle - start_triangle;
	std::cout << "triangle time: " << elapsed_seconds_triangle.count() << "s\n\n";
	std::cout << "triangle time(ratio): " << elapsed_seconds_triangle / elapsed_seconds * 100 << "%\n\n";

	std::chrono::duration<double> elapsed_seconds_warp = end_warp - start_warp;
	std::cout << "warp time: " << elapsed_seconds_warp.count() << "s\n\n";
	std::cout << "warp time(ratio): " << elapsed_seconds_warp / elapsed_seconds * 100 << "%\n\n";

	std::chrono::duration<double> elapsed_seconds_cloning = end_cloning - start_cloning;
	std::cout << "seamless cloning time: " << elapsed_seconds_cloning.count() << "s\n\n";
	std::cout << "seamless cloning time(ratio): " << elapsed_seconds_cloning / elapsed_seconds * 100 << "%\n\n";




	imwrite("Face Swapped.jpg", output);

	// imshow("Face Swapped", output);
	//  waitKey(0);
	//   destroyAllWindows();



	//end = clock();
	//result = (double)(end - start);
	//printf("%f\n", result/ CLOCKS_PER_SEC);

	return 1;
}
