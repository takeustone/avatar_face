#pragma warning(disable:4996)
#define _USE_MATH_DEFINES

#include <iostream>
#include <vector>
#include <Eigen/Dense>
#include <Eigen/Geometry>

#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>

typedef OpenMesh::PolyMesh_ArrayKernelT<>  MyMesh;

void readMesh(std::string filename, MyMesh &mesh)
{
	OpenMesh::IO::Options opt;
	opt += OpenMesh::IO::Options::VertexNormal;
	opt += OpenMesh::IO::Options::FaceNormal;
	//opt += OpenMesh::IO::Options::VertexTexCoord;

	mesh.request_vertex_normals();
	mesh.request_face_normals();
	//mesh.request_vertex_texcoords2D();

	std::cout << "Loading from file '" << filename << "'\n";
	if (OpenMesh::IO::read_mesh(mesh, filename, opt))
	{
		// update face and vertex normals     
		if (!opt.check(OpenMesh::IO::Options::FaceNormal))
			mesh.update_face_normals();
		else
			std::cout << "File provides face normals\n";

		if (!opt.check(OpenMesh::IO::Options::VertexNormal))
			mesh.update_vertex_normals();
		else
			std::cout << "File provides vertex normals\n";
	}
	else
	{
		std::cout << "Failed to load file.";
		exit(1);
	}

	// mesh info
	std::clog << "mesh: " << mesh.n_vertices() << " vertices, "
		<< mesh.n_edges() << " edge, "
		<< mesh.n_faces() << " faces\n";
}

int main(int argc, char* argv[])
{
///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
/////////////////neighborhood check test//////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////

	//MyMesh neighbor_check_mesh;
	//readMesh("Beauty_mean_191028.obj", neighbor_check_mesh);

	//std::vector<std::vector<int>> neighborIndices;
	//neighborIndices.resize(neighbor_check_mesh.n_vertices());

	//MyMesh::VertexIter vIt;
	//MyMesh::VertexVertexIter vvIt;

	//for (vIt = neighbor_check_mesh.vertices_begin(); vIt != neighbor_check_mesh.vertices_end(); ++vIt)
	//{
	//	for (vvIt = neighbor_check_mesh.vv_iter(vIt.handle()); vvIt; ++vvIt)
	//		neighborIndices[vIt->idx()].push_back(vvIt->idx());
	//}

	/////save neighbor vertices
	//FILE *fp_neighbor_vertices = fopen("neighbor_vetices.txt", "w"); // vertex index / num_neighbor / neighbor vertices
	//for (int i = 0; i < neighborIndices.size(); i++)
	//{
	//	fprintf(fp_neighbor_vertices, "%d %d ", i, neighborIndices[i].size());

	//	for (int j = 0; j < neighborIndices[i].size(); j++)
	//	{
	//		fprintf(fp_neighbor_vertices, "%d ", neighborIndices[i][j]);
	//	}
	//	fprintf(fp_neighbor_vertices, "\n");
	//}

	//fclose(fp_neighbor_vertices);

	//return 0;
	
///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////


	std::string basic_mesh_file = argv[1];
	std::string target_mesh_file = argv[2];
	std::string part_info_file = argv[3];

	std::vector<Eigen::Vector3f> basic_mesh_vertices;
	std::vector<Eigen::Vector3f> target_mesh_vertices;

	std::vector<Eigen::Vector3f> vertex_texture;
	std::vector<Eigen::Vector3i> face_index;
	std::vector<Eigen::Vector3i> face_vt_index;

	float v_x, v_y, v_z;
	float vt_x, vt_y, vt_z;
	int f_1, f_2, f_3;
	int f_vt_1, f_vt_2, f_vt_3;

	char *pStr;
	char buffer[255];
	FILE *fp_basic_mesh = fopen(basic_mesh_file.c_str(), "r");

	while (!feof(fp_basic_mesh))
	{
		pStr = fgets(buffer, sizeof(buffer), fp_basic_mesh);

		if (pStr == NULL)
			break;

		if (pStr[0] == 'v' && pStr[1] == ' ') {
			sscanf(buffer, "v %f %f %f", &v_x, &v_y, &v_z);
			basic_mesh_vertices.push_back(Eigen::Vector3f(v_x, v_y, v_z));
		}

		if (pStr[0] == 'v' && pStr[1] == 't') {
			sscanf(buffer, "vt %f %f %f", &vt_x, &vt_y, &vt_z);
			vertex_texture.push_back(Eigen::Vector3f(vt_x, vt_y, vt_z));
		}

		if (pStr[0] == 'f' && pStr[1] == ' ') {
			sscanf(buffer, "f %d/%d %d/%d %d/%d", &f_1, &f_vt_1, &f_2, &f_vt_2, &f_3, &f_vt_3);
			face_index.push_back(Eigen::Vector3i(f_1, f_2, f_3));
			face_vt_index.push_back(Eigen::Vector3i(f_vt_1, f_vt_2, f_vt_3));
		}
	}

	fclose(fp_basic_mesh);

	FILE *fp_target_mesh = fopen(target_mesh_file.c_str(), "r");

	while (!feof(fp_target_mesh))
	{
		pStr = fgets(buffer, sizeof(buffer), fp_target_mesh);

		if (pStr == NULL)
			break;

		if (pStr[0] == 'v' && pStr[1] == ' ') {
			sscanf(buffer, "v %f %f %f", &v_x, &v_y, &v_z);
			target_mesh_vertices.push_back(Eigen::Vector3f(v_x, v_y, v_z));
		}
	}

 	fclose(fp_target_mesh);


	int selected_index;
	std::vector<int> part_index;
	FILE *fp_part_info = fopen(part_info_file.c_str(), "r");

	while (!feof(fp_part_info))
	{
		pStr = fgets(buffer, sizeof(buffer), fp_part_info);

		if (pStr == NULL)
			break;

		sscanf(buffer, "%d", &selected_index);
		part_index.push_back(selected_index);
	}

	fclose(fp_part_info);

///////read vertex neighbor information
	int vertex_index;
	int current_neighbor_num;
	int each_neighbor;
	std::vector<std::vector<int>> neighbor_vertexIndices;  //basic_mesh_vertices
	neighbor_vertexIndices.resize(basic_mesh_vertices.size());

	FILE *fp_vertex_neighbor = fopen("neighbor_vetices.txt","r");  // vertex index / num_neighbor / neighbor vertices
	for (int i = 0; i < basic_mesh_vertices.size(); i++)
	{
		fscanf(fp_vertex_neighbor, "%d", &vertex_index);
		fscanf(fp_vertex_neighbor, "%d", &current_neighbor_num);

		for (int j = 0; j < current_neighbor_num; j++)
		{
			fscanf(fp_vertex_neighbor, "%d", &each_neighbor);
			neighbor_vertexIndices[i].push_back(each_neighbor);
		}
	}
	
	fclose(fp_vertex_neighbor);

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
	//FILE *fp_left_eye = fopen("eye_left_info.inf", "w");
	//FILE *fp_right_eye = fopen("eye_right_info.inf", "w");


	//for (int i = 0; i < part_index.size(); i++)
	//{
	//	if (basic_mesh_vertices[part_index[i]][0] < 0)
	//	{
	//		fprintf(fp_right_eye, "%d\n", part_index[i]);
	//	}
	//	else {
	//		fprintf(fp_left_eye, "%d\n", part_index[i]);
	//	}
	//}

	//fclose(fp_left_eye);
	//fclose(fp_right_eye);

	//return 0;
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
		
	if (argv[3][0] == 'e')
	{
		float basic_xr_pivot = -31.579998;
		float basic_yr_pivot = 20.705000;
		float basic_zr_pivot = -29.337502;

	////for scale calculation
		int r_eye_scale_feature[4] = {1171, 1155, 1048, 1165};

		float basic_left = basic_mesh_vertices[r_eye_scale_feature[0]][0]; //left
		float basic_right = basic_mesh_vertices[r_eye_scale_feature[1]][0]; //right
		float basic_bottom = basic_mesh_vertices[r_eye_scale_feature[2]][1]; //bottom
		float basic_top = basic_mesh_vertices[r_eye_scale_feature[3]][1]; //top

		float basic_row_distance = abs(basic_right - basic_left);
		float basic_col_distance = abs(basic_top - basic_bottom);

		float target_left = target_mesh_vertices[r_eye_scale_feature[0]][0]; // left
		float target_right = target_mesh_vertices[r_eye_scale_feature[1]][0]; //right
		float target_bottom = target_mesh_vertices[r_eye_scale_feature[2]][1]; //bottom
		float target_top = target_mesh_vertices[r_eye_scale_feature[3]][1]; //top

		float target_row_distance = abs(target_right - target_left);
		float target_col_distance = abs(target_top - target_bottom);

	//////pivot position
		int r_eye_feature[4] = { 1355, 1340, 439,1364 }; //L R B T
		int l_eye_feature[4] = { 2772, 2787, 1871,2795 };//L R B T		

		float xr_pivot = (target_mesh_vertices[r_eye_feature[0]][0] + target_mesh_vertices[r_eye_feature[1]][0]) /2;
		float yr_pivot = (target_mesh_vertices[r_eye_feature[2]][1] + target_mesh_vertices[r_eye_feature[3]][1]) / 2;
		float zr_pivot = (target_mesh_vertices[r_eye_feature[0]][2] + target_mesh_vertices[r_eye_feature[1]][2] + target_mesh_vertices[r_eye_feature[2]][2] + target_mesh_vertices[r_eye_feature[3]][2]) / 4;

	//////apply scale factor
		float x_scale = target_row_distance / basic_row_distance;
		float y_scale = target_col_distance / basic_col_distance;

		float scale_matrix[4][4];
		scale_matrix[0][0] = x_scale;	scale_matrix[0][1] = 0.0;		scale_matrix[0][2] = 0.0;		scale_matrix[0][3] = 0.0;
		scale_matrix[1][0] = 0.0;		scale_matrix[1][1] = y_scale;	scale_matrix[1][2] = 0.0;		scale_matrix[1][3] = 0.0;
		scale_matrix[2][0] = 0.0;		scale_matrix[2][1] = 0.0;		scale_matrix[2][2] = 1.0;		scale_matrix[2][3] = 0.0;
		scale_matrix[3][0] = 0.0;		scale_matrix[3][1] = 0.0;		scale_matrix[3][2] = 0.0;		scale_matrix[3][3] = 1.0;


		std::vector<Eigen::Vector3f> org_target_mesh_vertices;
		org_target_mesh_vertices = target_mesh_vertices;
				

		for (int i = 0; i < part_index.size(); i++)
		{
			float current_pos[4];
			current_pos[0] = 0.0; current_pos[1] = 0.0; current_pos[2] = 0.0; current_pos[3] = 1.0;

			float result_pos[4];
			result_pos[0] = 0.0; result_pos[1] = 0.0; result_pos[2] = 0.0; result_pos[3] = 0.0;

			current_pos[0] = basic_mesh_vertices[part_index[i]][0] - basic_xr_pivot;
			current_pos[1] = basic_mesh_vertices[part_index[i]][1] - basic_yr_pivot;
			current_pos[2] = basic_mesh_vertices[part_index[i]][2] - basic_zr_pivot;
			current_pos[3] = 1.0;

			//current_pos[0][0] = basic_mesh_vertices[part_index[i]][0];
			//current_pos[0][1] = basic_mesh_vertices[part_index[i]][1];
			//current_pos[0][2] = basic_mesh_vertices[part_index[i]][2];


			for (int row = 0; row < 4; row++)
			{
				//int col = 0;
				for (int inner = 0; inner < 4; inner++)
				{
					result_pos[row] += scale_matrix[row][inner] * current_pos[inner];
					
				}

			}

			result_pos[0] = result_pos[0] / result_pos[3];
			result_pos[1] = result_pos[1] / result_pos[3];
			result_pos[2] = result_pos[2] / result_pos[3];

			result_pos[0] = result_pos[0] + xr_pivot;
			result_pos[1] = result_pos[1] + yr_pivot;
			result_pos[2] = result_pos[2] + zr_pivot;

			target_mesh_vertices[part_index[i]][0] = result_pos[0];
			target_mesh_vertices[part_index[i]][1] = result_pos[1];
			target_mesh_vertices[part_index[i]][2] = result_pos[2];

			//product[row][col] += aMatrix[row][inner] * bMatrix[inner][col];
		}

		std::vector<Eigen::Vector3f> result_mesh_vertices;
		result_mesh_vertices = target_mesh_vertices;

		for (int i = 0; i < result_mesh_vertices.size(); i++)
		{
			float x_smooth = 0.0; float y_smooth = 0.0; float z_smooth = 0.0;
			for (int j = 0; j < neighbor_vertexIndices[i].size(); j++)
			{
				x_smooth += target_mesh_vertices[neighbor_vertexIndices[i][j]][0];
				y_smooth += target_mesh_vertices[neighbor_vertexIndices[i][j]][1];
				z_smooth += target_mesh_vertices[neighbor_vertexIndices[i][j]][2];
			}

			x_smooth /= (float)neighbor_vertexIndices[i].size();
			y_smooth /= (float)neighbor_vertexIndices[i].size();
			z_smooth /= (float)neighbor_vertexIndices[i].size();

			result_mesh_vertices[i][0] = x_smooth;
			result_mesh_vertices[i][1] = y_smooth;
			result_mesh_vertices[i][2] = z_smooth;
			
		}


		/*float basic_move[3];
		basic_move[0] = xr_pivot - basic_xr_pivot; basic_move[1] = yr_pivot - basic_yr_pivot; basic_move[2] = zr_pivot - basic_zr_pivot;

		for (int i = 0; i < part_index.size(); i++)
		{
			target_mesh_vertices[part_index[i]][0] = basic_mesh_vertices[part_index[i]][0] + basic_move[0];
			target_mesh_vertices[part_index[i]][1] = basic_mesh_vertices[part_index[i]][1] + basic_move[1];
			target_mesh_vertices[part_index[i]][2] = basic_mesh_vertices[part_index[i]][2] + basic_move[2];
		}*/

		///write obj
		FILE *fp_basic_copy_obj = fopen("basic_copy.obj","w");
		for (int i = 0; i < target_mesh_vertices.size(); i++)
		{
			//fprintf(fp_basic_copy_obj, "v %f %f %f\n", target_mesh_vertices[i][0], target_mesh_vertices[i][1], target_mesh_vertices[i][2]);
			fprintf(fp_basic_copy_obj, "v %f %f %f\n", result_mesh_vertices[i][0], result_mesh_vertices[i][1], result_mesh_vertices[i][2]);
			//fprintf(fp_basic_copy_obj, "v %f %f %f\n", result_mesh_vertices[i][0], result_mesh_vertices[i][1], org_target_mesh_vertices[i][2]);
		}

		for (int i = 0; i < vertex_texture.size(); i++)
		{
			fprintf(fp_basic_copy_obj, "vt %f %f %f \n", vertex_texture[i][0], vertex_texture[i][1], vertex_texture[i][2]);
		}

		for (int i = 0; i < face_index.size(); i++)
		{
			fprintf(fp_basic_copy_obj, "f %d/%d %d/%d %d/%d \n", face_index[i][0], face_vt_index[i][0], face_index[i][1], face_vt_index[i][1], face_index[i][2], face_vt_index[i][2]);
		}


		fclose(fp_basic_copy_obj);



		//printf("%f %f %f \n", xr_pivot, yr_pivot, zr_pivot);


	}





	return 0;
}