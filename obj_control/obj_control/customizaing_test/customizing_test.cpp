#pragma warning(disable:4996)
#define _USE_MATH_DEFINES

#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>

#include <windows.h>
#include <vector>
#include <Eigen/Dense>
#include <Eigen/Geometry>

typedef OpenMesh::PolyMesh_ArrayKernelT<>  MyMesh;

#define PATH "*.da"

void readMesh(std::string filename, MyMesh &mesh)
{
	OpenMesh::IO::Options opt;
	opt += OpenMesh::IO::Options::VertexNormal;
	opt += OpenMesh::IO::Options::FaceNormal;
	//opt += OpenMesh::IO::Options::VertexTexCoord;

	mesh.request_vertex_normals();
	mesh.request_face_normals();
	//mesh.request_vertex_texcoords1D();

	std::cout << "Loading from file '" << filename << "'\n";
	if (OpenMesh::IO::read_mesh(mesh, filename, opt))
	{
		// update face and vertex normals     
		if (!opt.check(OpenMesh::IO::Options::FaceNormal))
			mesh.update_face_normals();
		else
			std::cout << "File provides face normals\n";

		if (!opt.check(OpenMesh::IO::Options::VertexNormal))
			mesh.update_vertex_normals();
		else
			std::cout << "File provides vertex normals\n";
	}
	else
	{
		std::cout << "Failed to load file.";
		exit(1);
	}

	// mesh info
	std::clog << "mesh: " << mesh.n_vertices() << " vertices, "
		<< mesh.n_edges() << " edge, "
		<< mesh.n_faces() << " faces\n";
}

int main(int argc, char* argv[])
{
	float offset_threshold = atof(argv[4]);

	int flag = atoi(argv[3]);
	if (flag == 1)
	{
		int vertex_num = 4596;

		MyMesh basic_obj;
		MyMesh::VertexIter vIt;
		OpenMesh::Vec3f v;
		std::vector<Eigen::Vector3f> vertex_info;

		readMesh(argv[1], basic_obj);

		//vertex_info.resize(before_obj.n_vertices() - 44);

		//////////////vertex information acquisition
		for (vIt = basic_obj.vertices_begin(); vIt != basic_obj.vertices_end(); ++vIt)
		{
			int idx = vIt->idx();

			v = basic_obj.point(vIt.handle());
			vertex_info.push_back(Eigen::Vector3f(v[0], v[1], v[2]));
			//vertex_info[vIt->idx()] = Eigen::Vector3f(v[0], v[1], v[2]);

		}

		/////// f, vt information acquisition
		std::vector<Eigen::Vector3f> vertex_texture;
		std::vector<Eigen::Vector3i> face_index;
		std::vector<Eigen::Vector3i> face_vt_index;

		float vt_x, vt_y, vt_z;
		int f_1, f_2, f_3;
		int f_vt_1, f_vt_2, f_vt_3;

		char *pStr;
		char buffer[255];
		FILE *fp_face_info = fopen("face_info.dat", "r");

		while (!feof(fp_face_info))
		{
			pStr = fgets(buffer, sizeof(buffer), fp_face_info);

			if (pStr == NULL)
				break;

			if (pStr[0] == 'v' && pStr[1] == 't') {
				sscanf(buffer, "vt %f %f %f", &vt_x, &vt_y, &vt_z);
				vertex_texture.push_back(Eigen::Vector3f(vt_x, vt_y, vt_z));
			}

			if (pStr[0] == 'f' && pStr[1] == ' ') {
				sscanf(buffer, "f %d/%d %d/%d %d/%d", &f_1, &f_vt_1, &f_2, &f_vt_2, &f_3, &f_vt_3);
				face_index.push_back(Eigen::Vector3i(f_1, f_2, f_3));
				face_vt_index.push_back(Eigen::Vector3i(f_vt_1, f_vt_2, f_vt_3));
			}

		}

		fclose(fp_face_info);

		float offset_v1, offset_v2, offset_v3;
		std::vector<Eigen::Vector3f> offset_vertex;

		FILE *fp_offset_info = fopen(argv[2], "r");
		while (!feof(fp_offset_info))
		{
			pStr = fgets(buffer, sizeof(buffer), fp_face_info);

			if (pStr == NULL)
				break;

			sscanf(buffer, "%f, %f, %f", &offset_v1, &offset_v2, &offset_v3);
			offset_vertex.push_back(Eigen::Vector3f(offset_v1, offset_v2, offset_v3));
		}

		fclose(fp_offset_info);


		/// output

		int count = 0;

		std::string output_file_name = argv[2];
		output_file_name.erase(output_file_name.length() - 4, 4);

		std::string output_obj_name;
		output_obj_name = output_file_name + ".obj";

		std::string output_partInfor;
		output_partInfor = output_file_name + "_index.da";

		FILE* fp_part_index = fopen(output_partInfor.c_str(), "w");  // part index

		FILE* fp_after_obj = fopen(output_obj_name.c_str(), "w");  // obj check
		fprintf(fp_after_obj, "mtllib default.mtl \n");
		fprintf(fp_after_obj, "g blendShape1Set tweakSet1 \n");
				

		for (int i = 0; i < vertex_info.size(); i++)
		{
			if ((offset_vertex[i][0] > offset_threshold || offset_vertex[i][0] < -1 * offset_threshold) || (offset_vertex[i][1] > offset_threshold || offset_vertex[i][1] < -1 * offset_threshold) || (offset_vertex[i][2] > offset_threshold || offset_vertex[i][2] < -1 * offset_threshold))
			{
				fprintf(fp_after_obj, "v %f %f %f 255 0 0 \n", vertex_info[i][0], vertex_info[i][1], vertex_info[i][2]);
				count++;

				fprintf(fp_part_index, "%d\n", i);
			}
			else {
				fprintf(fp_after_obj, "v %f %f %f \n", vertex_info[i][0], vertex_info[i][1], vertex_info[i][2]);
			}

		}

		//fprintf(fp_part_index, "\n%d", count);
		printf("num_changed_vertex: %d\n", count);
		fclose(fp_part_index);

		for (int i = 0; i < vertex_texture.size(); i++)
		{
			fprintf(fp_after_obj, "vt %f %f %f \n", vertex_texture[i][0], vertex_texture[i][1], vertex_texture[i][2]);
		}

		for (int i = 0; i < face_index.size(); i++)
		{
			fprintf(fp_after_obj, "f %d/%d %d/%d %d/%d \n", face_index[i][0], face_vt_index[i][0], face_index[i][1], face_vt_index[i][1], face_index[i][2], face_vt_index[i][2]);
		}

		fclose(fp_after_obj);

	}
	else if(flag == 2) {
		std::string output_file_name = argv[2];
		output_file_name.erase(output_file_name.length() - 8, 8);
		
		std::string output_partInfor;
		output_partInfor = output_file_name + "_info.inf";

		HANDLE    hSrch;
		TCHAR    Path[MAX_PATH] = PATH;
		WIN32_FIND_DATA  data;
		BOOL    bResult = TRUE;
		char buf[MAX_PATH][MAX_PATH];

		int custom_count = 0;

		hSrch = FindFirstFile(Path, &data);

		while (bResult)
		{

			if (data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				printf("[  %s  ]  ", data.cFileName);
			}
			else
			{
				strcpy(buf[custom_count++], data.cFileName);
				printf( "%s \n" , data.cFileName) ;
			}

			bResult = FindNextFile(hSrch, &data);
		}

		FindClose(hSrch);

		////read information
		char *pStr;
		char buffer[255];
		int selected_index;

		int vertex_num = 4596;
		int *check_meshIndex = new int[vertex_num];

		for (int i = 0; i < vertex_num; i++)
		{
			check_meshIndex[i] = 0;
		}

		for (int i = 0; i < custom_count; i++)
		{
			std::string str(buf[i]);
			//readMesh(buf[i], forLearning_meshes[i]);
			FILE *fp_part_information = fopen(buf[i], "r");

			while (!feof(fp_part_information))
			{
				pStr = fgets(buffer, sizeof(buffer), fp_part_information);

				if (pStr == NULL)
					break;

				sscanf(buffer, "%d", &selected_index);
				check_meshIndex[selected_index] = 1;
			}

			fclose(fp_part_information);
		}

		FILE *fp_composite_info = fopen(output_partInfor.c_str(), "w");
		int check_count = 0;
		for (int i = 0; i < vertex_num; i++)   //output_partInfor
		{
			if (check_meshIndex[i] == 1)
			{
				check_count++;
				fprintf(fp_composite_info, "%d\n", i);

			}
		}

		printf("total points: %d\n", check_count);
		fclose(fp_composite_info);



		//////////////////////////////////////
		////read mesh topology//////////////////

		MyMesh basic_obj;
		MyMesh::VertexIter vIt;
		OpenMesh::Vec3f v;
		std::vector<Eigen::Vector3f> vertex_info;

		readMesh(argv[1], basic_obj);

		//vertex_info.resize(before_obj.n_vertices() - 44);

		//////////////vertex information acquisition
		for (vIt = basic_obj.vertices_begin(); vIt != basic_obj.vertices_end(); ++vIt)
		{
			int idx = vIt->idx();

			v = basic_obj.point(vIt.handle());
			vertex_info.push_back(Eigen::Vector3f(v[0], v[1], v[2]));
			//vertex_info[vIt->idx()] = Eigen::Vector3f(v[0], v[1], v[2]);

		}

		/////// f, vt information acquisition
		std::vector<Eigen::Vector3f> vertex_texture;
		std::vector<Eigen::Vector3i> face_index;
		std::vector<Eigen::Vector3i> face_vt_index;

		float vt_x, vt_y, vt_z;
		int f_1, f_2, f_3;
		int f_vt_1, f_vt_2, f_vt_3;

		char *pStr_obj;
		char buffer_obj[255];
		FILE *fp_face_info = fopen("face_info.dat", "r");

		while (!feof(fp_face_info))
		{
			pStr_obj = fgets(buffer_obj, sizeof(buffer_obj), fp_face_info);

			if (pStr_obj == NULL)
				break;

			if (pStr_obj[0] == 'v' && pStr_obj[1] == 't') {
				sscanf(buffer_obj, "vt %f %f %f", &vt_x, &vt_y, &vt_z);
				vertex_texture.push_back(Eigen::Vector3f(vt_x, vt_y, vt_z));
			}

			if (pStr_obj[0] == 'f' && pStr_obj[1] == ' ') {
				sscanf(buffer_obj, "f %d/%d %d/%d %d/%d", &f_1, &f_vt_1, &f_2, &f_vt_2, &f_3, &f_vt_3);
				face_index.push_back(Eigen::Vector3i(f_1, f_2, f_3));
				face_vt_index.push_back(Eigen::Vector3i(f_vt_1, f_vt_2, f_vt_3));
			}

		}

		fclose(fp_face_info);

		std::string output_infoObj;
		output_infoObj = output_file_name + "_info.obj";

		FILE *fp_inforObj = fopen(output_infoObj.c_str(), "w");
		fprintf(fp_inforObj, "mtllib default.mtl \n");
		fprintf(fp_inforObj, "g blendShape1Set tweakSet1 \n");

		for (int i = 0; i < vertex_num; i++)
		{
			if (check_meshIndex[i] == 1)
			{
				fprintf(fp_inforObj, "v %f %f %f 255 0 0\n", vertex_info[i][0], vertex_info[i][1], vertex_info[i][2]);
			}
			else
			{
				fprintf(fp_inforObj, "v %f %f %f \n", vertex_info[i][0], vertex_info[i][1], vertex_info[i][2]);
			}

		}	

		for (int i = 0; i < vertex_texture.size(); i++)
		{
			fprintf(fp_inforObj, "vt %f %f %f \n", vertex_texture[i][0], vertex_texture[i][1], vertex_texture[i][2]);
		}

		for (int i = 0; i < face_index.size(); i++)
		{
			fprintf(fp_inforObj, "f %d/%d %d/%d %d/%d \n", face_index[i][0], face_vt_index[i][0], face_index[i][1], face_vt_index[i][1], face_index[i][2], face_vt_index[i][2]);
		}

		fclose(fp_inforObj);

	}

	return 0;
}