#pragma warning(disable:4996)
#define _USE_MATH_DEFINES

#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>

#include <vector>
#include <Eigen/Dense>
#include <Eigen/Geometry>

typedef OpenMesh::PolyMesh_ArrayKernelT<>  MyMesh;

void readMesh(std::string filename, MyMesh &mesh)
{
	OpenMesh::IO::Options opt;
	opt += OpenMesh::IO::Options::VertexNormal;
	opt += OpenMesh::IO::Options::FaceNormal;
	//opt += OpenMesh::IO::Options::VertexTexCoord;

	mesh.request_vertex_normals();
	mesh.request_face_normals();
	//mesh.request_vertex_texcoords1D();

	std::cout << "Loading from file '" << filename << "'\n";
	if (OpenMesh::IO::read_mesh(mesh, filename, opt))
	{
		// update face and vertex normals     
		if (!opt.check(OpenMesh::IO::Options::FaceNormal))
			mesh.update_face_normals();
		else
			std::cout << "File provides face normals\n";

		if (!opt.check(OpenMesh::IO::Options::VertexNormal))
			mesh.update_vertex_normals();
		else
			std::cout << "File provides vertex normals\n";
	}
	else
	{
		std::cout << "Failed to load file.";
		exit(1);
	}

	// mesh info
	std::clog << "mesh: " << mesh.n_vertices() << " vertices, "
		<< mesh.n_edges() << " edge, "
		<< mesh.n_faces() << " faces\n";
}

int main(int argc, char* argv[])
{
	MyMesh basic_obj;
	MyMesh::VertexIter vIt;
	OpenMesh::Vec3f v;
	std::vector<Eigen::Vector3f> vertex_info;

	readMesh(argv[1], basic_obj);

	float min_x, max_x, min_y, max_y, min_z, max_z;
	min_x = -86.680000; max_x = 88.010002; min_y = -70.250000; max_y = 130.442902; min_z = -189.830002; max_z = 0.870000;

	float pivot_x = (min_x + max_x) / 2;
	float pivot_y = (min_y + max_y) / 2;
	float pivot_z = (min_z + max_z) / 2;

	printf("pivot_x = %f, pivot_y = %f, pivot_z = %f \n", pivot_x, pivot_y, pivot_z);

	for (vIt = basic_obj.vertices_begin(); vIt != basic_obj.vertices_end(); ++vIt)
	{
		v = basic_obj.point(vIt.handle());
		vertex_info.push_back(Eigen::Vector3f(v[0], v[1], v[2]));
	}

	/////// f, vt information acquisition
	std::vector<Eigen::Vector3f> vertex_texture;
	std::vector<Eigen::Vector3i> face_index;
	std::vector<Eigen::Vector3i> face_vt_index;

	float vt_x, vt_y, vt_z;
	int f_1, f_2, f_3;
	int f_vt_1, f_vt_2, f_vt_3;

	char *pStr;
	char buffer[255];
	FILE *fp_face_info = fopen("face_info.txt", "r");

	while (!feof(fp_face_info))
	{
		pStr = fgets(buffer, sizeof(buffer), fp_face_info);

		if (pStr == NULL)
			break;

		if (pStr[0] == 'v' && pStr[1] == 't') {
			sscanf(buffer, "vt %f %f %f", &vt_x, &vt_y, &vt_z);
			vertex_texture.push_back(Eigen::Vector3f(vt_x, vt_y, vt_z));
		}

		if (pStr[0] == 'f' && pStr[1] == ' ') {
			sscanf(buffer, "f %d/%d %d/%d %d/%d", &f_1, &f_vt_1, &f_2, &f_vt_2, &f_3, &f_vt_3);
			face_index.push_back(Eigen::Vector3i(f_1, f_2, f_3));
			face_vt_index.push_back(Eigen::Vector3i(f_vt_1, f_vt_2, f_vt_3));
		}

	}

	fclose(fp_face_info);

	/// output
	std::string output_obj_name = argv[1];
	output_obj_name.erase(output_obj_name.length() - 4, 4);
	output_obj_name = output_obj_name + "_pivot.obj";

	FILE* fp_after_obj = fopen(output_obj_name.c_str(), "w");
	fprintf(fp_after_obj, "mtllib default.mtl \n");
	fprintf(fp_after_obj, "g blendShape1Set tweakSet1 \n");

	for (int i = 0; i < vertex_info.size(); i++)
	{
		fprintf(fp_after_obj, "v %f %f %f \n", vertex_info[i][0] - pivot_x, vertex_info[i][1] - pivot_y, vertex_info[i][2] - pivot_z);
	}

	for (int i = 0; i < vertex_texture.size(); i++)
	{
		fprintf(fp_after_obj, "vt %f %f %f \n", vertex_texture[i][0], vertex_texture[i][1], vertex_texture[i][2]);
	}

	for (int i = 0; i < face_index.size(); i++)
	{
		fprintf(fp_after_obj, "f %d/%d %d/%d %d/%d \n", face_index[i][0], face_vt_index[i][0], face_index[i][1], face_vt_index[i][1], face_index[i][2], face_vt_index[i][2]);
	}

	fclose(fp_after_obj);


	//min_x = 9999; max_x = -9999; min_y = 9999; max_y = -9999; min_z = 9999; max_z = -9999;

	//for (vIt = basic_obj.vertices_begin(); vIt != basic_obj.vertices_end(); ++vIt)
	//{
	//	int idx = vIt->idx();
	//	
	//	v = basic_obj.point(vIt.handle());

	//	if (v[0] < min_x)
	//		min_x = v[0];
	//	if (v[0] > max_x)
	//		max_x = v[0];
	//	if (v[1] < min_y)
	//		min_y = v[1];
	//	if (v[1] > max_y)
	//		max_y = v[1];
	//	if (v[2] < min_z)
	//		min_z = v[2];
	//	if (v[2] > max_z)
	//		max_z = v[2];		
	//	
	//	//vertex_info.push_back(Eigen::Vector3f(v[0], v[1], v[2]));

	//	//vertex_info[vIt->idx()] = Eigen::Vector3f(v[0], v[1], v[2]);
	//	
	//}

	//printf("min_x = %f, max_x = %f, min_y = %f, max_y = %f, min_z = %f, max_z = %f \n", min_x, max_x, min_y, max_y, min_z, max_z);

	return 0;
}