#pragma warning(disable:4996)
#define _USE_MATH_DEFINES

#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>

#include <vector>
#include <Eigen/Dense>
#include <Eigen/Geometry>

typedef OpenMesh::PolyMesh_ArrayKernelT<>  MyMesh;

void readMesh(std::string filename, MyMesh &mesh)
{
	OpenMesh::IO::Options opt;
	opt += OpenMesh::IO::Options::VertexNormal;
	opt += OpenMesh::IO::Options::FaceNormal;
	//opt += OpenMesh::IO::Options::VertexTexCoord;

	mesh.request_vertex_normals();
	mesh.request_face_normals();
	//mesh.request_vertex_texcoords1D();

	std::cout << "Loading from file '" << filename << "'\n";
	if (OpenMesh::IO::read_mesh(mesh, filename, opt))
	{
		// update face and vertex normals     
		if (!opt.check(OpenMesh::IO::Options::FaceNormal))
			mesh.update_face_normals();
		else
			std::cout << "File provides face normals\n";

		if (!opt.check(OpenMesh::IO::Options::VertexNormal))
			mesh.update_vertex_normals();
		else
			std::cout << "File provides vertex normals\n";
	}
	else
	{
		std::cout << "Failed to load file.";
		exit(1);
	}

	// mesh info
	std::clog << "mesh: " << mesh.n_vertices() << " vertices, "
		<< mesh.n_edges() << " edge, "
		<< mesh.n_faces() << " faces\n";
}

int main(int argc, char* argv[])
{
	MyMesh before_obj, after_obj;
	MyMesh::VertexIter vIt;
	OpenMesh::Vec3f v;
	std::vector<Eigen::Vector3f> vertex_info;

	readMesh(argv[1], before_obj);

	if (before_obj.n_vertices() != 4640)
	{
		return 0;
	}

	//vertex_info.resize(before_obj.n_vertices() - 44);

	//////////////vertex information acquisition
	for (vIt = before_obj.vertices_begin(); vIt != before_obj.vertices_end(); ++vIt)
	{
		int idx = vIt->idx();
		if ( (idx <= 3775) || (idx >= 3820) )
		{
			v = before_obj.point(vIt.handle());
			vertex_info.push_back(Eigen::Vector3f(v[0], v[1], v[2]));
			//vertex_info[vIt->idx()] = Eigen::Vector3f(v[0], v[1], v[2]);
		}
	}

	/////// f, vt information acquisition
	std::vector<Eigen::Vector3f> vertex_texture;
	std::vector<Eigen::Vector3i> face_index;
	std::vector<Eigen::Vector3i> face_vt_index;

	float vt_x, vt_y, vt_z;
	int f_1, f_2, f_3;
	int f_vt_1, f_vt_2, f_vt_3;

	char *pStr;
	char buffer[255];
	FILE *fp_face_info = fopen("face_info.txt", "r");

	while (!feof(fp_face_info))
	{
		pStr = fgets(buffer, sizeof(buffer), fp_face_info);

		if (pStr == NULL)
			break;

		if (pStr[0] == 'v' && pStr[1] == 't') {
			sscanf(buffer, "vt %f %f %f", &vt_x, &vt_y, &vt_z);
			vertex_texture.push_back(Eigen::Vector3f(vt_x, vt_y, vt_z));
		}

		if (pStr[0] == 'f' && pStr[1] == ' ') {
			sscanf(buffer, "f %d/%d %d/%d %d/%d", &f_1, &f_vt_1, &f_2, &f_vt_2, &f_3, &f_vt_3);
			face_index.push_back(Eigen::Vector3i(f_1, f_2, f_3));
			face_vt_index.push_back(Eigen::Vector3i(f_vt_1, f_vt_2, f_vt_3));
		}

	}

	fclose(fp_face_info);


	/// output
	FILE* fp_after_obj = fopen(argv[1], "w");
	fprintf(fp_after_obj, "mtllib default.mtl \n");
	fprintf(fp_after_obj, "g blendShape1Set tweakSet1 \n");

	for (int i = 0; i < vertex_info.size(); i++)
	{
		fprintf(fp_after_obj, "v %f %f %f \n", vertex_info[i][0], vertex_info[i][1], vertex_info[i][2]);
	}

	for (int i = 0; i < vertex_texture.size(); i++)
	{
		fprintf(fp_after_obj, "vt %f %f %f \n", vertex_texture[i][0], vertex_texture[i][1], vertex_texture[i][2]);
	}

	for (int i = 0; i < face_index.size(); i++)
	{
		fprintf(fp_after_obj, "f %d/%d %d/%d %d/%d \n", face_index[i][0], face_vt_index[i][0], face_index[i][1], face_vt_index[i][1], face_index[i][2], face_vt_index[i][2]);
	}

	fclose(fp_after_obj);



	return 0;
}