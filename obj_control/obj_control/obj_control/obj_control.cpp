#pragma warning(disable:4996)
#define _USE_MATH_DEFINES

#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>

typedef OpenMesh::PolyMesh_ArrayKernelT<>  MyMesh;

void readMesh(std::string filename, MyMesh &mesh)
{
	OpenMesh::IO::Options opt;
	opt += OpenMesh::IO::Options::VertexNormal;
	opt += OpenMesh::IO::Options::FaceNormal;
	//opt += OpenMesh::IO::Options::VertexTexCoord;

	mesh.request_vertex_normals();
	mesh.request_face_normals();
	//mesh.request_vertex_texcoords1D();

	std::cout << "Loading from file '" << filename << "'\n";
	if (OpenMesh::IO::read_mesh(mesh, filename, opt))
	{
		// update face and vertex normals     
		if (!opt.check(OpenMesh::IO::Options::FaceNormal))
			mesh.update_face_normals();
		else
			std::cout << "File provides face normals\n";

		if (!opt.check(OpenMesh::IO::Options::VertexNormal))
			mesh.update_vertex_normals();
		else
			std::cout << "File provides vertex normals\n";
	}
	else
	{
		std::cout << "Failed to load file.";
		exit(1);
	}

	// mesh info
	std::clog << "mesh: " << mesh.n_vertices() << " vertices, "
		<< mesh.n_edges() << " edge, "
		<< mesh.n_faces() << " faces\n";
}

int main(int argc, char* argv[])
{
	MyMesh basic_mesh;
	MyMesh::VertexIter vIt;
	MyMesh::VertexVertexIter vvIt;
	MyMesh::VertexFaceIter vfIt;	

	std::vector<std::vector<int>> neighborIndices;

	//std::string mesh_name(argv[1]);

	readMesh(argv[1], basic_mesh);

	int vertex_num = 4596;

	int landmark_bool[4596];
	int eye_bool[4596];
	int nose_bool[4596];
	int mouse_bool[4596];
	int eyebrows_bool[4596];

	for (int i = 0; i < vertex_num; i++)
	{
		landmark_bool[i] = 0;
		eye_bool[i] = 0;
		nose_bool[i] = 0;
		mouse_bool[i] = 0;
		eyebrows_bool[i] = 0;
	}

	//////////////////////////////////////////////////////
	///////////////eyebrows feature points/////////////////////
	/////////////////////////////////////////////////////

	std::vector<int> landmark_r_eyebrow_indices;
	std::vector<int> landmark_l_eyebrow_indices;
	std::vector<int> inside_eyebrows_indices;

	int num_r_eyebrow_landmark = 0;
	int num_l_eyebrow_landmark = 0;
	int num_eyebrows = 0;

	int landmark_index;
	FILE *fp_r_eyebrow_landmark = fopen("r_eyeBrows_landmark.txt", "r");
	fscanf(fp_r_eyebrow_landmark, "%d \n", &num_r_eyebrow_landmark);
	fscanf(fp_r_eyebrow_landmark, "space\n");

	for (int i = 0; i < num_r_eyebrow_landmark; i++)
	{
		fscanf(fp_r_eyebrow_landmark, "%d \n", &landmark_index);
		landmark_r_eyebrow_indices.push_back(landmark_index);
		landmark_bool[landmark_index] = 1;
	}
	fclose(fp_r_eyebrow_landmark);

	/////Left eyebrow
	FILE *fp_l_eyebrow_landmark = fopen("l_eyeBrows_landmark.txt", "r");
	fscanf(fp_l_eyebrow_landmark, "%d \n", &num_l_eyebrow_landmark);
	fscanf(fp_l_eyebrow_landmark, "space\n");

	for (int i = 0; i < num_l_eyebrow_landmark; i++)
	{
		fscanf(fp_l_eyebrow_landmark, "%d \n", &landmark_index);
		landmark_l_eyebrow_indices.push_back(landmark_index);
		landmark_bool[landmark_index] = 1;
	}
	fclose(fp_l_eyebrow_landmark);

	////////////inside///////////////
	FILE *fp_eyebrows_inside = fopen(argv[9], "r"); // inside_eyes_indices
	fscanf(fp_eyebrows_inside, "%d \n", &num_eyebrows);
	fscanf(fp_eyebrows_inside, "space\n");

	int inside_index;
	for (int i = 0; i < num_eyebrows; i++)
	{
		fscanf(fp_eyebrows_inside, "%d\n", &inside_index);
		inside_eyebrows_indices.push_back(inside_index);
		eyebrows_bool[inside_index] = 1;
	}
	fclose(fp_eyebrows_inside);

	//////////////////////////////////////////////////////
	///////////////eye feature points/////////////////////
	/////////////////////////////////////////////////////

	std::vector<int> landmark_eyes_indices;
	std::vector<int> landmark_l_eyes_indices;
	std::vector<int> inside_eyes_indices;

	int num_eyes_landmark = 0;
	int num_l_eyes_landmark = 0;
	int num_eyes_inside = 0;


	/////Right landmark
	FILE *fp_eyes_landmark = fopen(argv[2], "r");
	fscanf(fp_eyes_landmark, "%d \n", &num_eyes_landmark);
	fscanf(fp_eyes_landmark, "space\n");

	for (int i = 0; i < num_eyes_landmark; i++)
	{
		fscanf(fp_eyes_landmark, "%d \n", &landmark_index);
		landmark_eyes_indices.push_back(landmark_index);
		landmark_bool[landmark_index] = 1;
	}
	fclose(fp_eyes_landmark);


	/////Left landmark
	FILE *fp_l_eyes_landmark = fopen(argv[3], "r");
	fscanf(fp_l_eyes_landmark, "%d \n", &num_l_eyes_landmark);
	fscanf(fp_l_eyes_landmark, "space\n");

	for (int i = 0; i < num_l_eyes_landmark; i++)
	{
		fscanf(fp_l_eyes_landmark, "%d \n", &landmark_index);
		landmark_l_eyes_indices.push_back(landmark_index);
		landmark_bool[landmark_index] = 1;
	}
	fclose(fp_l_eyes_landmark);

	///////inside
	
	FILE *fp_eyes_inside = fopen(argv[6], "r"); // inside_eyes_indices
	fscanf(fp_eyes_inside, "%d \n", &num_eyes_inside);
	fscanf(fp_eyes_inside, "space\n");

	for (int i = 0; i < num_eyes_inside; i++)
	{
		fscanf(fp_eyes_inside, "%d\n", &inside_index);
		inside_eyes_indices.push_back(inside_index);
		eye_bool[inside_index] = 1;
	}
	fclose(fp_eyes_inside);

////////////////////eye separation test//////////////////////////////////////
	FILE* fp_r_eye_part = fopen("Right_eye_part.txt", "w");
	FILE* fp_l_eye_part = fopen("Left_eye_part.txt", "w");
	
	OpenMesh::Vec3f vTemp;
	for (int i = 0; i < inside_eyes_indices.size(); i++)
	{
		OpenMesh::VertexHandle vh = basic_mesh.vertex_handle(inside_eyes_indices[i]);
		vTemp = basic_mesh.point(vh);

		if (vTemp[0] < 0.0) {
			fprintf(fp_r_eye_part, "%d\n", inside_eyes_indices[i]);
		}
		else {
			fprintf(fp_l_eye_part, "%d\n", inside_eyes_indices[i]);
		}


	}

	fclose(fp_l_eye_part);
	fclose(fp_r_eye_part);


	//////////////////////////////////////////////////////
	///////////////nose feature points/////////////////////
	/////////////////////////////////////////////////////

	std::vector<int> landmark_nose_indices;
	std::vector<int> inside_nose_indices;
	int num_nose_landmark = 0;
	int num_nose_inside = 0;

	/////landmark
	FILE* fp_nose_landmark = fopen(argv[4], "r");
	fscanf(fp_nose_landmark, "%d \n", &num_nose_landmark);
	fscanf(fp_nose_landmark, "space\n");

	for (int i = 0; i < num_nose_landmark; i++)
	{
		fscanf(fp_nose_landmark, "%d \n", &landmark_index);
		landmark_nose_indices.push_back(landmark_index);
		landmark_bool[landmark_index] = 1;
	}
	fclose(fp_nose_landmark);

	///////inside
	FILE *fp_nose_inside = fopen(argv[7], "r");
	fscanf(fp_nose_inside, "%d \n", &num_nose_inside);
	fscanf(fp_nose_inside, "space\n");

	for (int i = 0; i < num_nose_inside; i++)
	{
		fscanf(fp_nose_inside, "%d \n", &inside_index);
		inside_nose_indices.push_back(inside_index);
		nose_bool[inside_index] = 1;
	}
	fclose(fp_nose_inside);

	//////////////////////////////////////////////////////
	///////////////mouse feature points//////////////////
	/////////////////////////////////////////////////////

	std::vector<int> landmark_mouse_indices;
	std::vector<int> inside_mouse_indices;
	int num_mouse_landmark = 0;
	int num_mouse_inside = 0;


	FILE *fp_mouse_landmark = fopen(argv[5], "r");
	fscanf(fp_mouse_landmark, "%d \n", &num_mouse_landmark);
	fscanf(fp_mouse_landmark, "space\n");

	for (int i = 0; i < num_mouse_landmark; i++)
	{
		fscanf(fp_mouse_landmark, "%d \n", &landmark_index);
		landmark_mouse_indices.push_back(landmark_index);
		landmark_bool[landmark_index] = 1;
	}
	fclose(fp_mouse_landmark);

	/////inside
	FILE *fp_mouse_inside = fopen(argv[8], "r");
	fscanf(fp_mouse_inside, "%d \n", &num_mouse_inside);
	fscanf(fp_mouse_inside, "space\n");

	for (int i = 0; i < num_mouse_inside; i++)
	{
		fscanf(fp_mouse_inside, "%d \n", &inside_index);
		inside_mouse_indices.push_back(inside_index);
		mouse_bool[inside_index] = 1;
	}

	fclose(fp_mouse_inside); 

	

	//////////////////////////////////////////////////
	/////////////////texturing////////////////////////
	/////////////////////////////////////////////////

	FILE *fp_featureInfo = fopen("feature_information.obj", "w");

	OpenMesh::Vec3f t_v;

	for (int i = 0; i < vertex_num; i++)
	{
		OpenMesh::VertexHandle vh = basic_mesh.vertex_handle(i);
		int idx = vh.idx();

		t_v = basic_mesh.point(vh);
		fprintf(fp_featureInfo, "v %f %f %f ", t_v[0], t_v[1], t_v[2]);

		if (landmark_bool[idx] == 1) {
			fprintf(fp_featureInfo, "255 0 0\n");
		}
		else if (eye_bool[idx] == 1) {
			if(t_v[0] <0.0)
				fprintf(fp_featureInfo, "0 255 0\n");
			else 
				fprintf(fp_featureInfo, "0 255 255\n");
		}
		else if (nose_bool[idx] == 1) {
			fprintf(fp_featureInfo, "0 0 255\n");
		}
		else if (mouse_bool[idx] == 1) {
			fprintf(fp_featureInfo, "255 255 0\n");
		}
		else if (eyebrows_bool[idx] == 1) {
			fprintf(fp_featureInfo, "255 0 255\n");
		}
		else {
			fprintf(fp_featureInfo, "\n");
		}

	}

	MyMesh::FaceIter fIt;
	MyMesh::FaceVertexIter fvIt;

	for (fIt = basic_mesh.faces_begin(); fIt != basic_mesh.faces_end(); ++fIt)
	{
		fprintf(fp_featureInfo, "f ");
		for (fvIt = basic_mesh.fv_begin(fIt.handle()); fvIt != basic_mesh.fv_end(fIt.handle()); ++fvIt)
		{
			fprintf(fp_featureInfo, "%d ", fvIt.handle().idx() +1);
		}
		fprintf(fp_featureInfo, "\n");

	}




	//for (vIt = basic_mesh.vertices_begin(); vIt != basic_mesh.vertices_end(); ++vIt)
	//{
	//	//OpenMesh::VertexHandle vh = outputMesh.vertex_handle(landmark_eyes_indices[j]);
	//	//int idx = 
	//	t_v = basic_mesh.point(vIt.handle());
	//	fprintf(fp_featureInfo, "v %f %f %f ", t_v[0], t_v[1], t_v[2]);
	//	//if(landmark_bool[vIt.handle()] == 1)

	//}


	fclose(fp_featureInfo);







	///////Save vertice_face_iter
	////FILE *fp_neighbor_veticesFaceIter = fopen("neighbor_veticesFaceIter.txt", "w"); // vertex index / num neighbor faces / neighbor faces

	////for (int i = 0; i < neighborFaceIndices.size(); i++)
	////{
	////	fprintf(fp_neighbor_veticesFaceIter, "%d %d ", i, neighborFaceIndices[i].size());

	////	for (int j = 0; j < neighborFaceIndices[i].size(); j++)
	////	{
	////		fprintf(fp_neighbor_veticesFaceIter, "%d ", neighborFaceIndices[i][j]);
	////	}
	////	fprintf(fp_neighbor_veticesFaceIter, "\n");
	////}


	////fclose(fp_neighbor_veticesFaceIter);

	return 0;
}