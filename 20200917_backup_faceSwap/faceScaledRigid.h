#pragma once

#include <math.h>
#include <vector>
#include <iostream>

#include "glog/logging.h"
#include "ceres/ceres.h"
#include "ceres/rotation.h"
#include <Eigen/Dense>
#include <Eigen/Geometry>

class ResidualScaledPointICP
{
public:
	ResidualScaledPointICP(float *_src, float *_dest) : src(_src), dest(_dest) {}
	template<typename T> bool operator()(const T* const scale, const T* const rotation, const T* const translation, T* residual) const
	{
		Eigen::Matrix<T, 4, 1>  afterTrans, p;
		T ICP_rotation_matrix[9];

		p[0] = T(dest[0]);
		p[1] = T(dest[1]);
		p[2] = T(dest[2]);
		p[3] = T(1.0);

		ceres::AngleAxisToRotationMatrix(rotation, ICP_rotation_matrix);

		Eigen::Matrix<T, 4, 4> ICP_transformed_matrix;
		Eigen::Matrix<T, 4, 4> ICP_scaled_matrix;

		ICP_scaled_matrix(0, 0) = scale[0];	 ICP_scaled_matrix(0, 1) = T(0.0);		ICP_scaled_matrix(0, 2) = T(0.0);	ICP_scaled_matrix(0, 3) = T(0.0);
		ICP_scaled_matrix(1, 0) = T(0.0);	 ICP_scaled_matrix(1, 1) = scale[0];	ICP_scaled_matrix(1, 2) = T(0.0);	ICP_scaled_matrix(1, 3) = T(0.0);
		ICP_scaled_matrix(2, 0) = T(0.0);	 ICP_scaled_matrix(2, 1) = T(0.0);		ICP_scaled_matrix(2, 2) = scale[0]; ICP_scaled_matrix(2, 3) = T(0.0);
		ICP_scaled_matrix(3, 0) = T(0.0);	 ICP_scaled_matrix(3, 1) = T(0.0);		ICP_scaled_matrix(3, 2) = T(0.0);	ICP_scaled_matrix(3, 3) = T(1.0);

		/*ICP_scaled_matrix(0, 0) = T(0.8);	 ICP_scaled_matrix(0, 1) = T(0.0);		ICP_scaled_matrix(0, 2) = T(0.0);	ICP_scaled_matrix(0, 3) = T(0.0);
		ICP_scaled_matrix(1, 0) = T(0.0);	 ICP_scaled_matrix(1, 1) = T(0.8);		ICP_scaled_matrix(1, 2) = T(0.0);	ICP_scaled_matrix(1, 3) = T(0.0);
		ICP_scaled_matrix(2, 0) = T(0.0);	 ICP_scaled_matrix(2, 1) = T(0.0);		ICP_scaled_matrix(2, 2) = T(0.8);	ICP_scaled_matrix(2, 3) = T(0.0);
		ICP_scaled_matrix(3, 0) = T(0.0);	 ICP_scaled_matrix(3, 1) = T(0.0);		ICP_scaled_matrix(3, 2) = T(0.0);	ICP_scaled_matrix(3, 3) = T(1.0);*/



		ICP_transformed_matrix(0, 0) = ICP_rotation_matrix[0];	 ICP_transformed_matrix(0, 1) = ICP_rotation_matrix[3];		ICP_transformed_matrix(0, 2) = ICP_rotation_matrix[6];	ICP_transformed_matrix(0, 3) = translation[0];
		ICP_transformed_matrix(1, 0) = ICP_rotation_matrix[1];	 ICP_transformed_matrix(1, 1) = ICP_rotation_matrix[4];		ICP_transformed_matrix(1, 2) = ICP_rotation_matrix[7];	ICP_transformed_matrix(1, 3) = translation[1];
		ICP_transformed_matrix(2, 0) = ICP_rotation_matrix[2];	 ICP_transformed_matrix(2, 1) = ICP_rotation_matrix[5];		ICP_transformed_matrix(2, 2) = ICP_rotation_matrix[8];  ICP_transformed_matrix(2, 3) = translation[2];
		ICP_transformed_matrix(3, 0) = T(0.0);					 ICP_transformed_matrix(3, 1) = T(0.0);						ICP_transformed_matrix(3, 2) = T(0.0);					ICP_transformed_matrix(3, 3) = T(1.0);

		/*ICP_transformed_matrix(0, 0) = T(1.0);	 ICP_transformed_matrix(0, 1) = T(0.0);		ICP_transformed_matrix(0, 2) = T(0.0);	ICP_transformed_matrix(0, 3) = translation[0];
		ICP_transformed_matrix(1, 0) = T(0.0);	 ICP_transformed_matrix(1, 1) = T(1.0);		ICP_transformed_matrix(1, 2) = T(0.0);	ICP_transformed_matrix(1, 3) = translation[1];
		ICP_transformed_matrix(2, 0) = T(0.0);	 ICP_transformed_matrix(2, 1) = T(0.0);		ICP_transformed_matrix(2, 2) = T(1.0);  ICP_transformed_matrix(2, 3) = translation[2];
		ICP_transformed_matrix(3, 0) = T(0.0);	 ICP_transformed_matrix(3, 1) = T(0.0);		ICP_transformed_matrix(3, 2) = T(0.0);	ICP_transformed_matrix(3, 3) = T(1.0);*/

		afterTrans = ICP_scaled_matrix * ICP_transformed_matrix * p;
		afterTrans[0] = afterTrans[0] / afterTrans[3];
		afterTrans[1] = afterTrans[1] / afterTrans[3];
		afterTrans[2] = afterTrans[2] / afterTrans[3];

		residual[0] = afterTrans[0] - T(src[0]);
		residual[1] = afterTrans[1] - T(src[1]);
		residual[2] = afterTrans[2] - T(src[2]);

		return true;
	}
private:
	float *src, *dest;
};

class ResidualPointICP
{
public:
	ResidualPointICP(float *_src, float *_dest, double _partScale, double _width_scale, double _height_scale) : src(_src), dest(_dest), part_scale(_partScale), width_scale(_width_scale), height_scale(_height_scale) {}
	template<typename T> bool operator()(const T* const scale, const T* const rotation, const T* const translation, T* residual) const
	{
		Eigen::Matrix<T, 4, 1>  afterTrans, p;
		T ICP_rotation_matrix[9];

		p[0] = T(dest[0]);
		p[1] = T(dest[1]);
		p[2] = T(dest[2]);
		p[3] = T(1.0);

		ceres::AngleAxisToRotationMatrix(rotation, ICP_rotation_matrix);

		Eigen::Matrix<T, 4, 4> ICP_transformed_matrix;
		Eigen::Matrix<T, 4, 4> ICP_scaled_matrix;

		/*ICP_scaled_matrix(0, 0) = T(1.0);	 ICP_scaled_matrix(0, 1) = T(0.0);		ICP_scaled_matrix(0, 2) = T(0.0);	ICP_scaled_matrix(0, 3) = T(0.0);
		ICP_scaled_matrix(1, 0) = T(0.0);	 ICP_scaled_matrix(1, 1) = T(1.0);		ICP_scaled_matrix(1, 2) = T(0.0);	ICP_scaled_matrix(1, 3) = T(0.0);
		ICP_scaled_matrix(2, 0) = T(0.0);	 ICP_scaled_matrix(2, 1) = T(0.0);		ICP_scaled_matrix(2, 2) = T(1.0);	ICP_scaled_matrix(2, 3) = T(0.0);
		ICP_scaled_matrix(3, 0) = T(0.0);	 ICP_scaled_matrix(3, 1) = T(0.0);		ICP_scaled_matrix(3, 2) = T(0.0);	ICP_scaled_matrix(3, 3) = T(1.0);*/

		ICP_scaled_matrix(0, 0) = T(part_scale) * T(width_scale);	ICP_scaled_matrix(0, 1) = T(0.0);								ICP_scaled_matrix(0, 2) = T(0.0);			ICP_scaled_matrix(0, 3) = T(0.0);
		ICP_scaled_matrix(1, 0) = T(0.0);							ICP_scaled_matrix(1, 1) = T(part_scale) * T(height_scale);		ICP_scaled_matrix(1, 2) = T(0.0);			ICP_scaled_matrix(1, 3) = T(0.0);
		ICP_scaled_matrix(2, 0) = T(0.0);							ICP_scaled_matrix(2, 1) = T(0.0);								ICP_scaled_matrix(2, 2) = T(part_scale);	ICP_scaled_matrix(2, 3) = T(0.0);
		ICP_scaled_matrix(3, 0) = T(0.0);							ICP_scaled_matrix(3, 1) = T(0.0);								ICP_scaled_matrix(3, 2) = T(0.0);			ICP_scaled_matrix(3, 3) = T(1.0);

		ICP_transformed_matrix(0, 0) = ICP_rotation_matrix[0];	 ICP_transformed_matrix(0, 1) = ICP_rotation_matrix[3];		ICP_transformed_matrix(0, 2) = ICP_rotation_matrix[6];	ICP_transformed_matrix(0, 3) = translation[0];
		ICP_transformed_matrix(1, 0) = ICP_rotation_matrix[1];	 ICP_transformed_matrix(1, 1) = ICP_rotation_matrix[4];		ICP_transformed_matrix(1, 2) = ICP_rotation_matrix[7];	ICP_transformed_matrix(1, 3) = translation[1];
		ICP_transformed_matrix(2, 0) = ICP_rotation_matrix[2];	 ICP_transformed_matrix(2, 1) = ICP_rotation_matrix[5];		ICP_transformed_matrix(2, 2) = ICP_rotation_matrix[8];  ICP_transformed_matrix(2, 3) = translation[2];
		ICP_transformed_matrix(3, 0) = T(0.0);					 ICP_transformed_matrix(3, 1) = T(0.0);						ICP_transformed_matrix(3, 2) = T(0.0);					ICP_transformed_matrix(3, 3) = T(1.0);

		//ICP_transformed_matrix(0, 0) = T(1.0);	 ICP_transformed_matrix(0, 1) = T(0.0);		ICP_transformed_matrix(0, 2) = T(0.0);	ICP_transformed_matrix(0, 3) = translation[0];
		//ICP_transformed_matrix(1, 0) = T(0.0);	 ICP_transformed_matrix(1, 1) = T(1.0);		ICP_transformed_matrix(1, 2) = T(0.0);	ICP_transformed_matrix(1, 3) = translation[1];
		//ICP_transformed_matrix(2, 0) = T(0.0);	 ICP_transformed_matrix(2, 1) = T(0.0);		ICP_transformed_matrix(2, 2) = T(1.0);  ICP_transformed_matrix(2, 3) = translation[2];
		//ICP_transformed_matrix(3, 0) = T(0.0);	 ICP_transformed_matrix(3, 1) = T(0.0);		ICP_transformed_matrix(3, 2) = T(0.0);	ICP_transformed_matrix(3, 3) = T(1.0);

		afterTrans = ICP_scaled_matrix * ICP_transformed_matrix * p;
		afterTrans[0] = afterTrans[0] / afterTrans[3];
		afterTrans[1] = afterTrans[1] / afterTrans[3];
		afterTrans[2] = afterTrans[2] / afterTrans[3];

		residual[0] = afterTrans[0] - T(src[0]);
		residual[1] = afterTrans[1] - T(src[1]);
		residual[2] = afterTrans[2] - T(src[2]);

		return true;
	}
private:
	float *src, *dest;
	double part_scale;
	double width_scale, height_scale;
};

class ScaledRigid
{
public:
	ScaledRigid(std::vector<Eigen::Vector3f>& _vertices, std::vector<Eigen::Vector3f>& _target_vertices, std::vector<int>& _landmark_indices, std::vector<int>& _landmark_tar_indices, int _standard_landmark_num, int numIters = 100)
		: vertices(_vertices), target_vertices(_target_vertices), landmark_indices(_landmark_indices), landmark_tar_indices(_landmark_tar_indices), standard_landmark_num(_standard_landmark_num)
	{
		options.max_num_iterations = numIters;
		//options.minimizer_progress_to_stdout = true;
		//options.minimizer_type = ceres::LINE_SEARCH;
		options.num_threads = 6;
		options.num_linear_solver_threads = 6;
		options.update_state_every_iteration = true;

		ICP_scale = 1.0;
		ICP_rotation[0] = 0.0; ICP_rotation[1] = 0.0; ICP_rotation[2] = 0.0;
		ICP_translation[0] = 0.0; ICP_translation[1] = 0.0; ICP_translation[2] = 0.0;
	}

	void energyPointICP()
	{
		//for (int i = 0; i < landmark_dst_indices.size(); i++)
		for (int i = 0; i < standard_landmark_num; i++)
		{
			ResidualScaledPointICP* residual = new ResidualScaledPointICP(vertices[landmark_indices[i]].data(), target_vertices[landmark_tar_indices[i]].data());
			ceres::AutoDiffCostFunction<ResidualScaledPointICP, 3, 1, 3, 3> *cost = new ceres::AutoDiffCostFunction<ResidualScaledPointICP, 3, 1, 3, 3>(residual);
			problem.AddResidualBlock(cost, NULL, &ICP_scale, &ICP_rotation[0], &ICP_translation[0]);
		}
		//problem.SetParameterLowerBound(ICP_rotation, 2, 0.0);
		//problem.SetParameterUpperBound(ICP_rotation, 2, 0.00000000001);

	}

	void energeyMinimization()
	{
		ceres::Solve(options, &problem, &summary);
	}

	double getScale()
	{
		return ICP_scale;
	}
	double* getRotation()
	{
		return ICP_rotation;
	}

	double* getTranslation()
	{
		return ICP_translation;
	}

	void showFinal()
	{
		//std::cout << summary.BriefReport() << '\n';
		//std::cout << summary.FullReport() << '\n';
	}

	double run()
	{
		energyPointICP();
		energeyMinimization();
		showFinal();

		/*std::cout << "ceres__scale: " << ICP_scale << std::endl;
		std::cout << "ceres__rotation: " << ICP_rotation[0] << " " << ICP_rotation[1] << " " << ICP_rotation[2] << std::endl;
		std::cout << "ceres__translation:" << ICP_translation[0] << " " << ICP_translation[1] << "  " << ICP_translation[2] << std::endl;*/

		return summary.final_cost;
	}

	ceres::Solver::Summary summary;

private:
	std::vector<Eigen::Vector3f> &vertices, &target_vertices;
	std::vector<int> &landmark_indices, &landmark_tar_indices;

	double ICP_scale;
	double ICP_rotation[3], ICP_translation[3];

	ceres::Problem problem;
	ceres::Solver::Options options;

	int standard_landmark_num;
};

class RigidTR
{
public:
	RigidTR(std::vector<Eigen::Vector3f>& _vertices, std::vector<Eigen::Vector3f>& _target_vertices, std::vector<int>& _landmark_indices, int _standard_landmark_num, double _scale_factor, double _width_scale, double _height_scale, int numIters = 100)
		: vertices(_vertices), target_vertices(_target_vertices), landmark_indices(_landmark_indices), standard_landmark_num(_standard_landmark_num), scale_factor(_scale_factor), width_scale(_width_scale), height_scale(_height_scale)
	{
		options.max_num_iterations = numIters;
		//options.minimizer_progress_to_stdout = true;
		//options.minimizer_type = ceres::LINE_SEARCH;
		options.num_threads = 8;
		options.num_linear_solver_threads = 8;
		options.update_state_every_iteration = true;

		ICP_scale = scale_factor;
		ICP_rotation[0] = 0.0; ICP_rotation[1] = 0.0; ICP_rotation[2] = 0.0;
		ICP_translation[0] = 0.0; ICP_translation[1] = 0.0; ICP_translation[2] = 0.0;
	}

	void energyPointICP()
	{
		//for (int i = 0; i < landmark_dst_indices.size(); i++)
		for (int i = 0; i < standard_landmark_num; i++)
		{
			ResidualPointICP* residual = new ResidualPointICP(vertices[landmark_indices[i]].data(), target_vertices[landmark_indices[i]].data(), scale_factor, width_scale, height_scale);
			ceres::AutoDiffCostFunction<ResidualPointICP, 3, 1, 3, 3> *cost = new ceres::AutoDiffCostFunction<ResidualPointICP, 3, 1, 3, 3>(residual);
			problem.AddResidualBlock(cost, NULL, &ICP_scale, &ICP_rotation[0], &ICP_translation[0]);
		}
		problem.SetParameterLowerBound(ICP_rotation, 0, 0.0);
		problem.SetParameterUpperBound(ICP_rotation, 0, 0.00000000001);
		problem.SetParameterLowerBound(ICP_rotation, 1, 0.0);
		problem.SetParameterUpperBound(ICP_rotation, 1, 0.00000000001);
		problem.SetParameterLowerBound(ICP_rotation, 2, 0.0);
		problem.SetParameterUpperBound(ICP_rotation, 2, 0.00000000001);
	}

	void energeyMinimization()
	{
		ceres::Solve(options, &problem, &summary);
	}

	double getScale()
	{
		return ICP_scale;
	}
	double* getRotation()
	{
		return ICP_rotation;
	}

	double* getTranslation()
	{
		return ICP_translation;
	}

	void showFinal()
	{
		//std::cout << summary.BriefReport() << '\n';
		//std::cout << summary.FullReport() << '\n';
	}

	double run()
	{
		energyPointICP();
		energeyMinimization();
		showFinal();

		/*std::cout << "ceres__scale: " << ICP_scale << std::endl;
		std::cout << "ceres__rotation: " << ICP_rotation[0] << " " << ICP_rotation[1] << " " << ICP_rotation[2] << std::endl;
		std::cout << "ceres__translation:" << ICP_translation[0] << " " << ICP_translation[1] << "  " << ICP_translation[2] << std::endl;*/

		return summary.final_cost;
	}

	ceres::Solver::Summary summary;

private:
	std::vector<Eigen::Vector3f> &vertices, &target_vertices;
	std::vector<int> &landmark_indices;

	double scale_factor;
	double width_scale;
	double height_scale;

	double ICP_scale;
	double ICP_rotation[3], ICP_translation[3];

	ceres::Problem problem;
	ceres::Solver::Options options;

	int standard_landmark_num;
};
