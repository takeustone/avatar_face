#pragma once

#include <math.h>
#include <vector>
#include <iostream>

#include "glog/logging.h"
#include "ceres/ceres.h"
#include "ceres/rotation.h"
#include <Eigen/Dense>
#include <Eigen/Geometry>

class ResidualTV
{
public:
	ResidualTV(double _weight) : weight(_weight) {}
	template<typename T> bool operator()(const T* const dSrc, const T* const dNbr, T* residual) const
	{
		for (int i = 0; i < 3; i++)
			residual[i] = T(weight)*(dSrc[i] - dNbr[i]);

		return true;
	}
private:
	double weight;
};

class ResidualARAP
{
public:
	ResidualARAP(double _weight, float *_src, float *_nbr) : weight(_weight), src(_src), nbr(_nbr) {}
	template<typename T> bool operator()(const T* const dSrc, const T* const dNbr, const T* const localRot, T* residual) const
	{
		T currentDiff[3], templateDiff[3], rotTemplateDiff[3];

		for (int i = 0; i < 3; i++)
		{
			templateDiff[i] = T(src[i] - nbr[i]);
			currentDiff[i] = templateDiff[i] + dSrc[i] - dNbr[i];
		}

		ceres::AngleAxisRotatePoint(localRot, templateDiff, rotTemplateDiff);

		for (int i = 0; i < 3; i++)
			residual[i] = T(weight)*(currentDiff[i] - rotTemplateDiff[i]);

		return true;
	}
private:
	double weight;
	float *src, *nbr;
};

class ResidualCon
{
public:
	ResidualCon(double _weight, float *_dest, float *_src) : weight(_weight), dest(_dest), src(_src) {}
	template<typename T> bool operator()(const T* const dSrc, T* residual) const
	{
		for (int i = 0; i < 3; i++)
			residual[i] = T(weight)*(T(src[i]) + dSrc[i] - T(dest[i]));

		return true;
	}
private:
	double weight;
	float *src, *dest;
};

class ResidualConNormal
{
public:
	ResidualConNormal(double _weight, float *_dest, float *_destNormal, float *_src) : weight(_weight), dest(_dest), destNormal(_destNormal), src(_src) {}
	template<typename T> bool operator()(const T* const dSrc, T* residual) const
	{
		T diff[3], normal[3];

		for (int i = 0; i < 3; i++)
		{
			diff[i] = T(src[i]) + dSrc[i] - T(dest[i]);
			normal[i] = T(destNormal[i]);
		}
		residual[0] = T(weight) * ceres::DotProduct(diff, normal);

		return true;
	}
private:
	double weight;
	float *src, *dest, *destNormal;
};

class fitting_deform
{
public:
	fitting_deform(double _weightTV, double _weightARAP, double _weightLandCon, double _weightLandNormalCon, double _weightCon, double _weightConNormal,
		std::vector<Eigen::Vector3f>& _vertices, std::vector<Eigen::Vector3f>& _dVertices, std::vector<Eigen::Vector3f>& _verticeRots,
		std::vector<std::vector<int>> &_neighborIndex, std::vector<Eigen::Vector3f>& _target, std::vector<Eigen::Vector3f>& _targetNormal, std::vector<int>& _targetIndex,
		std::vector<Eigen::Vector3f>& _target_vertices,
		std::vector<int>& _landmark_src_indicesForLC, std::vector<int>& _landmark_dst_indicesForLC, std::vector<int>& _landmark_src_indicesForLNC, std::vector<int>& _landmark_dst_indicesForLNC, int numIters = 50)
		: weightTV(_weightTV), weightARAP(_weightARAP), weightLandCon(_weightLandCon), weightLandNormalCon(_weightLandNormalCon), weightCon(_weightCon), weightConNormal(_weightConNormal),
		vertices(_vertices), dVertices(_dVertices), verticeRots(_verticeRots), neighborIndex(_neighborIndex),
		target(_target), targetNormal(_targetNormal), targetIndex(_targetIndex), target_vertices(_target_vertices),
		landmark_src_indicesForLC(_landmark_src_indicesForLC), landmark_dst_indicesForLC(_landmark_dst_indicesForLC), landmark_src_indicesForLNC(_landmark_src_indicesForLNC), landmark_dst_indicesForLNC(_landmark_dst_indicesForLNC)
	{
		if (vertices.size() != dVertices.size())
		{
			std::cout << "vertex size does not match\n";
			exit(0);
		}
		else if (target.size() != targetIndex.size())
		{
			std::cout << "target size does not match\n";
			exit(0);
		}

		dNodes.resize(dVertices.size());
		nodeRot.resize(dVertices.size());

		for (int i = 0; i < dNodes.size(); i++)
		{
			dNodes[i] = dVertices[i].cast<double>();
			nodeRot[i] = verticeRots[i].cast<double>();
		}

		options.max_num_iterations = numIters;
		//options.minimizer_progress_to_stdout = true;
		options.minimizer_type = ceres::LINE_SEARCH;
		options.num_threads = 8;
		options.num_linear_solver_threads = 8;
		options.update_state_every_iteration = true;

	}  // fitting_deform

	void energySetup()
	{
		//TV
		for (int i = 0; i < neighborIndex.size(); i++)
		{
			for (int j = 0; j < neighborIndex[i].size(); j++)
			{
				if (i == neighborIndex[i][j])
					continue;

				ResidualTV* residual = new ResidualTV(weightTV);
				ceres::AutoDiffCostFunction<ResidualTV, 3, 3, 3> *cost = new ceres::AutoDiffCostFunction<ResidualTV, 3, 3, 3>(residual);
				problem.AddResidualBlock(cost, NULL, dNodes[i].data(), dNodes[neighborIndex[i][j]].data());
			}
		}

		// ARAP
		for (int i = 0; i < neighborIndex.size(); i++)
		{
			for (int j = 0; j < neighborIndex[i].size(); j++)
			{
				if (i == neighborIndex[i][j])
					continue;
				ResidualARAP* residual = new ResidualARAP(weightARAP, vertices[i].data(), vertices[neighborIndex[i][j]].data());
				ceres::AutoDiffCostFunction<ResidualARAP, 3, 3, 3, 3> *cost = new ceres::AutoDiffCostFunction<ResidualARAP, 3, 3, 3, 3>(residual);
				problem.AddResidualBlock(cost, NULL, dNodes[i].data(), dNodes[neighborIndex[i][j]].data(), nodeRot[i].data());
			}
		}
		////Landmark Con
		for (int i = 0; i < landmark_src_indicesForLC.size(); i++)
		{
			ResidualCon* residual = new ResidualCon(weightLandCon, target_vertices[landmark_dst_indicesForLC[i]].data(), vertices[landmark_src_indicesForLC[i]].data());
			ceres::AutoDiffCostFunction<ResidualCon, 3, 3> *cost = new ceres::AutoDiffCostFunction<ResidualCon, 3, 3>(residual);
			problem.AddResidualBlock(cost, NULL, dNodes[landmark_src_indicesForLC[i]].data());
		}

		////Landmark Normal Con
		//for (int i = 0; i < landmark_src_indicesForLNC.size(); i++)
		//{
		//	ResidualConNormal* residual = new ResidualConNormal(weightLandNormalCon, target_vertices[landmark_dst_indicesForLC[i]].data(), targetNormal[i].data(), vertices[targetIndex[i]].data());
		//	//ceres::AutoDiffCostFunction<ResidualConNormal, 1, 3> *cost = new ceres::AutoDiffCostFunction<ResidualConNormal, 1, 3>(residual);
		//	//problem.AddResidualBlock(cost, NULL, dNodes[targetIndex[i]].data());
		//}

		//Con_allVertices
		for (int i = 0; i < target.size(); i++)
		{
			ResidualCon* residual = new ResidualCon(weightCon, target[i].data(), vertices[targetIndex[i]].data());
			ceres::AutoDiffCostFunction<ResidualCon, 3, 3> *cost = new ceres::AutoDiffCostFunction<ResidualCon, 3, 3>(residual);
			problem.AddResidualBlock(cost, NULL, dNodes[targetIndex[i]].data());
		}

		//ConNormal
		for (int i = 0; i < target.size(); i++)
		{
			ResidualConNormal* residual = new ResidualConNormal(weightConNormal, target[i].data(), targetNormal[i].data(), vertices[targetIndex[i]].data());
			ceres::AutoDiffCostFunction<ResidualConNormal, 1, 3> *cost = new ceres::AutoDiffCostFunction<ResidualConNormal, 1, 3>(residual);
			problem.AddResidualBlock(cost, NULL, dNodes[targetIndex[i]].data());
		}

	} //energySetup()

	void energeyMinimization()
	{
		ceres::Solve(options, &problem, &summary);
	}

	void getDeformValue()
	{
		for (int i = 0; i < dNodes.size(); i++)
		{
			dVertices[i] = dNodes[i].cast<float>();
			verticeRots[i] = nodeRot[i].cast<float>();
		}
	}

	void showFinal()
	{
		//if (summary.termination_type == ceres::CONVERGENCE)
		//	std::cout << "energy minimization succeeded." << std::endl;
		//else if (summary.termination_type == ceres::NO_CONVERGENCE || summary.termination_type == ceres::FAILURE)
		//	std::cout << "energy minimization failed." << std::endl;

		std::cout << summary.BriefReport() << '\n';
	}

	double run()
	{
		energySetup();
		energeyMinimization();
		getDeformValue();
		showFinal();

		return summary.final_cost;
	}

	ceres::Solver::Summary summary;

private:
	double weightTV, weightARAP, weightLandCon, weightLandNormalCon, weightCon, weightConNormal;

	std::vector<Eigen::Vector3d> dNodes, nodeRot;
	std::vector<Eigen::Vector3f> &vertices, &dVertices, &verticeRots, &target_vertices;
	std::vector<std::vector<int>> &neighborIndex;

	std::vector<Eigen::Vector3f> &target, &targetNormal;
	std::vector<int> &targetIndex, &landmark_src_indicesForLC, &landmark_dst_indicesForLC, &landmark_src_indicesForLNC, &landmark_dst_indicesForLNC;

	ceres::Problem problem;
	ceres::Solver::Options options;

};
