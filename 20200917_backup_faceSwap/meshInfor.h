#pragma once

#include <math.h>
#include <vector>
#include <iostream>

typedef OpenMesh::PolyMesh_ArrayKernelT<>  MyMesh;

class LandmarkExceptContourEyebrows
{
public:
	LandmarkExceptContourEyebrows() {} // For ICP

	int landmarkExceptContourEyebrowsFromSrc[41] = { 45  , 1032, 31  ,27  ,115 ,88  ,24  ,575 ,602 ,362 ,379 ,376 ,7   ,369 ,366 ,534 ,863 ,866 ,
													849 ,853 ,856 ,79  ,119 ,117 ,25  ,604 ,606 ,566 ,565 ,673 ,26  ,186 ,78  ,259 ,307 ,43  ,794 ,
													746 ,638 ,34  ,151 };

	int landmarkExceptContourEyebrowsFromDst[41] = { 13407,16851,20049,22509,26188,26682,27175,26693,26209,15325,13119,13374,14857,15589,15826,14908,
													13439,13202,15424,15907,15652,33375,30926,30013,30250,30023,30949,33413,34700,35120,35115,35109,
													34678,33160,31838,31618,31849,33410,33619,33395,33607 };

};

class LandmarkExceptContour
{
public:
	LandmarkExceptContour() {}  // For LC energy term

	/*int landmarkExceptContourFromSrc[51] = { 236 ,167 ,131 ,195 ,168 ,655 ,682 ,618 ,654 ,723 ,45  ,1032,31  ,27  ,115 ,88  ,24  ,575 ,602 ,362 ,
											379 ,376 ,7   ,369 ,366 ,534 ,863 ,866 ,849 ,853 ,856 ,79  ,119 ,117 ,25  ,604 ,606 ,566 ,565 ,673 ,
											26  ,186 ,78  ,259 ,307 ,43  ,794 ,746 ,638 ,34  ,151 };*/

	//int landmarkExceptContourFromSrc[51] = { 236 ,167 ,131 ,195 ,168 ,655 ,682 ,618 ,654 ,723 , // eyebrows
	//										45  ,1032,31  ,27  ,115 ,88  ,24  ,575 ,602 , //nose
	//										209 ,205 ,273 ,1050   ,229 ,227 ,1048 ,760 ,692 ,696 ,714 ,716 , //eyes
	//										79  ,119 ,117 ,25  ,604 ,606 ,566 ,565 ,673 , 26  ,186 ,78  , // lip outer
	//										259 ,307 ,43  ,794 ,746 ,638 ,34  ,151 
	//										};

	int landmarkExceptContourFromSrc[59] = { 236 ,167 ,131 ,195 ,168 ,655 ,682 ,618 ,654 ,723 , // eyebrows
											45  ,1032,31  ,27  ,115 ,88  ,24  ,575 ,602 , //nose
											209 ,205 ,273 ,1050   ,229 ,227 ,1048 ,760 ,692 ,696 ,714 ,716 , //eyes
											79  ,119 ,117 ,25  ,604 ,606 ,566 ,565 ,673 , 26  ,186 ,78  , // lip outer
											259 ,307 ,43  ,794 ,746 ,638 ,34  ,151,
											1039, 1046, 470, 506, 1043, 1045, 957, 993// add nose
	};


	

	/*int landmarkExceptContourFromDst[51] = { 10877,9168 ,8444 ,8702 ,9203 ,9246 ,8763 ,8529 ,9281 ,11016,13407,16851,20049,22509,26188,26682,
											27175,26693,26209,15325,13119,13374,14857,15589,15826,14908,13439,13202,15424,15907,15652,33375,
											30926,30013,30250,30023,30949,33413,34700,35120,35115,35109,34678,33160,31838,31618,31849,33410,
											33619,33395,33607 };*/
	//int landmarkExceptContourFromDst[51] = { 10877,9168 ,8444 ,8702 ,9203 ,9246 ,8763 ,8529 ,9281 ,11016, // eyebrows
	//										13407,16851,20049,22509,26188,26682,27175,26693,26209,  //nose
	//										15560,12133,12391,14614,16819,17301,14906,12701,12466,15679,17138,16637, //eyes
	//										33375, 30926,30013,30250,30023,30949,33413,34700,35120,35115,35109,34678, //lip outer
	//										33160,31838,31618,31849,33410, 33619,33395,33607 
	//										};


	// temp current
	//int landmarkExceptContourFromDst[51] = { 10877,9168 ,8444 ,8702 ,9203 ,9246 ,8763 ,8529 ,9281 ,11016, // eyebrows
	//										13407,16851,20049,22509,26188,26682,27175,26693,26209,  //nose
	//										15560,12133,12391,14614,16819,17301,14906,12701,12466,15679,17138,16637, //eyes
	//										33375, 30926,30013,30250,30023,30949,33413,34700,35120,35115,35109,34678, //lip outer
	//										33160,31838,31618,31849,33410, 32741,32514,32950
	//};

	//int landmarkExceptContourFromDst[51] = { 10877,9168 ,8444 ,8702 ,9203 ,9246 ,8763 ,8529 ,9281 ,11016, // eyebrows
	//										13407,16851,20049,22509,26188,26682,27175,26693,26209,  //nose
	//										15559,11641,11899,14615,17311,17793,14905,11963,11729,15680,18123,17621, //eyes
	//										33375, 30926,30013,30250,30023,30949,33413,34700,35120,35115,35109,34678, //lip outer
	//										33160,31838,31618,31849,33410, 32741,32514,32950
	//};

	int landmarkExceptContourFromDst[59] = { 10877,9168 ,8444 ,8702 ,9203 ,9246 ,8763 ,8529 ,9281 ,11016, // eyebrows
											13407,16851,20049,22509,26188,26682,27175,26693,26209,  //nose
											15559,11641,11899,14615,17311,17793,14905,11963,11729,15680,18123,17621, //eyes
											33375, 30926,30013,30250,30023,30949,33413,34700,35120,35115,35109,34678, //lip outer
											33160,31838,31618,31849,33410, 32741,32514,32950,
											15611, 17825, 20777, 24457, 15381, 17350, 20302, 24493
	};

	
};

class LandmarkExceptContourEyes
{
public:
	LandmarkExceptContourEyes() {} // For LNC energy term (no implementation)

	int landmarkExceptContourEyesFromSrc[39] = { 236 ,167 ,131 ,195 ,168 ,655 ,682 ,618 ,654 ,723 ,45  ,1032,31  ,27  ,115 ,88  ,24  ,575 ,
												602 ,79  ,119 ,117 ,25  ,604 ,606 ,566 ,565 ,673 ,26  ,186 ,78  ,259 ,307 ,43  ,794 ,746 ,
												638 ,34  ,151 };

	int landmarkExceptContourEyesFromDst[39] = { 10877,9168 ,8444 ,8702 ,9203 ,9246 ,8763 ,8529 ,9281 ,11016,13407,16851,20049,22509,
												26188,26682,27175,26693,26209,33375,30926,30013,30250,30023,30949,33413,34700,35120,
												35115,35109,34678,33160,31838,31618,31849,33410,33619,33395,33607 };

};

class EyeInside
{
public:
	EyeInside() {}

	/*int eyeInsideFromSrc[90] = { 526 ,524 ,6   ,5   ,4   ,2   ,3   ,525 ,15  ,14  ,13  ,12  ,10  ,11  ,18  ,17  ,16  ,21  ,1   ,0   ,19  ,
								8   ,20  ,383 ,382 ,381 ,380 ,378 ,377 ,375 ,374 ,9   ,371 ,370 ,368 ,367 ,365 ,364 ,363 ,1013,1011,545 ,
								538 ,537 ,539 ,540 ,541 ,542 ,1012,530 ,529 ,531 ,532 ,533 ,546 ,535 ,536 ,547 ,862 ,861 ,864 ,865 ,867 ,
								868 ,869 ,544 ,543 ,870 ,548 ,528 ,527 ,850 ,851 ,852 ,854 ,855 ,857 ,858 ,362 ,379 ,376 ,7   ,369 ,366 ,
								534 ,863 ,866 ,849 ,853 ,856 };*/

	int eyeInsideFromSrc[182] = { 526 ,524 ,6   ,5   ,4   ,2   ,3   ,525 ,15  ,14  ,13  ,12  ,10  ,11  ,18  ,17  ,16  ,21  ,1   ,0   ,19  ,
								8   ,20  ,383 ,382 ,381 ,380 ,378 ,377 ,375 ,374 ,9   ,371 ,370 ,368 ,367 ,365 ,364 ,363 ,1013,1011,545 ,
								538 ,537 ,539 ,540 ,541 ,542 ,1012,530 ,529 ,531 ,532 ,533 ,546 ,535 ,536 ,547 ,862 ,861 ,864 ,865 ,867 ,
								868 ,869 ,544 ,543 ,870 ,548 ,528 ,527 ,850 ,851 ,852 ,854 ,855 ,857 ,858 ,362 ,379 ,376 ,7   ,369 ,366 ,
								534 ,863 ,866 ,849 ,853 ,856,   //current
								248, 283, 249, 291, 250, 251, 252, 237, 238, 239, 240, 241, 242, 243, 244, 301, 245, 300, 372, 246, 373,  //add right eyeInside
								292, 247, 275, 218, 282, 219, 290, 220, 221, 231, 222, 223, 224, 225, 226, 227, 228, 229, 302, 230, 299, 1050, 293, 217, 274,  //right eyeInside end
								734, 762, 735, 770, 736, 778, 737, 738, 739, 724, 725, 726, 727, 728, 729, 730, 731, 788, 732, 787, 859, 733, 860, //add left eyeInside
								779, 780, 704, 761, 705, 769, 706, 777, 707, 708, 718, 709, 710, 711, 712, 713, 714, 715, 716, 789, 717, 786, 1048 //add left eyeInside end
	};
	

};

class EyebollForTexture
{
public:
	EyebollForTexture() {}  // Texture mapping for eyeball
		
	int eyeBallForTextureFromSrc[28] = { 524, 18, 11, 10, 12, 13, 14, 15, 525, 3, 2, 4, 5, 6, // right eye
										1012, 542, 541, 540, 539, 537, 538, 545, 1011, 533, 532, 531, 529, 530 //left eye
										};  

	int eyeBallForTextureFromDst[28] = { 15101, 14361, 14113, 13864, 13860, 14103, 14347, 14837, 15328, 15822, 15824, 15827, 15830, 15588, //right eye
										14927, 14188, 13940, 13692, 13689, 13686, 13929, 14418, 15154, 15406, 15410, 15413, 15415, 15417 // left eye
										};

};

class FaceBoundary
{
public:
	FaceBoundary() {}  // Texture mapping for face boundary

	int faceBoundaryIndexFromLineOne[48] = { 22, 554, 755, 556, 763, 557, 771, 558, 560, 559, 552, 553, 555, 551, 810, 549,
											822, 550, 955, 924, 754, 753, 752, 751, 44, 264, 265, 266, 267, 437, 468, 63,
											335, 62, 323, 64, 68, 66, 65, 72, 73, 71, 284, 70, 276, 69, 268, 67	};

	int faceBoundaryIndexFromLineTwo[48] = { 54, 904, 903, 923, 922, 921, 920, 919, 918, 917, 963, 976, 975, 974,
											973, 972, 971, 970, 969, 968, 967, 966, 965, 964, 57, 477, 478, 479,
											480, 481, 482, 483, 484, 485, 486, 487, 488, 489, 476, 430, 431, 432,
											433, 434, 435, 436, 416, 417 };

	int faceBoundaryIndexFromRef[48] = { 53, 887, 888, 889, 890, 891, 892, 893, 961, 962, 916, 915, 914, 913, 912,
										911, 910, 909, 954, 925, 908, 907, 906, 905, 55, 418, 419, 420, 421, 438,
										467, 422, 423, 424, 425, 426, 427, 428, 429, 475, 474, 406, 405, 404, 403,
										402, 401, 400 };

	int faceBoundaryIndexFromRef2[48] = { 39, 655, 756, 682, 764, 618, 772, 654, 621, 723, 834, 894, 895, 896, 897, 898,
										899, 900, 953, 926, 901, 902, 885, 886, 52, 399, 398, 415, 414, 439, 466, 413,
										412, 411, 410, 409, 408, 407, 347, 236, 134, 167, 285, 131, 277, 195, 269, 168	};
	

	/////////////////////////
	//separate forehead from all
	int foreheadBoundaryFromLineOne[19] = { 22, 554, 755, 556, 763, 557, 771, 558, 560, 559, 72, 73, 71, 284, 70, 276, 69, 268, 67 };
	int foreheadBoundaryFromLineTwo[19] = { 54, 904, 903, 923, 922, 921, 920, 919, 918, 917, 430, 431, 432, 433, 434, 435, 436, 416, 417 };
	int foreheadBoundaryIndexFromRef[19] = { 53, 887, 888, 889, 890, 891, 892, 893, 961, 962, 475, 474, 406, 405, 404, 403,	402, 401, 400 };
	int foreheadBoundaryIndexFromRef2[19] = { 39, 655, 756, 682, 764, 618, 772, 654, 621, 723, 236, 134, 167, 285, 131, 277, 195, 269, 168 };


	int sideBoundaryFromLineOne[29] = { 552, 553, 555, 551, 810, 549, 822, 550, 955, 924, 754, 753, 752, 751, 44, 264, 265, 266, 267, 437, 468, 63,
										335, 62, 323, 64, 68, 66, 65 };
	int sideBoundaryFromLineTwo[29] = { 963, 976, 975, 974,	973, 972, 971, 970, 969, 968, 967, 966, 965, 964, 57, 477, 478, 479,
										480, 481, 482, 483, 484, 485, 486, 487, 488, 489, 476 };
	int sideBoundaryIndexFromRef[29] = { 916, 915, 914, 913, 912, 911, 910, 909, 954, 925, 908, 907, 906, 905, 55, 418, 419, 420, 421, 438,
										467, 422, 423, 424, 425, 426, 427, 428, 429 };
	int sideBoundaryIndexFromRef2[29] = { 834, 894, 895, 896, 897, 898, 899, 900, 953, 926, 901, 902, 885, 886, 52, 399, 398, 415, 414, 439, 466, 413,
										412, 411, 410, 409, 408, 407, 347 };


};

typedef struct {
	std::vector<cv::Vec3i> face_index;
	std::vector<cv::Vec3i> face_vt_index;
	std::vector<cv::Vec3i> face_vn_index;
	std::vector<cv::Vec2d> vertex_normal;
	std::vector<cv::Vec2d> vertex_texture;
	std::vector<cv::Vec3d> vertex_position;
	std::vector<cv::Vec3i> vertex_color;
} Mesh_infor;

typedef struct {
	size_t num_vertices;
	size_t num_faces;
	float *vertices;                   /// [xyz] * num_vertices
	float *facevarying_normals;        /// [xyz] * 3(triangle) * num_faces
	float *facevarying_tangents;       /// [xyz] * 3(triangle) * num_faces
	float *facevarying_binormals;      /// [xyz] * 3(triangle) * num_faces
	float *facevarying_uvs;            /// [xyz] * 3(triangle) * num_faces
	float *facevarying_vertex_colors;  /// [xyz] * 3(triangle) * num_faces
	unsigned int *faces;               /// triangle x num_faces
	unsigned int *material_ids;        /// index x num_faces
} Mesh;

struct float3 {
	float3() {}
	float3(float xx, float yy, float zz) {
		x = xx;
		y = yy;
		z = zz;
	}
	float3(const float *p) {
		x = p[0];
		y = p[1];
		z = p[2];
	}

	float3 operator*(float f) const { return float3(x * f, y * f, z * f); }
	float3 operator-(const float3 &f2) const {
		return float3(x - f2.x, y - f2.y, z - f2.z);
	}
	float3 operator*(const float3 &f2) const {
		return float3(x * f2.x, y * f2.y, z * f2.z);
	}
	float3 operator+(const float3 &f2) const {
		return float3(x + f2.x, y + f2.y, z + f2.z);
	}
	float3 &operator+=(const float3 &f2) {
		x += f2.x;
		y += f2.y;
		z += f2.z;
		return (*this);
	}
	float3 operator/(const float3 &f2) const {
		return float3(x / f2.x, y / f2.y, z / f2.z);
	}
	float operator[](int i) const { return (&x)[i]; }
	float &operator[](int i) { return (&x)[i]; }

	float3 neg() { return float3(-x, -y, -z); }

	float length() { return sqrtf(x * x + y * y + z * z); }

	void normalize() {
		float len = length();
		if (std::fabs(len) > 1.0e-6f) {
			float inv_len = 1.0f / len;
			x *= inv_len;
			y *= inv_len;
			z *= inv_len;
		}
	}

	float x, y, z;
	// float pad;  // for alignment
};

class meshInfor {

public:
	void readMesh(std::string filename, MyMesh &mesh)
	{
		OpenMesh::IO::Options opt;
		opt += OpenMesh::IO::Options::VertexNormal;
		opt += OpenMesh::IO::Options::FaceNormal;
		//opt += OpenMesh::IO::Options::VertexColor;
		opt += OpenMesh::IO::Options::VertexTexCoord;

		mesh.request_vertex_normals();
		mesh.request_face_normals();
		//mesh.request_vertex_colors();
		mesh.request_vertex_texcoords2D();
		//mesh.request_halfedge_texcoords2D();
		////mesh.texcoord2D();

		std::cout << "Loading from file '" << filename << "'\n";
		if (OpenMesh::IO::read_mesh(mesh, filename, opt))
		{
			// update face and vertex normals     
			if (!opt.check(OpenMesh::IO::Options::FaceNormal))
				mesh.update_face_normals();
			else
				std::cout << "File provides face normals\n";

			if (!opt.check(OpenMesh::IO::Options::VertexNormal))
				mesh.update_vertex_normals();
			else
				std::cout << "File provides vertex normals\n";

			if (!opt.check(OpenMesh::IO::Options::VertexColor))
				std::cout << "File provides vertex color\n";

			/*if (!opt.check(OpenMesh::IO::Options::VertexTexCoord))
				std::cout << "File provides vertex coord\n";*/
			if (mesh.has_vertex_texcoords2D())
				std::cout << "File provides vertex coord\n";

		}
		else
		{
			std::cout << "Failed to load file.";
			exit(1);
		}

		// mesh info
		std::clog << "mesh: " << mesh.n_vertices() << " vertices, "
			<< mesh.n_edges() << " edge, "
			<< mesh.n_faces() << " faces\n";
	}

	void LoadObjInfor(Mesh_infor &mesh_infor, const char *filename, int vertexFlag)
	{
		double v_x, v_y, v_z;
		double v_r, v_g, v_b;
		int V_R, V_G, V_B;
		double vt_x, vt_y;
		int f_1, f_2, f_3;
		int f_vt_1, f_vt_2, f_vt_3;

		char *pStr;
		char buffer[255];

		FILE *fp_meshInfor = fopen(filename, "r");

		while (!feof(fp_meshInfor))
		{
			pStr = fgets(buffer, sizeof(buffer), fp_meshInfor);

			if (pStr == NULL)
				break;

			if (vertexFlag == 0)  // no vertex color
			{
				if (pStr[0] == 'v' && pStr[1] == ' ') {
					sscanf(buffer, "v %lf %lf %lf", &v_x, &v_y, &v_z);
					mesh_infor.vertex_position.push_back(cv::Vec3d(v_x, v_y, v_z));
				}
			}
			else {  // need vertex color 
				if (pStr[0] == 'v' && pStr[1] == ' ') {
					sscanf(buffer, "v %lf %lf %lf %lf %lf %lf", &v_x, &v_y, &v_z, &v_r, &v_g, &v_b);
					mesh_infor.vertex_position.push_back(cv::Vec3d(v_x, v_y, v_z));
					V_R = int(255 * v_r);
					V_G = int(255 * v_g);
					V_B = int(255 * v_b);
					mesh_infor.vertex_color.push_back(cv::Vec3d(V_R, V_G, V_B));
				}
			}


			if (pStr[0] == 'v' && pStr[1] == 't') {
				sscanf(buffer, "vt %lf %lf", &vt_x, &vt_y);
				mesh_infor.vertex_texture.push_back(cv::Vec2d(vt_x, vt_y));
			}

			if (pStr[0] == 'f' && pStr[1] == ' ') {
				sscanf(buffer, "f %d/%d %d/%d %d/%d", &f_1, &f_vt_1, &f_2, &f_vt_2, &f_3, &f_vt_3);
				mesh_infor.face_index.push_back(cv::Vec3i(f_1, f_2, f_3));
				mesh_infor.face_vt_index.push_back(cv::Vec3i(f_vt_1, f_vt_2, f_vt_3));
			}

		}

		fclose(fp_meshInfor);

	}
};
