#pragma warning(disable:4996)
//#define _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES
#include <iostream>
#include <chrono>
#include <ctime>

#include <omp.h>

#include "glog/logging.h"
#include "ceres/ceres.h"
#include "ceres/rotation.h"

#include <opencv2/opencv.hpp>
#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>

#include "faceScaledRigid.h"
#include "faceDeform.h"
#include "meshInfor.h"
#include "nanort.h"

#include <windows.h>
#include <omp.h>

//#include <unordered_map>
//typedef OpenMesh::PolyMesh_ArrayKernelT<>  MyMesh;

using namespace std;

int main(int argc, char* argv[])
{
	std::chrono::time_point<std::chrono::system_clock> start, end;
	start = std::chrono::system_clock::now();

	////////////////////mesh information/////////
	meshInfor mesh_infor;

/////mesh reading
	MyMesh src_mesh, tar_mesh;
	std::string src_mesh_file = argv[1];
	std::string tar_mesh_file = argv[2];
	mesh_infor.readMesh(src_mesh_file, src_mesh);
	mesh_infor.readMesh(tar_mesh_file, tar_mesh);

	/*MyMesh::VertexIter aaa;
	OpenMesh::Vec2f bbb;
	std::vector<OpenMesh::Vec2f> ccc;
	for (aaa = tar_mesh.vertices_begin(); aaa != tar_mesh.vertices_end(); ++aaa)
	{
		bbb = tar_mesh.texcoord2D(aaa.handle());
		ccc.push_back(bbb);
	}*/	

	////// get mesh information
	//Mesh_infor src_objInfor, dst_objInfor;
	//mesh_infor.LoadObjInfor(src_objInfor, src_mesh_file.c_str(), 0);
	//mesh_infor.LoadObjInfor(dst_objInfor, tar_mesh_file.c_str(), 1);

	/////////landmark setting
	int num_landmarkForICP = atoi(argv[3]);
	int landmark_index;
	//landmark setting
	std::vector<int> landmark_src_indices;
	std::vector<int> landmark_dst_indices;

	LandmarkExceptContourEyebrows landmarkExceptContourEyebrows;
	   
	//src_landmark
	for (int i = 0; i < num_landmarkForICP; i++)
	{
		landmark_src_indices.push_back(landmarkExceptContourEyebrows.landmarkExceptContourEyebrowsFromSrc[i]);
	}

	//dst_landmark
	for (int i = 0; i < num_landmarkForICP; i++)
	{
		landmark_dst_indices.push_back(landmarkExceptContourEyebrows.landmarkExceptContourEyebrowsFromDst[i]);
	}

	/////////eyeball control
	int num_eyeballForTex = atoi(argv[8]);
	EyebollForTexture eyeballForTex;

	int *eyeball_tarIndex = new int[src_mesh.n_vertices()];

	int *eyeBall_bool = new int[src_mesh.n_vertices()];
	for (int i = 0; i < src_mesh.n_vertices(); i++)
	{
		eyeBall_bool[i] = 0;
	}
	for (int i = 0; i < num_eyeballForTex; i++)
	{
		eyeBall_bool[eyeballForTex.eyeBallForTextureFromSrc[i]] = 1;
		eyeball_tarIndex[eyeballForTex.eyeBallForTextureFromSrc[i]] = eyeballForTex.eyeBallForTextureFromDst[i];

	}

	////// setting for calculation
	MyMesh::VertexIter t_vIt;
	OpenMesh::Vec3f t_v;
	vector<Eigen::Vector3f> t_src, t_target;

	t_src.resize(src_mesh.n_vertices());
	t_target.resize(tar_mesh.n_vertices());

	for (t_vIt = src_mesh.vertices_begin(); t_vIt != src_mesh.vertices_end(); ++t_vIt)
	{
		t_v = src_mesh.point(t_vIt.handle());
		t_src[t_vIt->idx()] = Eigen::Vector3f(t_v[0], t_v[1], t_v[2]);
	}

	for (t_vIt = tar_mesh.vertices_begin(); t_vIt != tar_mesh.vertices_end(); ++t_vIt)
	{
		t_v = tar_mesh.point(t_vIt.handle());
		t_target[t_vIt->idx()] = Eigen::Vector3f(t_v[0], t_v[1], t_v[2]);
	}

	////////////// ICP calculation
	double ICP_scale = 1.0;
	double *ICP_rotation, *ICP_translation;

	ScaledRigid E_SICP(t_src, t_target, landmark_src_indices, landmark_dst_indices, num_landmarkForICP);
	double ICP_Cost = E_SICP.run();

	ICP_scale = E_SICP.getScale();
	ICP_rotation = E_SICP.getRotation();
	ICP_translation = E_SICP.getTranslation();

	/////ICP Result check
	double ICP_rotation_matrix[9];
	ceres::AngleAxisToRotationMatrix(ICP_rotation, ICP_rotation_matrix);

	Eigen::Matrix4f ICP_transformed_matrix;
	Eigen::Matrix4f ICP_scale_matrix;

	ICP_transformed_matrix(0, 0) = float(ICP_rotation_matrix[0]); ICP_transformed_matrix(0, 1) = float(ICP_rotation_matrix[3]); ICP_transformed_matrix(0, 2) = float(ICP_rotation_matrix[6]); ICP_transformed_matrix(0, 3) = float(ICP_translation[0]);
	ICP_transformed_matrix(1, 0) = float(ICP_rotation_matrix[1]); ICP_transformed_matrix(1, 1) = float(ICP_rotation_matrix[4]); ICP_transformed_matrix(1, 2) = float(ICP_rotation_matrix[7]); ICP_transformed_matrix(1, 3) = float(ICP_translation[1]);
	ICP_transformed_matrix(2, 0) = float(ICP_rotation_matrix[2]); ICP_transformed_matrix(2, 1) = float(ICP_rotation_matrix[5]); ICP_transformed_matrix(2, 2) = float(ICP_rotation_matrix[8]); ICP_transformed_matrix(2, 3) = float(ICP_translation[2]);
	ICP_transformed_matrix(3, 0) = 0.0f;						  ICP_transformed_matrix(3, 1) = 0.0f;							ICP_transformed_matrix(3, 2) = 0.0f;						  ICP_transformed_matrix(3, 3) = 1.0f;

	ICP_scale_matrix(0, 0) = float(ICP_scale);		ICP_scale_matrix(0, 1) = 0.0f;					ICP_scale_matrix(0, 2) = 0.0f;					ICP_scale_matrix(0, 3) = 0.0f;
	ICP_scale_matrix(1, 0) = 0.0f;					ICP_scale_matrix(1, 1) = float(ICP_scale);		ICP_scale_matrix(1, 2) = 0.0f;					ICP_scale_matrix(1, 3) = 0.0f;
	ICP_scale_matrix(2, 0) = 0.0f;					ICP_scale_matrix(2, 1) = 0.0f;					ICP_scale_matrix(2, 2) = float(ICP_scale);		ICP_scale_matrix(2, 3) = 0.0f;
	ICP_scale_matrix(3, 0) = 0.0f;					ICP_scale_matrix(3, 1) = 0.0f;					ICP_scale_matrix(3, 2) = 0.0f;					ICP_scale_matrix(3, 3) = 1.0f;

	MyMesh ICP_tar_mesh = tar_mesh;

	std::vector<Eigen::Vector3f> target_vertices;  //transformed ICP mesh
	target_vertices.resize(ICP_tar_mesh.n_vertices());

	for (t_vIt = ICP_tar_mesh.vertices_begin(); t_vIt != ICP_tar_mesh.vertices_end(); ++t_vIt)
	{
		t_v = ICP_tar_mesh.point(t_vIt.handle());
		Eigen::Vector4f target_transformed_points = Eigen::Vector4f(t_v[0], t_v[1], t_v[2], 1.0);


		target_transformed_points = ICP_scale_matrix * ICP_transformed_matrix * target_transformed_points;

		target_transformed_points(0) = target_transformed_points(0) / target_transformed_points(3);
		target_transformed_points(1) = target_transformed_points(1) / target_transformed_points(3);
		target_transformed_points(2) = target_transformed_points(2) / target_transformed_points(3);

		ICP_tar_mesh.point(t_vIt.handle()) = OpenMesh::Vec3f(target_transformed_points(0), target_transformed_points(1), target_transformed_points(2));

		target_vertices[t_vIt->idx()] = Eigen::Vector3f(target_transformed_points(0), target_transformed_points(1), target_transformed_points(2));

	}

	ICP_tar_mesh.update_face_normals();
	ICP_tar_mesh.update_vertex_normals();
	//ICP_tar_mesh.vertex_colors

	//ICP_mesh_check

	OpenMesh::IO::Options opt;
	opt += OpenMesh::IO::Options::VertexTexCoord;
	//opt += OpenMesh::IO::Options::VertexNormal;
	//opt += OpenMesh::IO::Options::FaceNormal;
	//opt += OpenMesh::IO::Options::VertexColor;

	OpenMesh::IO::write_mesh(ICP_tar_mesh, "ICP_tar_mesh.obj", opt);

	std::cout << "scale: " << ICP_scale << std::endl;
	std::cout << "rotation: " << ICP_rotation[0] << " " << ICP_rotation[1] << " " << ICP_rotation[2] << std::endl;
	std::cout << "translation:" << ICP_translation[0] << " " << ICP_translation[1] << "  " << ICP_translation[2] << std::endl;

	/////////////////////////////////////////
	//////////////face mapping//////////////

	int mapping_count = atoi(argv[4]);
	int eyeRegion_num = atoi(argv[5]);
	int num_landmarkForLC = atoi(argv[6]);
	int num_landmarkForLNC = atoi(argv[7]);

	int eyeRegionIndex;
	int *eyeRegion_bool = new int[src_mesh.n_vertices()];

	//int *landmarkPoints_bool = new int[src_mesh.n_vertices()];

	for (int i = 0; i < src_mesh.n_vertices(); i++)
	{
		eyeRegion_bool[i] = 0;
	}

	//read eye region
	EyeInside eyeInside;
	for (int i = 0; i < eyeRegion_num; i++)
	{
		eyeRegion_bool[eyeInside.eyeInsideFromSrc[i]] = 1;
	}

	OpenMesh::Vec3f v, v_normal;
	MyMesh::FaceIter fIt;
	MyMesh::FaceVertexIter fvIt;
	MyMesh::VertexIter vIt;
	MyMesh::VertexVertexIter vvIt;


	// setup data
	std::vector<Eigen::Vector3f> dVertices, verticeRots;
	dVertices.resize(src_mesh.n_vertices());
	verticeRots.resize(src_mesh.n_vertices());
	std::vector<std::vector<int>> neighborIndices;
	neighborIndices.resize(t_src.size());

	// set vertices and their neighbors
	for (vIt = src_mesh.vertices_begin(); vIt != src_mesh.vertices_end(); ++vIt)
	{
		dVertices[vIt->idx()] = Eigen::Vector3f(0, 0, 0);
		verticeRots[vIt->idx()] = Eigen::Vector3f(0, 0, 0);

		for (vvIt = src_mesh.vv_iter(vIt.handle()); vvIt; ++vvIt)
			neighborIndices[vIt->idx()].push_back(vvIt->idx());
	}

	MyMesh outputMesh = src_mesh;

	double weightTV, weightARAP, weightLandCon, weightLandNormalCon, weightCon, weightConNormal, disparity;
	weightTV = 1;
	weightARAP = 1;
	weightLandCon = 2.0;
	weightLandNormalCon = 1.5;
	weightCon = 1;
	weightConNormal = 1;
	disparity = 3.0;

	// build kdtree for target shape
	std::vector<cv::Point3f> points;
	for (vIt = ICP_tar_mesh.vertices_begin(); vIt != ICP_tar_mesh.vertices_end(); ++vIt)
	{
		v = ICP_tar_mesh.point(vIt.handle());
		points.push_back(cv::Point3f(v[0], v[1], v[2]));
		//target_vertices[vIt->idx()] = Eigen::Vector3f(v[0], v[1], v[2]);
	}

	cv::flann::KDTreeIndexParams indexParams;
	cv::flann::Index kdtree(cv::Mat(points).reshape(1), indexParams);

	for (int i = 0; i < mapping_count; i++)
	{
		std::vector<Eigen::Vector3f> targets, targetNormals;
		std::vector<int> targetIndices;

		OpenMesh::Vec3f vMean, vnMean, vTarget, vnTarget;

		for (vIt = outputMesh.vertices_begin(); vIt != outputMesh.vertices_end(); ++vIt)
		{
			int idx = vIt.handle().idx();

			if (eyeRegion_bool[idx] == 1)
				continue;

			vMean = outputMesh.point(vIt.handle());
			vnMean = outputMesh.normal(vIt.handle());

			std::vector<float> query;
			query.push_back(vMean[0]);
			query.push_back(vMean[1]);
			query.push_back(vMean[2]);
			std::vector<int> indices;
			std::vector<float> dists;
			kdtree.knnSearch(query, indices, dists, 1);

			OpenMesh::VertexHandle vhTarget = ICP_tar_mesh.vertex_handle(indices[0]);
			vTarget = ICP_tar_mesh.point(vhTarget);
			vnTarget = ICP_tar_mesh.normal(vhTarget);

			if (sqrt(dists[0]) < 5 && OpenMesh::dot(vnMean.normalize(), vnTarget.normalize()) > 0.7)
			{
				targetIndices.push_back(vIt.handle().idx());  // src에 대해서 target에 일치점이 있다고 가정되는 src index 
				if (eyeBall_bool[idx] == 1)
				{
					printf("checking");
					targets.push_back(Eigen::Vector3f(vTarget[0], vTarget[1], vTarget[2]-3.0));  //일치점이 있다고 가정되는 src index에 매핑되는 target position
				}	
				else
				{
					targets.push_back(Eigen::Vector3f(vTarget[0], vTarget[1], vTarget[2]));
				}
				targetNormals.push_back(Eigen::Vector3f(vnTarget[0], vnTarget[1], vnTarget[2]));
			}

		}

		std::cout << "matching: " << targets.size() << " correspondences found.\n";

		//////////////////////////////////////////////////////
		/////////////// Landmark Setting/////////////////////
		////////////////////////////////////////////////////

		std::vector<int> landmark_src_indicesForLC;
		std::vector<int> landmark_dst_indicesForLC;

		LandmarkExceptContour landmarkExceptContour;

		for (int kk = 0; kk < num_landmarkForLC; kk++)
		{
			landmark_src_indicesForLC.push_back(landmarkExceptContour.landmarkExceptContourFromSrc[kk]);
			//landmarkPoints_bool[landmarkExceptContour.landmarkExceptContourFromSrc[kk]] = 1;
		}

		for (int kk = 0; kk < num_landmarkForLC; kk++)
		{
			landmark_dst_indicesForLC.push_back(landmarkExceptContour.landmarkExceptContourFromDst[kk]);
		}

		////////landmark except contour eyes
		std::vector<int> landmark_src_indicesForLNC;
		std::vector<int> landmark_dst_indicesForLNC;

		LandmarkExceptContourEyes landmarkExceptContourEyes;

		for (int kk = 0; kk < num_landmarkForLNC; kk++)
		{
			landmark_src_indicesForLNC.push_back(landmarkExceptContourEyes.landmarkExceptContourEyesFromSrc[kk]);
		}
		for (int kk = 0; kk < num_landmarkForLNC; kk++)
		{
			landmark_dst_indicesForLNC.push_back(landmarkExceptContourEyes.landmarkExceptContourEyesFromDst[kk]);
		}


		/////////////////////////////////// Fitting energy calculation//////////////////////////////
		fitting_deform E(weightTV, weightARAP, weightLandCon, weightLandNormalCon, weightCon, weightConNormal, t_src, dVertices, verticeRots, neighborIndices,
			targets, targetNormals, targetIndices, target_vertices, landmark_src_indicesForLC, landmark_dst_indicesForLC, landmark_src_indicesForLNC, landmark_dst_indicesForLNC);

		double finalCost = E.run();

		for (vIt = outputMesh.vertices_begin(); vIt != outputMesh.vertices_end(); ++vIt)  // 전체 변형
		{
			int idx = vIt->idx();
			Eigen::Vector3f point = t_src[idx] + dVertices[idx];
			//if (eyeBall_bool[idx] == 1 || idx == 526 || idx == 1013)
			//{
			//	outputMesh.point(vIt.handle()) = OpenMesh::Vec3f(point.x(), point.y(), point.z()-disparity);
			////}
			//else
			////{
				outputMesh.point(vIt.handle()) = OpenMesh::Vec3f(point.x(), point.y(), point.z());
			//}
			
		}

		//// update vertex normal
		outputMesh.update_face_normals();
		outputMesh.update_vertex_normals();

		// save mesh
		//OpenMesh::IO::Options writeOpt = OpenMesh::IO::Options::VertexTexCoord;

		/*if (!OpenMesh::IO::write_mesh(outputMesh, "output_" + std::to_string(i) + ".obj"))
		{
			std::cerr << "write error\n";
			exit(1);
		}*/

		if (i == mapping_count - 1)
		{
			// save mesh
			//OpenMesh::IO::Options writeOpt = OpenMesh::IO::Options::VertexNormal;

			/*if (!OpenMesh::IO::write_mesh(outputMesh, "output_" + std::to_string(i) + ".obj"))
			{
				std::cerr << "write error\n";
				exit(1);
			}*/

			//////////std::vector<cv::Vec3i> face_index;
			//////////std::vector<cv::Vec3i> face_vt_index;
			//////////std::vector<cv::Vec2d> vertex_texture;

			//////////double vt_x, vt_y;
			//////////int f_1, f_2, f_3;
			//////////int f_vt_1, f_vt_2, f_vt_3;

			//////////char *pStr;
			//////////char buffer[255];
			//////////FILE *fp_objInfor = fopen("obj_infor.txt", "r");

			//////////while (!feof(fp_objInfor))
			//////////{
			//////////	pStr = fgets(buffer, sizeof(buffer), fp_objInfor);

			//////////	if (pStr == NULL)
			//////////		break;

			//////////	/*if (pStr[0] == 'v' && pStr[1] == ' ') {
			//////////		sscanf(buffer, "v %lf %lf %lf", &v_x, &v_y, &v_z);
			//////////		vertex_position.push_back(cv::Vec3d(v_x, v_y, v_z));
			//////////	}*/

			//////////	if (pStr[0] == 'v' && pStr[1] == 't') {
			//////////		sscanf(buffer, "vt %lf %lf", &vt_x, &vt_y);
			//////////		vertex_texture.push_back(cv::Vec2d(vt_x, vt_y));
			//////////	}

			//////////	if (pStr[0] == 'f' && pStr[1] == ' ') {
			//////////		sscanf(buffer, "f %d/%d %d/%d %d/%d", &f_1, &f_vt_1, &f_2, &f_vt_2, &f_3, &f_vt_3);
			//////////		face_index.push_back(cv::Vec3i(f_1, f_2, f_3));
			//////////		face_vt_index.push_back(cv::Vec3i(f_vt_1, f_vt_2, f_vt_3));
			//////////	}

			//////////}

			//////////fclose(fp_objInfor);

			//////////string objFileName = argv[2];
			//////////objFileName.erase(objFileName.length() - 4, 4);
			//////////string obj_result = objFileName + "_result.obj";

			//////////string objMtlFile = objFileName + ".mtl";
			//////////FILE *fp_deformResult = fopen(obj_result.c_str(), "w");

			//////////fprintf(fp_deformResult, "mtllib %s \n", objMtlFile);

			//////////for (vIt = outputMesh.vertices_begin(); vIt != outputMesh.vertices_end(); ++vIt)  // 전체 변형
			//////////{
			//////////	int idx = vIt->idx();
			//////////	Eigen::Vector3f point = t_src[idx] + dVertices[idx];

			//////////	fprintf(fp_deformResult, "v %f %f %f \n", point.x(), point.y(), point.z());				
			//////////}
			//////////for (int kk = 0; kk < vertex_texture.size(); kk++)
			//////////{
			//////////	fprintf(fp_deformResult, "vt %f %f\n", vertex_texture[kk][0], vertex_texture[kk][1]);
			//////////}

			//////////for (int kk = 0; kk < face_index.size(); kk++)
			//////////{
			//////////	fprintf(fp_deformResult, "f %d/%d %d/%d %d/%d \n", face_index[kk][0], face_vt_index[kk][0], face_index[kk][1], face_vt_index[kk][1], face_index[kk][2], face_vt_index[kk][2]);
			//////////}
			//////////fclose(fp_deformResult);
			
		}


		weightTV -= 0.6 / mapping_count;
		weightARAP -= 0.6 / mapping_count;

	}  // for (int i = 0; i < mapping_count; i++)

	delete[] eyeRegion_bool;

////////////////////////////////////////////////////////////////////////////////////
////////////////////////texture coordinate generation///////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

	Mesh mesh_dst;
	mesh_dst.num_vertices = ICP_tar_mesh.n_vertices();
	mesh_dst.num_faces = ICP_tar_mesh.n_faces();

	//printf("aaa: %d %d \n", mesh_dst.num_vertices, mesh_dst.num_faces);
	
	mesh_dst.vertices = new float[mesh_dst.num_vertices * 3];
	mesh_dst.faces = new unsigned int[mesh_dst.num_faces * 3];

	OpenMesh::Vec2f vt_index;
	std::vector<OpenMesh::Vec2f> vt_indices;
	for (t_vIt = ICP_tar_mesh.vertices_begin(); t_vIt != ICP_tar_mesh.vertices_end(); ++t_vIt)
	{
		int idx = t_vIt.handle().idx();
		vt_index = ICP_tar_mesh.texcoord2D(t_vIt.handle());
		vt_indices.push_back(vt_index);

		t_v = ICP_tar_mesh.point(t_vIt.handle());
		mesh_dst.vertices[3 * idx + 0] = t_v[0];
		mesh_dst.vertices[3 * idx + 1] = t_v[1];
		mesh_dst.vertices[3 * idx + 2] = t_v[2];
	}

	for (fIt = ICP_tar_mesh.faces_begin(); fIt != ICP_tar_mesh.faces_end(); ++fIt)
	{
		int idx = fIt.handle().idx();
		int face_count = 0;
		for (fvIt = ICP_tar_mesh.fv_iter(fIt.handle()); fvIt; ++fvIt)
		{
			mesh_dst.faces[3 * idx + face_count] = fvIt->idx();
			face_count++;
		}
	}

	nanort::BVHBuildOptions<float> build_options;  // Use default option
	build_options.cache_bbox = false;

	printf("  BVH build option:\n");
	printf("    # of leaf primitives: %d\n", build_options.min_leaf_primitives);
	printf("    SAH binsize         : %d\n", build_options.bin_size);

	nanort::TriangleMesh<float> triangle_mesh(mesh_dst.vertices, mesh_dst.faces,
		sizeof(float) * 3);
	nanort::TriangleSAHPred<float> triangle_pred(mesh_dst.vertices, mesh_dst.faces,
		sizeof(float) * 3);

	std::cout << "num_triangles = " << mesh_dst.num_faces << std::endl;

	bool ret = false;
	nanort::BVHAccel<float> accel;
	ret =
		accel.Build(static_cast<unsigned int>(mesh_dst.num_faces), triangle_mesh, triangle_pred, build_options);
	assert(ret);


	nanort::BVHBuildStatistics stats = accel.GetStatistics();

	printf("  BVH statistics:\n");
	printf("    # of leaf   nodes: %d\n", stats.num_leaf_nodes);
	printf("    # of branch nodes: %d\n", stats.num_branch_nodes);
	printf("  Max tree depth     : %d\n", stats.max_tree_depth);
	float bmin[3], bmax[3];
	accel.BoundingBox(bmin, bmax);
	printf("  Bmin               : %f, %f, %f\n", bmin[0], bmin[1], bmin[2]);
	printf("  Bmax               : %f, %f, %f\n", bmax[0], bmax[1], bmax[2]);

	std::vector<cv::Vec2d> calculatedVt;
	calculatedVt.resize(outputMesh.n_vertices());

	///////////////////eyeball control
	//////////int num_eyeballForTex = atoi(argv[8]);
	//////////EyebollForTexture eyeballForTex;

	//////////int *eyeball_tarIndex = new int[outputMesh.n_vertices()];

	//////////int *eyeBall_bool = new int[outputMesh.n_vertices()];
	//////////for (int i = 0; i < outputMesh.n_vertices(); i++)
	//////////{
	//////////	eyeBall_bool[i] = 0;
	//////////}
	//////////for (int i = 0; i < num_eyeballForTex; i++)
	//////////{
	//////////	eyeBall_bool[eyeballForTex.eyeBallForTextureFromSrc[i]] = 1;
	//////////	eyeball_tarIndex[eyeballForTex.eyeBallForTextureFromSrc[i]] = eyeballForTex.eyeBallForTextureFromDst[i];

	//////////}

//#ifdef _OPENMP
//#pragma omp parallel for
//#endif

	for (t_vIt = outputMesh.vertices_begin(); t_vIt != outputMesh.vertices_end(); ++t_vIt)
	{
		int idx = t_vIt.handle().idx();

		if (eyeBall_bool[idx] == 1)
		{
			calculatedVt[idx][0] = vt_indices[eyeball_tarIndex[idx]][0];
			calculatedVt[idx][1] = vt_indices[eyeball_tarIndex[idx]][1];
			continue;
		}
			   
		v = outputMesh.point(t_vIt.handle());
		nanort::Ray<float> ray;
		ray.org[0] = v[0];
		ray.org[1] = v[1];
		ray.org[2] = v[2];

		v_normal = outputMesh.normal(t_vIt.handle());
		float3 dir;
		dir[0] = v_normal[0];
		dir[1] = v_normal[1];
		dir[2] = v_normal[2];
		dir.normalize();
		ray.dir[0] = dir[0];
		ray.dir[1] = dir[1];
		ray.dir[2] = dir[2];

		float kFar = 1.0e+30f;
		ray.min_t = 0.0f;
		ray.max_t = kFar;

		nanort::TriangleIntersector<> triangle_intersector(
			mesh_dst.vertices, mesh_dst.faces, sizeof(float) * 3);
		nanort::TriangleIntersection<> isect;
		bool hit = accel.Traverse(ray, triangle_intersector, &isect);

		if (hit) {
			unsigned int fid = isect.prim_id;
			float u = isect.u;
			float v = isect.v;

			calculatedVt[idx][0] = (1.0 - u - v) * vt_indices[mesh_dst.faces[3 * fid + 0]][0]
				+ u * vt_indices[mesh_dst.faces[3 * fid + 1]][0] + v * vt_indices[mesh_dst.faces[3 * fid + 2]][0];
			calculatedVt[idx][1] = (1.0 - u - v) * vt_indices[mesh_dst.faces[3 * fid + 0]][1]
				+ u * vt_indices[mesh_dst.faces[3 * fid + 1]][1] + v * vt_indices[mesh_dst.faces[3 * fid + 2]][1];

		}
		else {
			dir[0] = -v_normal[0];
			dir[1] = -v_normal[1];
			dir[2] = -v_normal[2];
			dir.normalize();
			ray.dir[0] = dir[0];
			ray.dir[1] = dir[1];
			ray.dir[2] = dir[2];
			hit = accel.Traverse(ray, triangle_intersector, &isect);

			if (hit) {
				unsigned int fid = isect.prim_id;
				float u = isect.u;
				float v = isect.v;

				calculatedVt[idx][0] = (1.0 - u - v) * vt_indices[mesh_dst.faces[3 * fid + 0]][0]
					+ u * vt_indices[mesh_dst.faces[3 * fid + 1]][0] + v * vt_indices[mesh_dst.faces[3 * fid + 2]][0];
				calculatedVt[idx][1] = (1.0 - u - v) * vt_indices[mesh_dst.faces[3 * fid + 0]][1]
					+ u * vt_indices[mesh_dst.faces[3 * fid + 1]][1] + v * vt_indices[mesh_dst.faces[3 * fid + 2]][1];
			}
			else {
				calculatedVt[idx][0] = 0.0;
				calculatedVt[idx][1] = 0.0;
			}

		}

	}


	///////////////////////////////////////////
	///////// boundary check/////////////////
	/////////////////////////////////////////
	int num_boundaryFromTex = atoi(argv[9]);
	FaceBoundary faceBoundary;

	//////direct intepolation
	/*int *faceBoundary_index = new int[outputMesh.n_vertices()];
	int *faceBoundary_bool = new int[outputMesh.n_vertices()];
	for (int i = 0; i < outputMesh.n_vertices(); i++)
	{
		faceBoundary_bool[i] = 0;
	}
	for (int i = 0; i < num_boundaryFromTex; i++)
	{
		faceBoundary_bool[faceBoundary.faceBoundaryIndexFromLineOne[i]] = 1;
		faceBoundary_bool[faceBoundary.faceBoundaryIndexFromLineTwo[i]] = 1;
		faceBoundary_index[faceBoundary.faceBoundaryIndexFromLineOne[i]] = faceBoundary.faceBoundaryIndexFromRef[i];
		faceBoundary_index[faceBoundary.faceBoundaryIndexFromLineTwo[i]] = faceBoundary.faceBoundaryIndexFromRef[i];
	}*/

	/// ratio check
	/*int *faceBoundary_index = new int[outputMesh.n_vertices()];
	int *faceBoundaryFromLineOne_bool = new int[outputMesh.n_vertices()];
	int *faceBoundaryFromLineTwo_bool = new int[outputMesh.n_vertices()];

	for (int i = 0; i < outputMesh.n_vertices(); i++)
	{
		faceBoundaryFromLineOne_bool[i] = 0;
		faceBoundaryFromLineTwo_bool[i] = 0;
	}
	for (int i = 0; i < num_boundaryFromTex; i++)
	{
		faceBoundaryFromLineOne_bool[faceBoundary.faceBoundaryIndexFromLineOne[i]] = 1;
		faceBoundaryFromLineTwo_bool[faceBoundary.faceBoundaryIndexFromLineTwo[i]] = 1;
		faceBoundary_index[faceBoundary.faceBoundaryIndexFromLineOne[i]] = faceBoundary.faceBoundaryIndexFromRef[i];
		faceBoundary_index[faceBoundary.faceBoundaryIndexFromLineTwo[i]] = faceBoundary.faceBoundaryIndexFromRef[i];
	}*/

	int num_foreheadFromTex = 19;
	for (int i = 0; i < num_foreheadFromTex; i++)
	{

		float refDiff[2];
		refDiff[0] = calculatedVt[faceBoundary.foreheadBoundaryIndexFromRef2[i]][0] - calculatedVt[faceBoundary.foreheadBoundaryIndexFromRef[i]][0];
		refDiff[1] = calculatedVt[faceBoundary.foreheadBoundaryIndexFromRef2[i]][1] - calculatedVt[faceBoundary.foreheadBoundaryIndexFromRef[i]][1];

		calculatedVt[faceBoundary.foreheadBoundaryFromLineTwo[i]][0] = calculatedVt[faceBoundary.foreheadBoundaryIndexFromRef[i]][0] - refDiff[0];
		calculatedVt[faceBoundary.foreheadBoundaryFromLineTwo[i]][1] = calculatedVt[faceBoundary.foreheadBoundaryIndexFromRef[i]][1] - refDiff[1];

		calculatedVt[faceBoundary.foreheadBoundaryFromLineOne[i]][0] = calculatedVt[faceBoundary.foreheadBoundaryFromLineTwo[i]][0] - refDiff[0];
		calculatedVt[faceBoundary.foreheadBoundaryFromLineOne[i]][1] = calculatedVt[faceBoundary.foreheadBoundaryFromLineTwo[i]][1] - refDiff[1];

		/////////VT X축 계산
		if (calculatedVt[faceBoundary.foreheadBoundaryFromLineTwo[i]][0] <0.0)
		{
			calculatedVt[faceBoundary.foreheadBoundaryFromLineTwo[i]][0] = 0.0;
		}
		else if(calculatedVt[faceBoundary.foreheadBoundaryFromLineTwo[i]][0] > 1.0) {
			calculatedVt[faceBoundary.foreheadBoundaryFromLineTwo[i]][0] = 1.0;
		}

		if (calculatedVt[faceBoundary.foreheadBoundaryFromLineOne[i]][0] < 0.0)
		{
			calculatedVt[faceBoundary.foreheadBoundaryFromLineOne[i]][0] = 0.0;
		}
		else if (calculatedVt[faceBoundary.foreheadBoundaryFromLineOne[i]][0] > 1.0) {
			calculatedVt[faceBoundary.foreheadBoundaryFromLineOne[i]][0] = 1.0;
		}

		/////VT Y축 계산
		if (calculatedVt[faceBoundary.foreheadBoundaryFromLineTwo[i]][1] > -0.01)
		{
			calculatedVt[faceBoundary.foreheadBoundaryFromLineTwo[i]][1] = -0.01;
		}
		else if (calculatedVt[faceBoundary.foreheadBoundaryFromLineTwo[i]][1] < -1.0) {
			calculatedVt[faceBoundary.foreheadBoundaryFromLineTwo[i]][1] = -1.0;
		}

		if (calculatedVt[faceBoundary.foreheadBoundaryFromLineOne[i]][1] >= 0.0)
		{
			calculatedVt[faceBoundary.foreheadBoundaryFromLineOne[i]][1] = -0.000001;
		}
		else if (calculatedVt[faceBoundary.foreheadBoundaryFromLineOne[i]][1] < -1.0) {
			calculatedVt[faceBoundary.foreheadBoundaryFromLineOne[i]][1] = -1.0;
		}

	}

//////////////////side boundary
	int num_sideFromTex = 29;

	for (int i = 0; i < num_sideFromTex; i++)
	{
		float refDiff[2];
		refDiff[0] = calculatedVt[faceBoundary.sideBoundaryIndexFromRef2[i]][0] - calculatedVt[faceBoundary.sideBoundaryIndexFromRef[i]][0];
		refDiff[1] = calculatedVt[faceBoundary.sideBoundaryIndexFromRef2[i]][1] - calculatedVt[faceBoundary.sideBoundaryIndexFromRef[i]][1];

		refDiff[0] = refDiff[0] / 3;
		refDiff[1] = refDiff[1] / 3;

		calculatedVt[faceBoundary.sideBoundaryIndexFromRef[i]][0] = calculatedVt[faceBoundary.sideBoundaryIndexFromRef2[i]][0] - refDiff[0];
		calculatedVt[faceBoundary.sideBoundaryFromLineTwo[i]][0] = calculatedVt[faceBoundary.sideBoundaryIndexFromRef[i]][0] - refDiff[0];
		calculatedVt[faceBoundary.sideBoundaryFromLineOne[i]][0] = calculatedVt[faceBoundary.sideBoundaryFromLineTwo[i]][0] - refDiff[0];

		calculatedVt[faceBoundary.sideBoundaryIndexFromRef[i]][1] = calculatedVt[faceBoundary.sideBoundaryIndexFromRef2[i]][1] - refDiff[1];
		calculatedVt[faceBoundary.sideBoundaryFromLineTwo[i]][1] = calculatedVt[faceBoundary.sideBoundaryIndexFromRef[i]][1] - refDiff[1];
		calculatedVt[faceBoundary.sideBoundaryFromLineOne[i]][1] = calculatedVt[faceBoundary.sideBoundaryFromLineTwo[i]][1] - refDiff[1];


	}


	/////////////////////////////////////////////////////////
	/////////////write mesh information/////////////////////
	/////////////////////////////////////////////////////////

	OpenMesh::Vec3f vertexNormal;

	std::string objFileName = argv[2];
	objFileName.erase(objFileName.length() - 4, 4);
	std::string obj_result = objFileName + "_result.obj";
	std::string objMtlFile = objFileName + "New.mtl";

	FILE *fp_deformResult = fopen(obj_result.c_str(), "w");
	fprintf(fp_deformResult, "mtllib %s \n", objMtlFile);
	printf("mtlib %s \n", objMtlFile);
	//fprintf(fp_deformResult, "mtllib out_default.mtl \n");

		for (vIt = outputMesh.vertices_begin(); vIt != outputMesh.vertices_end(); ++vIt)  // 전체 변형
		{
			int idx = vIt->idx();
			//Eigen::Vector3f point = t_src[idx] + dVertices[idx];

			t_v = outputMesh.point(vIt.handle());
			vertexNormal = outputMesh.normal(vIt.handle());

			if (eyeBall_bool[idx] == 1 || idx == 526 || idx == 1013)
			{
				fprintf(fp_deformResult, "v %f %f %f \n", t_v[0], t_v[1], t_v[2]-disparity);
			}
			else {
				fprintf(fp_deformResult, "v %f %f %f \n", t_v[0], t_v[1], t_v[2]);
			}			
						
			fprintf(fp_deformResult, "vt %f %f \n", calculatedVt[idx][0], calculatedVt[idx][1]);
			fprintf(fp_deformResult, "vn %f %f %f \n", vertexNormal[0], vertexNormal[1], vertexNormal[2]);
			
			/*if (faceBoundaryFromLineOne_bool[idx] == 1)
			{
				fprintf(fp_deformResult, "vt %f 0.000000 \n", calculatedVt[faceBoundary_index[idx]][0]);
			}
			else if (faceBoundaryFromLineTwo_bool[idx] == 1)
			{
				fprintf(fp_deformResult, "vt %f %f \n", calculatedVt[faceBoundary_index[idx]][0], calculatedVt[faceBoundary_index[idx]][1]/2);
			}
			else
			{
				fprintf(fp_deformResult, "vt %f %f \n", calculatedVt[idx][0], calculatedVt[idx][1]);
			}	*/		
		
		}

		for (fIt = outputMesh.faces_begin(); fIt != outputMesh.faces_end(); ++fIt)
		{
			//int idx = fIt.handle().idx();			
			fprintf(fp_deformResult, "f ");
			for (fvIt = outputMesh.fv_iter(fIt.handle()); fvIt; ++fvIt)
			{
				fprintf(fp_deformResult, "%d/%d/%d ", fvIt->idx()+1, fvIt->idx()+1, fvIt->idx() + 1);
			}
			fprintf(fp_deformResult, "\n");
		}

	fclose(fp_deformResult);

	end = std::chrono::system_clock::now();

	std::chrono::duration<double> elapsed_seconds = end - start;
	std::cout << "elapsed time: " << elapsed_seconds.count() << "s\n\n";
	printf("\n\n\n");

	return 0;
}