#pragma warning(disable:4996)
//#define _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES
#include <iostream>
#include <chrono>
#include <ctime>

#include <omp.h>

#include "glog/logging.h"
#include "ceres/ceres.h"
#include "ceres/rotation.h"

#include <opencv2/opencv.hpp>
#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>

//#include "fit_deform.h"
#include "opt_scale_rigid_new.h"
#include "replace_face_new.h"

#include <windows.h>

#include <unordered_map>
typedef OpenMesh::PolyMesh_ArrayKernelT<>  MyMesh;

using namespace std;

// calculator object mesh gap
unordered_map<string, float> calcXpositionRate(MyMesh real_mesh, MyMesh std_mesh, MyMesh target_mesh);
unordered_map<string, float> calcYpositionRate(MyMesh real_mesh, MyMesh std_mesh, MyMesh target_mesh);
//

unordered_map<string, float> compareRealnStandardReal(MyMesh real_mesh, MyMesh std_mesh, MyMesh target_mesh);
unordered_map<string, float> compareMesh(MyMesh src_mesh, MyMesh target_mesh);
void getDistanceObj(MyMesh real_mesh_ts, MyMesh std_mesh_ts, MyMesh target_mesh_ts);
float calcCentralYgap_1(const int *src_indices, float *src_x_pos, float *src_y_pos, const int src_size, const int *tg_indices, float *tg_x_pos, float *tg_y_pos, const int tg_size); // 중심점 구해서 Y 차이
float calcCentralYgap_1_1(const int *indices, const int size, float *src_x_pos, float *src_y_pos, float *tg_x_pos, float *tg_y_pos); // 중심점을 특정 좌표의 차로 구함
float calcCentralYgap_2(const int *indices, const int size, float *src_x_pos, float *src_y_pos, float src_bottom_Y, float *tg_x_pos, float *tg_y_pos, float tg_bottom_Y); // 중심점 구해서 중심점부터 얼굴 하단까지의 길이 차이
float calcGap(float src, float target);
float calcCentralX(const int *landmark_idx, float *x_pos, float *y_pos, const int size);
float calcCentralY(const int *landmark_idx, float *x_pos, float *y_pos, const int size);
float calcMaxVal(float *arr, int arr_size);
float calcMinVal(float *arr, int arr_size);
float calcAverage(float *req, int size);
float calcDistance(float x1, float y1, float x2, float y2);
float calcXRating(float src_dis, float target_dis, float srcX);
float calcYRating(float src_dis, float target_dis, float srcY);

int eyeType(int received_data);
int noseType(int received_data);
int lipType(int received_data);


//#define PATH "D:\\Work\\Seerslab_data\\20181109_DB_check\\20181109_convert_lowpoly\\*.obj"

void readMesh(std::string filename, MyMesh &mesh)
{
	OpenMesh::IO::Options opt;
	opt += OpenMesh::IO::Options::VertexNormal;
	opt += OpenMesh::IO::Options::FaceNormal;
	//opt += OpenMesh::IO::Options::VertexTexCoord;

	mesh.request_vertex_normals();
	mesh.request_face_normals();
	//mesh.request_vertex_texcoords2D();

	std::cout << "Loading from file '" << filename << "'\n";
	if (OpenMesh::IO::read_mesh(mesh, filename, opt))
	{
		// update face and vertex normals     
		if (!opt.check(OpenMesh::IO::Options::FaceNormal))
			mesh.update_face_normals();
		else
			std::cout << "File provides face normals\n";

		if (!opt.check(OpenMesh::IO::Options::VertexNormal))
			mesh.update_vertex_normals();
		else
			std::cout << "File provides vertex normals\n";
	}
	else
	{
		std::cout << "Failed to load file.";
		exit(1);
	}

	// mesh info
	std::clog << "mesh: " << mesh.n_vertices() << " vertices, "
		<< mesh.n_edges() << " edge, "
		<< mesh.n_faces() << " faces\n";
}


int main(int argc, char* argv[])
{
	std::chrono::time_point<std::chrono::system_clock> start, end;
	start = std::chrono::system_clock::now();

	int current_shape_state = 1;
	int current_eyes_state = 1;
	int current_nose_state = 1;
	int current_mouse_state = 1;

	//shape parameters

	float real_mean_width = 162.02;//154.245163; // 미리 계산 필요
	float real_mean_height = 112.35;//131.72;//118.742485; // 미리 계산 필요

									//float beauty_mean_width = 154.245163; // 미리 계산 필요
	float beauty_mean_height = 118.742485; // 미리 계산 필요

	float real_shapeToBeauty_width = 0.966964;  //미리 계산 필요 (real mean과 beauty mean 사이의 얼굴형 가로 비율)
	float real_shapeToBeauty_height = 1.040051; // 미리 계산 필요(real mean과 beauty mean 사이의 얼굴형 세로 비율)

												//float kk;
												//kk = (29.8133106 / 2) * (10.9604645 / 2);

												/////////////////////////////////////////
												/////////eye parameters/////////////////
												///////////////////////////////////////

												/*   0902_mix_mean.obj // Beauty_mean.obj
												1. real mean width : right / left   24.2625389 / 24.4676723
												2. beauty mean width : right / left   29.8971539 / 29.8133106
												3. real mean height : right / left   8.55368328 / 8.62892342
												4. beauty mean height : right / left   10.4852533 / 10.9604645
												*/

												//eye for length
	float real_mean_eyeWidth_right = 27.425;//24.35;  // 미리 계산 필요(실제 값: 24.2625389)
	float real_mean_eyeHeight_right = 38.72;//30.7; // 미리 계산 필요기존 하나짜리 8.55368328 (실제값: 30.2964687)
	float real_mean_eyeWidth_left = 27.425;//24.35; // 미리 계산 필요 (실제값: 24.4676723)
	float real_mean_eyeHeight_left = 38.72;//30.7; // 미리 계산 필요 기존 하나짜리 8.62892342 (실제값: 30.1156254)

	float beauty_mean_eyeWidth_right = 29.85; // 미리 계산 필요(실제값: 29.8971539)
	float beauty_mean_eyeHeight_right = 39.0; // 미리 계산 필요기존 하나짜리 10.4852533 (실제값:37.7422600)
	float beauty_mean_eyeWidth_left = 29.85; // 미리 계산 필요 (실제값: 29.8133106)
	float beauty_mean_eyeHeight_left = 39.0; // 미리 계산 필요기존 하나짜리 10.9604645 (실제값: 40.2557831)

											 //eye for area
	float real_mean_eyeArea = 326.814118; // 미리 계산 161.321457 + 165.492661// 0902_mix_mean.obj -- right eye: 24.2625389 / 8.55368328 (old area:51.8835182)--- Left eye: 24.4676723 / 8.62892342 (old area:52.7824173)
	float beauty_mean_eyeArea = 434.1474;// 미리 계산 213.532913 + 220.614487 /// Beauty_mean.obj -- right eye: 29.8971539 / 10.4852533 (old area:78.3698044) -- Left eye: 29.8133106 / 10.9604645 (old area:81.6919327)

	float real_mean_eyeDistance = 50.17;//39.2823753;
	float beauty_mean_eyeDistance = 34.3454285;
	float beauty_mean_width = 148.341919; ///얼굴 가로와 상대적인 것을 위해
	float beauty_real_eyeDistance_ratio = (beauty_mean_width / real_mean_width) * (beauty_mean_eyeDistance / real_mean_eyeDistance); //

																																	 // lips for length
	float real_mean_lipsWidth = 49.1;//45.8352661; // 미리 계산 필요
	float real_mean_lipsHeight = 87.62784;//81.7896729; // 미리 계산 필요

	float beauty_mean_lipsWidth = 37.0956383; // 미리 계산 필요
	float beauty_mean_lipsHeight = 66.2038879; //미리 계산 필요


											   //nose for length
	float real_mean_noseWidth = 35;//38.4171181; // 미리 계산 필요
	float real_mean_noseHeight = 69.3139;//48.92;//53.4695473; // 미리 계산 필요

	float beauty_mean_noseWidth = 24.5890388; // 미리 계산 필요
	float beauty_mean_noseHeight = 48.6960640; //미리 계산 필요


	float eyeMove_factor; // 얼마나 눈을 벌릴 것인가

	double nose_down_xAngle = 0.0; // 실사일 때  0.06 들창코 막기 팩터
	double nose_down_zTranslation = 0.0; //실사일 때 -2.0 들창코 막기

										 //double nose_down_xAngle = 0.04; // 실사일 때  0.06 들창코 막기 팩터
										 //double nose_down_zTranslation = -2.0; //실사일 때 -2.0 들창코 막기

	float lip_move_factor = 0.0;  //입을 얼마나 내릴 것인지
	float lip_length_ratio; //입의 길이 비율

							//테스트 팩터
							//int eye_autoScale_bool = 0;
							//int nose_autoScale_bool = 0;
							//int mouse_autoScale_bool = 0;

							//int eyebrow_autoScale_bool = 0;

							//int eyebrow_bool = 0;	

							//double eye_scale_factor = 1.0;
							//double nose_scale_factor = 1.0;
							//double mouse_scale_factor = 1.0;
							//double eyebrow_scale_factor = 1.0;

							// 상용 팩터
	int eye_autoScale_bool = 0;
	int nose_autoScale_bool = 0;
	int mouse_autoScale_bool = 0;
	int eyebrow_autoScale_bool = 0;

	int eyebrow_bool = 0; // 눈썹 피팅할 것인가

	int distance_eyes_bool = 0; // 눈 사이의 거리를 계산할지

	int eye_scale_condition = 2; // 0: 조절 없음, 1: 면적으로 조정, 2: 종횡비 조정

	int length_lip_bool = 1;  // 입술 크기 비율 조절할 것인가

	int length_nose_bool = 0;  //코의 길이 비율 조절할 것인가

	int position_Y_bool = 0; // 얼굴 세로 비율 조절할 것인가

	int mixed_model_bool = 0; //실사와 섞을 것인가

	double eye_scale_factor = 0.87;
	//double nose_scale_factor = 0.802;
	//double mouse_scale_factor = 0.827;
	double nose_scale_factor = 1.0;//0.6987; //0.7398;
	double mouse_scale_factor = 1.0;
	double eyebrow_scale_factor = 1.10;

	double face_scale_width = 1.15;
	double leftEyebrow_scale_width = 1.0; //
	double rightEyebrow_scale_width = 1.0;
	double leftEye_scale_width = 1.1;
	double rightEye_scale_width = 1.1;
	//double nose_scale_width = 0.9; //현재 상용
	double nose_scale_width = 1.0;
								  
	double mouth_scale_width = 1.0;

	double face_scale_height = 1.0;
	double leftEyebrow_scale_height = 1.0;
	double rightEyebrow_scale_height = 1.0;
	double leftEye_scale_height = 1.0;
	double rightEye_scale_height = 1.0;
	//double nose_scale_height = 1.07;  //현재 상용
	double nose_scale_height = 1.0;
	double mouth_scale_height = 1.0;

	double nose_scale_z = 1.0;

	//sub scale for eyes
	double eye_scale_forSmallEyes = 0.1;

	int received_shape_state = atoi(argv[3]);
	int received_eyes_state = atoi(argv[4]);
	int received_nose_state = atoi(argv[5]);
	int received_mouth_state = atoi(argv[6]);

	//기본 모델 번호 세팅
	int change_shape_state = 100;
	int change_eyes_state = 18;
	int change_nose_state = 9;
	int change_mouse_state = 21;

	//eye tuning
	if (received_eyes_state > 135)
		received_eyes_state = 68;

	//nose tuning
	if (received_nose_state > 27)
		received_nose_state = 14;

	//mouth tuning
	if (received_mouth_state > 162)
		received_mouth_state = 41;

	change_eyes_state = eyeType(received_eyes_state);
	change_nose_state = noseType(received_nose_state);
	change_mouse_state = lipType(received_mouth_state);

	printf("received data%% eye: %d, nose: %d, lip: %d\n", received_eyes_state, received_nose_state, received_mouth_state);
	printf("shape: %d, eyes: %d, nose: %d, mouse: %d \n", change_shape_state, change_eyes_state, change_nose_state, change_mouse_state);
	//MyMesh kkk_mesh;
	//std::string kkk = argv[13];
	//readMesh(kkk, kkk_mesh);

	MyMesh mesh_3dmm;
	//std::string directory_3dmm = argv[13];
	std::string directory_3dmm = argv[1];
	readMesh(directory_3dmm, mesh_3dmm);

	////////SCALE factor of face ////////////////////////////////////////////
	float real_current_width;
	float real_current_height;
	float face_width_ratio;
	float face_height_ratio;

	//eye scale width height
	float real_current_eyeWidth_right;
	float real_current_eyeWidth_left;
	float real_current_eyeHeight_right;
	float real_current_eyeHeight_left;

	float beauty_current_eyeWidth_right;
	float beauty_current_eyesHeight_right;
	float beauty_current_eyeWidth_left;
	float beauty_current_eyesHeight_left;

	//float beauty_current_eyeWidth_right;
	//float beauty_current_eyeWidth_left;
	//float beauty_current_eyeHeight_right;
	//float beauty_current_eyeHeight_left;

	// eye scale area
	float real_current_eyeArea;
	float beauty_current_eyeArea;
	float eyeArea_ratio;

	float eye_distance_ratio;

	int shape_width_factor_1 = 2681;  // 32 vertex 인덱스들
	int shape_width_factor_2 = 610;  //0
	int shape_height_factor_1 = 41;   // 67
	int shape_height_factor_2 = 2411;  // 16

	int eye_width_factor_1 = 1340;
	int eye_width_factor_2 = 1355;
	int eye_height_factor_1 = 439;
	int eye_height_factor_2 = 1364;

	int left_eye_width_factor_1 = 2772;
	int left_eye_width_factor_2 = 2787;
	int left_eye_height_factor_1 = 2795;
	int left_eye_height_factor_2 = 1871;

	///////eye vertex index for area
	int R_eye_factor_t1 = 1358; //94
	int R_eye_factor_t2 = 1361;  //95
	int R_eye_factor_t3 = 1364; //96
	int R_eye_factor_t4 = 1366; //97
	int R_eye_factor_t5 = 1368;  //98

	int R_eye_factor_b1 = 432; //104
	int R_eye_factor_b2 = 802;  // 103
	int R_eye_factor_b3 = 439; //102
	int R_eye_factor_b4 = 1044; //101
	int R_eye_factor_b5 = 445; //100

	int L_eye_factor_t1 = 2800;
	int L_eye_factor_t2 = 2798;
	int L_eye_factor_t3 = 2795;
	int L_eye_factor_t4 = 2792;
	int L_eye_factor_t5 = 2790;

	int L_eye_factor_b1 = 1877;
	int L_eye_factor_b2 = 2476;
	int L_eye_factor_b3 = 1871;
	int L_eye_factor_b4 = 2234;
	int L_eye_factor_b5 = 1864;

	//int R_eye_factor_t1 = 2800;
	//int R_eye_factor_t2 = 2798;
	//int R_eye_factor_t3 = 2795;
	//int R_eye_factor_t4 = 2792;
	//int R_eye_factor_t5 = 2790;

	//int R_eye_factor_b1 = 1877;
	//int R_eye_factor_b2 = 2476;
	//int R_eye_factor_b3 = 1871;
	//int R_eye_factor_b4 = 2234;
	//int R_eye_factor_b5 = 1864;

	///////Lip vertex index for ratio
	float real_current_lipsWidth;
	float real_current_lipsHeight;

	float beauty_current_lipsWidth;
	float beauty_current_lipsHeight;

	int lip_width_R = 1425;  // landmark 117
	int lip_width_L = 2834;   // landmark 129

	int lips_factor_t1 = 1438;  //121
	int lips_factor_t2 = 1437;  //122
	int lips_factor_t3 = 79;  //123
	int lips_factor_t4 = 2869;  //124
	int lips_factor_t5 = 2870;   //125

	int lips_factor_b1 = 282; // 137
	int lips_factor_b2 = 364; // 136
	int lips_factor_b3 = 21; // 135
	int lips_factor_b4 = 1796; // 134
	int lips_factor_b5 = 1714; // 133

							   /////////eye distance factor
	int right_eye_in = 1340; //눈 사이의 떨어진 정도를 구하기 위한 vertex 인덱스
	int left_eye_in = 2772;

	////////////////////nose vertex index for ratio////////////
	float real_current_noseWidth;
	float real_current_noseHeight;

	float beauty_current_noseWidth;
	float beauty_current_noseHeight;

	int nose_width_R = 480;  // landmark 80
	int nose_width_L = 1912;   // landmark 86
	int nose_height_t = 41;	// landmark 67
	int nose_height_b = 62;	// landmark 83



	OpenMesh::VertexHandle vh;

	/////////////////shape scale control/////////////////////
	if (change_shape_state == 100) {
		OpenMesh::Vec3f v_shape_row1, v_shape_row2, v_shape_col1, v_shape_col2;

		vh = mesh_3dmm.vertex_handle(shape_width_factor_1);
		v_shape_row1 = mesh_3dmm.point(vh);

		vh = mesh_3dmm.vertex_handle(shape_width_factor_2);
		v_shape_row2 = mesh_3dmm.point(vh);

		vh = mesh_3dmm.vertex_handle(shape_height_factor_1);
		v_shape_col1 = mesh_3dmm.point(vh);

		vh = mesh_3dmm.vertex_handle(shape_height_factor_2);
		v_shape_col2 = mesh_3dmm.point(vh);

		Eigen::Vector3f shape_row_difference;
		Eigen::Vector3f shape_col_difference;

		float shape_width_distance;
		float shape_height_distance;

		shape_row_difference = Eigen::Vector3f(v_shape_row2[0] - v_shape_row1[0], v_shape_row2[1] - v_shape_row1[1], v_shape_row2[2] - v_shape_row1[2]);
		shape_width_distance = shape_row_difference.norm();

		shape_col_difference = Eigen::Vector3f(v_shape_col2[0] - v_shape_col1[0], v_shape_col2[1] - v_shape_col1[1], v_shape_col2[2] - v_shape_col1[2]);
		shape_height_distance = shape_col_difference.norm();

		face_width_ratio = shape_width_distance / real_mean_width;    ///////////ratio calculation
		face_height_ratio = shape_height_distance / real_mean_height;

		//temp check
		//face_width_ratio = (face_width_ratio - 1.0) / 2 + 1.0;
		//face_height_ratio = (face_height_ratio - 1.0) / 2 + 1.0;

		//face_width_ratio = face_width_ratio * 1.0;
		//face_height_ratio = face_height_ratio * 1.0;

		face_width_ratio = face_width_ratio / real_shapeToBeauty_width * face_scale_width;
		face_height_ratio = face_height_ratio / real_shapeToBeauty_height * face_scale_height;

		face_width_ratio = (face_width_ratio - 1.0) / 2 + 1.0;
		face_height_ratio = (face_height_ratio - 1.0) / 2 + 1.0;

		face_width_ratio = face_width_ratio / face_height_ratio;
		face_height_ratio = face_height_ratio / face_height_ratio;

		printf("shape ratio(width: %f, height: %f)", face_width_ratio, face_height_ratio);
	}

	/////////////////eye scale control/////////////////////
	if (change_eyes_state != 0) {
		//////OpenMesh::Vec3f v_eye_row1, v_eye_row2, v_eye_col1, v_eye_col2;

		//////vh = mesh_3dmm.vertex_handle(eye_width_factor_1);
		//////v_eye_row1 = mesh_3dmm.point(vh);

		//////vh = mesh_3dmm.vertex_handle(eye_width_factor_2);
		//////v_eye_row2 = mesh_3dmm.point(vh);

		//////vh = mesh_3dmm.vertex_handle(eye_height_factor_1);
		//////v_eye_col1 = mesh_3dmm.point(vh);

		//////vh = mesh_3dmm.vertex_handle(eye_height_factor_2);
		//////v_eye_col2 = mesh_3dmm.point(vh);

		//////Eigen::Vector3f eye_row_difference;
		//////Eigen::Vector3f eye_col_difference;

		//////float eye_width_distance;
		//////float eye_height_distance;

		//////eye_row_difference = Eigen::Vector3f(v_eye_row2[0] - v_eye_row1[0], v_eye_row2[1] - v_eye_row1[1], v_eye_row2[2] - v_eye_row1[2]);
		//////eye_width_distance = eye_row_difference.norm();

		//////eye_col_difference = Eigen::Vector3f(v_eye_col2[0] - v_eye_col1[0], v_eye_col2[1] - v_eye_col1[1], v_eye_col2[2] - v_eye_col1[2]);
		//////eye_height_distance = eye_col_difference.norm();


		//////// left eye
		//////OpenMesh::Vec3f v_leftEye_row1, v_leftEye_row2, v_leftEye_col1, v_leftEye_col2;

		//////vh = mesh_3dmm.vertex_handle(left_eye_width_factor_1);
		//////v_leftEye_row1 = mesh_3dmm.point(vh);

		//////vh = mesh_3dmm.vertex_handle(left_eye_width_factor_2);
		//////v_leftEye_row2 = mesh_3dmm.point(vh);

		//////vh = mesh_3dmm.vertex_handle(left_eye_height_factor_1);
		//////v_leftEye_col1 = mesh_3dmm.point(vh);

		//////vh = mesh_3dmm.vertex_handle(left_eye_height_factor_2);
		//////v_leftEye_col2 = mesh_3dmm.point(vh);

		//////Eigen::Vector3f leftEye_row_difference;
		//////Eigen::Vector3f leftEye_col_difference;

		//////float leftEye_width_distance;
		//////float leftEye_height_distance;

		//////leftEye_row_difference = Eigen::Vector3f(v_leftEye_row2[0] - v_leftEye_row1[0], v_leftEye_row2[1] - v_leftEye_row1[1], v_leftEye_row2[2] - v_leftEye_row1[2]);
		//////leftEye_width_distance = leftEye_row_difference.norm();

		//////leftEye_col_difference = Eigen::Vector3f(v_leftEye_col2[0] - v_leftEye_col1[0], v_leftEye_col2[1] - v_leftEye_col1[1], v_leftEye_col2[2] - v_leftEye_col1[2]);
		//////leftEye_height_distance = leftEye_col_difference.norm();

		////////area calculation 
		//////real_current_eyeArea = (eye_width_distance / 2) * (eye_height_distance / 2) + (leftEye_width_distance / 2) * (leftEye_height_distance / 2);   // real eye area

		////////////////////////////////////////////////////////////////
		if (eye_scale_condition == 1)
		{
			OpenMesh::Vec3f v_R_eye_t1, v_R_eye_t2, v_R_eye_t3, v_R_eye_t4, v_R_eye_t5;
			OpenMesh::Vec3f v_R_eye_b1, v_R_eye_b2, v_R_eye_b3, v_R_eye_b4, v_R_eye_b5;

			vh = mesh_3dmm.vertex_handle(R_eye_factor_t1);
			v_R_eye_t1 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(R_eye_factor_t2);
			v_R_eye_t2 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(R_eye_factor_t3);
			v_R_eye_t3 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(R_eye_factor_t4);
			v_R_eye_t4 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(R_eye_factor_t5);
			v_R_eye_t5 = mesh_3dmm.point(vh);

			//
			vh = mesh_3dmm.vertex_handle(R_eye_factor_b1);
			v_R_eye_b1 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(R_eye_factor_b2);
			v_R_eye_b2 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(R_eye_factor_b3);
			v_R_eye_b3 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(R_eye_factor_b4);
			v_R_eye_b4 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(R_eye_factor_b5);
			v_R_eye_b5 = mesh_3dmm.point(vh);

			///area
			Eigen::Vector3f area1_width, area2_width, area3_width, area4_width;
			Eigen::Vector3f area1_height, area2_height, area3_height, area4_height;

			float area1_width_distance, area2_width_distance, area3_width_distance, area4_width_distance, area5_width_distance;
			float area1_height_distance, area2_height_distance, area3_height_distance, area4_height_distance, area5_height_distance;

			area1_width = Eigen::Vector3f(v_R_eye_t2[0] - v_R_eye_t1[0], v_R_eye_t2[1] - v_R_eye_t1[1], v_R_eye_t2[2] - v_R_eye_t1[2]);
			area1_width_distance = area1_width.norm();

			area2_width = Eigen::Vector3f(v_R_eye_t3[0] - v_R_eye_t2[0], v_R_eye_t3[1] - v_R_eye_t2[1], v_R_eye_t3[2] - v_R_eye_t2[2]);
			area2_width_distance = area2_width.norm();

			area3_width = Eigen::Vector3f(v_R_eye_t4[0] - v_R_eye_t3[0], v_R_eye_t4[1] - v_R_eye_t3[1], v_R_eye_t4[2] - v_R_eye_t3[2]);
			area3_width_distance = area3_width.norm();

			area4_width = Eigen::Vector3f(v_R_eye_t5[0] - v_R_eye_t4[0], v_R_eye_t5[1] - v_R_eye_t4[1], v_R_eye_t5[2] - v_R_eye_t4[2]);
			area4_width_distance = area4_width.norm();

			//
			area1_height = Eigen::Vector3f(v_R_eye_t1[0] - v_R_eye_b1[0], v_R_eye_t1[1] - v_R_eye_b1[1], v_R_eye_t1[2] - v_R_eye_b1[2]);
			area1_height_distance = area1_height.norm();

			area2_height = Eigen::Vector3f(v_R_eye_t2[0] - v_R_eye_b2[0], v_R_eye_t2[1] - v_R_eye_b2[1], v_R_eye_t2[2] - v_R_eye_b2[2]);
			area2_height_distance = area2_height.norm();

			area3_height = Eigen::Vector3f(v_R_eye_t3[0] - v_R_eye_b3[0], v_R_eye_t3[1] - v_R_eye_b3[1], v_R_eye_t3[2] - v_R_eye_b3[2]);
			area3_height_distance = area3_height.norm();

			area4_height = Eigen::Vector3f(v_R_eye_t4[0] - v_R_eye_b4[0], v_R_eye_t4[1] - v_R_eye_b4[1], v_R_eye_t4[2] - v_R_eye_b4[2]);
			area4_height_distance = area4_height.norm();

			real_current_eyeArea = (area1_width_distance * area1_height_distance) + (area2_width_distance * area2_height_distance) + (area3_width_distance * area3_height_distance) + (area4_width_distance * area4_height_distance);

			////Left eye
			OpenMesh::Vec3f v_L_eye_t1, v_L_eye_t2, v_L_eye_t3, v_L_eye_t4, v_L_eye_t5;
			OpenMesh::Vec3f v_L_eye_b1, v_L_eye_b2, v_L_eye_b3, v_L_eye_b4, v_L_eye_b5;

			vh = mesh_3dmm.vertex_handle(L_eye_factor_t1);
			v_L_eye_t1 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(L_eye_factor_t2);
			v_L_eye_t2 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(L_eye_factor_t3);
			v_L_eye_t3 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(L_eye_factor_t4);
			v_L_eye_t4 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(L_eye_factor_t5);
			v_L_eye_t5 = mesh_3dmm.point(vh);

			///
			vh = mesh_3dmm.vertex_handle(L_eye_factor_b1);
			v_L_eye_b1 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(L_eye_factor_b2);
			v_L_eye_b2 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(L_eye_factor_b3);
			v_L_eye_b3 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(L_eye_factor_b4);
			v_L_eye_b4 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(L_eye_factor_b5);
			v_L_eye_b5 = mesh_3dmm.point(vh);

			///area
			Eigen::Vector3f area1_width_L, area2_width_L, area3_width_L, area4_width_L;
			Eigen::Vector3f area1_height_L, area2_height_L, area3_height_L, area4_height_L;

			float area1_width_distance_L, area2_width_distance_L, area3_width_distance_L, area4_width_distance_L;
			float area1_height_distance_L, area2_height_distance_L, area3_height_distance_L, area4_height_distance_L;

			area1_width_L = Eigen::Vector3f(v_L_eye_t2[0] - v_L_eye_t1[0], v_L_eye_t2[1] - v_L_eye_t1[1], v_L_eye_t2[2] - v_L_eye_t1[2]);
			area1_width_distance_L = area1_width_L.norm();

			area2_width_L = Eigen::Vector3f(v_L_eye_t3[0] - v_L_eye_t2[0], v_L_eye_t3[1] - v_L_eye_t2[1], v_L_eye_t3[2] - v_L_eye_t2[2]);
			area2_width_distance_L = area2_width_L.norm();

			area3_width_L = Eigen::Vector3f(v_L_eye_t4[0] - v_L_eye_t3[0], v_L_eye_t4[1] - v_L_eye_t3[1], v_L_eye_t4[2] - v_L_eye_t3[2]);
			area3_width_distance_L = area3_width_L.norm();

			area4_width_L = Eigen::Vector3f(v_L_eye_t5[0] - v_L_eye_t4[0], v_L_eye_t5[1] - v_L_eye_t4[1], v_L_eye_t5[2] - v_L_eye_t4[2]);
			area4_width_distance_L = area4_width_L.norm();

			//
			area1_height_L = Eigen::Vector3f(v_L_eye_t1[0] - v_L_eye_b1[0], v_L_eye_t1[1] - v_L_eye_b1[1], v_L_eye_t1[2] - v_L_eye_b1[2]);
			area1_height_distance_L = area1_height_L.norm();

			area2_height_L = Eigen::Vector3f(v_L_eye_t2[0] - v_L_eye_b2[0], v_L_eye_t2[1] - v_L_eye_b2[1], v_L_eye_t2[2] - v_L_eye_b2[2]);
			area2_height_distance_L = area2_height_L.norm();

			area3_height_L = Eigen::Vector3f(v_L_eye_t3[0] - v_L_eye_b3[0], v_L_eye_t3[1] - v_L_eye_b3[1], v_L_eye_t3[2] - v_L_eye_b3[2]);
			area3_height_distance_L = area3_height_L.norm();

			area4_height_L = Eigen::Vector3f(v_L_eye_t4[0] - v_L_eye_b4[0], v_L_eye_t4[1] - v_L_eye_b4[1], v_L_eye_t4[2] - v_L_eye_b4[2]);
			area4_height_distance_L = area4_height_L.norm();

			real_current_eyeArea = real_current_eyeArea + (area1_width_distance_L * area1_height_distance_L) + (area2_width_distance_L * area2_height_distance_L) + (area3_width_distance_L * area3_height_distance_L) + (area4_width_distance_L * area4_height_distance_L);

		} // if (eye_scale_condition == 1)

		if (eye_scale_condition == 2)
		{
			// RIGHT eye
			OpenMesh::Vec3f v_eye_row1, v_eye_row2;

			vh = mesh_3dmm.vertex_handle(eye_width_factor_1);
			v_eye_row1 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(eye_width_factor_2);
			v_eye_row2 = mesh_3dmm.point(vh);

			Eigen::Vector3f eye_row_difference;
			float eye_width_distance;

			eye_row_difference = Eigen::Vector3f(v_eye_row2[0] - v_eye_row1[0], v_eye_row2[1] - v_eye_row1[1], v_eye_row2[2] - v_eye_row1[2]);
			eye_width_distance = eye_row_difference.norm();

			real_current_eyeWidth_right = eye_width_distance;


			////height
			OpenMesh::Vec3f v_R_eye_t1, v_R_eye_t2, v_R_eye_t3, v_R_eye_t4, v_R_eye_t5;
			OpenMesh::Vec3f v_R_eye_b1, v_R_eye_b2, v_R_eye_b3, v_R_eye_b4, v_R_eye_b5;

			vh = mesh_3dmm.vertex_handle(R_eye_factor_t1);
			v_R_eye_t1 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(R_eye_factor_t2);
			v_R_eye_t2 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(R_eye_factor_t3);
			v_R_eye_t3 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(R_eye_factor_t4);
			v_R_eye_t4 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(R_eye_factor_t5);
			v_R_eye_t5 = mesh_3dmm.point(vh);

			//
			vh = mesh_3dmm.vertex_handle(R_eye_factor_b1);
			v_R_eye_b1 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(R_eye_factor_b2);
			v_R_eye_b2 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(R_eye_factor_b3);
			v_R_eye_b3 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(R_eye_factor_b4);
			v_R_eye_b4 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(R_eye_factor_b5);
			v_R_eye_b5 = mesh_3dmm.point(vh);

			/// height calculation
			Eigen::Vector3f col1_height, col2_height, col3_height, col4_height, col5_height;
			float col1_height_distance, col2_height_distance, col3_height_distance, col4_height_distance, col5_height_distance;

			col1_height = Eigen::Vector3f(v_R_eye_t1[0] - v_R_eye_b1[0], v_R_eye_t1[1] - v_R_eye_b1[1], v_R_eye_t1[2] - v_R_eye_b1[2]);
			col1_height_distance = col1_height.norm();

			col2_height = Eigen::Vector3f(v_R_eye_t2[0] - v_R_eye_b2[0], v_R_eye_t2[1] - v_R_eye_b2[1], v_R_eye_t2[2] - v_R_eye_b2[2]);
			col2_height_distance = col2_height.norm();

			col3_height = Eigen::Vector3f(v_R_eye_t3[0] - v_R_eye_b3[0], v_R_eye_t3[1] - v_R_eye_b3[1], v_R_eye_t3[2] - v_R_eye_b3[2]);
			col3_height_distance = col3_height.norm();

			col4_height = Eigen::Vector3f(v_R_eye_t4[0] - v_R_eye_b4[0], v_R_eye_t4[1] - v_R_eye_b4[1], v_R_eye_t4[2] - v_R_eye_b4[2]);
			col4_height_distance = col4_height.norm();

			col5_height = Eigen::Vector3f(v_R_eye_t5[0] - v_R_eye_b5[0], v_R_eye_t5[1] - v_R_eye_b5[1], v_R_eye_t5[2] - v_R_eye_b5[2]);
			col5_height_distance = col5_height.norm();

			real_current_eyeHeight_right = col1_height_distance + col2_height_distance + col3_height_distance + col4_height_distance + col5_height_distance;


			// Left eye
			OpenMesh::Vec3f v_leftEye_row1, v_leftEye_row2, v_leftEye_col1, v_leftEye_col2;

			vh = mesh_3dmm.vertex_handle(left_eye_width_factor_1);
			v_leftEye_row1 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(left_eye_width_factor_2);
			v_leftEye_row2 = mesh_3dmm.point(vh);

			Eigen::Vector3f leftEye_row_difference;
			float leftEye_width_distance;

			leftEye_row_difference = Eigen::Vector3f(v_leftEye_row2[0] - v_leftEye_row1[0], v_leftEye_row2[1] - v_leftEye_row1[1], v_leftEye_row2[2] - v_leftEye_row1[2]);
			leftEye_width_distance = leftEye_row_difference.norm();

			real_current_eyeWidth_left = leftEye_width_distance;


			//height
			OpenMesh::Vec3f v_L_eye_t1, v_L_eye_t2, v_L_eye_t3, v_L_eye_t4, v_L_eye_t5;
			OpenMesh::Vec3f v_L_eye_b1, v_L_eye_b2, v_L_eye_b3, v_L_eye_b4, v_L_eye_b5;

			vh = mesh_3dmm.vertex_handle(L_eye_factor_t1);
			v_L_eye_t1 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(L_eye_factor_t2);
			v_L_eye_t2 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(L_eye_factor_t3);
			v_L_eye_t3 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(L_eye_factor_t4);
			v_L_eye_t4 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(L_eye_factor_t5);
			v_L_eye_t5 = mesh_3dmm.point(vh);

			///
			vh = mesh_3dmm.vertex_handle(L_eye_factor_b1);
			v_L_eye_b1 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(L_eye_factor_b2);
			v_L_eye_b2 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(L_eye_factor_b3);
			v_L_eye_b3 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(L_eye_factor_b4);
			v_L_eye_b4 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(L_eye_factor_b5);
			v_L_eye_b5 = mesh_3dmm.point(vh);

			/// height calculation
			Eigen::Vector3f col1_height_L, col2_height_L, col3_height_L, col4_height_L, col5_height_L;
			float col1_height_distance_L, col2_height_distance_L, col3_height_distance_L, col4_height_distance_L, col5_height_distance_L;

			col1_height_L = Eigen::Vector3f(v_L_eye_t1[0] - v_L_eye_b1[0], v_L_eye_t1[1] - v_L_eye_b1[1], v_L_eye_t1[2] - v_L_eye_b1[2]);
			col1_height_distance_L = col1_height_L.norm();

			col2_height_L = Eigen::Vector3f(v_L_eye_t2[0] - v_L_eye_b2[0], v_L_eye_t2[1] - v_L_eye_b2[1], v_L_eye_t2[2] - v_L_eye_b2[2]);
			col2_height_distance_L = col2_height_L.norm();

			col3_height_L = Eigen::Vector3f(v_L_eye_t3[0] - v_L_eye_b3[0], v_L_eye_t3[1] - v_L_eye_b3[1], v_L_eye_t3[2] - v_L_eye_b3[2]);
			col3_height_distance_L = col3_height_L.norm();

			col4_height_L = Eigen::Vector3f(v_L_eye_t4[0] - v_L_eye_b4[0], v_L_eye_t4[1] - v_L_eye_b4[1], v_L_eye_t4[2] - v_L_eye_b4[2]);
			col4_height_distance_L = col4_height_L.norm();

			col5_height_L = Eigen::Vector3f(v_L_eye_t5[0] - v_L_eye_b5[0], v_L_eye_t5[1] - v_L_eye_b5[1], v_L_eye_t5[2] - v_L_eye_b5[2]);
			col5_height_distance_L = col5_height_L.norm();

			real_current_eyeHeight_left = col1_height_distance_L + col2_height_distance_L + col3_height_distance_L + col4_height_distance_L + col5_height_distance_L;

			float scale_control_for_smallEyes = (real_current_eyeWidth_right + real_current_eyeWidth_left) / 2;
			float factor_forSmallEyes = 0.0;

			if (scale_control_for_smallEyes < 25.25)
			{
				if (scale_control_for_smallEyes < 20.5)
					scale_control_for_smallEyes = 20.5;

				factor_forSmallEyes = scale_control_for_smallEyes - 19.5;
				factor_forSmallEyes = 1 / factor_forSmallEyes;

				eye_scale_forSmallEyes = eye_scale_forSmallEyes * factor_forSmallEyes;
				eye_scale_factor = eye_scale_factor * (1 - eye_scale_forSmallEyes);

			}

			float factor_forBigEyes = 0.0;
			if (scale_control_for_smallEyes > 25.25)
			{
				//factor_forBigEyes = scale_control_for_smallEyes - 24.25;
				factor_forBigEyes = scale_control_for_smallEyes / 25.25;

				eye_scale_factor = (factor_forBigEyes - 1) * 2 + 1;

				//eye_scale_factor = eye_scale_factor * factor_forBigEyes;

			}



		}  //if (eye_scale_condition == 2)

	}

	/////////////////distance between eyes////////////////////
	if (distance_eyes_bool == 1)
	{
		OpenMesh::Vec3f v_rightEye_in, v_leftEye_in;

		vh = mesh_3dmm.vertex_handle(right_eye_in);
		v_rightEye_in = mesh_3dmm.point(vh);

		vh = mesh_3dmm.vertex_handle(left_eye_in);
		v_leftEye_in = mesh_3dmm.point(vh);

		Eigen::Vector3f eye_distance_factor;
		float eye_distance;

		eye_distance_factor = Eigen::Vector3f(v_leftEye_in[0] - v_rightEye_in[0], v_leftEye_in[1] - v_rightEye_in[1], v_leftEye_in[2] - v_rightEye_in[2]);
		eye_distance = eye_distance_factor.norm();

		eyeMove_factor = (eye_distance - real_mean_eyeDistance) * beauty_real_eyeDistance_ratio * 0.5;
		printf("eyeMove_factor: %f\n", eyeMove_factor);
		//eyeMove_factor = 0;

	}

	////////////// Lip length //////////////////////////////
	if (length_lip_bool == 1)
	{
		///////lips width
		OpenMesh::Vec3f v_lip_right, v_lip_left;

		vh = mesh_3dmm.vertex_handle(lip_width_R);
		v_lip_right = mesh_3dmm.point(vh);

		vh = mesh_3dmm.vertex_handle(lip_width_L);
		v_lip_left = mesh_3dmm.point(vh);

		Eigen::Vector3f lips_row_difference;
		float lips_width_distance;

		lips_row_difference = Eigen::Vector3f(v_lip_right[0] - v_lip_left[0], v_lip_right[1] - v_lip_left[1], v_lip_right[2] - v_lip_left[2]);
		lips_width_distance = lips_row_difference.norm();

		real_current_lipsWidth = lips_width_distance;

		//lips height
		OpenMesh::Vec3f v_lip_t1, v_lip_t2, v_lip_t3, v_lip_t4, v_lip_t5;
		OpenMesh::Vec3f v_lip_b1, v_lip_b2, v_lip_b3, v_lip_b4, v_lip_b5;

		vh = mesh_3dmm.vertex_handle(lips_factor_t1);
		v_lip_t1 = mesh_3dmm.point(vh);

		vh = mesh_3dmm.vertex_handle(lips_factor_t2);
		v_lip_t2 = mesh_3dmm.point(vh);

		vh = mesh_3dmm.vertex_handle(lips_factor_t3);
		v_lip_t3 = mesh_3dmm.point(vh);

		vh = mesh_3dmm.vertex_handle(lips_factor_t4);
		v_lip_t4 = mesh_3dmm.point(vh);

		vh = mesh_3dmm.vertex_handle(lips_factor_t5);
		v_lip_t5 = mesh_3dmm.point(vh);

		////
		vh = mesh_3dmm.vertex_handle(lips_factor_b1);
		v_lip_b1 = mesh_3dmm.point(vh);

		vh = mesh_3dmm.vertex_handle(lips_factor_b2);
		v_lip_b2 = mesh_3dmm.point(vh);

		vh = mesh_3dmm.vertex_handle(lips_factor_b3);
		v_lip_b3 = mesh_3dmm.point(vh);

		vh = mesh_3dmm.vertex_handle(lips_factor_b4);
		v_lip_b4 = mesh_3dmm.point(vh);

		vh = mesh_3dmm.vertex_handle(lips_factor_b5);
		v_lip_b5 = mesh_3dmm.point(vh);

		/////distance calculation
		Eigen::Vector3f col1_lip_current_difference, col2_lip_current_difference, col3_lip_current_difference, col4_lip_current_difference, col5_lip_current_difference;
		float col1_lip_distance, col2_lip_distance, col3_lip_current_distance, col4_lip_current_distance, col5_lip_current_distance;

		col1_lip_current_difference = Eigen::Vector3f(v_lip_t1[0] - v_lip_b1[0], v_lip_t1[1] - v_lip_b1[1], v_lip_t1[2] - v_lip_b1[2]);
		col1_lip_distance = col1_lip_current_difference.norm();

		col2_lip_current_difference = Eigen::Vector3f(v_lip_t2[0] - v_lip_b2[0], v_lip_t2[1] - v_lip_b2[1], v_lip_t2[2] - v_lip_b2[2]);
		col2_lip_distance = col2_lip_current_difference.norm();

		col3_lip_current_difference = Eigen::Vector3f(v_lip_t3[0] - v_lip_b3[0], v_lip_t3[1] - v_lip_b3[1], v_lip_t3[2] - v_lip_b3[2]);
		col3_lip_current_distance = col3_lip_current_difference.norm();

		col4_lip_current_difference = Eigen::Vector3f(v_lip_t4[0] - v_lip_b4[0], v_lip_t4[1] - v_lip_b4[1], v_lip_t4[2] - v_lip_b4[2]);
		col4_lip_current_distance = col4_lip_current_difference.norm();

		col5_lip_current_difference = Eigen::Vector3f(v_lip_t5[0] - v_lip_b5[0], v_lip_t5[1] - v_lip_b5[1], v_lip_t5[2] - v_lip_b5[2]);
		col5_lip_current_distance = col5_lip_current_difference.norm();

		real_current_lipsHeight = col1_lip_distance + col2_lip_distance + col3_lip_current_distance + col4_lip_current_distance + col5_lip_current_distance;
	}

	////////////// nose Length //////////////////////////////
	if (length_nose_bool == 1)
	{
		///////nose width
		OpenMesh::Vec3f v_nose_right, v_nose_left;

		vh = mesh_3dmm.vertex_handle(nose_width_R);
		v_nose_right = mesh_3dmm.point(vh);

		vh = mesh_3dmm.vertex_handle(nose_width_L);
		v_nose_left = mesh_3dmm.point(vh);

		Eigen::Vector3f nose_row_difference;
		float nose_width_distance;

		nose_row_difference = Eigen::Vector3f(v_nose_right[0] - v_nose_left[0], v_nose_right[1] - v_nose_left[1], v_nose_right[2] - v_nose_left[2]);
		nose_width_distance = nose_row_difference.norm();

		real_current_noseWidth = nose_width_distance;

		//nose height
		OpenMesh::Vec3f v_nose_t, v_nose_b;

		vh = mesh_3dmm.vertex_handle(nose_height_t);
		v_nose_t = mesh_3dmm.point(vh);

		vh = mesh_3dmm.vertex_handle(nose_height_b);
		v_nose_b = mesh_3dmm.point(vh);

		Eigen::Vector3f nose_col_difference;
		float nose_height_distance;

		nose_col_difference = Eigen::Vector3f(v_nose_t[0] - v_nose_b[0], v_nose_t[1] - v_nose_b[1], v_nose_t[2] - v_nose_b[2]);
		nose_height_distance = nose_col_difference.norm();

		real_current_noseHeight = nose_height_distance;

	}



	///////////// read src mesh & target mesh
	MyMesh src_mesh, target_eyes_mesh, target_nose_mesh, target_mouse_mesh, target_shape_mesh;

	/////////////////////
	///shape landmark///
	int num_shape_landmark = 0;
	int num_shape_inside = 0;
	std::vector<int> landmark_shape_indices;
	std::vector<int> inside_shape_indices;

	/////////////////
	///cheek landmark////
	int num_rightCheek_landmark = 0;
	int num_leftCheek_landmark = 0;
	std::vector<int> landmark_rightCheek_indices;
	std::vector<int> landmark_leftCheek_indices;


	MyMesh beauty_mesh;

	//////use 3DMM Shape
	if (change_shape_state == 0)
	{
		//readMesh("default.obj", src_mesh);
		src_mesh = mesh_3dmm;
	}
	else if (change_shape_state == 100) //shape 3DMM
	{
		//readMesh("Beauty_mean_191028.obj", beauty_mesh);
		readMesh("Beauty_mean_191028.obj", beauty_mesh);
		//readMesh("weighted_sum.obj", beauty_mesh);

		src_mesh = beauty_mesh;
		target_shape_mesh = mesh_3dmm;


		if (mixed_model_bool == 1)
		{
			float ver_sum[3] = { 0.0f, 0.0f, 0.0f };
			OpenMesh::Vec3f each_v;
			OpenMesh::Vec3f each_v_1, each_v_2;

			MyMesh::VertexIter mean_It;
			for (mean_It = beauty_mesh.vertices_begin(); mean_It != beauty_mesh.vertices_end(); ++mean_It)
			{
				ver_sum[0] = 0.0f; ver_sum[1] = 0.0f; ver_sum[2] = 0.0f;

				each_v_1 = beauty_mesh.point(mean_It.handle());
				each_v_2 = mesh_3dmm.point(mean_It.handle());

				ver_sum[0] = 0.75 * each_v_1[0] + 0.25 * each_v_2[0];
				ver_sum[1] = 0.75 * each_v_1[1] + 0.25 * each_v_2[1];
				ver_sum[2] = 0.75 * each_v_1[2] + 0.25 * each_v_2[2];

				src_mesh.point(mean_It.handle()) = OpenMesh::Vec3f(ver_sum[0], ver_sum[1], ver_sum[2]);

			}

			OpenMesh::Vec3f v_mix_row1, v_mix_row2, v_mix_col1, v_mix_col2;

			//mixed model
			vh = src_mesh.vertex_handle(shape_width_factor_1);
			v_mix_row1 = src_mesh.point(vh);

			vh = src_mesh.vertex_handle(shape_width_factor_2);
			v_mix_row2 = src_mesh.point(vh);

			vh = src_mesh.vertex_handle(3787);  //정수리
			v_mix_col1 = src_mesh.point(vh);

			vh = src_mesh.vertex_handle(53);  //턱끝
			v_mix_col2 = src_mesh.point(vh);

			float x_width = abs(v_mix_row2[0] - v_mix_row1[0]);
			float y_height = abs(v_mix_col1[1] - v_mix_col2[1]);

			//beauty model
			vh = beauty_mesh.vertex_handle(shape_width_factor_1);
			v_mix_row1 = beauty_mesh.point(vh);

			vh = beauty_mesh.vertex_handle(shape_width_factor_2);
			v_mix_row2 = beauty_mesh.point(vh);

			vh = beauty_mesh.vertex_handle(3787);  //정수리
			v_mix_col1 = beauty_mesh.point(vh);

			vh = beauty_mesh.vertex_handle(53);  //턱끝
			v_mix_col2 = beauty_mesh.point(vh);

			float x_width_be = abs(v_mix_row2[0] - v_mix_row1[0]);
			float y_height_be = abs(v_mix_col1[1] - v_mix_col2[1]);

			//float width_ratio = x_width / x_width_be;
			//float height_ratio = y_height / y_height_be;

			float width_ratio = x_width_be / x_width;
			float height_ratio = y_height_be / y_height;

			///////mean calculation for mixed model
			float mix_mean_x = 0.0f, mix_mean_y = 0.0f, mix_mean_z = 0.0f;

			for (mean_It = src_mesh.vertices_begin(); mean_It != src_mesh.vertices_end(); ++mean_It)
			{
				each_v = src_mesh.point(mean_It.handle());
				mix_mean_x += each_v[0]; mix_mean_y += each_v[1]; mix_mean_z += each_v[2];
			}

			mix_mean_x = mix_mean_x / src_mesh.n_vertices();
			mix_mean_y = mix_mean_y / src_mesh.n_vertices();
			mix_mean_z = mix_mean_z / src_mesh.n_vertices();

			////scale 종횡비 조정

			Eigen::Matrix4f pra_scale_matrix;

			pra_scale_matrix(0, 0) = float(width_ratio);		pra_scale_matrix(0, 1) = 0.0f;						pra_scale_matrix(0, 2) = 0.0f;				pra_scale_matrix(0, 3) = 0.0f;
			pra_scale_matrix(1, 0) = 0.0f;							pra_scale_matrix(1, 1) = float(height_ratio);	pra_scale_matrix(1, 2) = 0.0f;				pra_scale_matrix(1, 3) = 0.0f;
			pra_scale_matrix(2, 0) = 0.0f;							pra_scale_matrix(2, 1) = 0.0f;						pra_scale_matrix(2, 2) = 1.0f;				pra_scale_matrix(2, 3) = 0.0f;
			pra_scale_matrix(3, 0) = 0.0f;							pra_scale_matrix(3, 1) = 0.0f;						pra_scale_matrix(3, 2) = 0.0f;				pra_scale_matrix(3, 3) = 1.0f;

			for (mean_It = src_mesh.vertices_begin(); mean_It != src_mesh.vertices_end(); ++mean_It)
			{
				each_v = src_mesh.point(mean_It.handle());
				Eigen::Vector4f target_transformed_points = Eigen::Vector4f(each_v[0] - mix_mean_x, each_v[1] - mix_mean_y, each_v[2] - mix_mean_z, 1.0);
				target_transformed_points = pra_scale_matrix * target_transformed_points;

				target_transformed_points(0) = target_transformed_points(0) / target_transformed_points(3);
				target_transformed_points(1) = target_transformed_points(1) / target_transformed_points(3);
				target_transformed_points(2) = target_transformed_points(2) / target_transformed_points(3);

				src_mesh.point(mean_It.handle()) = OpenMesh::Vec3f(target_transformed_points(0) + mix_mean_x, target_transformed_points(1) + mix_mean_y, target_transformed_points(2) + mix_mean_z);
				//target_shape_vertices[t_vIt->idx()] = Eigen::Vector3f(target_transformed_points(0) + mean_x, target_transformed_points(1) + mean_y, target_transformed_points(2) + mean_z);
			}

			vh = src_mesh.vertex_handle(3787);
			v_mix_row1 = src_mesh.point(vh);

			vh = beauty_mesh.vertex_handle(3787);
			v_mix_row2 = beauty_mesh.point(vh);

			for (mean_It = src_mesh.vertices_begin(); mean_It != src_mesh.vertices_end(); ++mean_It)
			{
				each_v = src_mesh.point(mean_It.handle());
				src_mesh.point(mean_It.handle()) = OpenMesh::Vec3f(each_v[0] + v_mix_row2[0] - v_mix_row1[0], each_v[1] + v_mix_row2[1] - v_mix_row1[1], each_v[2] + v_mix_row2[2] - v_mix_row1[2]);
			}

			src_mesh.update_face_normals();
			src_mesh.update_vertex_normals();

			OpenMesh::IO::write_mesh(src_mesh, "mixed_mesh.obj");

		}













		///////////////////////shape landmark ////////////////////////////
		int shape_landmark_index;
		FILE *fp_shape_landmark = fopen("shape_landmark.txt", "r");
		fscanf(fp_shape_landmark, "%d \n", &num_shape_landmark);
		fscanf(fp_shape_landmark, "space\n");

		for (int i = 0; i < num_shape_landmark; i++)
		{
			fscanf(fp_shape_landmark, "%d \n", &shape_landmark_index);
			landmark_shape_indices.push_back(shape_landmark_index);
		}
		fclose(fp_shape_landmark);

		////////////////////shape inside////////////////////////
		int shape_inside_index;
		FILE *fp_shape_inside = fopen("shape_inside.txt", "r");
		fscanf(fp_shape_inside, "%d\n", &num_shape_inside);
		fscanf(fp_shape_inside, "space\n");

		for (int i = 0; i < num_shape_inside; i++)
		{
			fscanf(fp_shape_inside, "%d \n", &shape_inside_index);
			inside_shape_indices.push_back(shape_inside_index);
		}

		fclose(fp_shape_inside);

		/////////////right cheek landmark////////////////////
		int right_landmark_index;
		FILE *fp_rightCheek_landmark = fopen("right_cheek_landmark.txt", "r");
		fscanf(fp_rightCheek_landmark, "%d\n", &num_rightCheek_landmark);
		fscanf(fp_rightCheek_landmark, "space\n");

		for (int i = 0; i < num_rightCheek_landmark; i++)
		{
			fscanf(fp_rightCheek_landmark, "%d \n", &right_landmark_index);
			landmark_rightCheek_indices.push_back(right_landmark_index);
		}

		fclose(fp_rightCheek_landmark);

		/////////////left cheek landmark////////////////////
		int left_landmark_index;
		FILE *fp_leftCheek_landmark = fopen("left_cheek_landmark.txt", "r");
		fscanf(fp_leftCheek_landmark, "%d\n", &num_leftCheek_landmark);
		fscanf(fp_leftCheek_landmark, "space\n");

		for (int i = 0; i < num_leftCheek_landmark; i++)
		{
			fscanf(fp_leftCheek_landmark, "%d \n", &left_landmark_index);
			landmark_leftCheek_indices.push_back(left_landmark_index);
		}

		fclose(fp_leftCheek_landmark);

	}
	else {
		std::string shape_type_file = "face_";
		shape_type_file = shape_type_file + std::to_string(change_shape_state) + ".obj";
		readMesh(shape_type_file, src_mesh);
	}

	//////////////////////////////////////////////////
	////////////part mesh & landmark & inside //////////////////////
	//////////////////////////////////////////////

	bool need_change_eye = false;
	bool need_change_nose = false;
	bool need_change_mouse = false;

	std::vector<int> landmark_eyes_indices;
	std::vector<int> landmark_nose_indices;
	std::vector<int> landmark_mouse_indices;

	std::vector<int> landmark_l_eyes_indices;

	std::vector<int> inside_eyes_indices;
	std::vector<int> inside_nose_indices;
	std::vector<int> inside_mouse_indices;

	std::vector<int> inside_l_eyes_indices;

	int num_eyes_landmark = 0;
	int num_nose_landmark = 0;
	int num_mouse_landmark = 0;

	int num_l_eyes_landmark = 0;

	int num_eyes_inside = 0;
	int num_nose_inside = 0;
	int num_mouse_inside = 0;

	int num_l_eyes_inside = 0;

	///////////////////////////////////////////////////
	////////////////eyebrow information///////////////
	//////////////////////////////////////////////////

	std::vector<int> landmark_r_eyebrow_indices;
	std::vector<int> landmark_l_eyebrow_indices;
	int num_r_eyebrow_landmark = 0;
	int num_l_eyebrow_landmark = 0;

	MyMesh target_eyebrow_mesh;

	if (eyebrow_bool == 1)
	{
		//readMesh("face_0.obj", target_eyebrow_mesh);
		target_eyebrow_mesh = mesh_3dmm;

		/////Right eyebrow
		int landmark_index;
		FILE *fp_r_eyebrow_landmark = fopen("r_eyeBrows_landmark.txt", "r");
		fscanf(fp_r_eyebrow_landmark, "%d \n", &num_r_eyebrow_landmark);
		fscanf(fp_r_eyebrow_landmark, "space\n");

		for (int i = 0; i < num_r_eyebrow_landmark; i++)
		{
			fscanf(fp_r_eyebrow_landmark, "%d \n", &landmark_index);
			landmark_r_eyebrow_indices.push_back(landmark_index);
		}
		fclose(fp_r_eyebrow_landmark);

		/////Left eyebrow
		FILE *fp_l_eyebrow_landmark = fopen("l_eyeBrows_landmark.txt", "r");
		fscanf(fp_l_eyebrow_landmark, "%d \n", &num_l_eyebrow_landmark);
		fscanf(fp_l_eyebrow_landmark, "space\n");

		for (int i = 0; i < num_l_eyebrow_landmark; i++)
		{
			fscanf(fp_l_eyebrow_landmark, "%d \n", &landmark_index);
			landmark_l_eyebrow_indices.push_back(landmark_index);
		}
		fclose(fp_l_eyebrow_landmark);

	}

	//////////////eye state///////////
	if (change_eyes_state != change_shape_state)
	{
		need_change_eye = true;

		/// mesh
		if (change_eyes_state == 0)
		{
			target_eyes_mesh = mesh_3dmm;
		}
		else
		{
			std::string eyes_type_file = "face_";
			eyes_type_file = eyes_type_file + std::to_string(change_eyes_state) + ".obj";
			readMesh(eyes_type_file, target_eyes_mesh);
		}

		/////Right landmark
		int landmark_index;
		//FILE *fp_eyes_landmark = fopen(argv[5], "r");
		FILE *fp_eyes_landmark = fopen("r_eyes_landmark.txt", "r");
		fscanf(fp_eyes_landmark, "%d \n", &num_eyes_landmark);
		fscanf(fp_eyes_landmark, "space\n");

		for (int i = 0; i < num_eyes_landmark; i++)
		{
			fscanf(fp_eyes_landmark, "%d \n", &landmark_index);
			landmark_eyes_indices.push_back(landmark_index);
		}
		fclose(fp_eyes_landmark);


		/////Left landmark
		//int landmark_index;
		//FILE *fp_l_eyes_landmark = fopen(argv[6], "r");
		FILE *fp_l_eyes_landmark = fopen("l_eyes_landmark.txt", "r");
		fscanf(fp_l_eyes_landmark, "%d \n", &num_l_eyes_landmark);
		fscanf(fp_l_eyes_landmark, "space\n");

		for (int i = 0; i < num_l_eyes_landmark; i++)
		{
			fscanf(fp_l_eyes_landmark, "%d \n", &landmark_index);
			landmark_l_eyes_indices.push_back(landmark_index);
		}
		fclose(fp_l_eyes_landmark);

		///////inside
		int inside_index;
		//FILE *fp_eyes_inside = fopen(argv[9], "r"); // inside_eyes_indices
		FILE *fp_eyes_inside = fopen("Right_eye_part.txt", "r");
		fscanf(fp_eyes_inside, "%d \n", &num_eyes_inside);
		fscanf(fp_eyes_inside, "space\n");

		for (int i = 0; i < num_eyes_inside; i++)
		{
			fscanf(fp_eyes_inside, "%d\n", &inside_index);
			inside_eyes_indices.push_back(inside_index);
		}
		fclose(fp_eyes_inside);

		///////////////inside left eye
		//FILE *fp_l_eyes_inside = fopen(argv[10], "r"); // inside_eyes_indices
		FILE *fp_l_eyes_inside = fopen("Left_eye_part.txt", "r");
		fscanf(fp_l_eyes_inside, "%d \n", &num_l_eyes_inside);
		fscanf(fp_l_eyes_inside, "space\n");

		for (int i = 0; i < num_l_eyes_inside; i++)
		{
			fscanf(fp_l_eyes_inside, "%d\n", &inside_index);
			inside_l_eyes_indices.push_back(inside_index);
		}
		fclose(fp_l_eyes_inside);

	}

	/////////////////////////////////beauty eye scale apply
	if (change_eyes_state != 0)
	{
		//////OpenMesh::Vec3f v_BeautyEye_row1, v_BeautyEye_row2, v_BeautyEye_col1, v_BeautyEye_col2;

		//////vh = target_eyes_mesh.vertex_handle(eye_width_factor_1);
		//////v_BeautyEye_row1 = target_eyes_mesh.point(vh);

		//////vh = target_eyes_mesh.vertex_handle(eye_width_factor_2);
		//////v_BeautyEye_row2 = target_eyes_mesh.point(vh);

		//////vh = target_eyes_mesh.vertex_handle(eye_height_factor_1);
		//////v_BeautyEye_col1 = target_eyes_mesh.point(vh);

		//////vh = target_eyes_mesh.vertex_handle(eye_height_factor_2);
		//////v_BeautyEye_col2 = target_eyes_mesh.point(vh);

		//////Eigen::Vector3f beauty_eyeRow_difference;
		//////Eigen::Vector3f beauty_eyeCol_difference;

		//////float beauty_eyeRow_distance;
		//////float beauty_eyeCol_distance;

		//////beauty_eyeRow_difference = Eigen::Vector3f(v_BeautyEye_row2[0] - v_BeautyEye_row1[0], v_BeautyEye_row2[1] - v_BeautyEye_row1[1], v_BeautyEye_row2[2] - v_BeautyEye_row1[2]);
		//////beauty_eyeRow_distance = beauty_eyeRow_difference.norm();

		//////beauty_eyeCol_difference = Eigen::Vector3f(v_BeautyEye_col2[0] - v_BeautyEye_col1[0], v_BeautyEye_col2[1] - v_BeautyEye_col1[1], v_BeautyEye_col2[2] - v_BeautyEye_col1[2]);
		//////beauty_eyeCol_distance = beauty_eyeCol_difference.norm();

		////////left eye
		//////OpenMesh::Vec3f v_BeautyLeftEye_row1, v_BeautyLeftEye_row2, v_BeautyLeftEye_col1, v_BeautyLeftEye_col2;

		//////vh = target_eyes_mesh.vertex_handle(left_eye_width_factor_1);
		//////v_BeautyLeftEye_row1 = target_eyes_mesh.point(vh);

		//////vh = target_eyes_mesh.vertex_handle(left_eye_width_factor_2);
		//////v_BeautyLeftEye_row2 = target_eyes_mesh.point(vh);

		//////vh = target_eyes_mesh.vertex_handle(left_eye_height_factor_1);
		//////v_BeautyLeftEye_col1 = target_eyes_mesh.point(vh);

		//////vh = target_eyes_mesh.vertex_handle(left_eye_height_factor_2);
		//////v_BeautyLeftEye_col2 = target_eyes_mesh.point(vh);

		//////Eigen::Vector3f beauty_leftEyeRow_difference;
		//////Eigen::Vector3f beauty_leftEyeCol_difference;

		//////float beauty_leftEyeRow_distance;
		//////float beauty_leftEyeCol_distance;

		//////beauty_leftEyeRow_difference = Eigen::Vector3f(v_BeautyLeftEye_row2[0] - v_BeautyLeftEye_row1[0], v_BeautyLeftEye_row2[1] - v_BeautyLeftEye_row1[1], v_BeautyLeftEye_row2[2] - v_BeautyLeftEye_row1[2]);
		//////beauty_leftEyeRow_distance = beauty_leftEyeRow_difference.norm();

		//////beauty_leftEyeCol_difference = Eigen::Vector3f(v_BeautyLeftEye_col2[0] - v_BeautyLeftEye_col1[0], v_BeautyLeftEye_col2[1] - v_BeautyLeftEye_col1[1], v_BeautyLeftEye_col2[2] - v_BeautyLeftEye_col1[2]);
		//////beauty_leftEyeCol_distance = beauty_leftEyeCol_difference.norm();

		//////
		//////////ratio calculation
		//////beauty_current_eyeArea = (beauty_eyeRow_distance/2) * (beauty_eyeCol_distance/2) + (beauty_leftEyeRow_distance/2) * (beauty_leftEyeCol_distance/2);

		//////eyeArea_ratio = real_current_eyeArea / real_mean_eyeArea * beauty_mean_eyeArea;
		//////eyeArea_ratio = eyeArea_ratio / beauty_current_eyeArea;
		//////eyeArea_ratio = sqrt(eyeArea_ratio);
		////////eyeArea_ratio = sqrt(eyeArea_ratio);

		//////eye_scale_factor = eye_scale_factor * eyeArea_ratio;	

		if (eye_scale_condition == 1)
		{

			OpenMesh::Vec3f v_R_eye_t1, v_R_eye_t2, v_R_eye_t3, v_R_eye_t4, v_R_eye_t5;
			OpenMesh::Vec3f v_R_eye_b1, v_R_eye_b2, v_R_eye_b3, v_R_eye_b4, v_R_eye_b5;

			vh = target_eyes_mesh.vertex_handle(R_eye_factor_t1);
			v_R_eye_t1 = target_eyes_mesh.point(vh);

			vh = target_eyes_mesh.vertex_handle(R_eye_factor_t2);
			v_R_eye_t2 = target_eyes_mesh.point(vh);

			vh = target_eyes_mesh.vertex_handle(R_eye_factor_t3);
			v_R_eye_t3 = target_eyes_mesh.point(vh);

			vh = target_eyes_mesh.vertex_handle(R_eye_factor_t4);
			v_R_eye_t4 = target_eyes_mesh.point(vh);

			vh = target_eyes_mesh.vertex_handle(R_eye_factor_t5);
			v_R_eye_t5 = target_eyes_mesh.point(vh);

			//
			vh = target_eyes_mesh.vertex_handle(R_eye_factor_b1);
			v_R_eye_b1 = target_eyes_mesh.point(vh);

			vh = target_eyes_mesh.vertex_handle(R_eye_factor_b2);
			v_R_eye_b2 = target_eyes_mesh.point(vh);

			vh = target_eyes_mesh.vertex_handle(R_eye_factor_b3);
			v_R_eye_b3 = target_eyes_mesh.point(vh);

			vh = target_eyes_mesh.vertex_handle(R_eye_factor_b4);
			v_R_eye_b4 = target_eyes_mesh.point(vh);

			vh = target_eyes_mesh.vertex_handle(R_eye_factor_b5);
			v_R_eye_b5 = target_eyes_mesh.point(vh);

			///area
			Eigen::Vector3f area1_width, area2_width, area3_width, area4_width;
			Eigen::Vector3f area1_height, area2_height, area3_height, area4_height;

			float area1_width_distance, area2_width_distance, area3_width_distance, area4_width_distance, area5_width_distance;
			float area1_height_distance, area2_height_distance, area3_height_distance, area4_height_distance, area5_height_distance;

			area1_width = Eigen::Vector3f(v_R_eye_t2[0] - v_R_eye_t1[0], v_R_eye_t2[1] - v_R_eye_t1[1], v_R_eye_t2[2] - v_R_eye_t1[2]);
			area1_width_distance = area1_width.norm();

			area2_width = Eigen::Vector3f(v_R_eye_t3[0] - v_R_eye_t2[0], v_R_eye_t3[1] - v_R_eye_t2[1], v_R_eye_t3[2] - v_R_eye_t2[2]);
			area2_width_distance = area2_width.norm();

			area3_width = Eigen::Vector3f(v_R_eye_t4[0] - v_R_eye_t3[0], v_R_eye_t4[1] - v_R_eye_t3[1], v_R_eye_t4[2] - v_R_eye_t3[2]);
			area3_width_distance = area3_width.norm();

			area4_width = Eigen::Vector3f(v_R_eye_t5[0] - v_R_eye_t4[0], v_R_eye_t5[1] - v_R_eye_t4[1], v_R_eye_t5[2] - v_R_eye_t4[2]);
			area4_width_distance = area4_width.norm();

			//
			area1_height = Eigen::Vector3f(v_R_eye_t1[0] - v_R_eye_b1[0], v_R_eye_t1[1] - v_R_eye_b1[1], v_R_eye_t1[2] - v_R_eye_b1[2]);
			area1_height_distance = area1_height.norm();

			area2_height = Eigen::Vector3f(v_R_eye_t2[0] - v_R_eye_b2[0], v_R_eye_t2[1] - v_R_eye_b2[1], v_R_eye_t2[2] - v_R_eye_b2[2]);
			area2_height_distance = area2_height.norm();

			area3_height = Eigen::Vector3f(v_R_eye_t3[0] - v_R_eye_b3[0], v_R_eye_t3[1] - v_R_eye_b3[1], v_R_eye_t3[2] - v_R_eye_b3[2]);
			area3_height_distance = area3_height.norm();

			area4_height = Eigen::Vector3f(v_R_eye_t4[0] - v_R_eye_b4[0], v_R_eye_t4[1] - v_R_eye_b4[1], v_R_eye_t4[2] - v_R_eye_b4[2]);
			area4_height_distance = area4_height.norm();

			beauty_current_eyeArea = (area1_width_distance * area1_height_distance) + (area2_width_distance * area2_height_distance) + (area3_width_distance * area3_height_distance) + (area4_width_distance * area4_height_distance);

			////Left eye
			OpenMesh::Vec3f v_L_eye_t1, v_L_eye_t2, v_L_eye_t3, v_L_eye_t4, v_L_eye_t5;
			OpenMesh::Vec3f v_L_eye_b1, v_L_eye_b2, v_L_eye_b3, v_L_eye_b4, v_L_eye_b5;

			vh = target_eyes_mesh.vertex_handle(L_eye_factor_t1);
			v_L_eye_t1 = target_eyes_mesh.point(vh);

			vh = target_eyes_mesh.vertex_handle(L_eye_factor_t2);
			v_L_eye_t2 = target_eyes_mesh.point(vh);

			vh = target_eyes_mesh.vertex_handle(L_eye_factor_t3);
			v_L_eye_t3 = target_eyes_mesh.point(vh);

			vh = target_eyes_mesh.vertex_handle(L_eye_factor_t4);
			v_L_eye_t4 = target_eyes_mesh.point(vh);

			vh = target_eyes_mesh.vertex_handle(L_eye_factor_t5);
			v_L_eye_t5 = target_eyes_mesh.point(vh);

			///
			vh = target_eyes_mesh.vertex_handle(L_eye_factor_b1);
			v_L_eye_b1 = target_eyes_mesh.point(vh);

			vh = target_eyes_mesh.vertex_handle(L_eye_factor_b2);
			v_L_eye_b2 = target_eyes_mesh.point(vh);

			vh = target_eyes_mesh.vertex_handle(L_eye_factor_b3);
			v_L_eye_b3 = target_eyes_mesh.point(vh);

			vh = target_eyes_mesh.vertex_handle(L_eye_factor_b4);
			v_L_eye_b4 = target_eyes_mesh.point(vh);

			vh = target_eyes_mesh.vertex_handle(L_eye_factor_b5);
			v_L_eye_b5 = target_eyes_mesh.point(vh);

			///area
			Eigen::Vector3f area1_width_L, area2_width_L, area3_width_L, area4_width_L;
			Eigen::Vector3f area1_height_L, area2_height_L, area3_height_L, area4_height_L;

			float area1_width_distance_L, area2_width_distance_L, area3_width_distance_L, area4_width_distance_L;
			float area1_height_distance_L, area2_height_distance_L, area3_height_distance_L, area4_height_distance_L;

			area1_width_L = Eigen::Vector3f(v_L_eye_t2[0] - v_L_eye_t1[0], v_L_eye_t2[1] - v_L_eye_t1[1], v_L_eye_t2[2] - v_L_eye_t1[2]);
			area1_width_distance_L = area1_width_L.norm();

			area2_width_L = Eigen::Vector3f(v_L_eye_t3[0] - v_L_eye_t2[0], v_L_eye_t3[1] - v_L_eye_t2[1], v_L_eye_t3[2] - v_L_eye_t2[2]);
			area2_width_distance_L = area2_width_L.norm();

			area3_width_L = Eigen::Vector3f(v_L_eye_t4[0] - v_L_eye_t3[0], v_L_eye_t4[1] - v_L_eye_t3[1], v_L_eye_t4[2] - v_L_eye_t3[2]);
			area3_width_distance_L = area3_width_L.norm();

			area4_width_L = Eigen::Vector3f(v_L_eye_t5[0] - v_L_eye_t4[0], v_L_eye_t5[1] - v_L_eye_t4[1], v_L_eye_t5[2] - v_L_eye_t4[2]);
			area4_width_distance_L = area4_width_L.norm();

			//
			area1_height_L = Eigen::Vector3f(v_L_eye_t1[0] - v_L_eye_b1[0], v_L_eye_t1[1] - v_L_eye_b1[1], v_L_eye_t1[2] - v_L_eye_b1[2]);
			area1_height_distance_L = area1_height_L.norm();

			area2_height_L = Eigen::Vector3f(v_L_eye_t2[0] - v_L_eye_b2[0], v_L_eye_t2[1] - v_L_eye_b2[1], v_L_eye_t2[2] - v_L_eye_b2[2]);
			area2_height_distance_L = area2_height_L.norm();

			area3_height_L = Eigen::Vector3f(v_L_eye_t3[0] - v_L_eye_b3[0], v_L_eye_t3[1] - v_L_eye_b3[1], v_L_eye_t3[2] - v_L_eye_b3[2]);
			area3_height_distance_L = area3_height_L.norm();

			area4_height_L = Eigen::Vector3f(v_L_eye_t4[0] - v_L_eye_b4[0], v_L_eye_t4[1] - v_L_eye_b4[1], v_L_eye_t4[2] - v_L_eye_b4[2]);
			area4_height_distance_L = area4_height_L.norm();

			beauty_current_eyeArea = beauty_current_eyeArea + (area1_width_distance_L * area1_height_distance_L) + (area2_width_distance_L * area2_height_distance_L) + (area3_width_distance_L * area3_height_distance_L) + (area4_width_distance_L * area4_height_distance_L);

			eyeArea_ratio = real_current_eyeArea / real_mean_eyeArea * beauty_mean_eyeArea;
			eyeArea_ratio = eyeArea_ratio / beauty_current_eyeArea;
			eyeArea_ratio = sqrt(eyeArea_ratio);
			eyeArea_ratio = sqrt(eyeArea_ratio);

			eye_scale_factor = eye_scale_factor * eyeArea_ratio;
		} //if (eye_scale_condition == 1)

		if (eye_scale_condition == 2)
		{
			// right eye
			OpenMesh::Vec3f v_BeautyEye_row1, v_BeautyEye_row2;

			vh = target_eyes_mesh.vertex_handle(eye_width_factor_1);
			v_BeautyEye_row1 = target_eyes_mesh.point(vh);

			vh = target_eyes_mesh.vertex_handle(eye_width_factor_2);
			v_BeautyEye_row2 = target_eyes_mesh.point(vh);

			Eigen::Vector3f beauty_eyeRow_difference;
			float beauty_eyeRow_distance;

			beauty_eyeRow_difference = Eigen::Vector3f(v_BeautyEye_row2[0] - v_BeautyEye_row1[0], v_BeautyEye_row2[1] - v_BeautyEye_row1[1], v_BeautyEye_row2[2] - v_BeautyEye_row1[2]);
			beauty_eyeRow_distance = beauty_eyeRow_difference.norm();

			beauty_current_eyeWidth_right = beauty_eyeRow_distance;


			////right eye height
			OpenMesh::Vec3f v_R_eye_t1, v_R_eye_t2, v_R_eye_t3, v_R_eye_t4, v_R_eye_t5;
			OpenMesh::Vec3f v_R_eye_b1, v_R_eye_b2, v_R_eye_b3, v_R_eye_b4, v_R_eye_b5;

			vh = target_eyes_mesh.vertex_handle(R_eye_factor_t1);
			v_R_eye_t1 = target_eyes_mesh.point(vh);

			vh = target_eyes_mesh.vertex_handle(R_eye_factor_t2);
			v_R_eye_t2 = target_eyes_mesh.point(vh);

			vh = target_eyes_mesh.vertex_handle(R_eye_factor_t3);
			v_R_eye_t3 = target_eyes_mesh.point(vh);

			vh = target_eyes_mesh.vertex_handle(R_eye_factor_t4);
			v_R_eye_t4 = target_eyes_mesh.point(vh);

			vh = target_eyes_mesh.vertex_handle(R_eye_factor_t5);
			v_R_eye_t5 = target_eyes_mesh.point(vh);

			//
			vh = target_eyes_mesh.vertex_handle(R_eye_factor_b1);
			v_R_eye_b1 = target_eyes_mesh.point(vh);

			vh = target_eyes_mesh.vertex_handle(R_eye_factor_b2);
			v_R_eye_b2 = target_eyes_mesh.point(vh);

			vh = target_eyes_mesh.vertex_handle(R_eye_factor_b3);
			v_R_eye_b3 = target_eyes_mesh.point(vh);

			vh = target_eyes_mesh.vertex_handle(R_eye_factor_b4);
			v_R_eye_b4 = target_eyes_mesh.point(vh);

			vh = target_eyes_mesh.vertex_handle(R_eye_factor_b5);
			v_R_eye_b5 = target_eyes_mesh.point(vh);


			/// height calculation
			Eigen::Vector3f col1_height, col2_height, col3_height, col4_height, col5_height;
			float col1_height_distance, col2_height_distance, col3_height_distance, col4_height_distance, col5_height_distance;

			col1_height = Eigen::Vector3f(v_R_eye_t1[0] - v_R_eye_b1[0], v_R_eye_t1[1] - v_R_eye_b1[1], v_R_eye_t1[2] - v_R_eye_b1[2]);
			col1_height_distance = col1_height.norm();

			col2_height = Eigen::Vector3f(v_R_eye_t2[0] - v_R_eye_b2[0], v_R_eye_t2[1] - v_R_eye_b2[1], v_R_eye_t2[2] - v_R_eye_b2[2]);
			col2_height_distance = col2_height.norm();

			col3_height = Eigen::Vector3f(v_R_eye_t3[0] - v_R_eye_b3[0], v_R_eye_t3[1] - v_R_eye_b3[1], v_R_eye_t3[2] - v_R_eye_b3[2]);
			col3_height_distance = col3_height.norm();

			col4_height = Eigen::Vector3f(v_R_eye_t4[0] - v_R_eye_b4[0], v_R_eye_t4[1] - v_R_eye_b4[1], v_R_eye_t4[2] - v_R_eye_b4[2]);
			col4_height_distance = col4_height.norm();

			col5_height = Eigen::Vector3f(v_R_eye_t5[0] - v_R_eye_b5[0], v_R_eye_t5[1] - v_R_eye_b5[1], v_R_eye_t5[2] - v_R_eye_b5[2]);
			col5_height_distance = col5_height.norm();

			beauty_current_eyesHeight_right = col1_height_distance + col2_height_distance + col3_height_distance + col4_height_distance + col5_height_distance;


			//left eye
			OpenMesh::Vec3f v_BeautyLeftEye_row1, v_BeautyLeftEye_row2, v_BeautyLeftEye_col1, v_BeautyLeftEye_col2;

			vh = target_eyes_mesh.vertex_handle(left_eye_width_factor_1);
			v_BeautyLeftEye_row1 = target_eyes_mesh.point(vh);

			vh = target_eyes_mesh.vertex_handle(left_eye_width_factor_2);
			v_BeautyLeftEye_row2 = target_eyes_mesh.point(vh);

			Eigen::Vector3f beauty_leftEyeRow_difference;

			float beauty_leftEyeRow_distance;

			beauty_leftEyeRow_difference = Eigen::Vector3f(v_BeautyLeftEye_row2[0] - v_BeautyLeftEye_row1[0], v_BeautyLeftEye_row2[1] - v_BeautyLeftEye_row1[1], v_BeautyLeftEye_row2[2] - v_BeautyLeftEye_row1[2]);
			beauty_leftEyeRow_distance = beauty_leftEyeRow_difference.norm();

			beauty_current_eyeWidth_left = beauty_leftEyeRow_distance;

			//height calculation
			OpenMesh::Vec3f v_L_eye_t1, v_L_eye_t2, v_L_eye_t3, v_L_eye_t4, v_L_eye_t5;
			OpenMesh::Vec3f v_L_eye_b1, v_L_eye_b2, v_L_eye_b3, v_L_eye_b4, v_L_eye_b5;

			vh = mesh_3dmm.vertex_handle(L_eye_factor_t1);
			v_L_eye_t1 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(L_eye_factor_t2);
			v_L_eye_t2 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(L_eye_factor_t3);
			v_L_eye_t3 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(L_eye_factor_t4);
			v_L_eye_t4 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(L_eye_factor_t5);
			v_L_eye_t5 = mesh_3dmm.point(vh);

			///
			vh = mesh_3dmm.vertex_handle(L_eye_factor_b1);
			v_L_eye_b1 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(L_eye_factor_b2);
			v_L_eye_b2 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(L_eye_factor_b3);
			v_L_eye_b3 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(L_eye_factor_b4);
			v_L_eye_b4 = mesh_3dmm.point(vh);

			vh = mesh_3dmm.vertex_handle(L_eye_factor_b5);
			v_L_eye_b5 = mesh_3dmm.point(vh);

			/// height calculation
			Eigen::Vector3f col1_height_L, col2_height_L, col3_height_L, col4_height_L, col5_height_L;
			float col1_height_distance_L, col2_height_distance_L, col3_height_distance_L, col4_height_distance_L, col5_height_distance_L;

			col1_height_L = Eigen::Vector3f(v_L_eye_t1[0] - v_L_eye_b1[0], v_L_eye_t1[1] - v_L_eye_b1[1], v_L_eye_t1[2] - v_L_eye_b1[2]);
			col1_height_distance_L = col1_height_L.norm();

			col2_height_L = Eigen::Vector3f(v_L_eye_t2[0] - v_L_eye_b2[0], v_L_eye_t2[1] - v_L_eye_b2[1], v_L_eye_t2[2] - v_L_eye_b2[2]);
			col2_height_distance_L = col2_height_L.norm();

			col3_height_L = Eigen::Vector3f(v_L_eye_t3[0] - v_L_eye_b3[0], v_L_eye_t3[1] - v_L_eye_b3[1], v_L_eye_t3[2] - v_L_eye_b3[2]);
			col3_height_distance_L = col3_height_L.norm();

			col4_height_L = Eigen::Vector3f(v_L_eye_t4[0] - v_L_eye_b4[0], v_L_eye_t4[1] - v_L_eye_b4[1], v_L_eye_t4[2] - v_L_eye_b4[2]);
			col4_height_distance_L = col4_height_L.norm();

			col5_height_L = Eigen::Vector3f(v_L_eye_t5[0] - v_L_eye_b5[0], v_L_eye_t5[1] - v_L_eye_b5[1], v_L_eye_t5[2] - v_L_eye_b5[2]);
			col5_height_distance_L = col5_height_L.norm();

			beauty_current_eyesHeight_left = col1_height_distance_L + col2_height_distance_L + col3_height_distance_L + col4_height_distance_L + col5_height_distance_L;


			printf("3dmm_eye_Rightwidth: %f\n", real_current_eyeWidth_right);
			printf("3dmm_eye_Leftwidth: %f\n", real_current_eyeWidth_left);

			//ratio calculation
			rightEye_scale_width = rightEye_scale_width * real_current_eyeWidth_right / real_mean_eyeWidth_right * beauty_mean_eyeWidth_right;
			printf("expression_eye_Rightwidth: %f\n", rightEye_scale_width);
			rightEye_scale_width = rightEye_scale_width / beauty_current_eyeWidth_right;

			rightEye_scale_height = rightEye_scale_height * real_current_eyeHeight_right / real_mean_eyeHeight_right * beauty_mean_eyeHeight_right;
			rightEye_scale_height = rightEye_scale_height / beauty_current_eyesHeight_right;

			leftEye_scale_width = leftEye_scale_width * real_current_eyeWidth_left / real_mean_eyeWidth_left * beauty_mean_eyeWidth_left;
			printf("expression_eye_Leftwidth: %f\n", leftEye_scale_width);
			leftEye_scale_width = leftEye_scale_width / beauty_current_eyeWidth_left;

			leftEye_scale_height = leftEye_scale_height * real_current_eyeHeight_left / real_mean_eyeHeight_left * beauty_mean_eyeHeight_left;
			leftEye_scale_height = leftEye_scale_height / beauty_current_eyesHeight_left;

			float mean_width_scale = rightEye_scale_width + leftEye_scale_width;
			float mean_height_scale = rightEye_scale_height + leftEye_scale_height;

			rightEye_scale_width = mean_width_scale / 2;
			rightEye_scale_height = mean_height_scale / 2;
			leftEye_scale_width = mean_width_scale / 2;
			leftEye_scale_height = mean_height_scale / 2;



			//////beauty_current_eyeArea = (beauty_eyeRow_distance/2) * (beauty_eyeCol_distance/2) + (beauty_leftEyeRow_distance/2) * (beauty_leftEyeCol_distance/2);

			//////eyeArea_ratio = real_current_eyeArea / real_mean_eyeArea * beauty_mean_eyeArea;
			//////eyeArea_ratio = eyeArea_ratio / beauty_current_eyeArea;
			//////eyeArea_ratio = sqrt(eyeArea_ratio);
			////////eyeArea_ratio = sqrt(eyeArea_ratio);

			//////eye_scale_factor = eye_scale_factor * eyeArea_ratio;	

		} // if (eye_scale_condition == 2)
	}

	//////////////nose state//////
	if (change_nose_state != change_shape_state)
	{
		/////landmark
		int landmark_index;
		//FILE* fp_nose_landmark = fopen(argv[7], "r");
		FILE* fp_nose_landmark = fopen("nose_landmark.txt", "r");
		fscanf(fp_nose_landmark, "%d \n", &num_nose_landmark);
		fscanf(fp_nose_landmark, "space\n");

		for (int i = 0; i < num_nose_landmark; i++)
		{
			fscanf(fp_nose_landmark, "%d \n", &landmark_index);
			landmark_nose_indices.push_back(landmark_index);
		}
		fclose(fp_nose_landmark);

		///////inside
		int inside_index;
		//FILE *fp_nose_inside = fopen(argv[11], "r");
		FILE *fp_nose_inside = fopen("nose_inside.txt", "r");
		fscanf(fp_nose_inside, "%d \n", &num_nose_inside);
		fscanf(fp_nose_inside, "space\n");

		for (int i = 0; i < num_nose_inside; i++)
		{
			fscanf(fp_nose_inside, "%d \n", &inside_index);
			inside_nose_indices.push_back(inside_index);
		}
		fclose(fp_nose_inside);

		/////// mesh
		if (change_nose_state != change_eyes_state)
		{
			need_change_nose = true;

			if (change_nose_state == 0)
			{
				target_nose_mesh = mesh_3dmm;
			}
			else {
				std::string nose_type_file = "face_";
				nose_type_file = nose_type_file + std::to_string(change_nose_state) + ".obj";
				readMesh(nose_type_file, target_nose_mesh);
			}

		}
		else if (change_nose_state == change_eyes_state)
		{
			need_change_nose = true;
			target_nose_mesh = target_eyes_mesh;
		}

	}

	//////////////mouse state//////
	if (change_mouse_state != change_shape_state)
	{
		/////landmark
		int landmark_index;
		//FILE *fp_mouse_landmark = fopen(argv[8], "r");
		FILE *fp_mouse_landmark = fopen("mouth_landmark.txt", "r");
		fscanf(fp_mouse_landmark, "%d \n", &num_mouse_landmark);
		fscanf(fp_mouse_landmark, "space\n");

		for (int i = 0; i < num_mouse_landmark; i++)
		{
			fscanf(fp_mouse_landmark, "%d \n", &landmark_index);
			landmark_mouse_indices.push_back(landmark_index);
		}
		fclose(fp_mouse_landmark);

		/////inside
		int inside_index;
		//FILE *fp_mouse_inside = fopen(argv[12], "r");
		FILE *fp_mouse_inside = fopen("mouth_inside.txt", "r");
		fscanf(fp_mouse_inside, "%d \n", &num_mouse_inside);
		fscanf(fp_mouse_inside, "space\n");

		for (int i = 0; i < num_mouse_inside; i++)
		{
			fscanf(fp_mouse_inside, "%d \n", &inside_index);
			inside_mouse_indices.push_back(inside_index);
		}

		fclose(fp_mouse_inside);

		///////mesh
		if ((change_mouse_state != change_eyes_state) && (change_mouse_state != change_nose_state))
		{
			need_change_mouse = true;

			if (change_mouse_state == 0)
			{
				target_mouse_mesh = mesh_3dmm;
			}
			else {
				std::string mouse_type_file = "face_";
				mouse_type_file = mouse_type_file + std::to_string(change_mouse_state) + ".obj";
				readMesh(mouse_type_file, target_mouse_mesh);
			}

		}
		else if (change_mouse_state == change_eyes_state)
		{
			need_change_mouse = true;
			target_mouse_mesh = target_eyes_mesh;
		}
		else if (change_mouse_state == change_nose_state)
		{
			need_change_mouse = true;
			target_mouse_mesh = target_nose_mesh;
		}
	}

	if (length_lip_bool == 1)
	{
		OpenMesh::Vec3f v_Beautylips_row1, v_Beautylips_row2;

		vh = target_mouse_mesh.vertex_handle(lip_width_R);
		v_Beautylips_row1 = target_mouse_mesh.point(vh);

		vh = target_mouse_mesh.vertex_handle(lip_width_L);
		v_Beautylips_row2 = target_mouse_mesh.point(vh);

		Eigen::Vector3f beauty_lipsRow_difference;
		float beauty_lipsRow_distance;

		beauty_lipsRow_difference = Eigen::Vector3f(v_Beautylips_row2[0] - v_Beautylips_row1[0], v_Beautylips_row2[1] - v_Beautylips_row1[1], v_Beautylips_row2[2] - v_Beautylips_row1[2]);
		beauty_lipsRow_distance = beauty_lipsRow_difference.norm();

		//ratio calculation
		mouth_scale_width = mouth_scale_width * real_current_lipsWidth / real_mean_lipsWidth * beauty_mean_lipsWidth;
		mouth_scale_width = mouth_scale_width / beauty_lipsRow_distance;

		/// height ratio calculation
		OpenMesh::Vec3f v_lip_t1, v_lip_t2, v_lip_t3, v_lip_t4, v_lip_t5;
		OpenMesh::Vec3f v_lip_b1, v_lip_b2, v_lip_b3, v_lip_b4, v_lip_b5;

		vh = target_mouse_mesh.vertex_handle(lips_factor_t1);
		v_lip_t1 = target_mouse_mesh.point(vh);

		vh = target_mouse_mesh.vertex_handle(lips_factor_t2);
		v_lip_t2 = target_mouse_mesh.point(vh);

		vh = target_mouse_mesh.vertex_handle(lips_factor_t3);
		v_lip_t3 = target_mouse_mesh.point(vh);

		vh = target_mouse_mesh.vertex_handle(lips_factor_t4);
		v_lip_t4 = target_mouse_mesh.point(vh);

		vh = target_mouse_mesh.vertex_handle(lips_factor_t5);
		v_lip_t5 = target_mouse_mesh.point(vh);

		////
		vh = target_mouse_mesh.vertex_handle(lips_factor_b1);
		v_lip_b1 = target_mouse_mesh.point(vh);

		vh = target_mouse_mesh.vertex_handle(lips_factor_b2);
		v_lip_b2 = target_mouse_mesh.point(vh);

		vh = target_mouse_mesh.vertex_handle(lips_factor_b3);
		v_lip_b3 = target_mouse_mesh.point(vh);

		vh = target_mouse_mesh.vertex_handle(lips_factor_b4);
		v_lip_b4 = target_mouse_mesh.point(vh);

		vh = target_mouse_mesh.vertex_handle(lips_factor_b5);
		v_lip_b5 = target_mouse_mesh.point(vh);

		/////distance calculation
		Eigen::Vector3f col1_lip_current_difference, col2_lip_current_difference, col3_lip_current_difference, col4_lip_current_difference, col5_lip_current_difference;
		float col1_lip_distance, col2_lip_distance, col3_lip_current_distance, col4_lip_current_distance, col5_lip_current_distance;

		col1_lip_current_difference = Eigen::Vector3f(v_lip_t1[0] - v_lip_b1[0], v_lip_t1[1] - v_lip_b1[1], v_lip_t1[2] - v_lip_b1[2]);
		col1_lip_distance = col1_lip_current_difference.norm();

		col2_lip_current_difference = Eigen::Vector3f(v_lip_t2[0] - v_lip_b2[0], v_lip_t2[1] - v_lip_b2[1], v_lip_t2[2] - v_lip_b2[2]);
		col2_lip_distance = col2_lip_current_difference.norm();

		col3_lip_current_difference = Eigen::Vector3f(v_lip_t3[0] - v_lip_b3[0], v_lip_t3[1] - v_lip_b3[1], v_lip_t3[2] - v_lip_b3[2]);
		col3_lip_current_distance = col3_lip_current_difference.norm();

		col4_lip_current_difference = Eigen::Vector3f(v_lip_t4[0] - v_lip_b4[0], v_lip_t4[1] - v_lip_b4[1], v_lip_t4[2] - v_lip_b4[2]);
		col4_lip_current_distance = col4_lip_current_difference.norm();

		col5_lip_current_difference = Eigen::Vector3f(v_lip_t5[0] - v_lip_b5[0], v_lip_t5[1] - v_lip_b5[1], v_lip_t5[2] - v_lip_b5[2]);
		col5_lip_current_distance = col5_lip_current_difference.norm();

		beauty_current_lipsHeight = col1_lip_distance + col2_lip_distance + col3_lip_current_distance + col4_lip_current_distance + col5_lip_current_distance;

		mouth_scale_height = mouth_scale_height * real_current_lipsHeight / real_mean_lipsHeight * beauty_mean_lipsHeight;
		mouth_scale_height = mouth_scale_height / beauty_current_lipsHeight;

	}

	if (length_nose_bool == 1)
	{
		///////nose width
		OpenMesh::Vec3f v_nose_right, v_nose_left;

		vh = target_nose_mesh.vertex_handle(nose_width_R);
		v_nose_right = target_nose_mesh.point(vh);

		vh = target_nose_mesh.vertex_handle(nose_width_L);
		v_nose_left = target_nose_mesh.point(vh);

		Eigen::Vector3f nose_row_difference;
		float nose_width_distance;

		nose_row_difference = Eigen::Vector3f(v_nose_right[0] - v_nose_left[0], v_nose_right[1] - v_nose_left[1], v_nose_right[2] - v_nose_left[2]);
		nose_width_distance = nose_row_difference.norm();

		beauty_current_noseWidth = nose_width_distance;

		//ratio calculation
		nose_scale_width = nose_scale_width * real_current_noseWidth / real_mean_noseWidth * beauty_mean_noseWidth;
		nose_scale_width = nose_scale_width / beauty_current_noseWidth;

		// beauty nose height
		OpenMesh::Vec3f v_nose_t, v_nose_b;

		vh = target_nose_mesh.vertex_handle(nose_height_t);
		v_nose_t = target_nose_mesh.point(vh);

		vh = target_nose_mesh.vertex_handle(nose_height_b);
		v_nose_b = target_nose_mesh.point(vh);

		Eigen::Vector3f nose_col_difference;
		float nose_height_distance;

		nose_col_difference = Eigen::Vector3f(v_nose_t[0] - v_nose_b[0], v_nose_t[1] - v_nose_b[1], v_nose_t[2] - v_nose_b[2]);
		nose_height_distance = nose_col_difference.norm();

		beauty_current_noseHeight = nose_height_distance;

		//ratio calculation
		nose_scale_height = nose_scale_height * real_current_noseHeight / real_mean_noseHeight * beauty_mean_noseHeight;
		nose_scale_height = nose_scale_height / beauty_current_noseHeight;

	}
	//////////////////////////////////////////////////////////
	//////////////////shape ratio calculation/////////////////
	//////////////////////////////////////////////////////////
	float temp_mean_x = 0.0f, temp_mean_y = 0.0f, temp_mean_z = 0.0f;
	MyMesh::VertexIter temp_vIt;
	OpenMesh::Vec3f temp_v;
	std::vector<Eigen::Vector3f> temp_src;

	MyMesh temp_src_mesh = src_mesh;
	temp_src.resize(temp_src_mesh.n_vertices());

	for (temp_vIt = temp_src_mesh.vertices_begin(); temp_vIt != temp_src_mesh.vertices_end(); ++temp_vIt)
	{
		temp_v = temp_src_mesh.point(temp_vIt.handle());
		temp_src[temp_vIt->idx()] = Eigen::Vector3f(temp_v[0], temp_v[1], temp_v[2]);
		temp_mean_x += temp_v[0]; temp_mean_y += temp_v[1]; temp_mean_z += temp_v[2];
	}

	temp_mean_x = temp_mean_x / temp_src_mesh.n_vertices();
	temp_mean_y = temp_mean_y / temp_src_mesh.n_vertices();
	temp_mean_z = temp_mean_z / temp_src_mesh.n_vertices();



	if (change_shape_state == 100)
	{

		Eigen::Matrix4f pra_scale_matrix;

		pra_scale_matrix(0, 0) = float(face_width_ratio);		pra_scale_matrix(0, 1) = 0.0f;						pra_scale_matrix(0, 2) = 0.0f;				pra_scale_matrix(0, 3) = 0.0f;
		pra_scale_matrix(1, 0) = 0.0f;							pra_scale_matrix(1, 1) = float(face_height_ratio);	pra_scale_matrix(1, 2) = 0.0f;				pra_scale_matrix(1, 3) = 0.0f;
		pra_scale_matrix(2, 0) = 0.0f;							pra_scale_matrix(2, 1) = 0.0f;						pra_scale_matrix(2, 2) = 1.0f;				pra_scale_matrix(2, 3) = 0.0f;
		pra_scale_matrix(3, 0) = 0.0f;							pra_scale_matrix(3, 1) = 0.0f;						pra_scale_matrix(3, 2) = 0.0f;				pra_scale_matrix(3, 3) = 1.0f;

		for (temp_vIt = temp_src_mesh.vertices_begin(); temp_vIt != temp_src_mesh.vertices_end(); ++temp_vIt)
		{
			temp_v = temp_src_mesh.point(temp_vIt.handle());
			Eigen::Vector4f target_transformed_points = Eigen::Vector4f(temp_v[0] - temp_mean_x, temp_v[1] - temp_mean_y, temp_v[2] - temp_mean_z, 1.0);
			target_transformed_points = pra_scale_matrix * target_transformed_points;

			target_transformed_points(0) = target_transformed_points(0) / target_transformed_points(3);
			target_transformed_points(1) = target_transformed_points(1) / target_transformed_points(3);
			target_transformed_points(2) = target_transformed_points(2) / target_transformed_points(3);

			temp_src_mesh.point(temp_vIt.handle()) = OpenMesh::Vec3f(target_transformed_points(0) + temp_mean_x, target_transformed_points(1) + temp_mean_y, target_transformed_points(2) + temp_mean_z);
			//target_shape_vertices[t_vIt->idx()] = Eigen::Vector3f(target_transformed_points(0) + mean_x, target_transformed_points(1) + mean_y, target_transformed_points(2) + mean_z);
		}

		temp_src_mesh.update_face_normals();
		temp_src_mesh.update_vertex_normals();

		OpenMesh::IO::write_mesh(temp_src_mesh, "ICP_scaled_shape.obj");

	}

	src_mesh = temp_src_mesh;
	////////////pivot calculation/////////////

	float mean_x = 0.0f, mean_y = 0.0f, mean_z = 0.0f;
	float tar_mean_eyes_x = 0.0f, tar_mean_eyes_y = 0.0f, tar_mean_eyes_z = 0.0f;
	float tar_mean_nose_x = 0.0f, tar_mean_nose_y = 0.0f, tar_mean_nose_z = 0.0f;
	float tar_mean_mouse_x = 0.0f, tar_mean_mouse_y = 0.0f, tar_mean_mouse_z = 0.0f;

	MyMesh::VertexIter t_vIt;
	OpenMesh::Vec3f t_v;
	std::vector<Eigen::Vector3f> t_src, t_target_eyes, t_target_nose, t_target_mouse;

	t_src.resize(src_mesh.n_vertices());
	t_target_eyes.resize(target_eyes_mesh.n_vertices());
	t_target_nose.resize(target_nose_mesh.n_vertices());
	t_target_mouse.resize(target_mouse_mesh.n_vertices());

	OpenMesh::Vec3f v, v_normal;

	//////////////src mean calculation//////////////////////

	for (t_vIt = src_mesh.vertices_begin(); t_vIt != src_mesh.vertices_end(); ++t_vIt)
	{
		t_v = src_mesh.point(t_vIt.handle());
		t_src[t_vIt->idx()] = Eigen::Vector3f(t_v[0], t_v[1], t_v[2]);
		mean_x += t_v[0]; mean_y += t_v[1]; mean_z += t_v[2];
	}

	mean_x = mean_x / src_mesh.n_vertices();
	mean_y = mean_y / src_mesh.n_vertices();
	mean_z = mean_z / src_mesh.n_vertices();



	//////////////////condition shape == 100
	float tar_mean_shape_x = 0.0f, tar_mean_shape_y = 0.0f, tar_mean_shape_z = 0.0f;
	if (change_shape_state == 100)
	{
		for (t_vIt = target_shape_mesh.vertices_begin(); t_vIt != target_shape_mesh.vertices_end(); ++t_vIt)
		{
			t_v = target_shape_mesh.point(t_vIt.handle());
			tar_mean_shape_x += t_v[0]; tar_mean_shape_y += t_v[1]; tar_mean_shape_z += t_v[2];
		}

		tar_mean_shape_x = tar_mean_shape_x / target_shape_mesh.n_vertices();
		tar_mean_shape_y = tar_mean_shape_y / target_shape_mesh.n_vertices();
		tar_mean_shape_z = tar_mean_shape_z / target_shape_mesh.n_vertices();

	}

	/////////target mean/////////////
	float tar_mean_eyebrow_x = 0.0f, tar_mean_eyebrow_y = 0.0f, tar_mean_eyebrow_z = 0.0f;

	if (eyebrow_bool == true)
	{

		for (t_vIt = target_eyebrow_mesh.vertices_begin(); t_vIt != target_eyebrow_mesh.vertices_end(); ++t_vIt)
		{
			t_v = target_eyebrow_mesh.point(t_vIt.handle());
			//t_target_eyes[t_vIt->idx()] = Eigen::Vector3f(t_v[0], t_v[1], t_v[2]);
			tar_mean_eyebrow_x += t_v[0]; tar_mean_eyebrow_y += t_v[1]; tar_mean_eyebrow_z += t_v[2];
		}

		tar_mean_eyebrow_x = tar_mean_eyebrow_x / target_eyebrow_mesh.n_vertices();
		tar_mean_eyebrow_y = tar_mean_eyebrow_y / target_eyebrow_mesh.n_vertices();
		tar_mean_eyebrow_z = tar_mean_eyebrow_z / target_eyebrow_mesh.n_vertices();

	}

	///////////////////eyes mean////////////////
	if (need_change_eye == true)
	{

		for (t_vIt = target_eyes_mesh.vertices_begin(); t_vIt != target_eyes_mesh.vertices_end(); ++t_vIt)
		{
			t_v = target_eyes_mesh.point(t_vIt.handle());
			//t_target_eyes[t_vIt->idx()] = Eigen::Vector3f(t_v[0], t_v[1], t_v[2]);
			tar_mean_eyes_x += t_v[0]; tar_mean_eyes_y += t_v[1]; tar_mean_eyes_z += t_v[2];
		}

		tar_mean_eyes_x = tar_mean_eyes_x / target_eyes_mesh.n_vertices();
		tar_mean_eyes_y = tar_mean_eyes_y / target_eyes_mesh.n_vertices();
		tar_mean_eyes_z = tar_mean_eyes_z / target_eyes_mesh.n_vertices();

	}

	///////////////////nose mean////////////////
	if (need_change_nose == true)
	{

		for (t_vIt = target_nose_mesh.vertices_begin(); t_vIt != target_nose_mesh.vertices_end(); ++t_vIt)
		{
			t_v = target_nose_mesh.point(t_vIt.handle());
			//t_target_nose[t_vIt->idx()] = Eigen::Vector3f(t_v[0], t_v[1], t_v[2]);
			tar_mean_nose_x += t_v[0]; tar_mean_nose_y += t_v[1]; tar_mean_nose_z += t_v[2];
		}

		tar_mean_nose_x = tar_mean_nose_x / target_nose_mesh.n_vertices();
		tar_mean_nose_y = tar_mean_nose_y / target_nose_mesh.n_vertices();
		tar_mean_nose_z = tar_mean_nose_z / target_nose_mesh.n_vertices();

	}

	///////////////////mouse mean////////////////
	if (need_change_mouse == true)
	{

		for (t_vIt = target_mouse_mesh.vertices_begin(); t_vIt != target_mouse_mesh.vertices_end(); ++t_vIt)
		{
			t_v = target_mouse_mesh.point(t_vIt.handle());
			//t_target_mouse[t_vIt->idx()] = Eigen::Vector3f(t_v[0], t_v[1], t_v[2]);
			tar_mean_mouse_x += t_v[0]; tar_mean_mouse_y += t_v[1]; tar_mean_mouse_z += t_v[2];
		}

		tar_mean_mouse_x = tar_mean_mouse_x / target_mouse_mesh.n_vertices();
		tar_mean_mouse_y = tar_mean_mouse_y / target_mouse_mesh.n_vertices();
		tar_mean_mouse_z = tar_mean_mouse_z / target_mouse_mesh.n_vertices();

	}

	/////////////Scaled ICP setting///////////////////

	std::vector<Eigen::Vector3f> pre_vertices;
	pre_vertices.resize(src_mesh.n_vertices());

	//////////mean move //////////////

	for (t_vIt = src_mesh.vertices_begin(); t_vIt != src_mesh.vertices_end(); ++t_vIt) {
		t_v = src_mesh.point(t_vIt.handle());
		pre_vertices[t_vIt->idx()] = Eigen::Vector3f(t_v[0] - mean_x, t_v[1] - mean_y, t_v[2] - mean_z);
	}


	///////////////shape_state == 100 move/////
	std::vector<Eigen::Vector3f> pre_targetShape_vertices;
	pre_targetShape_vertices.resize(target_shape_mesh.n_vertices());

	MyMesh tar_shape_move;
	tar_shape_move = target_shape_mesh;

	if (change_shape_state == 100)
	{
		for (t_vIt = target_shape_mesh.vertices_begin(); t_vIt != target_shape_mesh.vertices_end(); ++t_vIt)
		{
			t_v = target_shape_mesh.point(t_vIt.handle());
			tar_shape_move.point(t_vIt.handle()) = OpenMesh::Vec3f(t_v[0] - tar_mean_shape_x, t_v[1] - tar_mean_shape_y, t_v[2] - tar_mean_shape_z);
			pre_targetShape_vertices[t_vIt->idx()] = Eigen::Vector3f(t_v[0] - tar_mean_shape_x, t_v[1] - tar_mean_shape_y, t_v[2] - tar_mean_shape_z);
		}

	}


	/////target move /////////////////////
	std::vector<Eigen::Vector3f> pre_targetEyes_vertices;
	std::vector<Eigen::Vector3f> pre_targetNose_vertices;
	std::vector<Eigen::Vector3f> pre_targetMouse_vertices;

	pre_targetEyes_vertices.resize(target_eyes_mesh.n_vertices());
	pre_targetNose_vertices.resize(target_nose_mesh.n_vertices());
	pre_targetMouse_vertices.resize(target_mouse_mesh.n_vertices());

	double eyes_scale = 1.0, nose_scale = 1.0, mouse_scale = 1.0, eyes_scale_left = 1.0;
	double *eyes_rotation, *eyes_translation, *nose_rotation, *nose_translation, *mouse_rotation, *mouse_translation, *eyes_rotation_left, *eyes_translation_left;

	MyMesh tar_eyes_move, tar_nose_move, tar_mouse_move;
	tar_eyes_move = target_eyes_mesh;
	tar_nose_move = target_nose_mesh;
	tar_mouse_move = target_mouse_mesh;

	MyMesh tar_eyebrow_move;
	tar_eyebrow_move = target_eyebrow_mesh;

	std::vector<Eigen::Vector3f> pre_targetEyebrow_vertices;
	pre_targetEyebrow_vertices.resize(target_eyebrow_mesh.n_vertices());

	if (eyebrow_bool == 1)
	{
		for (t_vIt = target_eyebrow_mesh.vertices_begin(); t_vIt != target_eyebrow_mesh.vertices_end(); ++t_vIt) {
			t_v = target_eyebrow_mesh.point(t_vIt.handle());
			tar_eyebrow_move.point(t_vIt.handle()) = OpenMesh::Vec3f(t_v[0] - tar_mean_eyebrow_x, t_v[1] - tar_mean_eyebrow_y, t_v[2] - tar_mean_eyebrow_z);
			pre_targetEyebrow_vertices[t_vIt->idx()] = Eigen::Vector3f(t_v[0] - tar_mean_eyebrow_x, t_v[1] - tar_mean_eyebrow_y, t_v[2] - tar_mean_eyebrow_z);
		}

	}


	if (need_change_eye == true)
	{
		for (t_vIt = target_eyes_mesh.vertices_begin(); t_vIt != target_eyes_mesh.vertices_end(); ++t_vIt) {
			t_v = target_eyes_mesh.point(t_vIt.handle());
			tar_eyes_move.point(t_vIt.handle()) = OpenMesh::Vec3f(t_v[0] - tar_mean_eyes_x, t_v[1] - tar_mean_eyes_y, t_v[2] - tar_mean_eyes_z);
			pre_targetEyes_vertices[t_vIt->idx()] = Eigen::Vector3f(t_v[0] - tar_mean_eyes_x, t_v[1] - tar_mean_eyes_y, t_v[2] - tar_mean_eyes_z);
		}

	}

	if (need_change_nose == true)
	{
		for (t_vIt = target_nose_mesh.vertices_begin(); t_vIt != target_nose_mesh.vertices_end(); ++t_vIt) {
			t_v = target_nose_mesh.point(t_vIt.handle());
			tar_nose_move.point(t_vIt.handle()) = OpenMesh::Vec3f(t_v[0] - tar_mean_nose_x, t_v[1] - tar_mean_nose_y, t_v[2] - tar_mean_nose_z);
			pre_targetNose_vertices[t_vIt->idx()] = Eigen::Vector3f(t_v[0] - tar_mean_nose_x, t_v[1] - tar_mean_nose_y, t_v[2] - tar_mean_nose_z);
		}

	}

	if (need_change_mouse == true)
	{
		for (t_vIt = target_mouse_mesh.vertices_begin(); t_vIt != target_mouse_mesh.vertices_end(); ++t_vIt) {
			t_v = target_mouse_mesh.point(t_vIt.handle());
			tar_mouse_move.point(t_vIt.handle()) = OpenMesh::Vec3f(t_v[0] - tar_mean_mouse_x, t_v[1] - tar_mean_mouse_y, t_v[2] - tar_mean_mouse_z);
			pre_targetMouse_vertices[t_vIt->idx()] = Eigen::Vector3f(t_v[0] - tar_mean_mouse_x, t_v[1] - tar_mean_mouse_y, t_v[2] - tar_mean_mouse_z);
		}

	}

	///////////////////////////////////////////////////////////////////////
	/////////////part height ratio////////////////////////////////////////////////////////
	float eye_R_Y = 0.0;
	float eye_L_Y = 0.0;
	float nose_Y = 0.0;
	float lips_Y = 0.0;

	float eye_average_Y = 0.0;

	if (position_Y_bool == 1)
	{
		MyMesh std_mesh;
		std::unordered_map<std::string, float> res_set;
		readMesh("0902_mix_mean.obj", std_mesh);

		res_set = calcYpositionRate(mesh_3dmm, std_mesh, temp_src_mesh);

		eye_R_Y = res_set.find("eye_R_Y")->second;
		eye_L_Y = res_set.find("eye_L_Y")->second;
		nose_Y = res_set.find("nose_Y")->second;
		lips_Y = res_set.find("lips_Y")->second;

		eye_average_Y = (eye_R_Y + eye_L_Y) / 2;

		if (lips_Y < -2.8)
		{
			lips_Y = -2.8;
		}
		if (lips_Y > 2.8)
		{
			lips_Y = 2.8;
		}


		//height
		//std::unordered_map<std::string, float> res_set_width;
		//res_set_width = calcXpositionRate(mesh_3dmm, std_mesh, temp_src_mesh);

	}







	//////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////


	///////////////shape_state == 100 ICP/////
	double shape_scale = 1.0;
	double *shape_rotation, *shape_translation;

	MyMesh target_transformed_shape = tar_shape_move;
	std::vector<Eigen::Vector3f> target_shape_vertices;
	target_shape_vertices.resize(target_transformed_shape.n_vertices());

	if (change_shape_state == 100)
	{
		ScaledRigid E_SICP_shape(pre_vertices, pre_targetShape_vertices, landmark_shape_indices, num_shape_landmark);
		double test_cost = E_SICP_shape.run();

		shape_scale = E_SICP_shape.getScale();
		shape_rotation = E_SICP_shape.getRotation();
		shape_translation = E_SICP_shape.getTranslation();

		double pra_rotation_matrix[9];
		ceres::AngleAxisToRotationMatrix(shape_rotation, pra_rotation_matrix);

		Eigen::Matrix4f pra_transformed_matrix;
		Eigen::Matrix4f pra_scale_matrix;

		pra_transformed_matrix(0, 0) = float(pra_rotation_matrix[0]); pra_transformed_matrix(0, 1) = float(pra_rotation_matrix[3]); pra_transformed_matrix(0, 2) = float(pra_rotation_matrix[6]); pra_transformed_matrix(0, 3) = float(shape_translation[0]);
		pra_transformed_matrix(1, 0) = float(pra_rotation_matrix[1]); pra_transformed_matrix(1, 1) = float(pra_rotation_matrix[4]); pra_transformed_matrix(1, 2) = float(pra_rotation_matrix[7]); pra_transformed_matrix(1, 3) = float(shape_translation[1]);
		pra_transformed_matrix(2, 0) = float(pra_rotation_matrix[2]); pra_transformed_matrix(2, 1) = float(pra_rotation_matrix[5]); pra_transformed_matrix(2, 2) = float(pra_rotation_matrix[8]); pra_transformed_matrix(2, 3) = float(shape_translation[2]);
		pra_transformed_matrix(3, 0) = 0.0f;						  pra_transformed_matrix(3, 1) = 0.0f;							pra_transformed_matrix(3, 2) = 0.0f;						  pra_transformed_matrix(3, 3) = 1.0f;

		pra_scale_matrix(0, 0) = float(shape_scale);		pra_scale_matrix(0, 1) = 0.0f;						pra_scale_matrix(0, 2) = 0.0f;						pra_scale_matrix(0, 3) = 0.0f;
		pra_scale_matrix(1, 0) = 0.0f;						pra_scale_matrix(1, 1) = float(shape_scale);		pra_scale_matrix(1, 2) = 0.0f;						pra_scale_matrix(1, 3) = 0.0f;
		pra_scale_matrix(2, 0) = 0.0f;						pra_scale_matrix(2, 1) = 0.0f;						pra_scale_matrix(2, 2) = float(shape_scale);		pra_scale_matrix(2, 3) = 0.0f;
		pra_scale_matrix(3, 0) = 0.0f;						pra_scale_matrix(3, 1) = 0.0f;						pra_scale_matrix(3, 2) = 0.0f;						pra_scale_matrix(3, 3) = 1.0f;

		std::cout << "shape_scale: " << shape_scale << std::endl;
		std::cout << "shape_rotation: " << shape_rotation[0] << " " << shape_rotation[1] << " " << shape_rotation[2] << std::endl;
		std::cout << "shape_translation:" << shape_translation[0] << " " << shape_translation[1] << "  " << shape_translation[2] << std::endl;

		for (t_vIt = target_transformed_shape.vertices_begin(); t_vIt != target_transformed_shape.vertices_end(); ++t_vIt)
		{
			t_v = target_transformed_shape.point(t_vIt.handle());
			Eigen::Vector4f target_transformed_points = Eigen::Vector4f(t_v[0], t_v[1], t_v[2], 1.0);

			target_transformed_points = pra_scale_matrix * pra_transformed_matrix * target_transformed_points;

			target_transformed_points(0) = target_transformed_points(0) / target_transformed_points(3);
			target_transformed_points(1) = target_transformed_points(1) / target_transformed_points(3);
			target_transformed_points(2) = target_transformed_points(2) / target_transformed_points(3);

			target_transformed_shape.point(t_vIt.handle()) = OpenMesh::Vec3f(target_transformed_points(0) + mean_x, target_transformed_points(1) + mean_y, target_transformed_points(2) + mean_z);
			target_shape_vertices[t_vIt->idx()] = Eigen::Vector3f(target_transformed_points(0) + mean_x, target_transformed_points(1) + mean_y, target_transformed_points(2) + mean_z);

		}

		target_transformed_shape.update_face_normals();
		target_transformed_shape.update_vertex_normals();

		//ICP_mesh_check
		OpenMesh::IO::write_mesh(target_transformed_shape, "ICP_shape_mesh.obj");

	}

	///////////////shape_state == 100 right cheek ICP/////
	double rightCheek_scale = 1.0;
	double *rightCheek_rotation, *rightCheek_translation;

	MyMesh target_rightCheek = tar_shape_move;
	std::vector<Eigen::Vector3f> target_rightCheek_vertices;
	target_rightCheek_vertices.resize(target_rightCheek.n_vertices());

	if (change_shape_state == 100)
	{
		ScaledRigid E_SICP_rightCheek(pre_vertices, pre_targetShape_vertices, landmark_rightCheek_indices, num_rightCheek_landmark);
		double test_cost = E_SICP_rightCheek.run();

		rightCheek_scale = E_SICP_rightCheek.getScale();
		rightCheek_rotation = E_SICP_rightCheek.getRotation();
		rightCheek_translation = E_SICP_rightCheek.getTranslation();

		double pra_rotation_matrix[9];
		ceres::AngleAxisToRotationMatrix(rightCheek_rotation, pra_rotation_matrix);

		Eigen::Matrix4f pra_transformed_matrix;
		Eigen::Matrix4f pra_scale_matrix;

		pra_transformed_matrix(0, 0) = float(pra_rotation_matrix[0]); pra_transformed_matrix(0, 1) = float(pra_rotation_matrix[3]); pra_transformed_matrix(0, 2) = float(pra_rotation_matrix[6]); pra_transformed_matrix(0, 3) = float(rightCheek_translation[0]);
		pra_transformed_matrix(1, 0) = float(pra_rotation_matrix[1]); pra_transformed_matrix(1, 1) = float(pra_rotation_matrix[4]); pra_transformed_matrix(1, 2) = float(pra_rotation_matrix[7]); pra_transformed_matrix(1, 3) = float(rightCheek_translation[1]);
		pra_transformed_matrix(2, 0) = float(pra_rotation_matrix[2]); pra_transformed_matrix(2, 1) = float(pra_rotation_matrix[5]); pra_transformed_matrix(2, 2) = float(pra_rotation_matrix[8]); pra_transformed_matrix(2, 3) = float(rightCheek_translation[2]);
		pra_transformed_matrix(3, 0) = 0.0f;						  pra_transformed_matrix(3, 1) = 0.0f;							pra_transformed_matrix(3, 2) = 0.0f;						  pra_transformed_matrix(3, 3) = 1.0f;

		pra_scale_matrix(0, 0) = float(rightCheek_scale);		pra_scale_matrix(0, 1) = 0.0f;						pra_scale_matrix(0, 2) = 0.0f;						pra_scale_matrix(0, 3) = 0.0f;
		pra_scale_matrix(1, 0) = 0.0f;							pra_scale_matrix(1, 1) = float(rightCheek_scale);	pra_scale_matrix(1, 2) = 0.0f;						pra_scale_matrix(1, 3) = 0.0f;
		pra_scale_matrix(2, 0) = 0.0f;							pra_scale_matrix(2, 1) = 0.0f;						pra_scale_matrix(2, 2) = float(rightCheek_scale);	pra_scale_matrix(2, 3) = 0.0f;
		pra_scale_matrix(3, 0) = 0.0f;							pra_scale_matrix(3, 1) = 0.0f;						pra_scale_matrix(3, 2) = 0.0f;						pra_scale_matrix(3, 3) = 1.0f;

		std::cout << "rightCheek_scale: " << rightCheek_scale << std::endl;
		std::cout << "rightCheek_rotation: " << rightCheek_rotation[0] << " " << rightCheek_rotation[1] << " " << rightCheek_rotation[2] << std::endl;
		std::cout << "rightCheek_translation:" << rightCheek_translation[0] << " " << rightCheek_translation[1] << "  " << rightCheek_translation[2] << std::endl;

		for (t_vIt = target_rightCheek.vertices_begin(); t_vIt != target_rightCheek.vertices_end(); ++t_vIt)
		{
			t_v = target_rightCheek.point(t_vIt.handle());
			Eigen::Vector4f target_transformed_points = Eigen::Vector4f(t_v[0], t_v[1], t_v[2], 1.0);

			target_transformed_points = pra_scale_matrix * pra_transformed_matrix * target_transformed_points;

			target_transformed_points(0) = target_transformed_points(0) / target_transformed_points(3);
			target_transformed_points(1) = target_transformed_points(1) / target_transformed_points(3);
			target_transformed_points(2) = target_transformed_points(2) / target_transformed_points(3);

			target_rightCheek.point(t_vIt.handle()) = OpenMesh::Vec3f(target_transformed_points(0) + mean_x, target_transformed_points(1) + mean_y, target_transformed_points(2) + mean_z);
			target_rightCheek_vertices[t_vIt->idx()] = Eigen::Vector3f(target_transformed_points(0) + mean_x, target_transformed_points(1) + mean_y, target_transformed_points(2) + mean_z);

		}

		target_rightCheek.update_face_normals();
		target_rightCheek.update_vertex_normals();

		//ICP_mesh_check
		//OpenMesh::IO::write_mesh(target_rightCheek, "ICP_rightCheek_mesh.obj");

	}


	///////////////shape_state == 100 left cheek ICP/////
	double leftCheek_scale = 1.0;
	double *leftCheek_rotation, *leftCheek_translation;

	MyMesh target_leftCheek = tar_shape_move;
	std::vector<Eigen::Vector3f> target_leftCheek_vertices;
	target_leftCheek_vertices.resize(target_leftCheek.n_vertices());

	if (change_shape_state == 100)
	{
		ScaledRigid E_SICP_leftCheek(pre_vertices, pre_targetShape_vertices, landmark_leftCheek_indices, num_leftCheek_landmark);
		double test_cost = E_SICP_leftCheek.run();

		leftCheek_scale = E_SICP_leftCheek.getScale();
		leftCheek_rotation = E_SICP_leftCheek.getRotation();
		leftCheek_translation = E_SICP_leftCheek.getTranslation();

		double pra_rotation_matrix[9];
		ceres::AngleAxisToRotationMatrix(leftCheek_rotation, pra_rotation_matrix);

		Eigen::Matrix4f pra_transformed_matrix;
		Eigen::Matrix4f pra_scale_matrix;

		pra_transformed_matrix(0, 0) = float(pra_rotation_matrix[0]); pra_transformed_matrix(0, 1) = float(pra_rotation_matrix[3]); pra_transformed_matrix(0, 2) = float(pra_rotation_matrix[6]); pra_transformed_matrix(0, 3) = float(leftCheek_translation[0]);
		pra_transformed_matrix(1, 0) = float(pra_rotation_matrix[1]); pra_transformed_matrix(1, 1) = float(pra_rotation_matrix[4]); pra_transformed_matrix(1, 2) = float(pra_rotation_matrix[7]); pra_transformed_matrix(1, 3) = float(leftCheek_translation[1]);
		pra_transformed_matrix(2, 0) = float(pra_rotation_matrix[2]); pra_transformed_matrix(2, 1) = float(pra_rotation_matrix[5]); pra_transformed_matrix(2, 2) = float(pra_rotation_matrix[8]); pra_transformed_matrix(2, 3) = float(leftCheek_translation[2]);
		pra_transformed_matrix(3, 0) = 0.0f;						  pra_transformed_matrix(3, 1) = 0.0f;							pra_transformed_matrix(3, 2) = 0.0f;						  pra_transformed_matrix(3, 3) = 1.0f;

		pra_scale_matrix(0, 0) = float(leftCheek_scale);		pra_scale_matrix(0, 1) = 0.0f;						pra_scale_matrix(0, 2) = 0.0f;						pra_scale_matrix(0, 3) = 0.0f;
		pra_scale_matrix(1, 0) = 0.0f;							pra_scale_matrix(1, 1) = float(leftCheek_scale);	pra_scale_matrix(1, 2) = 0.0f;						pra_scale_matrix(1, 3) = 0.0f;
		pra_scale_matrix(2, 0) = 0.0f;							pra_scale_matrix(2, 1) = 0.0f;						pra_scale_matrix(2, 2) = float(leftCheek_scale);	pra_scale_matrix(2, 3) = 0.0f;
		pra_scale_matrix(3, 0) = 0.0f;							pra_scale_matrix(3, 1) = 0.0f;						pra_scale_matrix(3, 2) = 0.0f;						pra_scale_matrix(3, 3) = 1.0f;

		std::cout << "leftCheek_scale: " << leftCheek_scale << std::endl;
		std::cout << "leftCheek_rotation: " << leftCheek_rotation[0] << " " << leftCheek_rotation[1] << " " << leftCheek_rotation[2] << std::endl;
		std::cout << "leftCheek_translation:" << leftCheek_translation[0] << " " << leftCheek_translation[1] << "  " << leftCheek_translation[2] << std::endl;

		for (t_vIt = target_leftCheek.vertices_begin(); t_vIt != target_leftCheek.vertices_end(); ++t_vIt)
		{
			t_v = target_leftCheek.point(t_vIt.handle());
			Eigen::Vector4f target_transformed_points = Eigen::Vector4f(t_v[0], t_v[1], t_v[2], 1.0);

			target_transformed_points = pra_scale_matrix * pra_transformed_matrix * target_transformed_points;

			target_transformed_points(0) = target_transformed_points(0) / target_transformed_points(3);
			target_transformed_points(1) = target_transformed_points(1) / target_transformed_points(3);
			target_transformed_points(2) = target_transformed_points(2) / target_transformed_points(3);

			target_leftCheek.point(t_vIt.handle()) = OpenMesh::Vec3f(target_transformed_points(0) + mean_x, target_transformed_points(1) + mean_y, target_transformed_points(2) + mean_z);
			target_leftCheek_vertices[t_vIt->idx()] = Eigen::Vector3f(target_transformed_points(0) + mean_x, target_transformed_points(1) + mean_y, target_transformed_points(2) + mean_z);

		}

		target_leftCheek.update_face_normals();
		target_leftCheek.update_vertex_normals();

		//ICP_mesh_check
		//OpenMesh::IO::write_mesh(target_leftCheek, "ICP_leftCheek_mesh.obj");

	}

	////////energy_for_Scaled ICP//	

	//////////////////////////////
	////////eyebrow ICP//////////
	/////////////////////////////

	//eyebrow right ICP
	double eyebrow_scale_right = 1.0;
	double *eyebrow_rotation_right, *eyebrow_translation_right;

	MyMesh target_transformed_eyebrow_right = tar_eyebrow_move;
	std::vector<Eigen::Vector3f> target_eyebrow_right_vertices;  //transformed final mesh
	target_eyebrow_right_vertices.resize(target_transformed_eyebrow_right.n_vertices());

	if (eyebrow_bool == 1)
	{
		if (eyebrow_autoScale_bool == 1)
		{
			ScaledRigid E_SICP_eyebrow(pre_vertices, pre_targetEyebrow_vertices, landmark_r_eyebrow_indices, num_r_eyebrow_landmark); //eyebrow
			double test_cost = E_SICP_eyebrow.run();

			eyebrow_scale_right = E_SICP_eyebrow.getScale();
			eyebrow_rotation_right = E_SICP_eyebrow.getRotation();
			eyebrow_translation_right = E_SICP_eyebrow.getTranslation();
		}
		else
		{
			RigidTR E_SICP_eyebrow(pre_vertices, pre_targetEyebrow_vertices, landmark_r_eyebrow_indices, num_r_eyebrow_landmark, eyebrow_scale_factor, rightEyebrow_scale_width, rightEyebrow_scale_height); //eyebrow
			double test_cost = E_SICP_eyebrow.run();

			eyebrow_scale_right = E_SICP_eyebrow.getScale();
			eyebrow_rotation_right = E_SICP_eyebrow.getRotation();
			eyebrow_translation_right = E_SICP_eyebrow.getTranslation();

		}

		double pra_rotation_matrix[9];
		ceres::AngleAxisToRotationMatrix(eyebrow_rotation_right, pra_rotation_matrix);

		Eigen::Matrix4f pra_transformed_matrix;
		Eigen::Matrix4f pra_scale_matrix;

		pra_transformed_matrix(0, 0) = float(pra_rotation_matrix[0]); pra_transformed_matrix(0, 1) = float(pra_rotation_matrix[3]); pra_transformed_matrix(0, 2) = float(pra_rotation_matrix[6]); pra_transformed_matrix(0, 3) = float(eyebrow_translation_right[0]);
		pra_transformed_matrix(1, 0) = float(pra_rotation_matrix[1]); pra_transformed_matrix(1, 1) = float(pra_rotation_matrix[4]); pra_transformed_matrix(1, 2) = float(pra_rotation_matrix[7]); pra_transformed_matrix(1, 3) = float(eyebrow_translation_right[1]);
		pra_transformed_matrix(2, 0) = float(pra_rotation_matrix[2]); pra_transformed_matrix(2, 1) = float(pra_rotation_matrix[5]); pra_transformed_matrix(2, 2) = float(pra_rotation_matrix[8]); pra_transformed_matrix(2, 3) = float(eyebrow_translation_right[2]);
		pra_transformed_matrix(3, 0) = 0.0f;						  pra_transformed_matrix(3, 1) = 0.0f;							pra_transformed_matrix(3, 2) = 0.0f;						  pra_transformed_matrix(3, 3) = 1.0f;

		pra_scale_matrix(0, 0) = float(eyebrow_scale_right);		pra_scale_matrix(0, 1) = 0.0f;								pra_scale_matrix(0, 2) = 0.0f;								pra_scale_matrix(0, 3) = 0.0f;
		pra_scale_matrix(1, 0) = 0.0f;								pra_scale_matrix(1, 1) = float(eyebrow_scale_right);		pra_scale_matrix(1, 2) = 0.0f;								pra_scale_matrix(1, 3) = 0.0f;
		pra_scale_matrix(2, 0) = 0.0f;								pra_scale_matrix(2, 1) = 0.0f;								pra_scale_matrix(2, 2) = float(eyebrow_scale_right);		pra_scale_matrix(2, 3) = 0.0f;
		pra_scale_matrix(3, 0) = 0.0f;								pra_scale_matrix(3, 1) = 0.0f;								pra_scale_matrix(3, 2) = 0.0f;								pra_scale_matrix(3, 3) = 1.0f;

		std::cout << "eyebrow_scale_right: " << eyebrow_scale_right << std::endl;
		std::cout << "eyebrow_rotation_right: " << eyebrow_rotation_right[0] << " " << eyebrow_rotation_right[1] << " " << eyebrow_rotation_right[2] << std::endl;
		std::cout << "eyebrow_translation_right:" << eyebrow_translation_right[0] << " " << eyebrow_translation_right[1] << "  " << eyebrow_translation_right[2] << std::endl;

		for (t_vIt = target_transformed_eyebrow_right.vertices_begin(); t_vIt != target_transformed_eyebrow_right.vertices_end(); ++t_vIt)
		{
			t_v = target_transformed_eyebrow_right.point(t_vIt.handle());
			Eigen::Vector4f target_transformed_points = Eigen::Vector4f(t_v[0], t_v[1], t_v[2], 1.0);

			target_transformed_points = pra_scale_matrix * pra_transformed_matrix * target_transformed_points;

			target_transformed_points(0) = target_transformed_points(0) / target_transformed_points(3);
			target_transformed_points(1) = target_transformed_points(1) / target_transformed_points(3);
			target_transformed_points(2) = target_transformed_points(2) / target_transformed_points(3);

			target_transformed_eyebrow_right.point(t_vIt.handle()) = OpenMesh::Vec3f(target_transformed_points(0) + mean_x, target_transformed_points(1) + mean_y, target_transformed_points(2) + mean_z);
			target_eyebrow_right_vertices[t_vIt->idx()] = Eigen::Vector3f(target_transformed_points(0) + mean_x, target_transformed_points(1) + mean_y, target_transformed_points(2) + mean_z);

		}

		target_transformed_eyebrow_right.update_face_normals();
		target_transformed_eyebrow_right.update_vertex_normals();

		//ICP_mesh_check
		//OpenMesh::IO::write_mesh(target_transformed_eyebrow_right, "ICP_eyebrow_right_mesh.obj");

	}

	//eyebrow Left ICP
	double eyebrow_scale_left = 1.0;
	double *eyebrow_rotation_left, *eyebrow_translation_left;

	MyMesh target_transformed_eyebrow_left = tar_eyebrow_move;
	std::vector<Eigen::Vector3f> target_eyebrow_left_vertices;  //transformed final mesh
	target_eyebrow_left_vertices.resize(target_transformed_eyebrow_left.n_vertices());

	if (eyebrow_bool == 1)
	{
		if (eyebrow_autoScale_bool == 1)
		{
			ScaledRigid E_SICP_eyebrow(pre_vertices, pre_targetEyebrow_vertices, landmark_l_eyebrow_indices, num_l_eyebrow_landmark); //eyebrow
			double test_cost = E_SICP_eyebrow.run();

			eyebrow_scale_left = E_SICP_eyebrow.getScale();
			eyebrow_rotation_left = E_SICP_eyebrow.getRotation();
			eyebrow_translation_left = E_SICP_eyebrow.getTranslation();
		}
		else
		{
			RigidTR E_SICP_eyebrow(pre_vertices, pre_targetEyebrow_vertices, landmark_l_eyebrow_indices, num_l_eyebrow_landmark, eyebrow_scale_factor, leftEyebrow_scale_width, leftEyebrow_scale_height); //eyebrow
			double test_cost = E_SICP_eyebrow.run();

			eyebrow_scale_left = E_SICP_eyebrow.getScale();
			eyebrow_rotation_left = E_SICP_eyebrow.getRotation();
			eyebrow_translation_left = E_SICP_eyebrow.getTranslation();
		}

		double pra_rotation_matrix[9];
		ceres::AngleAxisToRotationMatrix(eyebrow_rotation_left, pra_rotation_matrix);

		Eigen::Matrix4f pra_transformed_matrix;
		Eigen::Matrix4f pra_scale_matrix;

		pra_transformed_matrix(0, 0) = float(pra_rotation_matrix[0]); pra_transformed_matrix(0, 1) = float(pra_rotation_matrix[3]); pra_transformed_matrix(0, 2) = float(pra_rotation_matrix[6]); pra_transformed_matrix(0, 3) = float(eyebrow_translation_left[0]);
		pra_transformed_matrix(1, 0) = float(pra_rotation_matrix[1]); pra_transformed_matrix(1, 1) = float(pra_rotation_matrix[4]); pra_transformed_matrix(1, 2) = float(pra_rotation_matrix[7]); pra_transformed_matrix(1, 3) = float(eyebrow_translation_left[1]);
		pra_transformed_matrix(2, 0) = float(pra_rotation_matrix[2]); pra_transformed_matrix(2, 1) = float(pra_rotation_matrix[5]); pra_transformed_matrix(2, 2) = float(pra_rotation_matrix[8]); pra_transformed_matrix(2, 3) = float(eyebrow_translation_left[2]);
		pra_transformed_matrix(3, 0) = 0.0f;						  pra_transformed_matrix(3, 1) = 0.0f;							pra_transformed_matrix(3, 2) = 0.0f;						  pra_transformed_matrix(3, 3) = 1.0f;

		pra_scale_matrix(0, 0) = float(eyebrow_scale_left);			pra_scale_matrix(0, 1) = 0.0f;								pra_scale_matrix(0, 2) = 0.0f;								pra_scale_matrix(0, 3) = 0.0f;
		pra_scale_matrix(1, 0) = 0.0f;								pra_scale_matrix(1, 1) = float(eyebrow_scale_left);			pra_scale_matrix(1, 2) = 0.0f;								pra_scale_matrix(1, 3) = 0.0f;
		pra_scale_matrix(2, 0) = 0.0f;								pra_scale_matrix(2, 1) = 0.0f;								pra_scale_matrix(2, 2) = float(eyebrow_scale_left);			pra_scale_matrix(2, 3) = 0.0f;
		pra_scale_matrix(3, 0) = 0.0f;								pra_scale_matrix(3, 1) = 0.0f;								pra_scale_matrix(3, 2) = 0.0f;								pra_scale_matrix(3, 3) = 1.0f;

		std::cout << "eyebrow_scale_left: " << eyebrow_scale_left << std::endl;
		std::cout << "eyebrow_rotation_left: " << eyebrow_rotation_left[0] << " " << eyebrow_rotation_left[1] << " " << eyebrow_rotation_left[2] << std::endl;
		std::cout << "eyebrow_translation_left:" << eyebrow_translation_left[0] << " " << eyebrow_translation_left[1] << "  " << eyebrow_translation_left[2] << std::endl;

		for (t_vIt = target_transformed_eyebrow_left.vertices_begin(); t_vIt != target_transformed_eyebrow_left.vertices_end(); ++t_vIt)
		{
			t_v = target_transformed_eyebrow_left.point(t_vIt.handle());
			Eigen::Vector4f target_transformed_points = Eigen::Vector4f(t_v[0], t_v[1], t_v[2], 1.0);

			target_transformed_points = pra_scale_matrix * pra_transformed_matrix * target_transformed_points;

			target_transformed_points(0) = target_transformed_points(0) / target_transformed_points(3);
			target_transformed_points(1) = target_transformed_points(1) / target_transformed_points(3);
			target_transformed_points(2) = target_transformed_points(2) / target_transformed_points(3);

			target_transformed_eyebrow_left.point(t_vIt.handle()) = OpenMesh::Vec3f(target_transformed_points(0) + mean_x, target_transformed_points(1) + mean_y, target_transformed_points(2) + mean_z);
			target_eyebrow_left_vertices[t_vIt->idx()] = Eigen::Vector3f(target_transformed_points(0) + mean_x, target_transformed_points(1) + mean_y, target_transformed_points(2) + mean_z);

		}

		target_transformed_eyebrow_left.update_face_normals();
		target_transformed_eyebrow_left.update_vertex_normals();

		//ICP_mesh_check
		//OpenMesh::IO::write_mesh(target_transformed_eyebrow_left, "ICP_eyebrow_left_mesh.obj");

	}

	//////////eyes icp/////

	MyMesh target_transformed_eyes = tar_eyes_move;
	std::vector<Eigen::Vector3f> target_eyes_vertices;  //transformed final mesh
	target_eyes_vertices.resize(target_transformed_eyes.n_vertices());

	if (need_change_eye == true)
	{
		if (eye_autoScale_bool == 1)
		{
			ScaledRigid E_SICP_eyes(pre_vertices, pre_targetEyes_vertices, landmark_eyes_indices, num_eyes_landmark); //eyes

			double test_cost = E_SICP_eyes.run();

			eyes_scale = E_SICP_eyes.getScale();
			eyes_rotation = E_SICP_eyes.getRotation();
			eyes_translation = E_SICP_eyes.getTranslation();
		}
		else
		{
			RigidTR E_SICP_eyes(pre_vertices, pre_targetEyes_vertices, landmark_eyes_indices, num_eyes_landmark, eye_scale_factor, rightEye_scale_width, rightEye_scale_height); //eyes

			double test_cost = E_SICP_eyes.run();

			eyes_scale = E_SICP_eyes.getScale();
			eyes_rotation = E_SICP_eyes.getRotation();
			eyes_translation = E_SICP_eyes.getTranslation();

			eyes_rotation[0] = eyes_rotation[0];
			eyes_rotation[1] = eyes_rotation[1];
			eyes_rotation[2] = eyes_rotation[2];
			eyes_translation[0] = eyes_translation[0];
			eyes_translation[1] = eyes_translation[1];
			eyes_translation[2] = eyes_translation[2];
		}



		double pra_rotation_matrix[9];
		ceres::AngleAxisToRotationMatrix(eyes_rotation, pra_rotation_matrix);

		Eigen::Matrix4f pra_transformed_matrix;
		Eigen::Matrix4f pra_scale_matrix;

		pra_transformed_matrix(0, 0) = float(pra_rotation_matrix[0]); pra_transformed_matrix(0, 1) = float(pra_rotation_matrix[3]); pra_transformed_matrix(0, 2) = float(pra_rotation_matrix[6]); pra_transformed_matrix(0, 3) = float(eyes_translation[0]);
		pra_transformed_matrix(1, 0) = float(pra_rotation_matrix[1]); pra_transformed_matrix(1, 1) = float(pra_rotation_matrix[4]); pra_transformed_matrix(1, 2) = float(pra_rotation_matrix[7]); pra_transformed_matrix(1, 3) = float(eyes_translation[1]);
		pra_transformed_matrix(2, 0) = float(pra_rotation_matrix[2]); pra_transformed_matrix(2, 1) = float(pra_rotation_matrix[5]); pra_transformed_matrix(2, 2) = float(pra_rotation_matrix[8]); pra_transformed_matrix(2, 3) = float(eyes_translation[2]);
		pra_transformed_matrix(3, 0) = 0.0f;						  pra_transformed_matrix(3, 1) = 0.0f;							pra_transformed_matrix(3, 2) = 0.0f;						  pra_transformed_matrix(3, 3) = 1.0f;

		pra_scale_matrix(0, 0) = float(eyes_scale) * float(rightEye_scale_width);		pra_scale_matrix(0, 1) = 0.0f;													pra_scale_matrix(0, 2) = 0.0f;					pra_scale_matrix(0, 3) = 0.0f;
		pra_scale_matrix(1, 0) = 0.0f;													pra_scale_matrix(1, 1) = float(eyes_scale) * float(rightEye_scale_height);		pra_scale_matrix(1, 2) = 0.0f;					pra_scale_matrix(1, 3) = 0.0f;
		pra_scale_matrix(2, 0) = 0.0f;													pra_scale_matrix(2, 1) = 0.0f;													pra_scale_matrix(2, 2) = float(eyes_scale);		pra_scale_matrix(2, 3) = 0.0f;
		pra_scale_matrix(3, 0) = 0.0f;													pra_scale_matrix(3, 1) = 0.0f;													pra_scale_matrix(3, 2) = 0.0f;					pra_scale_matrix(3, 3) = 1.0f;

		std::cout << "eyes_scale: " << eyes_scale << std::endl;
		std::cout << "eyes_rotation: " << eyes_rotation[0] << " " << eyes_rotation[1] << " " << eyes_rotation[2] << std::endl;
		std::cout << "eyes_translation:" << eyes_translation[0] << " " << eyes_translation[1] << "  " << eyes_translation[2] << std::endl;
		std::cout << "right eyes_width: " << rightEye_scale_width << "  right eye_height: " << rightEye_scale_height << std::endl << std::endl;

		for (t_vIt = target_transformed_eyes.vertices_begin(); t_vIt != target_transformed_eyes.vertices_end(); ++t_vIt)
		{
			t_v = target_transformed_eyes.point(t_vIt.handle());
			Eigen::Vector4f target_transformed_points = Eigen::Vector4f(t_v[0], t_v[1], t_v[2], 1.0);

			target_transformed_points = pra_scale_matrix * pra_transformed_matrix * target_transformed_points;

			target_transformed_points(0) = target_transformed_points(0) / target_transformed_points(3);
			target_transformed_points(1) = target_transformed_points(1) / target_transformed_points(3);
			target_transformed_points(2) = target_transformed_points(2) / target_transformed_points(3);

			target_transformed_eyes.point(t_vIt.handle()) = OpenMesh::Vec3f(target_transformed_points(0) + mean_x - eyeMove_factor, target_transformed_points(1) + mean_y + eye_average_Y, target_transformed_points(2) + mean_z);
			target_eyes_vertices[t_vIt->idx()] = Eigen::Vector3f(target_transformed_points(0) + mean_x - eyeMove_factor, target_transformed_points(1) + mean_y + eye_average_Y, target_transformed_points(2) + mean_z);

		}

		target_transformed_eyes.update_face_normals();
		target_transformed_eyes.update_vertex_normals();

		//target_transformed_eyes = target_eyes_mesh; //jws
		//ICP_mesh_check
		OpenMesh::IO::write_mesh(target_transformed_eyes, "ICP_eyes_mesh.obj");
	}


	//////////eyes Left icp/////
	MyMesh target_transformed_eyes_left = tar_eyes_move;
	std::vector<Eigen::Vector3f> target_eyes_vertices_left;  //transformed final mesh
	target_eyes_vertices_left.resize(target_transformed_eyes_left.n_vertices());

	if (need_change_eye == true)
	{
		if (eye_autoScale_bool == 1)
		{
			ScaledRigid E_SICP_eyes(pre_vertices, pre_targetEyes_vertices, landmark_l_eyes_indices, num_l_eyes_landmark);

			double test_cost = E_SICP_eyes.run();

			eyes_scale_left = E_SICP_eyes.getScale();
			eyes_rotation_left = E_SICP_eyes.getRotation();
			eyes_translation_left = E_SICP_eyes.getTranslation();
		}
		else
		{
			RigidTR E_SICP_eyes(pre_vertices, pre_targetEyes_vertices, landmark_l_eyes_indices, num_l_eyes_landmark, eye_scale_factor, leftEye_scale_width, leftEye_scale_height); //eyes


			double test_cost = E_SICP_eyes.run();

			eyes_scale_left = E_SICP_eyes.getScale();
			eyes_rotation_left = E_SICP_eyes.getRotation();
			eyes_translation_left = E_SICP_eyes.getTranslation();

			eyes_rotation_left[0] = eyes_rotation_left[0];
			eyes_rotation_left[1] = eyes_rotation_left[1];
			eyes_rotation_left[2] = eyes_rotation_left[2];
			eyes_translation_left[0] = eyes_translation_left[0];
			eyes_translation_left[1] = eyes_translation_left[1];
			eyes_translation_left[2] = eyes_translation_left[2];
		}


		double pra_rotation_matrix[9];
		ceres::AngleAxisToRotationMatrix(eyes_rotation_left, pra_rotation_matrix);

		Eigen::Matrix4f pra_transformed_matrix;
		Eigen::Matrix4f pra_scale_matrix;

		pra_transformed_matrix(0, 0) = float(pra_rotation_matrix[0]); pra_transformed_matrix(0, 1) = float(pra_rotation_matrix[3]); pra_transformed_matrix(0, 2) = float(pra_rotation_matrix[6]); pra_transformed_matrix(0, 3) = float(eyes_translation_left[0]);
		pra_transformed_matrix(1, 0) = float(pra_rotation_matrix[1]); pra_transformed_matrix(1, 1) = float(pra_rotation_matrix[4]); pra_transformed_matrix(1, 2) = float(pra_rotation_matrix[7]); pra_transformed_matrix(1, 3) = float(eyes_translation_left[1]);
		pra_transformed_matrix(2, 0) = float(pra_rotation_matrix[2]); pra_transformed_matrix(2, 1) = float(pra_rotation_matrix[5]); pra_transformed_matrix(2, 2) = float(pra_rotation_matrix[8]); pra_transformed_matrix(2, 3) = float(eyes_translation_left[2]);
		pra_transformed_matrix(3, 0) = 0.0f;						  pra_transformed_matrix(3, 1) = 0.0f;							pra_transformed_matrix(3, 2) = 0.0f;						  pra_transformed_matrix(3, 3) = 1.0f;

		pra_scale_matrix(0, 0) = float(eyes_scale_left) * float(leftEye_scale_width);		pra_scale_matrix(0, 1) = 0.0f;														pra_scale_matrix(0, 2) = 0.0f;							pra_scale_matrix(0, 3) = 0.0f;
		pra_scale_matrix(1, 0) = 0.0f;														pra_scale_matrix(1, 1) = float(eyes_scale_left) * float(leftEye_scale_height);		pra_scale_matrix(1, 2) = 0.0f;							pra_scale_matrix(1, 3) = 0.0f;
		pra_scale_matrix(2, 0) = 0.0f;														pra_scale_matrix(2, 1) = 0.0f;														pra_scale_matrix(2, 2) = float(eyes_scale_left);		pra_scale_matrix(2, 3) = 0.0f;
		pra_scale_matrix(3, 0) = 0.0f;														pra_scale_matrix(3, 1) = 0.0f;														pra_scale_matrix(3, 2) = 0.0f;							pra_scale_matrix(3, 3) = 1.0f;

		std::cout << "left_eyes_scale: " << eyes_scale_left << std::endl;
		std::cout << "left_eyes_rotation: " << eyes_rotation_left[0] << " " << eyes_rotation_left[1] << " " << eyes_rotation_left[2] << std::endl;
		std::cout << "left_eyes_translation:" << eyes_translation_left[0] << " " << eyes_translation_left[1] << "  " << eyes_translation_left[2] << std::endl;
		std::cout << "left eyes_width: " << leftEye_scale_width << "  left eye_height: " << leftEye_scale_height << std::endl << std::endl;

		for (t_vIt = target_transformed_eyes_left.vertices_begin(); t_vIt != target_transformed_eyes_left.vertices_end(); ++t_vIt)
		{
			t_v = target_transformed_eyes_left.point(t_vIt.handle());
			Eigen::Vector4f target_transformed_points = Eigen::Vector4f(t_v[0], t_v[1], t_v[2], 1.0);

			target_transformed_points = pra_scale_matrix * pra_transformed_matrix * target_transformed_points;

			target_transformed_points(0) = target_transformed_points(0) / target_transformed_points(3);
			target_transformed_points(1) = target_transformed_points(1) / target_transformed_points(3);
			target_transformed_points(2) = target_transformed_points(2) / target_transformed_points(3);

			target_transformed_eyes_left.point(t_vIt.handle()) = OpenMesh::Vec3f(target_transformed_points(0) + mean_x + eyeMove_factor, target_transformed_points(1) + mean_y + eye_average_Y, target_transformed_points(2) + mean_z);
			target_eyes_vertices_left[t_vIt->idx()] = Eigen::Vector3f(target_transformed_points(0) + mean_x + eyeMove_factor, target_transformed_points(1) + mean_y + eye_average_Y, target_transformed_points(2) + mean_z);

		}

		target_transformed_eyes_left.update_face_normals();
		target_transformed_eyes_left.update_vertex_normals();

		//target_transformed_eyes_left = target_eyes_mesh; //jws

		//ICP_mesh_check
		OpenMesh::IO::write_mesh(target_transformed_eyes_left, "ICP_eyes_mesh_left.obj");
	}


	///////////nose ICP//////////////////
	MyMesh target_transformed_nose = tar_nose_move;
	std::vector<Eigen::Vector3f> target_nose_vertices;  //transformed final mesh
	target_nose_vertices.resize(target_transformed_nose.n_vertices());
	if (need_change_nose == true)
	{
		if (nose_autoScale_bool == 1)
		{
			ScaledRigid E_SICP_nose(pre_vertices, pre_targetNose_vertices, landmark_nose_indices, num_nose_landmark); //nose
			double test_cost = E_SICP_nose.run();

			nose_scale = E_SICP_nose.getScale();
			nose_rotation = E_SICP_nose.getRotation();
			nose_translation = E_SICP_nose.getTranslation();
		}
		else
		{
			RigidTR E_SICP_nose(pre_vertices, pre_targetNose_vertices, landmark_nose_indices, num_nose_landmark, nose_scale_factor, nose_scale_width, nose_scale_height); //nose
			double test_cost = E_SICP_nose.run();

			nose_scale = E_SICP_nose.getScale();
			nose_rotation = E_SICP_nose.getRotation();
			nose_translation = E_SICP_nose.getTranslation();

			nose_rotation[0] = nose_rotation[0] + nose_down_xAngle;//+ 0.08;  //nose down factor
			nose_rotation[1] = nose_rotation[1];
			nose_rotation[2] = nose_rotation[2];
			nose_translation[0] = nose_translation[0];
			nose_translation[1] = nose_translation[1];
			nose_translation[2] = nose_translation[2] + nose_down_zTranslation; //- 3; //nose down factor
		}


		double pra_rotation_matrix[9];
		ceres::AngleAxisToRotationMatrix(nose_rotation, pra_rotation_matrix);

		Eigen::Matrix4f pra_transformed_matrix;
		Eigen::Matrix4f pra_scale_matrix;

		pra_transformed_matrix(0, 0) = float(pra_rotation_matrix[0]); pra_transformed_matrix(0, 1) = float(pra_rotation_matrix[3]); pra_transformed_matrix(0, 2) = float(pra_rotation_matrix[6]); pra_transformed_matrix(0, 3) = float(nose_translation[0]);
		pra_transformed_matrix(1, 0) = float(pra_rotation_matrix[1]); pra_transformed_matrix(1, 1) = float(pra_rotation_matrix[4]); pra_transformed_matrix(1, 2) = float(pra_rotation_matrix[7]); pra_transformed_matrix(1, 3) = float(nose_translation[1]);
		pra_transformed_matrix(2, 0) = float(pra_rotation_matrix[2]); pra_transformed_matrix(2, 1) = float(pra_rotation_matrix[5]); pra_transformed_matrix(2, 2) = float(pra_rotation_matrix[8]); pra_transformed_matrix(2, 3) = float(nose_translation[2]);
		pra_transformed_matrix(3, 0) = 0.0f;						  pra_transformed_matrix(3, 1) = 0.0f;							pra_transformed_matrix(3, 2) = 0.0f;						  pra_transformed_matrix(3, 3) = 1.0f;

		pra_scale_matrix(0, 0) = float(nose_scale) * float(nose_scale_width);		pra_scale_matrix(0, 1) = 0.0f;												pra_scale_matrix(0, 2) = 0.0f;											pra_scale_matrix(0, 3) = 0.0f;
		pra_scale_matrix(1, 0) = 0.0f;												pra_scale_matrix(1, 1) = float(nose_scale) * float(nose_scale_height);		pra_scale_matrix(1, 2) = 0.0f;											pra_scale_matrix(1, 3) = 0.0f;
		pra_scale_matrix(2, 0) = 0.0f;												pra_scale_matrix(2, 1) = 0.0f;												pra_scale_matrix(2, 2) = float(nose_scale) * float(nose_scale_z);		pra_scale_matrix(2, 3) = 0.0f;
		pra_scale_matrix(3, 0) = 0.0f;												pra_scale_matrix(3, 1) = 0.0f;												pra_scale_matrix(3, 2) = 0.0f;					pra_scale_matrix(3, 3) = 1.0f;

		std::cout << "nose_scale: " << nose_scale << std::endl;
		std::cout << "nose_rotation: " << nose_rotation[0] << " " << nose_rotation[1] << " " << nose_rotation[2] << std::endl;
		std::cout << "nose_translation:" << nose_translation[0] << " " << nose_translation[1] << "  " << nose_translation[2] << std::endl;
		std::cout << "nose_width_factor: " << nose_scale_width << "  nose_height_factor: " << nose_scale_height << std::endl << std::endl;

		for (t_vIt = target_transformed_nose.vertices_begin(); t_vIt != target_transformed_nose.vertices_end(); ++t_vIt)
		{
			t_v = target_transformed_nose.point(t_vIt.handle());
			Eigen::Vector4f target_transformed_points = Eigen::Vector4f(t_v[0], t_v[1], t_v[2], 1.0);

			target_transformed_points = pra_scale_matrix * pra_transformed_matrix * target_transformed_points;

			target_transformed_points(0) = target_transformed_points(0) / target_transformed_points(3);
			target_transformed_points(1) = target_transformed_points(1) / target_transformed_points(3);
			target_transformed_points(2) = target_transformed_points(2) / target_transformed_points(3);

			target_transformed_nose.point(t_vIt.handle()) = OpenMesh::Vec3f(target_transformed_points(0) + mean_x, target_transformed_points(1) + mean_y + nose_Y, target_transformed_points(2) + mean_z);
			target_nose_vertices[t_vIt->idx()] = Eigen::Vector3f(target_transformed_points(0) + mean_x, target_transformed_points(1) + mean_y + nose_Y, target_transformed_points(2) + mean_z);

		}

		target_transformed_nose.update_face_normals();
		target_transformed_nose.update_vertex_normals();

		//target_transformed_nose = target_eyes_mesh; //jws
		//ICP_mesh_check
		OpenMesh::IO::write_mesh(target_transformed_nose, "ICP_nose_mesh.obj");
	}

	//////////////mouse ICP////
	MyMesh target_transformed_mouse = tar_mouse_move;
	std::vector<Eigen::Vector3f> target_mouse_vertices;  //transformed final mesh
	target_mouse_vertices.resize(target_transformed_mouse.n_vertices());

	if (need_change_mouse == true)
	{
		if (mouse_autoScale_bool == 1)
		{
			ScaledRigid E_SICP_mouse(pre_vertices, pre_targetMouse_vertices, landmark_mouse_indices, num_mouse_landmark); //mouse
			double test_cost = E_SICP_mouse.run();

			mouse_scale = E_SICP_mouse.getScale();
			mouse_rotation = E_SICP_mouse.getRotation();
			mouse_translation = E_SICP_mouse.getTranslation();
		}
		else
		{
			RigidTR E_SICP_mouse(pre_vertices, pre_targetMouse_vertices, landmark_mouse_indices, num_mouse_landmark, mouse_scale_factor, mouth_scale_width, mouth_scale_height); //mouse
			double test_cost = E_SICP_mouse.run();

			mouse_scale = E_SICP_mouse.getScale();
			mouse_rotation = E_SICP_mouse.getRotation();
			mouse_translation = E_SICP_mouse.getTranslation();

			mouse_rotation[0] = mouse_rotation[0];
			mouse_rotation[1] = mouse_rotation[1];
			mouse_rotation[2] = mouse_rotation[2];
			mouse_translation[0] = mouse_translation[0];
			mouse_translation[1] = mouse_translation[1];
			mouse_translation[2] = mouse_translation[2];
		}


		double pra_rotation_matrix[9];
		ceres::AngleAxisToRotationMatrix(mouse_rotation, pra_rotation_matrix);

		Eigen::Matrix4f pra_transformed_matrix;
		Eigen::Matrix4f pra_scale_matrix;

		pra_transformed_matrix(0, 0) = float(pra_rotation_matrix[0]); pra_transformed_matrix(0, 1) = float(pra_rotation_matrix[3]); pra_transformed_matrix(0, 2) = float(pra_rotation_matrix[6]); pra_transformed_matrix(0, 3) = float(mouse_translation[0]);
		pra_transformed_matrix(1, 0) = float(pra_rotation_matrix[1]); pra_transformed_matrix(1, 1) = float(pra_rotation_matrix[4]); pra_transformed_matrix(1, 2) = float(pra_rotation_matrix[7]); pra_transformed_matrix(1, 3) = float(mouse_translation[1]) - lip_move_factor;
		pra_transformed_matrix(2, 0) = float(pra_rotation_matrix[2]); pra_transformed_matrix(2, 1) = float(pra_rotation_matrix[5]); pra_transformed_matrix(2, 2) = float(pra_rotation_matrix[8]); pra_transformed_matrix(2, 3) = float(mouse_translation[2]);
		pra_transformed_matrix(3, 0) = 0.0f;						  pra_transformed_matrix(3, 1) = 0.0f;							pra_transformed_matrix(3, 2) = 0.0f;						  pra_transformed_matrix(3, 3) = 1.0f;

		pra_scale_matrix(0, 0) = float(mouse_scale) * float(mouth_scale_width);		pra_scale_matrix(0, 1) = 0.0f;												pra_scale_matrix(0, 2) = 0.0f;					pra_scale_matrix(0, 3) = 0.0f;
		pra_scale_matrix(1, 0) = 0.0f;												pra_scale_matrix(1, 1) = float(mouse_scale) * float(mouth_scale_height);	pra_scale_matrix(1, 2) = 0.0f;					pra_scale_matrix(1, 3) = 0.0f;
		pra_scale_matrix(2, 0) = 0.0f;												pra_scale_matrix(2, 1) = 0.0f;												pra_scale_matrix(2, 2) = float(mouse_scale);	pra_scale_matrix(2, 3) = 0.0f;
		pra_scale_matrix(3, 0) = 0.0f;												pra_scale_matrix(3, 1) = 0.0f;												pra_scale_matrix(3, 2) = 0.0f;					pra_scale_matrix(3, 3) = 1.0f;

		std::cout << "lips_scale: " << mouse_scale << std::endl;
		std::cout << "lips_rotation: " << mouse_rotation[0] << " " << mouse_rotation[1] << " " << mouse_rotation[2] << std::endl;
		std::cout << "lips_translation:" << mouse_translation[0] << " " << mouse_translation[1] << "  " << mouse_translation[2] << std::endl;

		std::cout << "lips row col scale: " << mouth_scale_width << " " << mouth_scale_height << std::endl;

		for (t_vIt = target_transformed_mouse.vertices_begin(); t_vIt != target_transformed_mouse.vertices_end(); ++t_vIt)
		{
			t_v = target_transformed_mouse.point(t_vIt.handle());
			Eigen::Vector4f target_transformed_points = Eigen::Vector4f(t_v[0], t_v[1], t_v[2], 1.0);

			target_transformed_points = pra_scale_matrix * pra_transformed_matrix * target_transformed_points;

			target_transformed_points(0) = target_transformed_points(0) / target_transformed_points(3);
			target_transformed_points(1) = target_transformed_points(1) / target_transformed_points(3);
			target_transformed_points(2) = target_transformed_points(2) / target_transformed_points(3);

			target_transformed_mouse.point(t_vIt.handle()) = OpenMesh::Vec3f(target_transformed_points(0) + mean_x, target_transformed_points(1) + mean_y + lips_Y, target_transformed_points(2) + mean_z);
			target_mouse_vertices[t_vIt->idx()] = Eigen::Vector3f(target_transformed_points(0) + mean_x, target_transformed_points(1) + mean_y + lips_Y, target_transformed_points(2) + mean_z);

		}

		target_transformed_mouse.update_face_normals();
		target_transformed_mouse.update_vertex_normals();

		//target_transformed_mouse = target_eyes_mesh; //jws
		//ICP_mesh_check
		OpenMesh::IO::write_mesh(target_transformed_mouse, "ICP_lip_mesh.obj");
	}


	///////////////////////////////////////////////////////////////////////
	////////////////// fitting ///////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////

	int fitting_count = 1;

	std::vector<Eigen::Vector3f> dVertices, verticeRots;
	dVertices.resize(src_mesh.n_vertices());
	verticeRots.resize(src_mesh.n_vertices());

	std::vector<std::vector<int>> neighborIndices;
	neighborIndices.resize(src_mesh.n_vertices());

	MyMesh::VertexIter vIt;
	MyMesh::VertexVertexIter vvIt;


	for (vIt = src_mesh.vertices_begin(); vIt != src_mesh.vertices_end(); ++vIt)
	{
		dVertices[vIt->idx()] = Eigen::Vector3f(0, 0, 0);
		verticeRots[vIt->idx()] = Eigen::Vector3f(0, 0, 0);

		for (vvIt = src_mesh.vv_iter(vIt.handle()); vvIt; ++vvIt)
			neighborIndices[vIt->idx()].push_back(vvIt->idx());
	}

	MyMesh outputMesh = src_mesh;

	double weightTV, weightARAP, weightCon, weightLandNormal, weightConInside, weightConNormal;
	weightTV = 1.0;
	weightARAP = 1.0;
	weightCon = 2.0; //landmark
	weightLandNormal = 1.5; //landmark normal

	weightConInside = 1.5; // inside point
	weightConNormal = 2.0; // inside normal

						   //std::vector<cv::Point3f> points_eyes; //target_transformed eyes
						   //std::vector<cv::Point3f> points_nose; //target_transformed nose
						   //std::vector<cv::Point3f> points_mouse; //target_transformed mouse

						   //std::vector<cv::Point3f> points_left_eye; 

						   //std::vector<int> fitting_eyes_indices;
						   //std::vector<int> fitting_nose_indices;
						   //std::vector<int> fitting_mouse_indices;

						   //std::vector<int> fitting_left_eye_indices; //Add left eye

	std::vector<int> landmark_all;

	//std::vector<int> fitting_right_eyebrows_indices;
	//std::vector<int> fitting_left_eyebrows_indices;	

	// related to landmark
	std::vector<Eigen::Vector3f> target_vertices;
	std::vector<Eigen::Vector3f> target_Normals;
	//target_vertices.resize(num_eyes_landmark + num_nose_landmark + num_mouse_landmark);

	// related to inside
	std::vector<Eigen::Vector3f> targets, targetNormals;
	std::vector<int> targetIndices;

	if (change_shape_state == 100)
	{
		for (int j = 0; j < num_shape_landmark; j++)
		{
			landmark_all.push_back(landmark_shape_indices[j]);

			OpenMesh::VertexHandle vh = target_transformed_shape.vertex_handle(landmark_shape_indices[j]);
			v = target_transformed_shape.point(vh);

			target_vertices.push_back(Eigen::Vector3f(v[0], v[1], v[2]));

			v_normal = target_transformed_shape.normal(vh);
			target_Normals.push_back(Eigen::Vector3f(v_normal[0], v_normal[1], v_normal[2]));
		}

		for (int j = 0; j < num_shape_inside; j++)
		{
			OpenMesh::VertexHandle vh = target_transformed_shape.vertex_handle(inside_shape_indices[j]);
			v = target_transformed_shape.point(vh);
			targets.push_back(Eigen::Vector3f(v[0], v[1], v[2]));

			v_normal = target_transformed_shape.normal(vh);
			targetNormals.push_back(Eigen::Vector3f(v_normal[0], v_normal[1], v_normal[2]));

			int idx = vh.idx();
			targetIndices.push_back(idx);

		}

		//////right cheek input
		for (int j = 0; j < num_rightCheek_landmark; j++)
		{
			landmark_all.push_back(landmark_rightCheek_indices[j]);

			OpenMesh::VertexHandle vh = target_rightCheek.vertex_handle(landmark_rightCheek_indices[j]);
			v = target_rightCheek.point(vh);

			target_vertices.push_back(Eigen::Vector3f(v[0], v[1], v[2]));

			v_normal = target_rightCheek.normal(vh);
			target_Normals.push_back(Eigen::Vector3f(v_normal[0], v_normal[1], v_normal[2]));

		}

		///// Left cheek input
		for (int j = 0; j < num_leftCheek_landmark; j++)
		{
			landmark_all.push_back(landmark_leftCheek_indices[j]);

			OpenMesh::VertexHandle vh = target_leftCheek.vertex_handle(landmark_leftCheek_indices[j]);
			v = target_leftCheek.point(vh);

			target_vertices.push_back(Eigen::Vector3f(v[0], v[1], v[2]));

			v_normal = target_leftCheek.normal(vh);
			target_Normals.push_back(Eigen::Vector3f(v_normal[0], v_normal[1], v_normal[2]));
		}


	}

	if (eyebrow_bool == 1)
	{
		//right eyebrow landmark - points
		for (int j = 0; j < num_r_eyebrow_landmark; j++)
		{
			//fitting_right_eyebrows_indices.push_back(landmark_r_eyebrow_indices[j]);
			landmark_all.push_back(landmark_r_eyebrow_indices[j]);

			OpenMesh::VertexHandle vh = target_transformed_eyebrow_right.vertex_handle(landmark_r_eyebrow_indices[j]);
			v = target_transformed_eyebrow_right.point(vh);
			//points_eyes.push_back(cv::Point3f(v[0], v[1], v[2]));
			target_vertices.push_back(Eigen::Vector3f(v[0], v[1], v[2]));

			v_normal = target_transformed_eyebrow_right.normal(vh);
			target_Normals.push_back(Eigen::Vector3f(v_normal[0], v_normal[1], v_normal[2]));
		}

		for (int j = 0; j < num_l_eyebrow_landmark; j++)
		{
			//fitting_left_eyebrows_indices.push_back(landmark_l_eyebrow_indices[j]);
			landmark_all.push_back(landmark_l_eyebrow_indices[j]);

			OpenMesh::VertexHandle vh = target_transformed_eyebrow_left.vertex_handle(landmark_l_eyebrow_indices[j]);
			v = target_transformed_eyebrow_left.point(vh);
			//points_eyes.push_back(cv::Point3f(v[0], v[1], v[2]));
			target_vertices.push_back(Eigen::Vector3f(v[0], v[1], v[2]));

			v_normal = target_transformed_eyebrow_left.normal(vh);
			target_Normals.push_back(Eigen::Vector3f(v_normal[0], v_normal[1], v_normal[2]));

		}

	}

	///////////////////////////////eye fitting parts//////////////////////////////
	if (need_change_eye == true) {

		//right eye landmark - points
		for (int j = 0; j < num_eyes_landmark; j++)
		{
			//fitting_eyes_indices.push_back(landmark_eyes_indices[j]);
			landmark_all.push_back(landmark_eyes_indices[j]);

			OpenMesh::VertexHandle vh = target_transformed_eyes.vertex_handle(landmark_eyes_indices[j]);
			v = target_transformed_eyes.point(vh);
			//points_eyes.push_back(cv::Point3f(v[0], v[1], v[2]));
			target_vertices.push_back(Eigen::Vector3f(v[0], v[1], v[2]));

			v_normal = target_transformed_eyes.normal(vh);
			target_Normals.push_back(Eigen::Vector3f(v_normal[0], v_normal[1], v_normal[2]));
		}

		//right eye inside - points
		for (int j = 0; j < num_eyes_inside; j++)
		{
			//fitting_eyes_indices.push_back(inside_eyes_indices[j]);

			OpenMesh::VertexHandle vh = target_transformed_eyes.vertex_handle(inside_eyes_indices[j]);
			v = target_transformed_eyes.point(vh);
			//points_eyes.push_back(cv::Point3f(v[0], v[1], v[2]));
			targets.push_back(Eigen::Vector3f(v[0], v[1], v[2]));

			v_normal = target_transformed_eyes.normal(vh);
			targetNormals.push_back(Eigen::Vector3f(v_normal[0], v_normal[1], v_normal[2]));

			int idx = vh.idx();
			targetIndices.push_back(idx);
		}

		//Left eye landmark - points
		for (int j = 0; j < num_l_eyes_landmark; j++)
		{
			//fitting_left_eye_indices.push_back(landmark_l_eyes_indices[j]);
			landmark_all.push_back(landmark_l_eyes_indices[j]);

			OpenMesh::VertexHandle vh = target_transformed_eyes_left.vertex_handle(landmark_l_eyes_indices[j]);
			v = target_transformed_eyes_left.point(vh);
			//points_left_eye.push_back(cv::Point3f(v[0], v[1], v[2]));
			target_vertices.push_back(Eigen::Vector3f(v[0], v[1], v[2]));

			v_normal = target_transformed_eyes_left.normal(vh);
			target_Normals.push_back(Eigen::Vector3f(v_normal[0], v_normal[1], v_normal[2]));
		}

		//Left eye inside - points
		for (int j = 0; j < num_l_eyes_inside; j++)
		{
			//fitting_left_eye_indices.push_back(inside_l_eyes_indices[j]);

			OpenMesh::VertexHandle vh = target_transformed_eyes_left.vertex_handle(inside_l_eyes_indices[j]);
			v = target_transformed_eyes_left.point(vh);
			//points_left_eye.push_back(cv::Point3f(v[0], v[1], v[2]));
			targets.push_back(Eigen::Vector3f(v[0], v[1], v[2]));

			v_normal = target_transformed_eyes.normal(vh);
			targetNormals.push_back(Eigen::Vector3f(v_normal[0], v_normal[1], v_normal[2]));

			int idx = vh.idx();
			targetIndices.push_back(idx);

		}


	}

	//nose landmark - points
	if (need_change_nose == true) {
		for (int j = 0; j < num_nose_landmark; j++)
		{
			//fitting_nose_indices.push_back(landmark_nose_indices[j]);
			landmark_all.push_back(landmark_nose_indices[j]);

			OpenMesh::VertexHandle vh = target_transformed_nose.vertex_handle(landmark_nose_indices[j]);
			v = target_transformed_nose.point(vh);
			// points_nose.push_back(cv::Point3f(v[0], v[1], v[2]));
			target_vertices.push_back(Eigen::Vector3f(v[0], v[1], v[2]));

			v_normal = target_transformed_nose.normal(vh);
			target_Normals.push_back(Eigen::Vector3f(v_normal[0], v_normal[1], v_normal[2]));

		}

		//nose inside - points
		for (int j = 0; j < num_nose_inside; j++)
		{
			//fitting_nose_indices.push_back(inside_nose_indices[j]);

			OpenMesh::VertexHandle vh = target_transformed_nose.vertex_handle(inside_nose_indices[j]);
			v = target_transformed_nose.point(vh);
			//points_nose.push_back(cv::Point3f(v[0], v[1], v[2]));
			targets.push_back(Eigen::Vector3f(v[0], v[1], v[2]));

			v_normal = target_transformed_nose.normal(vh);
			targetNormals.push_back(Eigen::Vector3f(v_normal[0], v_normal[1], v_normal[2]));

			int idx = vh.idx();
			targetIndices.push_back(idx);
		}

	}

	//mouse landmark - points
	if (need_change_mouse == true) {
		for (int j = 0; j < num_mouse_landmark; j++)
		{
			//fitting_mouse_indices.push_back(landmark_mouse_indices[j]);
			landmark_all.push_back(landmark_mouse_indices[j]);

			OpenMesh::VertexHandle vh = target_transformed_mouse.vertex_handle(landmark_mouse_indices[j]);
			v = target_transformed_mouse.point(vh);
			//points_mouse.push_back(cv::Point3f(v[0], v[1], v[2]));
			target_vertices.push_back(Eigen::Vector3f(v[0], v[1], v[2]));

			v_normal = target_transformed_mouse.normal(vh);
			target_Normals.push_back(Eigen::Vector3f(v_normal[0], v_normal[1], v_normal[2]));
		}

		//mouse inside - points
		for (int j = 0; j < num_mouse_inside; j++)
		{
			//fitting_mouse_indices.push_back(inside_mouse_indices[j]);

			OpenMesh::VertexHandle vh = target_transformed_mouse.vertex_handle(inside_mouse_indices[j]);
			v = target_transformed_mouse.point(vh);
			//points_mouse.push_back(cv::Point3f(v[0], v[1], v[2]));
			targets.push_back(Eigen::Vector3f(v[0], v[1], v[2]));

			v_normal = target_transformed_mouse.normal(vh);
			targetNormals.push_back(Eigen::Vector3f(v_normal[0], v_normal[1], v_normal[2]));

			int idx = vh.idx();
			targetIndices.push_back(idx);
		}

	}


	//save vertex information declaration
	std::string output_vertex_file = argv[2];
	//output_vertex_file.erase(output_vertex_file.length() - 10, 10);
	//output_vertex_file = output_vertex_file + "default.obj";
	FILE * fp_save_vertex = fopen(output_vertex_file.c_str(), "w");


	for (int i = 0; i < fitting_count; i++)
	{
		/////////find nearst neightbor (variables) ///////////////////////////////////////
		//std::vector<Eigen::Vector3f> targets, targetNormals;
		//std::vector<int> targetIndices;

		OpenMesh::Vec3f v_src, vn_src, vTarget, vnTarget;


		auto_fit energy(weightTV, weightARAP, weightCon, weightLandNormal, weightConInside, weightConNormal, t_src, dVertices, verticeRots, neighborIndices, targets, targetNormals, targetIndices, target_vertices, target_Normals, landmark_all);

		double fitting_cost = energy.run();

		for (vIt = outputMesh.vertices_begin(); vIt != outputMesh.vertices_end(); ++vIt)  // 전체 변형
		{
			int idx = vIt->idx();
			Eigen::Vector3f point = t_src[idx] + dVertices[idx];
			outputMesh.point(vIt.handle()) = OpenMesh::Vec3f(point.x(), point.y(), point.z());

			//save vertex information
			if (i == fitting_count - 1)
			{
				//printf("kk %d %f %f %f \n", i, point.x(), point.y(), point.z());
				fprintf(fp_save_vertex, "%f %f %f ", point.x(), point.y(), point.z());
			}
		}

		//// update vertex normal
		outputMesh.update_face_normals();
		outputMesh.update_vertex_normals();

		// save mesh
		//OpenMesh::IO::Options writeOpt = OpenMesh::IO::Options::VertexTexCoord;
		if (i == fitting_count - 1) {
			if (!OpenMesh::IO::write_mesh(outputMesh, "output_" + std::to_string(i) + ".obj"))
			{
				std::cerr << "write error\n";
				exit(1);
			}
		}


		//weightTV -= 0.015;
		//weightARAP -= 0.015;

	} // for(fitting_count)

	fclose(fp_save_vertex);

	end = std::chrono::system_clock::now();

	std::chrono::duration<double> elapsed_seconds = end - start;
	std::cout << "elapsed time: " << elapsed_seconds.count() << "s\n\n";
	printf("\n\n\n");

	return 0;
}

// 19.10.10
unordered_map<string, float> calcXpositionRate(MyMesh real_mesh, MyMesh std_mesh, MyMesh target_mesh) {
	/*
	std::cout << endl;
	std::cout << "--------------- calcXpositionRate fucntion START ---------------" << endl;
	std::cout << endl;
	*/
	MyMesh::VertexIter t_vIt;
	OpenMesh::Vec3f t_v;

	std::vector<Eigen::Vector3f> t_real, t_std, t_tg;
	t_real.resize(real_mesh.n_vertices());
	t_std.resize(std_mesh.n_vertices());
	t_tg.resize(target_mesh.n_vertices());

	float *real_x_pos = new float[real_mesh.n_vertices()];
	float *real_y_pos = new float[real_mesh.n_vertices()];

	float *std_x_pos = new float[std_mesh.n_vertices()];
	float *std_y_pos = new float[std_mesh.n_vertices()];

	float *tg_x_pos = new float[target_mesh.n_vertices()];
	float *tg_y_pos = new float[target_mesh.n_vertices()];

	// real mesh vertex info
	for (t_vIt = real_mesh.vertices_begin(); t_vIt != real_mesh.vertices_end(); ++t_vIt) {
		t_v = real_mesh.point(t_vIt.handle());
		t_real[t_vIt->idx()] = Eigen::Vector3f(t_v[0], t_v[1], t_v[2]);
		real_x_pos[t_vIt->idx()] = t_v[0];
		real_y_pos[t_vIt->idx()] = t_v[1];
		//std::cout << t_vIt;
	}
	//
	// standard mesh vertex info
	for (t_vIt = std_mesh.vertices_begin(); t_vIt != std_mesh.vertices_end(); ++t_vIt) {
		t_v = std_mesh.point(t_vIt.handle());
		t_std[t_vIt->idx()] = Eigen::Vector3f(t_v[0], t_v[1], t_v[2]);
		std_x_pos[t_vIt->idx()] = t_v[0];
		std_y_pos[t_vIt->idx()] = t_v[1];
		//std::cout << t_vIt;
	}
	//
	// target mesh vertex info
	for (t_vIt = target_mesh.vertices_begin(); t_vIt != target_mesh.vertices_end(); ++t_vIt) {
		t_v = target_mesh.point(t_vIt.handle());
		t_tg[t_vIt->idx()] = Eigen::Vector3f(t_v[0], t_v[1], t_v[2]);
		tg_x_pos[t_vIt->idx()] = t_v[0];
		tg_y_pos[t_vIt->idx()] = t_v[1];
		//std::cout << t_vIt;
	}
	//

	// calc mesh face width
	int face_R_vertex_idx = 610;								int face_L_vertex_idx = 2681;
	float real_face_R_vertex = real_x_pos[face_R_vertex_idx];	float real_face_L_vertex = real_x_pos[face_L_vertex_idx];
	float std_face_R_vertex = std_x_pos[face_R_vertex_idx];		float std_face_L_vertex = std_x_pos[face_L_vertex_idx];
	float tg_face_R_vertex = tg_x_pos[face_R_vertex_idx];		float tg_face_L_vertex = tg_x_pos[face_L_vertex_idx];
	float real_face_width = real_face_L_vertex - real_face_R_vertex;	float std_face_width = std_face_L_vertex - std_face_R_vertex;	float tg_face_width = tg_face_L_vertex - tg_face_R_vertex;

	/*
	std::cout << endl;
	std::cout << "REAL FACE R VERTEX X : " << real_face_R_vertex << " FACE L X : " << real_face_L_vertex << endl;
	std::cout << "STANDARD FACE R VERTEX X : " << std_face_R_vertex << " FACE L X : " << std_face_L_vertex << endl;
	std::cout << "TARGET FACE R VERTEX X : " << tg_face_R_vertex << " FACE L X : " << tg_face_L_vertex << endl;
	std::cout << endl;
	std::cout << "REAL FACE WIDTH : " << real_face_width << endl;
	std::cout << "STANDARD FACE WIDTH : " << std_face_width << endl;
	std::cout << "TARGET FACE WIDTH : " << tg_face_width << endl;
	*/
	//

	// calc mesh eye R <-> eye L width
	int eye_R_vertex_idx = 1340;								int eye_L_vertex_idx = 2772;
	float real_eye_R_vertex = real_x_pos[eye_R_vertex_idx];		float real_eye_L_vertex = real_x_pos[eye_L_vertex_idx];
	float std_eye_R_vertex = std_x_pos[eye_R_vertex_idx];		float std_eye_L_vertex = std_x_pos[eye_L_vertex_idx];
	float tg_eye_R_vertex = tg_x_pos[eye_R_vertex_idx];			float tg_eye_L_vertex = tg_x_pos[eye_L_vertex_idx];

	float real_eye_width = real_eye_L_vertex - real_eye_R_vertex;		float std_eye_width = std_eye_L_vertex - std_eye_R_vertex;	float tg_eye_width = tg_eye_L_vertex - tg_eye_R_vertex;
	/*
	std::cout << endl;
	std::cout << "REAL EYE R VERTEX X : " << real_eye_R_vertex << " EYE L X : " << real_eye_L_vertex << endl;
	std::cout << "STANDARD EYE R VERTEX X : " << std_eye_R_vertex << " EYE L X : " << std_eye_L_vertex << endl;
	std::cout << "TARGET EYE R VERTEX X : " << tg_eye_R_vertex << " EYE L X : " << tg_eye_L_vertex << endl;
	std::cout << endl;
	std::cout << "REAL EYE WIDTH : " << real_eye_width << endl;
	std::cout << "STANDARD EYE WIDTH : " << std_eye_width << endl;
	std::cout << "TARGET EYE WIDTH : " << tg_eye_width << endl;
	*/
	//

	// calc eye gap by rate
	//float real_std_eye_gap = real_eye_width - std_eye_width;
	float real_std_eye_gap = (real_eye_width / real_face_width) - (std_eye_width / std_face_width);
	/*
	std::cout << endl;
	std::cout << "real_std_eye_gap : " << real_std_eye_gap << endl;
	*/
	//float real_tg_eye_gap = (real_std_eye_gap * tg_face_width) / real_face_width;
	float real_tg_eye_gap = real_std_eye_gap * tg_face_width;
	std::cout << endl;
	std::cout << "EYE MOVE X : " << real_tg_eye_gap << endl;
	/*
	float eye_R_X_pos = (real_tg_eye_gap/2);	float eye_L_X_pos = -(real_tg_eye_gap / 2);
	std::cout << endl;
	std::cout << "REAL EYE R MOVE X : " << eye_R_X_pos << endl;
	std::cout << "REAL EYE L MOVE X : " << eye_L_X_pos << endl;
	*/
	//

	// set res
	unordered_map<string, float> res_set;
	res_set.clear();

	res_set.insert(unordered_map<string, float>::value_type("eye_move_X", real_tg_eye_gap));

	/*
	std::cout << endl;
	std::cout << "--------------- calcXpositionRate fucntion END ---------------" << endl;
	std::cout << endl;
	*/
	return res_set;
}

unordered_map<string, float> calcYpositionRate(MyMesh real_mesh, MyMesh std_mesh, MyMesh target_mesh) {
	/*
	std::cout << endl;
	std::cout << "--------------- calcYpositionRate fucntion START ---------------" << endl;
	std::cout << endl;
	*/
	MyMesh::VertexIter t_vIt;
	OpenMesh::Vec3f t_v;

	std::vector<Eigen::Vector3f> t_real, t_std, t_tg;
	t_real.resize(real_mesh.n_vertices());
	t_std.resize(std_mesh.n_vertices());
	t_tg.resize(target_mesh.n_vertices());

	float *real_x_pos = new float[real_mesh.n_vertices()];
	float *real_y_pos = new float[real_mesh.n_vertices()];

	float *std_x_pos = new float[std_mesh.n_vertices()];
	float *std_y_pos = new float[std_mesh.n_vertices()];

	float *tg_x_pos = new float[target_mesh.n_vertices()];
	float *tg_y_pos = new float[target_mesh.n_vertices()];

	// real mesh vertex info
	for (t_vIt = real_mesh.vertices_begin(); t_vIt != real_mesh.vertices_end(); ++t_vIt) {
		t_v = real_mesh.point(t_vIt.handle());
		t_real[t_vIt->idx()] = Eigen::Vector3f(t_v[0], t_v[1], t_v[2]);
		real_x_pos[t_vIt->idx()] = t_v[0];
		real_y_pos[t_vIt->idx()] = t_v[1];
		//std::cout << t_vIt;
	}
	//
	// standard mesh vertex info
	for (t_vIt = std_mesh.vertices_begin(); t_vIt != std_mesh.vertices_end(); ++t_vIt) {
		t_v = std_mesh.point(t_vIt.handle());
		t_std[t_vIt->idx()] = Eigen::Vector3f(t_v[0], t_v[1], t_v[2]);
		std_x_pos[t_vIt->idx()] = t_v[0];
		std_y_pos[t_vIt->idx()] = t_v[1];
		//std::cout << t_vIt;
	}
	//
	// target mesh vertex info
	for (t_vIt = target_mesh.vertices_begin(); t_vIt != target_mesh.vertices_end(); ++t_vIt) {
		t_v = target_mesh.point(t_vIt.handle());
		t_tg[t_vIt->idx()] = Eigen::Vector3f(t_v[0], t_v[1], t_v[2]);
		tg_x_pos[t_vIt->idx()] = t_v[0];
		tg_y_pos[t_vIt->idx()] = t_v[1];
		//std::cout << t_vIt;
	}
	//

	// calc mesh face height
	int face_top_vertex_idx = 610;									int face_bottom_vertex_idx = 2411;
	float real_face_top_vertex = real_y_pos[face_top_vertex_idx];	float real_face_bottom_vertex = real_y_pos[face_bottom_vertex_idx];
	float std_face_top_vertex = std_y_pos[face_top_vertex_idx];		float std_face_bottom_vertex = std_y_pos[face_bottom_vertex_idx];
	float tg_face_top_vertex = tg_y_pos[face_top_vertex_idx];		float tg_face_bottom_vertex = tg_y_pos[face_bottom_vertex_idx];

	float real_face_height = real_face_top_vertex - real_face_bottom_vertex;	float std_face_height = std_face_top_vertex - std_face_bottom_vertex;	float tg_face_height = tg_face_top_vertex - tg_face_bottom_vertex;

	/*
	std::cout << endl;
	std::cout << "REAL FACE TOP VERTEX Y : " << real_face_top_vertex << " FACE BOTTOM Y : " << real_face_bottom_vertex << endl;
	std::cout << "STANDARD FACE TOP VERTEX Y : " << std_face_top_vertex << " FACE BOTTOM Y : " << std_face_bottom_vertex << endl;
	std::cout << "TARGET FACE TOP VERTEX Y : " << tg_face_top_vertex << " FACE BOTTOM Y : " << tg_face_bottom_vertex << endl;
	std::cout << endl;
	std::cout << "REAL FACE HEIGHT : " << real_face_height << endl;
	std::cout << "STANDARD FACE HEIGHT : " << std_face_height << endl;
	std::cout << "TARGET FACE HEIGHT : " << tg_face_height << endl;
	*/
	//

	// calc mesh eye, nose, lips Y average
	// eye_R landmark
	const static int eye_landmark_size = 12;
	const static int eye_R_landmark_indices[eye_landmark_size] =
	{ 1355, 1358, 1361, 1364, 1366, 1368, 1340, 445, 1044, 439, 802, 432 };

	// eye_L landmark
	const static int eye_L_landmark_indices[eye_landmark_size] =
	{ 2787, 2790, 2792, 2795, 2798, 2800, 2772, 1877, 2476, 1871, 2234, 1864 };

	// nose landmark
	const static int nose_landmark_size = 26;
	const static int nose_landmark_indices[nose_landmark_size] =
	{ 704, 553, 1037, 1013, 1001, 1004, 480, 913, 462, 62, 2136, 1985, 2469, 2448, 2438, 2436, 1912, 2344, 2944, 41, 80, 70, 58, 36, 35, 34 };

	// lips landmark
	const static int lips_landmark_size = 38;
	const static int lips_landmark_indices[lips_landmark_size] =
	{ 1425, 1445, 1442, 1440, 1438, 1437, 79, 2869, 2870, 2872, 2874, 2877, 2834, 2885, 2887, 1716, 1714, 1796,
		21, 364, 282, 284, 1455, 1453, 1397, 1394, 1391, 0, 2823, 2826, 2829, 2828, 2841, 2843, 1, 1411, 1409, 1406 };

	float real_ave_eye_R_Y = 0.0f;		float real_ave_eye_L_Y = 0.0f;		float real_ave_nose_Y = 0.0f;		float real_ave_lips_Y = 0.0f;
	float std_ave_eye_R_Y = 0.0f;		float std_ave_eye_L_Y = 0.0f;		float std_ave_nose_Y = 0.0f;		float std_ave_lips_Y = 0.0f;
	float tg_ave_eye_R_Y = 0.0f;		float tg_ave_eye_L_Y = 0.0f;		float tg_ave_nose_Y = 0.0f;			float tg_ave_lips_Y = 0.0f;

	for (int i = 0; i < eye_landmark_size; i++) {
		real_ave_eye_R_Y += real_y_pos[eye_R_landmark_indices[i]];	std_ave_eye_R_Y += std_y_pos[eye_R_landmark_indices[i]];	tg_ave_eye_R_Y += tg_y_pos[eye_R_landmark_indices[i]];
		real_ave_eye_L_Y += real_y_pos[eye_L_landmark_indices[i]];	std_ave_eye_L_Y += std_y_pos[eye_L_landmark_indices[i]];	tg_ave_eye_L_Y += tg_y_pos[eye_L_landmark_indices[i]];
	}
	for (int i = 0; i < nose_landmark_size; i++) {
		real_ave_nose_Y += real_y_pos[nose_landmark_indices[i]];	std_ave_nose_Y += std_y_pos[nose_landmark_indices[i]];	tg_ave_nose_Y += tg_y_pos[nose_landmark_indices[i]];
	}
	for (int i = 0; i < lips_landmark_size; i++) {
		real_ave_lips_Y += real_y_pos[lips_landmark_indices[i]];	std_ave_lips_Y += std_y_pos[lips_landmark_indices[i]];	tg_ave_lips_Y += tg_y_pos[lips_landmark_indices[i]];
	}

	real_ave_eye_R_Y = real_ave_eye_R_Y / eye_landmark_size;	real_ave_eye_L_Y = real_ave_eye_L_Y / eye_landmark_size;	real_ave_nose_Y = real_ave_nose_Y / nose_landmark_size;		real_ave_lips_Y = real_ave_lips_Y / lips_landmark_size;
	std_ave_eye_R_Y = std_ave_eye_R_Y / eye_landmark_size;		std_ave_eye_L_Y = std_ave_eye_L_Y / eye_landmark_size;		std_ave_nose_Y = std_ave_nose_Y / nose_landmark_size;		std_ave_lips_Y = std_ave_lips_Y / lips_landmark_size;
	tg_ave_eye_R_Y = tg_ave_eye_R_Y / eye_landmark_size;		tg_ave_eye_L_Y = tg_ave_eye_L_Y / eye_landmark_size;		tg_ave_nose_Y = tg_ave_nose_Y / nose_landmark_size;			tg_ave_lips_Y = tg_ave_lips_Y / lips_landmark_size;
	/*
	std::cout << endl;
	std::cout << "REAL AVERAGE Y EYE R : " << real_ave_eye_R_Y << " EYE L : " << real_ave_eye_L_Y << " NOSE : " << real_ave_nose_Y << " LIPS : " << real_ave_lips_Y << endl;
	std::cout << "STANDARD AVERAGE Y EYE R : " << std_ave_eye_R_Y << " EYE L : " << std_ave_eye_L_Y << " NOSE : " << std_ave_nose_Y << " LIPS : " << std_ave_lips_Y << endl;
	std::cout << "TARGET AVERAGE Y EYE R : " << tg_ave_eye_R_Y << " EYE L : " << tg_ave_eye_L_Y << " NOSE : " << tg_ave_nose_Y << " LIPS : " << tg_ave_lips_Y << endl;
	*/
	//

	// calc eye, nose, lips height
	float real_eye_R_height = real_ave_eye_R_Y - real_face_bottom_vertex;	float real_eye_L_height = real_ave_eye_L_Y - real_face_bottom_vertex;
	float real_nose_height = real_ave_nose_Y - real_face_bottom_vertex;		float real_lips_height = real_ave_lips_Y - real_face_bottom_vertex;

	float std_eye_R_height = std_ave_eye_R_Y - std_face_bottom_vertex;		float std_eye_L_height = std_ave_eye_L_Y - std_face_bottom_vertex;
	float std_nose_height = std_ave_nose_Y - std_face_bottom_vertex;		float std_lips_height = std_ave_lips_Y - std_face_bottom_vertex;

	float tg_eye_R_height = tg_ave_eye_R_Y - tg_face_bottom_vertex;			float tg_eye_L_height = tg_ave_eye_L_Y - tg_face_bottom_vertex;
	float tg_nose_height = tg_ave_nose_Y - tg_face_bottom_vertex;			float tg_lips_height = tg_ave_lips_Y - tg_face_bottom_vertex;
	//

	// calc mesh eye, nose, lips gap by rate
	float real_std_eye_R_gap = (real_eye_R_height / real_face_height) - (std_eye_R_height / std_face_height);
	float real_std_eye_L_gap = (real_eye_L_height / real_face_height) - (std_eye_L_height / std_face_height);
	float real_std_nose_gap = (real_nose_height / real_face_height) - (std_nose_height / std_face_height);
	float real_std_lips_gap = (real_lips_height / real_face_height) - (std_lips_height / std_face_height);

	float real_tg_eye_R_gap = real_std_eye_R_gap * tg_face_height;
	float real_tg_eye_L_gap = real_std_eye_L_gap * tg_face_height;
	float real_tg_nose_gap = real_std_nose_gap * tg_face_height;
	float real_tg_lips_gap = real_std_lips_gap * tg_face_height;

	std::cout << "EYE R MOVE Y : " << real_tg_eye_R_gap << endl;
	std::cout << "EYE L MOVE Y : " << real_tg_eye_L_gap << endl;
	std::cout << "NOSE MOVE Y : " << real_tg_nose_gap << endl;
	std::cout << "LIPS MOVE Y : " << real_tg_lips_gap << endl;
	//

	// set res
	unordered_map<string, float> res_set;
	res_set.clear();

	res_set.insert(unordered_map<string, float>::value_type("eye_R_Y", real_tg_eye_R_gap));
	res_set.insert(unordered_map<string, float>::value_type("eye_L_Y", real_tg_eye_L_gap));
	res_set.insert(unordered_map<string, float>::value_type("nose_Y", real_tg_nose_gap));
	res_set.insert(unordered_map<string, float>::value_type("lips_Y", real_tg_lips_gap));
	/*
	std::cout << endl;
	std::cout << "--------------- calcYpositionRate fucntion END ---------------" << endl;
	std::cout << endl;
	*/
	return res_set;
}
//

//
void getDistanceObj(MyMesh real_mesh_ts, MyMesh std_mesh_ts, MyMesh target_mesh_ts) {
	std::cout << endl;
	std::cout << "--------------- getDistanceObj fucntion START ---------------" << endl;
	std::cout << endl;

	// real mesh vertex 조회
	MyMesh::VertexIter real_t_vIt;
	OpenMesh::Vec3f real_t_v;

	std::vector<Eigen::Vector3f> t_real;
	t_real.resize(real_mesh_ts.n_vertices());

	float *real_x_pos = new float[real_mesh_ts.n_vertices()];
	float *real_y_pos = new float[real_mesh_ts.n_vertices()];
	float *real_z_pos = new float[real_mesh_ts.n_vertices()];

	for (real_t_vIt = real_mesh_ts.vertices_begin(); real_t_vIt != real_mesh_ts.vertices_end(); ++real_t_vIt)
	{
		real_t_v = real_mesh_ts.point(real_t_vIt.handle());
		t_real[real_t_vIt->idx()] = Eigen::Vector3f(real_t_v[0], real_t_v[1], real_t_v[2]);

		// SRC X,Y POS 값 저장
		real_x_pos[real_t_vIt->idx()] = real_t_v[0];
		real_y_pos[real_t_vIt->idx()] = real_t_v[1];
		real_z_pos[real_t_vIt->idx()] = real_t_v[2];
	}

	// face distance 0 to 32
	const int face_node_right = 610;
	const int face_node_left = 2681;
	const int face_node_bottom = 2411;

	// eye R
	const int eye_R_node_right = 1355;
	const int eye_R_node_left = 1340;

	// eye L
	const int eye_L_node_right = 2772;
	const int eye_L_node_left = 2787;

	float *real_face_right = new float[3]; // 0:X 1:Y 2:Z
	float *real_face_left = new float[3];
	float *real_face_bottom = new float[3];

	// eye R vertex
	float *real_eye_R_right = new float[3];
	float *real_eye_R_left = new float[3];

	// eye L vertex
	float *real_eye_L_right = new float[3];
	float *real_eye_L_left = new float[3];

	for (int i = 0; i < real_mesh_ts.n_vertices(); i++) {
		if (i == face_node_right) {
			real_face_right[0] = real_x_pos[i];
			real_face_right[1] = real_y_pos[i];
			real_face_right[2] = real_z_pos[i];
		}
		if (i == face_node_left) {
			real_face_left[0] = real_x_pos[i];
			real_face_left[1] = real_y_pos[i];
			real_face_left[2] = real_z_pos[i];
		}
		if (i == face_node_bottom) {
			real_face_bottom[0] = real_x_pos[i];
			real_face_bottom[1] = real_y_pos[i];
			real_face_bottom[2] = real_z_pos[i];
		}
		// eye R vertex
		if (i == eye_R_node_right) {
			real_eye_R_right[0] = real_x_pos[i];
			real_eye_R_right[1] = real_y_pos[i];
			real_eye_R_right[2] = real_z_pos[i];
		}
		if (i == eye_R_node_left) {
			real_eye_R_left[0] = real_x_pos[i];
			real_eye_R_left[1] = real_y_pos[i];
			real_eye_R_left[2] = real_z_pos[i];
		}

		// eye L vertex
		if (i == eye_L_node_right) {
			real_eye_L_right[0] = real_x_pos[i];
			real_eye_L_right[1] = real_y_pos[i];
			real_eye_L_right[2] = real_z_pos[i];
		}
		if (i == eye_L_node_left) {
			real_eye_L_left[0] = real_x_pos[i];
			real_eye_L_left[1] = real_y_pos[i];
			real_eye_L_left[2] = real_z_pos[i];
		}
	}
	//std::cout << "X : " << real_eye_L_left[0] << " Y : " << real_eye_L_left[1] << " Z : " << real_eye_L_left[2] << endl;

	float face_width_dis_x = real_face_left[0] - real_face_right[0];
	float face_height_dis_y = real_face_left[1] - real_face_bottom[1];
	std::cout << "Face WIDTH Distance(X) : " << face_width_dis_x << endl;
	std::cout << "Face HEIGHT Distance(Y) : " << face_height_dis_y << endl;
	std::cout << endl;

	float face_width_dis_xy = sqrt(pow(real_face_left[0] - real_face_right[0], 2) + pow(real_face_left[1] - real_face_right[1], 2));
	float face_height_dis_xy = sqrt(pow(real_face_right[0] - real_face_bottom[0], 2) + pow(real_face_right[1] - real_face_bottom[1], 2));
	std::cout << "Face WIDTH Distance(X,Y) : " << face_width_dis_xy << endl;
	std::cout << "Face HEIGHT Distance(X,Y) : " << face_height_dis_xy << endl;
	std::cout << endl;

	float face_width_dis_xyz = sqrt(pow(real_face_left[0] - real_face_right[0], 2) + pow(real_face_left[1] - real_face_right[1], 2) + pow(real_face_left[2] - real_face_right[2], 2));
	float face_height_dis_xyz = sqrt(pow(real_face_left[0] - real_face_bottom[0], 2) + pow(real_face_left[1] - real_face_bottom[1], 2) + pow(real_face_left[2] - real_face_bottom[2], 2));
	std::cout << "Face WIDTH Distance(X,Y,Z) : " << face_width_dis_xyz << endl;
	std::cout << "Face HEIGHT Distance(X,Y,Z) : " << face_height_dis_xyz << endl;
	std::cout << endl;

	/*
	float eye_FR_width_dis_xyz = sqrt(pow(real_face_right[0] - real_eye_R_right[0], 2) + pow(real_face_right[1] - real_eye_R_right[1], 2) + pow(real_face_right[2] - real_eye_R_right[2], 2));
	float eye_R_width_dis_xyz = sqrt(pow(real_eye_R_left[0] - real_eye_R_right[0], 2) + pow(real_eye_R_left[1] - real_eye_R_right[1], 2) + pow(real_eye_R_left[2] - real_eye_R_right[2], 2));
	std::cout << "eye FR<->ER WIDTH Distance(X,Y,Z) : " << eye_FR_width_dis_xyz << endl;
	std::cout << "eye ER WIDTH Distance(X,Y,Z) : " << eye_R_width_dis_xyz << endl;
	std::cout << endl;

	float eye_RL_width_dis_xyz = sqrt(pow(real_eye_R_left[0] - real_eye_L_right[0], 2) + pow(real_eye_R_left[1] - real_eye_L_right[1], 2) + pow(real_eye_R_left[2] - real_eye_L_right[2], 2));
	std::cout << "eye ER<->EL WIDTH Distance(X,Y,Z) : " << eye_RL_width_dis_xyz << endl;
	std::cout << endl;

	float eye_L_width_dis_xyz = sqrt(pow(real_eye_L_left[0] - real_eye_L_right[0], 2) + pow(real_eye_L_left[1] - real_eye_L_right[1], 2) + pow(real_eye_L_left[2] - real_eye_L_right[2], 2));
	float eye_FL_width_dis_xyz = sqrt(pow(real_eye_L_left[0] - real_face_left[0], 2) + pow(real_eye_L_left[1] - real_face_left[1], 2) + pow(real_eye_L_left[2] - real_face_left[2], 2));
	std::cout << "ete EL WIDTH Distance(X,Y,Z) : " << eye_L_width_dis_xyz << endl;
	std::cout << "ete FL<->EL WIDTH Distance(X,Y,Z) : " << eye_FL_width_dis_xyz << endl;
	std::cout << endl;


	float eye_FR_width_dis_xyz = sqrt(pow(real_face_right[0] - real_eye_R_right[0], 2) + pow(real_face_right[1] - real_eye_R_right[1], 2));
	float eye_R_width_dis_xyz = sqrt(pow(real_eye_R_left[0] - real_eye_R_right[0], 2) + pow(real_eye_R_left[1] - real_eye_R_right[1], 2));
	std::cout << "eye FR<->ER WIDTH Distance(X,Y) : " << eye_FR_width_dis_xyz << endl;
	std::cout << "eye ER WIDTH Distance(X,Y) : " << eye_R_width_dis_xyz << endl;
	std::cout << endl;

	float eye_RL_width_dis_xyz = sqrt(pow(real_eye_R_left[0] - real_eye_L_right[0], 2) + pow(real_eye_R_left[1] - real_eye_L_right[1], 2));
	std::cout << "eye ER<->EL WIDTH Distance(X,Y) : " << eye_RL_width_dis_xyz << endl;
	std::cout << endl;

	float eye_L_width_dis_xyz = sqrt(pow(real_eye_L_left[0] - real_eye_L_right[0], 2) + pow(real_eye_L_left[1] - real_eye_L_right[1], 2));
	float eye_FL_width_dis_xyz = sqrt(pow(real_eye_L_left[0] - real_face_left[0], 2) + pow(real_eye_L_left[1] - real_face_left[1], 2));
	std::cout << "ete EL WIDTH Distance(X,Y) : " << eye_L_width_dis_xyz << endl;
	std::cout << "ete FL<->EL WIDTH Distance(X,Y) : " << eye_FL_width_dis_xyz << endl;
	std::cout << endl;
	*/

	float eye_FR_width_dis_xyz = real_eye_R_right[0] - real_face_right[0];
	float eye_R_width_dis_xyz = real_eye_R_left[0] - real_eye_R_right[0];
	std::cout << "eye FR<->ER WIDTH Distance(X) : " << eye_FR_width_dis_xyz << endl;
	std::cout << "eye ER WIDTH Distance(X) : " << eye_R_width_dis_xyz << endl;
	std::cout << endl;

	float eye_RL_width_dis_xyz = real_eye_L_right[0] - real_eye_R_left[0];
	std::cout << "eye ER<->EL WIDTH Distance(X) : " << eye_RL_width_dis_xyz << endl;
	std::cout << endl;

	float eye_L_width_dis_xyz = real_eye_L_left[0] - real_eye_L_right[0];
	float eye_FL_width_dis_xyz = real_face_left[0] - real_eye_L_left[0];
	std::cout << "ete EL WIDTH Distance(X) : " << eye_L_width_dis_xyz << endl;
	std::cout << "ete FL<->EL WIDTH Distance(X) : " << eye_FL_width_dis_xyz << endl;
	std::cout << endl;

	for (int i = 0; i < real_mesh_ts.n_vertices(); i++) {
		if (i == 610) {

		}
		if (i == 1355) {

		}
		if (i == 1340) {

		}
		if (i == 704) {

		}
		if (i == 2136) {

		}
		if (i == 2772) {

		}
		if (i == 2787) {

		}
		if (i == 2681) {

		}
	}
	std::cout << endl;
	std::cout << "--------------- getDistanceObj fucntion END ---------------" << endl;
	std::cout << endl;
}
//

//
unordered_map<string, float> compareRealnStandardReal(MyMesh real_mesh, MyMesh std_mesh, MyMesh target_mesh) {
	std::cout << endl;
	std::cout << "--------------- compareRealnStandardReal fucntion START ---------------" << endl;
	std::cout << endl;

	// face node Y (IDX 0)
	const static int face_node_top_idx = 610;
	const static int face_node_bottom_idx = 2411;

	// real mesh vertex 조회
	MyMesh::VertexIter real_t_vIt;
	OpenMesh::Vec3f real_t_v;

	std::vector<Eigen::Vector3f> t_real;
	t_real.resize(real_mesh.n_vertices());

	float *real_x_pos = new float[real_mesh.n_vertices()];
	float *real_y_pos = new float[real_mesh.n_vertices()];
	float *real_z_pos = new float[real_mesh.n_vertices()];

	float real_face_top_X = 0.0f;
	float real_face_top_Y = 0.0f;
	float real_face_top_Z = 0.0f;

	float real_face_bottom_X = 0.0f;
	float real_face_bottom_Y = 0.0f;
	float real_face_bottom_Z = 0.0f;

	for (real_t_vIt = real_mesh.vertices_begin(); real_t_vIt != real_mesh.vertices_end(); ++real_t_vIt)
	{
		real_t_v = real_mesh.point(real_t_vIt.handle());
		t_real[real_t_vIt->idx()] = Eigen::Vector3f(real_t_v[0], real_t_v[1], real_t_v[2]);

		// SRC X,Y POS 값 저장
		real_x_pos[real_t_vIt->idx()] = real_t_v[0];
		real_y_pos[real_t_vIt->idx()] = real_t_v[1];
		real_z_pos[real_t_vIt->idx()] = real_t_v[2];

		if (real_t_vIt->idx() == face_node_top_idx) {
			real_face_top_X = real_t_v[0];
			real_face_top_Y = real_t_v[1];
			real_face_top_Z = real_t_v[2];
		}
		if (real_t_vIt->idx() == face_node_bottom_idx) {
			real_face_bottom_X = real_t_v[0];
			real_face_bottom_Y = real_t_v[1];
			real_face_bottom_Z = real_t_v[2];
		}
	}
	//

	// standard real mesh vertex 조회
	MyMesh::VertexIter std_t_vIt;
	OpenMesh::Vec3f std_t_v;

	std::vector<Eigen::Vector3f> t_std;
	t_std.resize(std_mesh.n_vertices());

	float *std_x_pos = new float[std_mesh.n_vertices()];
	float *std_y_pos = new float[std_mesh.n_vertices()];
	float *std_z_pos = new float[std_mesh.n_vertices()];

	float std_face_top_X = 0.0f;
	float std_face_top_Y = 0.0f;
	float std_face_top_Z = 0.0f;

	float std_face_bottom_X = 0.0f;
	float std_face_bottom_Y = 0.0f;
	float std_face_bottom_Z = 0.0f;

	for (std_t_vIt = std_mesh.vertices_begin(); std_t_vIt != std_mesh.vertices_end(); ++std_t_vIt)
	{
		//std::cout << tg_t_vIt.handle().idx() << endl;

		std_t_v = std_mesh.point(std_t_vIt.handle());
		t_std[std_t_vIt->idx()] = Eigen::Vector3f(std_t_v[0], std_t_v[1], std_t_v[2]);

		// standard X,Y POS 값 저장
		std_x_pos[std_t_vIt->idx()] = std_t_v[0];
		std_y_pos[std_t_vIt->idx()] = std_t_v[1];
		std_z_pos[std_t_vIt->idx()] = std_t_v[2];

		if (std_t_vIt->idx() == face_node_top_idx) {
			std_face_top_X = std_t_v[0];
			std_face_top_Y = std_t_v[1];
			std_face_top_Z = std_t_v[2];
		}
		if (std_t_vIt->idx() == face_node_bottom_idx) {
			std_face_bottom_X = std_t_v[0];
			std_face_bottom_Y = std_t_v[1];
			std_face_bottom_Z = std_t_v[2];
		}
	}
	//

	// scaled mesh vertex 조회
	MyMesh::VertexIter tg_t_vIt;
	OpenMesh::Vec3f tg_t_v;

	std::vector<Eigen::Vector3f> t_tg;
	t_tg.resize(target_mesh.n_vertices());

	float *tg_x_pos = new float[target_mesh.n_vertices()];
	float *tg_y_pos = new float[target_mesh.n_vertices()];
	float *tg_z_pos = new float[target_mesh.n_vertices()];

	float tg_face_top_X = 0.0f;
	float tg_face_top_Y = 0.0f;
	float tg_face_top_Z = 0.0f;

	float tg_face_bottom_X = 0.0f;
	float tg_face_bottom_Y = 0.0f;
	float tg_face_bottom_Z = 0.0f;

	for (tg_t_vIt = target_mesh.vertices_begin(); tg_t_vIt != target_mesh.vertices_end(); ++tg_t_vIt)
	{
		//std::cout << tg_t_vIt.handle().idx() << endl;

		tg_t_v = target_mesh.point(tg_t_vIt.handle());
		t_tg[tg_t_vIt->idx()] = Eigen::Vector3f(tg_t_v[0], tg_t_v[1], tg_t_v[2]);

		// TARGET X,Y POS 값 저장
		tg_x_pos[tg_t_vIt->idx()] = tg_t_v[0];
		tg_y_pos[tg_t_vIt->idx()] = tg_t_v[1];
		tg_z_pos[tg_t_vIt->idx()] = tg_t_v[2];

		if (tg_t_vIt->idx() == face_node_top_idx) {
			tg_face_top_X = tg_t_v[0];
			tg_face_top_Y = tg_t_v[1];
			tg_face_top_Z = tg_t_v[2];
		}
		if (tg_t_vIt->idx() == face_node_bottom_idx) {
			tg_face_bottom_X = tg_t_v[0];
			tg_face_bottom_Y = tg_t_v[1];
			tg_face_bottom_Z = tg_t_v[2];
		}
	}
	//
	std::cout << endl;

	// calc face height each models
	float real_face_node_dis = calcDistance(real_face_top_X, real_face_top_Y, real_face_top_X, real_face_bottom_Y);
	float std_face_node_dis = calcDistance(std_face_top_X, std_face_top_Y, std_face_top_X, std_face_bottom_Y);
	float tg_face_node_dis = calcDistance(tg_face_top_X, tg_face_top_Y, tg_face_top_X, tg_face_bottom_Y);

	std::cout << "\t REAL FACE NODE DEISTANCE (VERTEX IDX " << face_node_top_idx << " ~ " << face_node_bottom_idx << ") (A) : " << real_face_node_dis << endl;
	std::cout << "\t STANDARD FACE NODE DEISTANCE (VERTEX IDX " << face_node_top_idx << " ~ " << face_node_bottom_idx << ") (B) : " << std_face_node_dis << endl;
	std::cout << "\t TARGET(BEAUTY) FACE NODE DEISTANCE (VERTEX IDX " << face_node_top_idx << " ~ " << face_node_bottom_idx << ") (C) : " << tg_face_node_dis << endl;

	// eye_R landmark
	const static int eye_landmark_size = 12;
	const static int eye_R_landmark_idx[eye_landmark_size] =
	{ 1355, 1358, 1361, 1364, 1366, 1368, 1340, 445, 1044, 439, 802, 432 };

	// eye_L landmark
	const static int eye_L_landmark_idx[eye_landmark_size] =
	{ 2787, 2790, 2792, 2795, 2798, 2800, 2772, 1877, 2476, 1871, 2234, 1864 };

	// nose landmark
	const static int nose_landmark_size = 19;
	const static int nose_landmark_idx[nose_landmark_size] =
	{ 704, 553, 1037, 1013, 1001, 1004, 480, 913, 462, 62, 2136, 1985, 2469, 2448, 2438, 2436, 1912, 2344, 2944 };

	// lips landmark
	const static int lips_landmark_size = 38;
	const static int lips_landmark_idx[lips_landmark_size] =
	{ 1425, 1445, 1442, 1440, 1438, 1437, 79, 2869, 2870, 2872, 2874, 2877, 2834, 2885, 2887, 1716, 1714, 1796,
		21, 364, 282, 284, 1455, 1453, 1397, 1394, 1391, 0, 2823, 2826, 2829, 2828, 2841, 2843, 1, 1411, 1409, 1406 };

	const static int lips_landmark_out_size = 20;
	const static int lips_landmark_in_size = 18;
	const static int lips_out_landmark_idx[lips_landmark_out_size] =
	{ 1425, 1445, 1442, 1440, 1438, 1437, 79, 2869, 2870, 2872, 2874, 2877, 2834, 1397, 1394, 1391, 0, 2823, 2826, 2829 };

	const static int lips_in_landmark_idx[lips_landmark_in_size] =
	{ 2885, 2887, 1716, 1714, 1796, 21, 364, 282, 284, 1455, 1453, 2828, 2841, 2843, 1, 1411, 1409, 1406 };

	float real_eye_R_node_X = calcCentralX(eye_R_landmark_idx, real_x_pos, real_y_pos, eye_landmark_size);
	float real_eye_R_node_Y = calcCentralY(eye_R_landmark_idx, real_x_pos, real_y_pos, eye_landmark_size);

	float real_eye_L_node_X = calcCentralX(eye_L_landmark_idx, real_x_pos, real_y_pos, eye_landmark_size);
	float real_eye_L_node_Y = calcCentralY(eye_L_landmark_idx, real_x_pos, real_y_pos, eye_landmark_size);

	float real_nose_node_X = calcCentralX(nose_landmark_idx, real_x_pos, real_y_pos, nose_landmark_size);
	float real_nose_node_Y = calcCentralY(nose_landmark_idx, real_x_pos, real_y_pos, nose_landmark_size);

	float real_lips_out_node_X = calcCentralX(lips_out_landmark_idx, real_x_pos, real_y_pos, lips_landmark_out_size);
	float real_lips_out_node_Y = calcCentralY(lips_out_landmark_idx, real_x_pos, real_y_pos, lips_landmark_out_size);

	float real_lips_in_node_X = calcCentralX(lips_in_landmark_idx, real_x_pos, real_y_pos, lips_landmark_in_size);
	float real_lips_in_node_Y = calcCentralY(lips_in_landmark_idx, real_x_pos, real_y_pos, lips_landmark_in_size);

	std::cout << endl;
	std::cout << "\t REAL EYE R NODE " << " Y : " << real_eye_R_node_Y << endl;
	std::cout << "\t REAL EYE L NODE " << " Y : " << real_eye_L_node_Y << endl;
	std::cout << "\t REAL NOSE NODE : " << " Y : " << real_nose_node_Y << endl;
	std::cout << "\t REAL LIPS TOP NODE " << " Y : " << real_lips_out_node_Y << endl;
	std::cout << "\t REAL LIPS BOTTOM NODE " << " Y : " << real_lips_in_node_Y << endl;

	std::cout << endl;
	std::cout << "\t STD EYE R NODE " << " Y : " << calcCentralY(eye_R_landmark_idx, std_x_pos, std_y_pos, eye_landmark_size) << endl;
	std::cout << "\t STD EYE L NODE " << " Y : " << calcCentralY(eye_L_landmark_idx, std_x_pos, std_y_pos, eye_landmark_size) << endl;
	std::cout << "\t STD NOSE NODE : " << " Y : " << calcCentralY(nose_landmark_idx, std_x_pos, std_y_pos, nose_landmark_size) << endl;
	std::cout << "\t STD LIPS TOP NODE " << " Y : " << calcCentralY(lips_out_landmark_idx, std_x_pos, std_y_pos, lips_landmark_out_size) << endl;
	std::cout << "\t STD LIPS BOTTOM NODE " << " Y : " << calcCentralY(lips_in_landmark_idx, std_x_pos, std_y_pos, lips_landmark_in_size) << endl;
	/*
	float central_gap_eye_R_Y = calcGap(real_eye_R_node_Y, calcCentralY(eye_R_landmark_idx, std_x_pos, std_y_pos, eye_landmark_size));
	float central_gap_eye_L_Y = calcGap(real_eye_L_node_Y, calcCentralY(eye_L_landmark_idx, std_x_pos, std_y_pos, eye_landmark_size));
	float central_gap_nose_Y = calcGap(real_nose_node_Y, calcCentralY(nose_landmark_idx, std_x_pos, std_y_pos, nose_landmark_size));
	float central_gap_lips_out_Y = calcGap(real_lips_out_node_Y, calcCentralY(lips_out_landmark_idx, std_x_pos, std_y_pos, lips_landmark_out_size));
	float central_gap_lips_in_Y = calcGap(real_lips_in_node_Y, calcCentralY(lips_in_landmark_idx, std_x_pos, std_y_pos, lips_landmark_in_size));
	*/
	float central_gap_eye_R_Y = calcGap(calcCentralY(eye_R_landmark_idx, std_x_pos, std_y_pos, eye_landmark_size), real_eye_R_node_Y);
	float central_gap_eye_L_Y = calcGap(calcCentralY(eye_L_landmark_idx, std_x_pos, std_y_pos, eye_landmark_size), real_eye_L_node_Y);
	float central_gap_nose_Y = calcGap(calcCentralY(nose_landmark_idx, std_x_pos, std_y_pos, nose_landmark_size), real_nose_node_Y);
	float central_gap_lips_out_Y = calcGap(calcCentralY(lips_out_landmark_idx, std_x_pos, std_y_pos, lips_landmark_out_size), real_lips_out_node_Y);
	float central_gap_lips_in_Y = calcGap(calcCentralY(lips_in_landmark_idx, std_x_pos, std_y_pos, lips_landmark_in_size), real_lips_in_node_Y);

	std::cout << endl;
	std::cout << "\t GAP EYE R NODE : " << central_gap_eye_R_Y << endl;
	std::cout << "\t GAP EYE L NODE : " << central_gap_eye_L_Y << endl;
	std::cout << "\t GAP NOSE NODE : " << central_gap_nose_Y << endl;
	std::cout << "\t GAP LIPS TOP NODE : " << central_gap_lips_out_Y << endl;
	std::cout << "\t GAP LIPS BOTTOM NODE : " << central_gap_lips_in_Y << endl;


	//
	float std_eye_R_node_X = calcCentralX(eye_R_landmark_idx, std_x_pos, std_y_pos, eye_landmark_size);
	float std_eye_R_node_Y = calcCentralY(eye_R_landmark_idx, std_x_pos, std_y_pos, eye_landmark_size);

	float std_eye_L_node_X = calcCentralX(eye_L_landmark_idx, std_x_pos, std_y_pos, eye_landmark_size);
	float std_eye_L_node_Y = calcCentralY(eye_L_landmark_idx, std_x_pos, std_y_pos, eye_landmark_size);

	float std_nose_node_X = calcCentralX(nose_landmark_idx, std_x_pos, std_y_pos, nose_landmark_size);
	float std_nose_node_Y = calcCentralY(nose_landmark_idx, std_x_pos, std_y_pos, nose_landmark_size);

	float std_lips_out_node_X = calcCentralX(lips_out_landmark_idx, std_x_pos, std_y_pos, lips_landmark_out_size);
	float std_lips_out_node_Y = calcCentralY(lips_out_landmark_idx, std_x_pos, std_y_pos, lips_landmark_out_size);

	float std_lips_in_node_X = calcCentralX(lips_in_landmark_idx, std_x_pos, std_y_pos, lips_landmark_in_size);
	float std_lips_in_node_Y = calcCentralY(lips_in_landmark_idx, std_x_pos, std_y_pos, lips_landmark_in_size);


	//
	float tg_eye_R_node_X = calcCentralX(eye_R_landmark_idx, tg_x_pos, tg_y_pos, eye_landmark_size);
	float tg_eye_R_node_Y = calcCentralY(eye_R_landmark_idx, tg_x_pos, tg_y_pos, eye_landmark_size);

	float tg_eye_L_node_X = calcCentralX(eye_L_landmark_idx, tg_x_pos, tg_y_pos, eye_landmark_size);
	float tg_eye_L_node_Y = calcCentralY(eye_L_landmark_idx, tg_x_pos, tg_y_pos, eye_landmark_size);

	float tg_nose_node_X = calcCentralX(nose_landmark_idx, tg_x_pos, tg_y_pos, nose_landmark_size);
	float tg_nose_node_Y = calcCentralY(nose_landmark_idx, tg_x_pos, tg_y_pos, nose_landmark_size);

	float tg_lips_out_node_X = calcCentralX(lips_out_landmark_idx, tg_x_pos, tg_y_pos, lips_landmark_out_size);
	float tg_lips_out_node_Y = calcCentralY(lips_out_landmark_idx, tg_x_pos, tg_y_pos, lips_landmark_out_size);

	float tg_lips_in_node_X = calcCentralX(lips_in_landmark_idx, tg_x_pos, tg_y_pos, lips_landmark_in_size);
	float tg_lips_in_node_Y = calcCentralY(lips_in_landmark_idx, tg_x_pos, tg_y_pos, lips_landmark_in_size);

	/*
	float eye_R_rate = calcDistance(tg_eye_R_node_X, tg_eye_R_node_Y, tg_eye_R_node_X, tg_face_bottom_Y) / calcDistance(real_eye_R_node_X, real_eye_R_node_Y, real_eye_R_node_X, real_face_bottom_Y);
	float eye_L_rate = calcDistance(tg_eye_L_node_X, tg_eye_L_node_Y, tg_eye_L_node_X, tg_face_bottom_Y) / calcDistance(real_eye_L_node_X, real_eye_L_node_Y, real_eye_L_node_X, real_face_bottom_Y);
	float nose_rate = calcDistance(tg_nose_node_X, tg_nose_node_Y, tg_nose_node_X, tg_face_bottom_Y) / calcDistance(real_nose_node_X, real_nose_node_Y, real_nose_node_X, real_face_bottom_Y);
	float lips_out_rate = calcDistance(tg_lips_out_node_X, tg_lips_out_node_Y, tg_lips_out_node_X, tg_face_bottom_Y) / calcDistance(real_lips_out_node_X, real_lips_out_node_Y, real_lips_out_node_X, real_face_bottom_Y);
	float lips_in_rate = calcDistance(tg_lips_in_node_X, tg_lips_in_node_Y, tg_lips_in_node_X, tg_face_bottom_Y) / calcDistance(real_lips_in_node_X, real_lips_in_node_Y, real_lips_in_node_X, real_face_bottom_Y);
	*/
	/*
	float eye_R_rate = central_gap_eye_R_Y / calcDistance(real_eye_R_node_X, real_eye_R_node_Y, real_eye_R_node_X, real_face_bottom_Y);
	float eye_L_rate = central_gap_eye_L_Y / calcDistance(real_eye_L_node_X, real_eye_L_node_Y, real_eye_L_node_X, real_face_bottom_Y);
	float nose_rate = central_gap_nose_Y / calcDistance(real_nose_node_X, real_nose_node_Y, real_nose_node_X, real_face_bottom_Y);
	float lips_out_rate = central_gap_lips_out_Y / calcDistance(real_lips_out_node_X, real_lips_out_node_Y, real_lips_out_node_X, real_face_bottom_Y);
	float lips_in_rate = central_gap_lips_in_Y / calcDistance(real_lips_in_node_X, real_lips_in_node_Y, real_lips_in_node_X, real_face_bottom_Y);
	*/

	float eye_R_rate = calcDistance(tg_eye_R_node_X, tg_eye_R_node_Y, tg_eye_R_node_X, tg_face_bottom_Y) / calcDistance(std_eye_R_node_X, std_eye_R_node_Y, std_eye_R_node_X, std_face_bottom_Y);
	float eye_L_rate = calcDistance(tg_eye_L_node_X, tg_eye_L_node_Y, tg_eye_L_node_X, tg_face_bottom_Y) / calcDistance(std_eye_L_node_X, std_eye_L_node_Y, std_eye_L_node_X, std_face_bottom_Y);
	float nose_rate = calcDistance(tg_nose_node_X, tg_nose_node_Y, tg_nose_node_X, tg_face_bottom_Y) / calcDistance(std_nose_node_X, std_nose_node_Y, std_nose_node_X, std_face_bottom_Y);
	float lips_out_rate = calcDistance(tg_lips_out_node_X, tg_lips_out_node_Y, tg_lips_out_node_X, tg_face_bottom_Y) / calcDistance(std_lips_out_node_X, std_lips_out_node_Y, std_lips_out_node_X, std_face_bottom_Y);
	float lips_in_rate = calcDistance(tg_lips_in_node_X, tg_lips_in_node_Y, tg_lips_in_node_X, tg_face_bottom_Y) / calcDistance(std_lips_in_node_X, std_lips_in_node_Y, std_lips_in_node_X, std_face_bottom_Y);

	float rate = tg_face_node_dis / std_face_node_dis;

	float res_eye_R = central_gap_eye_R_Y * eye_R_rate;
	float res_eye_L = central_gap_eye_L_Y * eye_L_rate;
	float res_nose = central_gap_nose_Y * nose_rate;
	float res_lips_out = central_gap_lips_out_Y * lips_out_rate;
	float res_lips_in = central_gap_lips_in_Y * lips_in_rate;

	unordered_map<string, float> res_set;
	res_set.clear();

	res_set.insert(unordered_map<string, float>::value_type("eye_R_Y", res_eye_R));
	res_set.insert(unordered_map<string, float>::value_type("eye_L_Y", res_eye_L));
	res_set.insert(unordered_map<string, float>::value_type("nose_Y", res_nose));
	res_set.insert(unordered_map<string, float>::value_type("lips_top_Y", res_lips_out));
	res_set.insert(unordered_map<string, float>::value_type("lips_bottom_Y", res_lips_in));

	std::cout << endl;
	std::cout << "\t RES EYE R NODE : " << res_eye_R << endl;
	std::cout << "\t RES EYE L NODE : " << res_eye_L << endl;
	std::cout << "\t RES NOSE NODE : " << res_nose << endl;
	std::cout << "\t RES LIPS TOP NODE : " << res_lips_out << endl;
	std::cout << "\t RES LIPS BOTTOM NODE : " << res_lips_in << endl;

	std::cout << endl;
	std::cout << "--------------- compareRealnStandardReal fucntion END ---------------" << endl;
	std::cout << endl;

	return res_set;
}
//

//
unordered_map<string, float> compareRealnStandardReal_v3_1003(MyMesh real_mesh, MyMesh std_mesh, MyMesh target_mesh) {
	std::cout << endl;
	std::cout << "--------------- compareRealnStandardReal fucntion START ---------------" << endl;
	std::cout << endl;

	// face node Y (IDX 0)
	const static int face_node_top_idx = 610;
	const static int face_node_bottom_idx = 2411;

	// real mesh vertex 조회
	MyMesh::VertexIter real_t_vIt;
	OpenMesh::Vec3f real_t_v;

	std::vector<Eigen::Vector3f> t_real;
	t_real.resize(real_mesh.n_vertices());

	float *real_x_pos = new float[real_mesh.n_vertices()];
	float *real_y_pos = new float[real_mesh.n_vertices()];

	float real_face_top_X = 0.0f;
	float real_face_top_Y = 0.0f;

	float real_face_bottom_X = 0.0f;
	float real_face_bottom_Y = 0.0f;

	for (real_t_vIt = real_mesh.vertices_begin(); real_t_vIt != real_mesh.vertices_end(); ++real_t_vIt)
	{
		real_t_v = real_mesh.point(real_t_vIt.handle());
		t_real[real_t_vIt->idx()] = Eigen::Vector3f(real_t_v[0], real_t_v[1], real_t_v[2]);

		// SRC X,Y POS 값 저장
		real_x_pos[real_t_vIt->idx()] = real_t_v[0];
		real_y_pos[real_t_vIt->idx()] = real_t_v[1];

		if (real_t_vIt->idx() == face_node_top_idx) {
			real_face_top_X = real_t_v[0];
			real_face_top_Y = real_t_v[1];
		}
		if (real_t_vIt->idx() == face_node_bottom_idx) {
			real_face_bottom_X = real_t_v[0];
			real_face_bottom_Y = real_t_v[1];
		}
	}
	//

	// standard real mesh vertex 조회
	MyMesh::VertexIter std_t_vIt;
	OpenMesh::Vec3f std_t_v;

	std::vector<Eigen::Vector3f> t_std;
	t_std.resize(std_mesh.n_vertices());

	float *std_x_pos = new float[std_mesh.n_vertices()];
	float *std_y_pos = new float[std_mesh.n_vertices()];
	float std_face_top_X = 0.0f;
	float std_face_top_Y = 0.0f;

	float std_face_bottom_X = 0.0f;
	float std_face_bottom_Y = 0.0f;

	for (std_t_vIt = std_mesh.vertices_begin(); std_t_vIt != std_mesh.vertices_end(); ++std_t_vIt)
	{
		//std::cout << tg_t_vIt.handle().idx() << endl;

		std_t_v = std_mesh.point(std_t_vIt.handle());
		t_std[std_t_vIt->idx()] = Eigen::Vector3f(std_t_v[0], std_t_v[1], std_t_v[2]);

		// standard X,Y POS 값 저장
		std_x_pos[std_t_vIt->idx()] = std_t_v[0];
		std_y_pos[std_t_vIt->idx()] = std_t_v[1];

		if (std_t_vIt->idx() == face_node_top_idx) {
			std_face_top_X = std_t_v[0];
			std_face_top_Y = std_t_v[1];
		}
		if (std_t_vIt->idx() == face_node_bottom_idx) {
			std_face_bottom_X = std_t_v[0];
			std_face_bottom_Y = std_t_v[1];
		}
	}
	//

	// scaled mesh vertex 조회
	MyMesh::VertexIter tg_t_vIt;
	OpenMesh::Vec3f tg_t_v;

	std::vector<Eigen::Vector3f> t_tg;
	t_tg.resize(target_mesh.n_vertices());

	float *tg_x_pos = new float[target_mesh.n_vertices()];
	float *tg_y_pos = new float[target_mesh.n_vertices()];
	float tg_face_top_X = 0.0f;
	float tg_face_top_Y = 0.0f;

	float tg_face_bottom_X = 0.0f;
	float tg_face_bottom_Y = 0.0f;

	for (tg_t_vIt = target_mesh.vertices_begin(); tg_t_vIt != target_mesh.vertices_end(); ++tg_t_vIt)
	{
		//std::cout << tg_t_vIt.handle().idx() << endl;

		tg_t_v = target_mesh.point(tg_t_vIt.handle());
		t_tg[tg_t_vIt->idx()] = Eigen::Vector3f(tg_t_v[0], tg_t_v[1], tg_t_v[2]);

		// TARGET X,Y POS 값 저장
		tg_x_pos[tg_t_vIt->idx()] = tg_t_v[0];
		tg_y_pos[tg_t_vIt->idx()] = tg_t_v[1];

		if (tg_t_vIt->idx() == face_node_top_idx) {
			tg_face_top_X = tg_t_v[0];
			tg_face_top_Y = tg_t_v[1];
		}
		if (tg_t_vIt->idx() == face_node_bottom_idx) {
			tg_face_bottom_X = tg_t_v[0];
			tg_face_bottom_Y = tg_t_v[1];
		}
	}
	//
	std::cout << endl;

	// calc face height each models
	float real_face_node_dis = calcDistance(real_face_top_X, real_face_top_Y, real_face_bottom_X, real_face_bottom_Y);
	float std_face_node_dis = calcDistance(std_face_top_X, std_face_top_Y, std_face_bottom_X, std_face_bottom_Y);
	float tg_face_node_dis = calcDistance(tg_face_top_X, tg_face_top_Y, tg_face_bottom_X, tg_face_bottom_Y);

	std::cout << "\t REAL FACE NODE DEISTANCE (VERTEX IDX " << face_node_top_idx << " ~ " << face_node_bottom_idx << ") (A) : " << real_face_node_dis << endl;
	std::cout << "\t STANDARD FACE NODE DEISTANCE (VERTEX IDX " << face_node_top_idx << " ~ " << face_node_bottom_idx << ") (B) : " << std_face_node_dis << endl;
	std::cout << "\t TARGET(BEAUTY) FACE NODE DEISTANCE (VERTEX IDX " << face_node_top_idx << " ~ " << face_node_bottom_idx << ") (C) : " << tg_face_node_dis << endl;

	// eye_R landmark
	const static int eye_landmark_size = 12;
	const static int eye_R_landmark_idx[eye_landmark_size] =
	{ 1355, 1358, 1361, 1364, 1366, 1368, 1340, 445, 1044, 439, 802, 432 };

	// eye_L landmark
	const static int eye_L_landmark_idx[eye_landmark_size] =
	{ 2787, 2790, 2792, 2795, 2798, 2800, 2772, 1877, 2476, 1871, 2234, 1864 };

	// nose landmark
	const static int nose_landmark_size = 19;
	const static int nose_landmark_idx[nose_landmark_size] =
	{ 704, 553, 1037, 1013, 1001, 1004, 480, 913, 462, 62, 2136, 1985, 2469, 2448, 2438, 2436, 1912, 2344, 2944 };

	// lips landmark
	const static int lips_landmark_size = 38;
	const static int lips_landmark_idx[lips_landmark_size] =
	{ 1425, 1445, 1442, 1440, 1438, 1437, 79, 2869, 2870, 2872, 2874, 2877, 2834,
		2885, 2887, 1716, 1714, 1796, 21, 364, 282, 284, 1455, 1453, 1397, 1394, 1391,
		0, 2823, 2826, 2829, 2828, 2841, 2843, 1, 1411, 1409, 1406 };

	const static int lips_landmark_out_size = 20;
	const static int lips_landmark_in_size = 18;
	const static int lips_out_landmark_idx[lips_landmark_out_size] =
	{ 1425, 1445, 1442, 1440, 1438, 1437, 79, 2869, 2870, 2872, 2874, 2877, 2834,
		1397, 1394, 1391, 0, 2823, 2826, 2829 };

	const static int lips_in_landmark_idx[lips_landmark_in_size] =
	{ 2885, 2887, 1716, 1714, 1796, 21, 364, 282, 284, 1455, 1453, 2828, 2841, 2843, 1, 1411, 1409, 1406 };

	float real_eye_R_node_X = calcCentralX(eye_R_landmark_idx, real_x_pos, real_y_pos, eye_landmark_size);
	float real_eye_R_node_Y = calcCentralY(eye_R_landmark_idx, real_x_pos, real_y_pos, eye_landmark_size);

	float real_eye_L_node_X = calcCentralX(eye_L_landmark_idx, real_x_pos, real_y_pos, eye_landmark_size);
	float real_eye_L_node_Y = calcCentralY(eye_L_landmark_idx, real_x_pos, real_y_pos, eye_landmark_size);

	float real_nose_node_X = calcCentralX(nose_landmark_idx, real_x_pos, real_y_pos, nose_landmark_size);
	float real_nose_node_Y = calcCentralY(nose_landmark_idx, real_x_pos, real_y_pos, nose_landmark_size);

	float real_lips_out_node_X = calcCentralX(lips_out_landmark_idx, real_x_pos, real_y_pos, lips_landmark_out_size);
	float real_lips_out_node_Y = calcCentralY(lips_out_landmark_idx, real_x_pos, real_y_pos, lips_landmark_out_size);

	float real_lips_in_node_X = calcCentralX(lips_in_landmark_idx, real_x_pos, real_y_pos, lips_landmark_in_size);
	float real_lips_in_node_Y = calcCentralY(lips_in_landmark_idx, real_x_pos, real_y_pos, lips_landmark_in_size);

	std::cout << endl;
	std::cout << "\t REAL EYE R NODE (A1) X : " << real_eye_R_node_X << " Y : " << real_eye_R_node_Y << endl;
	std::cout << "\t REAL EYE L NODE (A2) X : " << real_eye_L_node_X << " Y : " << real_eye_L_node_Y << endl;
	std::cout << "\t REAL NOSE NODE (A3) X : " << real_nose_node_X << " Y : " << real_nose_node_Y << endl;
	std::cout << "\t REAL LIPS TOP NODE (A4) X : " << real_lips_out_node_X << " Y : " << real_lips_out_node_Y << endl;
	std::cout << "\t REAL LIPS BOTTOM NODE (A5) X : " << real_lips_in_node_X << " Y : " << real_lips_in_node_Y << endl;

	float central_gap_eye_R_Y = calcGap(real_eye_R_node_Y, calcCentralY(eye_R_landmark_idx, std_x_pos, std_y_pos, eye_landmark_size));
	float central_gap_eye_L_Y = calcGap(real_eye_L_node_Y, calcCentralY(eye_L_landmark_idx, std_x_pos, std_y_pos, eye_landmark_size));
	float central_gap_nose_Y = calcGap(real_nose_node_Y, calcCentralY(nose_landmark_idx, std_x_pos, std_y_pos, nose_landmark_size));
	float central_gap_lips_out_Y = calcGap(real_lips_out_node_Y, calcCentralY(lips_out_landmark_idx, std_x_pos, std_y_pos, lips_landmark_out_size));
	float central_gap_lips_in_Y = calcGap(real_lips_in_node_Y, calcCentralY(lips_in_landmark_idx, std_x_pos, std_y_pos, lips_landmark_in_size));

	std::cout << endl;
	std::cout << "\t GAP EYE R NODE : " << central_gap_eye_R_Y << endl;
	std::cout << "\t GAP EYE L NODE : " << central_gap_eye_L_Y << endl;
	std::cout << "\t GAP NOSE NODE : " << central_gap_nose_Y << endl;
	std::cout << "\t GAP LIPS TOP NODE : " << central_gap_lips_out_Y << endl;
	std::cout << "\t GAP LIPS BOTTOM NODE : " << central_gap_lips_in_Y << endl;

	float tg_eye_R_node_X = calcCentralX(eye_R_landmark_idx, tg_x_pos, tg_y_pos, eye_landmark_size);
	float tg_eye_R_node_Y = calcCentralY(eye_R_landmark_idx, tg_x_pos, tg_y_pos, eye_landmark_size);

	float tg_eye_L_node_X = calcCentralX(eye_L_landmark_idx, tg_x_pos, tg_y_pos, eye_landmark_size);
	float tg_eye_L_node_Y = calcCentralY(eye_L_landmark_idx, tg_x_pos, tg_y_pos, eye_landmark_size);

	float tg_nose_node_X = calcCentralX(nose_landmark_idx, tg_x_pos, tg_y_pos, nose_landmark_size);
	float tg_nose_node_Y = calcCentralY(nose_landmark_idx, tg_x_pos, tg_y_pos, nose_landmark_size);

	float tg_lips_out_node_X = calcCentralX(lips_out_landmark_idx, tg_x_pos, tg_y_pos, lips_landmark_out_size);
	float tg_lips_out_node_Y = calcCentralY(lips_out_landmark_idx, tg_x_pos, tg_y_pos, lips_landmark_out_size);

	float tg_lips_in_node_X = calcCentralX(lips_in_landmark_idx, tg_x_pos, tg_y_pos, lips_landmark_in_size);
	float tg_lips_in_node_Y = calcCentralY(lips_in_landmark_idx, tg_x_pos, tg_y_pos, lips_landmark_in_size);

	std::cout << endl;
	std::cout << "\t TARGET EYE R NODE (A1) X : " << tg_eye_R_node_X << " Y : " << tg_eye_R_node_Y << endl;
	std::cout << "\t TARGET EYE L NODE (A2) X : " << tg_eye_L_node_X << " Y : " << tg_eye_L_node_Y << endl;
	std::cout << "\t TARGET NOSE NODE (A3) X : " << tg_nose_node_X << " Y : " << tg_nose_node_Y << endl;
	std::cout << "\t TARGET LIPS TOP NODE (A4) X : " << tg_lips_out_node_X << " Y : " << tg_lips_out_node_Y << endl;
	std::cout << "\t TARGET LIPS BOTTOM NODE (A5) X : " << tg_lips_in_node_X << " Y : " << tg_lips_in_node_Y << endl;

	float rate = tg_face_node_dis / real_face_node_dis;

	float res_eye_R = central_gap_eye_R_Y * rate;
	float res_eye_L = central_gap_eye_L_Y * rate;
	float res_nose = central_gap_nose_Y * rate;
	float res_lips_out = central_gap_lips_out_Y * rate;
	float res_lips_in = central_gap_lips_in_Y * rate;

	unordered_map<string, float> res_set;
	res_set.clear();

	res_set.insert(unordered_map<string, float>::value_type("eye_R_Y", res_eye_R));
	res_set.insert(unordered_map<string, float>::value_type("eye_L_Y", res_eye_L));
	res_set.insert(unordered_map<string, float>::value_type("nose_Y", res_nose));
	res_set.insert(unordered_map<string, float>::value_type("lips_top_Y", res_lips_out));
	res_set.insert(unordered_map<string, float>::value_type("lips_bottom_Y", res_lips_in));

	std::cout << endl;
	std::cout << "\t RES EYE R NODE : " << res_eye_R << endl;
	std::cout << "\t RES EYE L NODE : " << res_eye_L << endl;
	std::cout << "\t RES NOSE NODE : " << res_nose << endl;
	std::cout << "\t RES LIPS TOP NODE : " << res_lips_out << endl;
	std::cout << "\t RES LIPS BOTTOM NODE : " << res_lips_in << endl;

	std::cout << endl;
	std::cout << "--------------- compareRealnStandardReal fucntion END ---------------" << endl;
	std::cout << endl;

	return res_set;
}
//

//
unordered_map<string, float> compareRealnStandardReal_V2_1002(MyMesh real_mesh, MyMesh std_mesh, MyMesh target_mesh) {
	std::cout << endl;
	std::cout << "--------------- compareRealnStandardReal fucntion START ---------------" << endl;
	std::cout << endl;

	// face node Y (IDX 0)
	const static int face_node_top_idx = 610;
	const static int face_node_bottom_idx = 2411;

	// real mesh vertex 조회
	MyMesh::VertexIter real_t_vIt;
	OpenMesh::Vec3f real_t_v;

	std::vector<Eigen::Vector3f> t_real;
	t_real.resize(real_mesh.n_vertices());

	float *real_x_pos = new float[real_mesh.n_vertices()];
	float *real_y_pos = new float[real_mesh.n_vertices()];

	float real_face_top_X = 0.0f;
	float real_face_top_Y = 0.0f;

	float real_face_bottom_X = 0.0f;
	float real_face_bottom_Y = 0.0f;

	for (real_t_vIt = real_mesh.vertices_begin(); real_t_vIt != real_mesh.vertices_end(); ++real_t_vIt)
	{
		real_t_v = real_mesh.point(real_t_vIt.handle());
		t_real[real_t_vIt->idx()] = Eigen::Vector3f(real_t_v[0], real_t_v[1], real_t_v[2]);

		// SRC X,Y POS 값 저장
		real_x_pos[real_t_vIt->idx()] = real_t_v[0];
		real_y_pos[real_t_vIt->idx()] = real_t_v[1];

		if (real_t_vIt->idx() == face_node_top_idx) {
			real_face_top_X = real_t_v[0];
			real_face_top_Y = real_t_v[1];
		}
		if (real_t_vIt->idx() == face_node_bottom_idx) {
			real_face_bottom_X = real_t_v[0];
			real_face_bottom_Y = real_t_v[1];
		}
	}
	//

	// standard real mesh vertex 조회
	MyMesh::VertexIter std_t_vIt;
	OpenMesh::Vec3f std_t_v;

	std::vector<Eigen::Vector3f> t_std;
	t_std.resize(std_mesh.n_vertices());

	float *std_x_pos = new float[std_mesh.n_vertices()];
	float *std_y_pos = new float[std_mesh.n_vertices()];
	float std_face_top_X = 0.0f;
	float std_face_top_Y = 0.0f;

	float std_face_bottom_X = 0.0f;
	float std_face_bottom_Y = 0.0f;

	for (std_t_vIt = std_mesh.vertices_begin(); std_t_vIt != std_mesh.vertices_end(); ++std_t_vIt)
	{
		//std::cout << tg_t_vIt.handle().idx() << endl;

		std_t_v = std_mesh.point(std_t_vIt.handle());
		t_std[std_t_vIt->idx()] = Eigen::Vector3f(std_t_v[0], std_t_v[1], std_t_v[2]);

		// standard X,Y POS 값 저장
		std_x_pos[std_t_vIt->idx()] = std_t_v[0];
		std_y_pos[std_t_vIt->idx()] = std_t_v[1];

		if (std_t_vIt->idx() == face_node_top_idx) {
			std_face_top_X = std_t_v[0];
			std_face_top_Y = std_t_v[1];
		}
		if (std_t_vIt->idx() == face_node_bottom_idx) {
			std_face_bottom_X = std_t_v[0];
			std_face_bottom_Y = std_t_v[1];
		}
	}
	//

	// scaled mesh vertex 조회
	MyMesh::VertexIter tg_t_vIt;
	OpenMesh::Vec3f tg_t_v;

	std::vector<Eigen::Vector3f> t_tg;
	t_tg.resize(target_mesh.n_vertices());

	float *tg_x_pos = new float[target_mesh.n_vertices()];
	float *tg_y_pos = new float[target_mesh.n_vertices()];
	float tg_face_top_X = 0.0f;
	float tg_face_top_Y = 0.0f;

	float tg_face_bottom_X = 0.0f;
	float tg_face_bottom_Y = 0.0f;

	for (tg_t_vIt = target_mesh.vertices_begin(); tg_t_vIt != target_mesh.vertices_end(); ++tg_t_vIt)
	{
		//std::cout << tg_t_vIt.handle().idx() << endl;

		tg_t_v = target_mesh.point(tg_t_vIt.handle());
		t_tg[tg_t_vIt->idx()] = Eigen::Vector3f(tg_t_v[0], tg_t_v[1], tg_t_v[2]);

		// TARGET X,Y POS 값 저장
		tg_x_pos[tg_t_vIt->idx()] = tg_t_v[0];
		tg_y_pos[tg_t_vIt->idx()] = tg_t_v[1];

		if (tg_t_vIt->idx() == face_node_top_idx) {
			tg_face_top_X = tg_t_v[0];
			tg_face_top_Y = tg_t_v[1];
		}
		if (tg_t_vIt->idx() == face_node_bottom_idx) {
			tg_face_bottom_X = tg_t_v[0];
			tg_face_bottom_Y = tg_t_v[1];
		}
	}
	//
	std::cout << endl;

	// calc face height each models
	float real_face_node_dis = calcDistance(real_face_top_X, real_face_top_Y, real_face_bottom_X, real_face_bottom_Y);
	float std_face_node_dis = calcDistance(std_face_top_X, std_face_top_Y, std_face_bottom_X, std_face_bottom_Y);
	float tg_face_node_dis = calcDistance(tg_face_top_X, tg_face_top_Y, tg_face_bottom_X, tg_face_bottom_Y);

	//float real_face_node_dis = real_face_top_Y - real_face_bottom_Y;
	//float std_face_node_dis = std_face_top_Y - std_face_bottom_Y;
	//float tg_face_node_dis = tg_face_top_Y - tg_face_bottom_Y;

	// calc real & standard mesh eye node
	// eye_R landmark
	const static int eye_landmark_size = 12;
	const static int eye_R_landmark_idx[eye_landmark_size] =
	{ 1355, 1358, 1361, 1364, 1366, 1368, 1340, 445, 1044, 439, 802, 432 };

	// eye_L landmark
	const static int eye_L_landmark_idx[eye_landmark_size] =
	{ 2787, 2790, 2792, 2795, 2798, 2800, 2772, 1877, 2476, 1871, 2234, 1864 };

	/*
	x1 : 다각형의 x 좌표 중 가장 작은 값
	y1 : 다각형의 y 좌표 중 가장 작은 값
	x2 : 다각형의 x 좌표 중 가장 큰 값
	y2 : 다각형의 y 좌표 중 가장 큰 값
	center.x = x1 + ((x2 - x1) / 2);
	center.y = y1 + ((y2 - y1) / 2);
	*/
	float *max_min_eye_R_X_val = new float[eye_landmark_size];
	float *max_min_eye_R_Y_val = new float[eye_landmark_size];
	float *max_min_eye_L_X_val = new float[eye_landmark_size];
	float *max_min_eye_L_Y_val = new float[eye_landmark_size];

	for (int i = 0; i < eye_landmark_size; i++) {
		max_min_eye_R_X_val[i] = real_x_pos[eye_R_landmark_idx[i]];
		max_min_eye_R_Y_val[i] = real_y_pos[eye_R_landmark_idx[i]];

		max_min_eye_L_X_val[i] = real_x_pos[eye_L_landmark_idx[i]];
		max_min_eye_L_Y_val[i] = real_y_pos[eye_L_landmark_idx[i]];
	}

	float eye_R_max_X = calcMaxVal(max_min_eye_R_X_val, eye_landmark_size);
	float eye_R_max_Y = calcMaxVal(max_min_eye_R_Y_val, eye_landmark_size);
	float eye_R_min_X = calcMinVal(max_min_eye_R_X_val, eye_landmark_size);
	float eye_R_min_Y = calcMinVal(max_min_eye_R_Y_val, eye_landmark_size);

	float eye_L_max_X = calcMaxVal(max_min_eye_L_X_val, eye_landmark_size);
	float eye_L_max_Y = calcMaxVal(max_min_eye_L_Y_val, eye_landmark_size);
	float eye_L_min_X = calcMinVal(max_min_eye_L_X_val, eye_landmark_size);
	float eye_L_min_Y = calcMinVal(max_min_eye_L_Y_val, eye_landmark_size);

	float real_eye_R_node_X = eye_R_min_X + ((eye_R_max_X - eye_R_min_X) / 2);
	float real_eye_R_node_Y = eye_R_min_Y + ((eye_R_max_Y - eye_R_min_Y) / 2);
	float real_eye_L_node_X = eye_L_min_X + ((eye_L_max_X - eye_L_min_X) / 2);
	float real_eye_L_node_Y = eye_L_min_Y + ((eye_L_max_Y - eye_L_min_Y) / 2);

	// calc real & standard mesh nose node
	// nose landmark
	//const static int nose_landmark_size = 26;
	//const static int nose_landmark_idx[nose_landmark_size] =
	//{ 41, 80, 70, 58, 36, 35, 34, 704, 553, 1037, 1013, 1001, 1004, 480, 913, 462, 62, 2136, 1985, 2469, 2448, 2438, 2436, 1912, 2344, 2944 };

	const static int nose_landmark_size = 19;
	const static int nose_landmark_idx[nose_landmark_size] =
	{ 704, 553, 1037, 1013, 1001, 1004, 480, 913, 462, 62, 2136, 1985, 2469, 2448, 2438, 2436, 1912, 2344, 2944 };

	float *max_min_nose_X_val = new float[nose_landmark_size];
	float *max_min_nose_Y_val = new float[nose_landmark_size];

	for (int i = 0; i < nose_landmark_size; i++) {
		max_min_nose_X_val[i] = real_x_pos[nose_landmark_idx[i]];
		max_min_nose_Y_val[i] = real_y_pos[nose_landmark_idx[i]];
	}

	float nose_max_X = calcMaxVal(max_min_nose_X_val, nose_landmark_size);
	float nose_max_Y = calcMaxVal(max_min_nose_Y_val, nose_landmark_size);
	float nose_min_X = calcMinVal(max_min_nose_X_val, nose_landmark_size);
	float nose_min_Y = calcMinVal(max_min_nose_Y_val, nose_landmark_size);

	float real_nose_node_X = nose_min_X + ((nose_max_X - nose_min_X) / 2);
	float real_nose_node_Y = nose_min_Y + ((nose_max_Y - nose_min_Y) / 2);

	// calc real & standard mesh lips node
	// lips landmark
	const static int lips_landmark_size = 38;
	const static int lips_landmark_idx[lips_landmark_size] =
	{ 1425, 1445, 1442, 1440, 1438, 1437, 79, 2869, 2870, 2872, 2874, 2877, 2834,
		2885, 2887, 1716, 1714, 1796, 21, 364, 282, 284, 1455, 1453, 1397, 1394, 1391,
		0, 2823, 2826, 2829, 2828, 2841, 2843, 1, 1411, 1409, 1406 };

	const static int lips_landmark_out_size = 24;
	const static int lips_landmark_in_size = 14;

	const static int lips_out_landmark_idx[lips_landmark_out_size] =
	{ 1425, 1445, 1442, 1440, 1438, 1437, 79, 2869, 2870, 2872, 2874, 2877, 2834,
		2885, 2887, 1716, 1714, 1796, 21, 364, 282, 284, 1455, 1453 };

	const static int lips_in_landmark_idx[lips_landmark_in_size] =
	{ 1397, 1394, 1391, 0, 2823, 2826, 2829, 2828, 2841, 2843, 1, 1411, 1409, 1406 };

	float *max_min_lips_out_X_val = new float[lips_landmark_out_size];
	float *max_min_lips_out_Y_val = new float[lips_landmark_out_size];
	float *max_min_lips_in_X_val = new float[lips_landmark_in_size];
	float *max_min_lips_in_Y_val = new float[lips_landmark_in_size];

	for (int i = 0; i < lips_landmark_out_size; i++) {
		max_min_lips_out_X_val[i] = real_x_pos[lips_out_landmark_idx[i]];
		max_min_lips_out_Y_val[i] = real_y_pos[lips_out_landmark_idx[i]];
	}

	for (int i = 0; i < lips_landmark_in_size; i++) {
		max_min_lips_in_X_val[i] = real_x_pos[lips_in_landmark_idx[i]];
		max_min_lips_in_Y_val[i] = real_y_pos[lips_in_landmark_idx[i]];
	}

	float lips_out_max_X = calcMaxVal(max_min_lips_out_X_val, lips_landmark_out_size);
	float lips_out_max_Y = calcMaxVal(max_min_lips_out_Y_val, lips_landmark_out_size);
	float lips_out_min_X = calcMinVal(max_min_lips_out_X_val, lips_landmark_out_size);
	float lips_out_min_Y = calcMinVal(max_min_lips_out_Y_val, lips_landmark_out_size);

	float lips_in_max_X = calcMaxVal(max_min_lips_in_X_val, lips_landmark_in_size);
	float lips_in_max_Y = calcMaxVal(max_min_lips_in_Y_val, lips_landmark_in_size);
	float lips_in_min_X = calcMinVal(max_min_lips_in_X_val, lips_landmark_in_size);
	float lips_in_min_Y = calcMinVal(max_min_lips_in_Y_val, lips_landmark_in_size);

	float real_lips_out_node_X = lips_out_min_X + ((lips_out_max_X - lips_out_min_X) / 2);
	float real_lips_out_node_Y = lips_out_min_Y + ((lips_out_max_Y - lips_out_min_Y) / 2);

	float real_lips_in_node_X = lips_in_min_X + ((lips_in_max_X - lips_in_min_X) / 2);
	float real_lips_in_node_Y = lips_in_min_Y + ((lips_in_max_Y - lips_in_min_Y) / 2);

	std::cout << "\t REAL FACE NODE DEISTANCE (VERTEX IDX " << face_node_top_idx << " ~ " << face_node_bottom_idx << ") (A) : " << real_face_node_dis << endl;
	std::cout << "\t STANDARD FACE NODE DEISTANCE (VERTEX IDX " << face_node_top_idx << " ~ " << face_node_bottom_idx << ") (B) : " << std_face_node_dis << endl;
	std::cout << "\t TARGET(BEAUTY) FACE NODE DEISTANCE (VERTEX IDX " << face_node_top_idx << " ~ " << face_node_bottom_idx << ") (C) : " << tg_face_node_dis << endl;

	std::cout << endl;
	std::cout << "\t REAL EYE R NODE (A1) X : " << real_eye_R_node_X << " Y : " << real_eye_R_node_Y << endl;
	std::cout << "\t REAL EYE L NODE (A2) X : " << real_eye_L_node_X << " Y : " << real_eye_L_node_Y << endl;
	std::cout << "\t REAL NOSE NODE (A3) X : " << real_nose_node_X << " Y : " << real_nose_node_Y << endl;
	//std::cout << "\t REAL LIPS NODE (A4) X : " << real_lips_node_X << " Y : " << real_lips_node_Y << endl;
	std::cout << "\t REAL LIPS OUT NODE (A4) X : " << real_lips_out_node_X << " Y : " << real_lips_out_node_Y << endl;
	std::cout << "\t REAL LIPS IN NODE (A5) X : " << real_lips_in_node_X << " Y : " << real_lips_in_node_Y << endl;


	float real_eye_R_node = calcDistance(real_eye_R_node_X, real_eye_R_node_Y, real_eye_R_node_X, real_face_bottom_Y);
	float real_eye_L_node = calcDistance(real_eye_L_node_X, real_eye_L_node_Y, real_eye_L_node_X, real_face_bottom_Y);
	float real_nose_node = calcDistance(real_nose_node_X, real_nose_node_Y, real_nose_node_X, real_face_bottom_Y);
	//float real_lips_node = calcDistance(real_lips_node_X, real_lips_node_Y, real_lips_node_X, real_face_bottom_Y);
	float real_lips_out_node = calcDistance(real_lips_out_node_X, real_lips_out_node_Y, real_lips_out_node_X, real_face_bottom_Y);
	float real_lips_in_node = calcDistance(real_lips_in_node_X, real_lips_in_node_Y, real_lips_in_node_X, real_face_bottom_Y);

	std::cout << endl;
	std::cout << "\t REAL EYE R NODE (A1) : " << real_eye_R_node << endl;
	std::cout << "\t REAL EYE L NODE (A2) : " << real_eye_L_node << endl;
	std::cout << "\t REAL NOSE NODE (A3) : " << real_nose_node << endl;
	//std::cout << "\t REAL LIPS NODE (A4) : " << real_lips_node << endl;
	std::cout << "\t REAL LIPS OUT NODE (A4) : " << real_lips_out_node << endl;
	std::cout << "\t REAL LIPS IN NODE (A5) : " << real_lips_in_node << endl;

	float std_eye_R_node = (real_eye_R_node * std_face_node_dis) / real_face_node_dis;
	float std_eye_L_node = (real_eye_L_node * std_face_node_dis) / real_face_node_dis;
	float std_nose_node = (real_nose_node * std_face_node_dis) / real_face_node_dis;
	//float std_lips_node = (real_lips_node * std_face_node_dis) / real_face_node_dis;
	float std_lips_out_node = (real_lips_out_node * std_face_node_dis) / real_face_node_dis;
	float std_lips_in_node = (real_lips_in_node * std_face_node_dis) / real_face_node_dis;

	std::cout << endl;
	std::cout << "\t STD EYE R NODE (B1) : " << std_eye_R_node << endl;
	std::cout << "\t STD EYE L NODE (B2) : " << std_eye_L_node << endl;
	std::cout << "\t STD NOSE NODE (B3) : " << std_nose_node << endl;
	//std::cout << "\t STD LIPS NODE (B4) : " << std_lips_node << endl;
	std::cout << "\t STD LIPS OUT NODE (B4) : " << std_lips_out_node << endl;
	std::cout << "\t STD LIPS IN NODE (B5) : " << std_lips_in_node << endl;

	float tg_eye_R_node = (std_eye_R_node * tg_face_node_dis) / std_face_node_dis;
	float tg_eye_L_node = (std_eye_L_node * tg_face_node_dis) / std_face_node_dis;
	float tg_nose_node = (std_nose_node * tg_face_node_dis) / std_face_node_dis;
	//float tg_lips_node = (std_lips_node * tg_face_node_dis) / std_face_node_dis;
	float tg_lips_out_node = (std_lips_out_node * tg_face_node_dis) / std_face_node_dis;
	float tg_lips_in_node = (std_lips_in_node * tg_face_node_dis) / std_face_node_dis;

	std::cout << endl;
	std::cout << "\t TARGET EYE R NODE (C1) : " << tg_eye_R_node << endl;
	std::cout << "\t TARGET EYE L NODE (C2) : " << tg_eye_L_node << endl;
	std::cout << "\t TARGET NOSE NODE (C3) : " << tg_nose_node << endl;
	//std::cout << "\t TARGET LIPS NODE (C4) : " << tg_lips_node << endl;
	std::cout << "\t TARGET LIPS OUT NODE (C4) : " << tg_lips_out_node << endl;
	std::cout << "\t TARGET LIPS IN NODE (C5) : " << tg_lips_in_node << endl;

	/*
	float res_eye_R = tg_eye_R_node - real_eye_R_node;
	float res_eye_L = tg_eye_L_node - real_eye_L_node;
	float res_nose = tg_nose_node - real_nose_node;
	float res_lips = tg_lips_node - real_lips_node;
	*/

	float tmp_res_eye_R = tg_eye_R_node + tg_face_bottom_Y;
	float tmp_res_eye_L = tg_eye_L_node + tg_face_bottom_Y;
	float tmp_res_nose = tg_nose_node + tg_face_bottom_Y;
	//float tmp_res_lips = tg_lips_node + tg_face_bottom_Y;
	float tmp_res_lips_out = tg_lips_out_node + tg_face_bottom_Y;
	float tmp_res_lips_in = tg_lips_in_node + tg_face_bottom_Y;

	float res_eye_R = tmp_res_eye_R - real_eye_R_node_Y;
	float res_eye_L = tmp_res_eye_L - real_eye_L_node_Y;
	float res_nose = tmp_res_nose - real_nose_node_Y;
	//float res_lips = tmp_res_lips - real_lips_node_Y;
	float res_lips_out = tmp_res_lips_out - real_lips_out_node_Y;
	float res_lips_in = tmp_res_lips_in - real_lips_in_node_Y;

	//real_eye_R_node_X, real_eye_R_node_Y, real_eye_R_node_X, real_face_bottom_Y
	unordered_map<string, float> res_set;
	res_set.clear();

	res_set.insert(unordered_map<string, float>::value_type("eye_R_Y", res_eye_R));
	res_set.insert(unordered_map<string, float>::value_type("eye_L_Y", res_eye_L));
	res_set.insert(unordered_map<string, float>::value_type("nose_Y", res_nose));
	//res_set.insert(unordered_map<string, float>::value_type("lips_Y", res_lips));
	res_set.insert(unordered_map<string, float>::value_type("lips_out_Y", res_lips_out));
	res_set.insert(unordered_map<string, float>::value_type("lips_in_Y", res_lips_in));

	std::cout << endl;
	std::cout << "\t RES EYE R NODE : " << res_eye_R << endl;
	std::cout << "\t RES EYE L NODE : " << res_eye_L << endl;
	std::cout << "\t RES NOSE NODE : " << res_nose << endl;
	//std::cout << "\t RES LIPS NODE : " << res_lips << endl;
	std::cout << "\t RES LIPS OUT NODE : " << res_lips_out << endl;
	std::cout << "\t RES LIPS IN NODE : " << res_lips_in << endl;

	std::cout << endl;
	std::cout << "--------------- compareRealnStandardReal fucntion END ---------------" << endl;
	std::cout << endl;

	return res_set;
}
//

//
unordered_map<string, float> compareRealnStandardReal_v1_1001(MyMesh real_mesh, MyMesh std_mesh, MyMesh target_mesh) {
	std::cout << endl;
	std::cout << "--------------- compareRealnStandardReal fucntion START ---------------" << endl;
	std::cout << endl;

	// face node Y (IDX 0)
	const static int face_node_idx = 610;
	const static int face_node_top_idx = 610;
	const static int face_node_bottom_idx = 2411;

	// real mesh vertex 조회
	MyMesh::VertexIter real_t_vIt;
	OpenMesh::Vec3f real_t_v;

	std::vector<Eigen::Vector3f> t_real;
	t_real.resize(real_mesh.n_vertices());

	float *real_x_pos = new float[real_mesh.n_vertices()];
	float *real_y_pos = new float[real_mesh.n_vertices()];

	float real_face_top_X = 0.0f;
	float real_face_top_Y = 0.0f;

	float real_face_bottom_X = 0.0f;
	float real_face_bottom_Y = 0.0f;

	for (real_t_vIt = real_mesh.vertices_begin(); real_t_vIt != real_mesh.vertices_end(); ++real_t_vIt)
	{
		real_t_v = real_mesh.point(real_t_vIt.handle());
		t_real[real_t_vIt->idx()] = Eigen::Vector3f(real_t_v[0], real_t_v[1], real_t_v[2]);

		// SRC X,Y POS 값 저장
		real_x_pos[real_t_vIt->idx()] = real_t_v[0];
		real_y_pos[real_t_vIt->idx()] = real_t_v[1];

		if (real_t_vIt->idx() == face_node_top_idx) {
			real_face_top_X = real_t_v[0];
			real_face_top_Y = real_t_v[1];
		}
		if (real_t_vIt->idx() == face_node_bottom_idx) {
			real_face_bottom_X = real_t_v[0];
			real_face_bottom_Y = real_t_v[1];
		}
	}
	//

	// standard real mesh vertex 조회
	MyMesh::VertexIter std_t_vIt;
	OpenMesh::Vec3f std_t_v;

	std::vector<Eigen::Vector3f> t_std;
	t_std.resize(std_mesh.n_vertices());

	float *std_x_pos = new float[std_mesh.n_vertices()];
	float *std_y_pos = new float[std_mesh.n_vertices()];
	float std_face_top_X = 0.0f;
	float std_face_top_Y = 0.0f;

	float std_face_bottom_X = 0.0f;
	float std_face_bottom_Y = 0.0f;

	for (std_t_vIt = std_mesh.vertices_begin(); std_t_vIt != std_mesh.vertices_end(); ++std_t_vIt)
	{
		//std::cout << tg_t_vIt.handle().idx() << endl;

		std_t_v = std_mesh.point(std_t_vIt.handle());
		t_std[std_t_vIt->idx()] = Eigen::Vector3f(std_t_v[0], std_t_v[1], std_t_v[2]);

		// standard X,Y POS 값 저장
		std_x_pos[std_t_vIt->idx()] = std_t_v[0];
		std_y_pos[std_t_vIt->idx()] = std_t_v[1];

		if (std_t_vIt->idx() == face_node_top_idx) {
			std_face_top_X = std_t_v[0];
			std_face_top_Y = std_t_v[1];
		}
		if (std_t_vIt->idx() == face_node_bottom_idx) {
			std_face_bottom_X = std_t_v[0];
			std_face_bottom_Y = std_t_v[1];
		}
	}
	//

	// scaled mesh vertex 조회
	MyMesh::VertexIter tg_t_vIt;
	OpenMesh::Vec3f tg_t_v;

	std::vector<Eigen::Vector3f> t_tg;
	t_tg.resize(target_mesh.n_vertices());

	float *tg_x_pos = new float[target_mesh.n_vertices()];
	float *tg_y_pos = new float[target_mesh.n_vertices()];
	float tg_face_top_X = 0.0f;
	float tg_face_top_Y = 0.0f;

	float tg_face_bottom_X = 0.0f;
	float tg_face_bottom_Y = 0.0f;

	for (tg_t_vIt = target_mesh.vertices_begin(); tg_t_vIt != target_mesh.vertices_end(); ++tg_t_vIt)
	{
		//std::cout << tg_t_vIt.handle().idx() << endl;

		tg_t_v = target_mesh.point(tg_t_vIt.handle());
		t_tg[tg_t_vIt->idx()] = Eigen::Vector3f(tg_t_v[0], tg_t_v[1], tg_t_v[2]);

		// TARGET X,Y POS 값 저장
		tg_x_pos[tg_t_vIt->idx()] = tg_t_v[0];
		tg_y_pos[tg_t_vIt->idx()] = tg_t_v[1];

		if (tg_t_vIt->idx() == face_node_top_idx) {
			tg_face_top_X = tg_t_v[0];
			tg_face_top_Y = tg_t_v[1];
		}
		if (tg_t_vIt->idx() == face_node_bottom_idx) {
			tg_face_bottom_X = tg_t_v[0];
			tg_face_bottom_Y = tg_t_v[1];
		}
	}
	//
	std::cout << endl;

	// calc face height each models
	float real_face_node_dis = calcDistance(real_face_top_X, real_face_top_Y, real_face_bottom_X, real_face_bottom_Y);
	float std_face_node_dis = calcDistance(std_face_top_X, std_face_top_Y, std_face_bottom_X, std_face_bottom_Y);
	float tg_face_node_dis = calcDistance(tg_face_top_X, tg_face_top_Y, tg_face_bottom_X, tg_face_bottom_Y);

	//float real_face_node_dis = real_face_top_Y - real_face_bottom_Y;
	//float std_face_node_dis = std_face_top_Y - std_face_bottom_Y;
	//float tg_face_node_dis = tg_face_top_Y - tg_face_bottom_Y;

	// calc real & standard mesh eye node
	// eye_R landmark
	const static int eye_landmark_size = 12;
	const static int eye_R_landmark_idx[eye_landmark_size] =
	{ 1355, 1358, 1361, 1364, 1366, 1368, 1340, 445, 1044, 439, 802, 432 };

	// eye_L landmark
	const static int eye_L_landmark_idx[eye_landmark_size] =
	{ 2787, 2790, 2792, 2795, 2798, 2800, 2772, 1877, 2476, 1871, 2234, 1864 };

	float tmp_real_eye_R_X = 0.0f;
	float tmp_real_eye_R_Y = 0.0f;
	float tmp_real_eye_L_X = 0.0f;
	float tmp_real_eye_L_Y = 0.0f;

	for (int i = 0; i < eye_landmark_size; i++) {
		tmp_real_eye_R_X += real_x_pos[eye_R_landmark_idx[i]];
		tmp_real_eye_R_Y += real_y_pos[eye_R_landmark_idx[i]];

		tmp_real_eye_L_X += real_x_pos[eye_L_landmark_idx[i]];
		tmp_real_eye_L_Y += real_y_pos[eye_L_landmark_idx[i]];
	}

	float real_eye_R_node_X = tmp_real_eye_R_X / eye_landmark_size;
	float real_eye_R_node_Y = tmp_real_eye_R_Y / eye_landmark_size;
	float real_eye_L_node_X = tmp_real_eye_L_X / eye_landmark_size;
	float real_eye_L_node_Y = tmp_real_eye_L_Y / eye_landmark_size;

	// calc real & standard mesh nose node
	// nose landmark
	const static int nose_landmark_size = 26;
	const static int nose_landmark_idx[nose_landmark_size] =
	{ 41, 80, 70, 58, 36, 35, 34, 704, 553, 1037, 1013, 1001, 1004, 480, 913, 462, 62, 2136, 1985, 2469, 2448, 2438, 2436, 1912, 2344, 2944 };

	float tmp_real_nose_X = 0.0f;
	float tmp_real_nose_Y = 0.0f;

	for (int i = 0; i < nose_landmark_size; i++) {
		tmp_real_nose_X += real_x_pos[nose_landmark_idx[i]];
		tmp_real_nose_Y += real_y_pos[nose_landmark_idx[i]];
	}

	float real_nose_node_X = tmp_real_nose_X / nose_landmark_size;
	float real_nose_node_Y = tmp_real_nose_Y / nose_landmark_size;

	// calc real & standard mesh lips node
	// lips landmark
	const static int lips_landmark_size = 38;
	const static int lips_landmark_idx[lips_landmark_size] =
	{ 1425, 1445, 1442, 1440, 1438, 1437, 79, 2869, 2870, 2872, 2874, 2877, 2834,
		2885, 2887, 1716, 1714, 1796, 21, 364, 282, 284, 1455, 1453, 1397, 1394, 1391,
		0, 2823, 2826, 2829, 2828, 2841, 2843, 1, 1411, 1409, 1406 };

	float tmp_real_lips_X = 0.0f;
	float tmp_real_lips_Y = 0.0f;

	for (int i = 0; i < lips_landmark_size; i++) {
		tmp_real_lips_X += real_x_pos[lips_landmark_idx[i]];
		tmp_real_lips_Y += real_y_pos[lips_landmark_idx[i]];
	}

	float real_lips_node_X = tmp_real_lips_X / lips_landmark_size;
	float real_lips_node_Y = tmp_real_lips_Y / lips_landmark_size;

	std::cout << "\t REAL FACE NODE DEISTANCE (VERTEX IDX " << face_node_top_idx << " ~ " << face_node_bottom_idx << ") (A) : " << real_face_node_dis << endl;
	std::cout << "\t STANDARD FACE NODE DEISTANCE (VERTEX IDX " << face_node_top_idx << " ~ " << face_node_bottom_idx << ") (B) : " << std_face_node_dis << endl;
	std::cout << "\t TARGET(BEAUTY) FACE NODE DEISTANCE (VERTEX IDX " << face_node_top_idx << " ~ " << face_node_bottom_idx << ") (C) : " << tg_face_node_dis << endl;

	std::cout << endl;
	std::cout << "\t REAL EYE R NODE (A1) X : " << real_eye_R_node_X << " Y : " << real_eye_R_node_Y << endl;
	std::cout << "\t REAL EYE L NODE (A2) X : " << real_eye_L_node_X << " Y : " << real_eye_L_node_Y << endl;
	std::cout << "\t REAL NOSE NODE (A3) X : " << real_nose_node_X << " Y : " << real_nose_node_Y << endl;
	std::cout << "\t REAL LIPS NODE (A4) X : " << real_lips_node_X << " Y : " << real_lips_node_Y << endl;

	float real_eye_R_node = calcDistance(real_eye_R_node_X, real_eye_R_node_Y, real_eye_R_node_X, real_face_bottom_Y);
	float real_eye_L_node = calcDistance(real_eye_L_node_X, real_eye_L_node_Y, real_eye_L_node_X, real_face_bottom_Y);
	float real_nose_node = calcDistance(real_nose_node_X, real_nose_node_Y, real_nose_node_X, real_face_bottom_Y);
	float real_lips_node = calcDistance(real_lips_node_X, real_lips_node_Y, real_lips_node_X, real_face_bottom_Y);

	std::cout << endl;
	std::cout << "\t REAL EYE R NODE (A1) : " << real_eye_R_node << endl;
	std::cout << "\t REAL EYE L NODE (A2) : " << real_eye_L_node << endl;
	std::cout << "\t REAL NOSE NODE (A3) : " << real_nose_node << endl;
	std::cout << "\t REAL LIPS NODE (A4) : " << real_lips_node << endl;

	float std_eye_R_node = (real_eye_R_node * std_face_node_dis) / real_face_node_dis;
	float std_eye_L_node = (real_eye_L_node * std_face_node_dis) / real_face_node_dis;
	float std_nose_node = (real_nose_node * std_face_node_dis) / real_face_node_dis;
	float std_lips_node = (real_lips_node * std_face_node_dis) / real_face_node_dis;

	std::cout << endl;
	std::cout << "\t STD EYE R NODE (B1) : " << std_eye_R_node << endl;
	std::cout << "\t STD EYE L NODE (B2) : " << std_eye_L_node << endl;
	std::cout << "\t STD NOSE NODE (B3) : " << std_nose_node << endl;
	std::cout << "\t STD LIPS NODE (B4) : " << std_lips_node << endl;

	float tg_eye_R_node = (std_eye_R_node * tg_face_node_dis) / std_face_node_dis;
	float tg_eye_L_node = (std_eye_L_node * tg_face_node_dis) / std_face_node_dis;
	float tg_nose_node = (std_nose_node * tg_face_node_dis) / std_face_node_dis;
	float tg_lips_node = (std_lips_node * tg_face_node_dis) / std_face_node_dis;

	std::cout << endl;
	std::cout << "\t TARGET EYE R NODE (C1) : " << tg_eye_R_node << endl;
	std::cout << "\t TARGET EYE L NODE (C2) : " << tg_eye_L_node << endl;
	std::cout << "\t TARGET NOSE NODE (C3) : " << tg_nose_node << endl;
	std::cout << "\t TARGET LIPS NODE (C4) : " << tg_lips_node << endl;
	/*
	float res_eye_R = tg_eye_R_node - real_eye_R_node;
	float res_eye_L = tg_eye_L_node - real_eye_L_node;
	float res_nose = tg_nose_node - real_nose_node;
	float res_lips = tg_lips_node - real_lips_node;
	*/

	float tmp_res_eye_R = tg_eye_R_node + tg_face_bottom_Y;
	float tmp_res_eye_L = tg_eye_L_node + tg_face_bottom_Y;
	float tmp_res_nose = tg_nose_node + tg_face_bottom_Y;
	float tmp_res_lips = tg_lips_node + tg_face_bottom_Y;

	float res_eye_R = tmp_res_eye_R - real_eye_R_node_Y;
	float res_eye_L = tmp_res_eye_L - real_eye_L_node_Y;
	float res_nose = tmp_res_nose - real_nose_node_Y;
	float res_lips = tmp_res_lips - real_lips_node_Y;

	//real_eye_R_node_X, real_eye_R_node_Y, real_eye_R_node_X, real_face_bottom_Y
	unordered_map<string, float> res_set;
	res_set.clear();

	res_set.insert(unordered_map<string, float>::value_type("eye_R_Y", res_eye_R));
	res_set.insert(unordered_map<string, float>::value_type("eye_L_Y", res_eye_L));
	res_set.insert(unordered_map<string, float>::value_type("nose_Y", res_nose));
	res_set.insert(unordered_map<string, float>::value_type("lips_Y", res_lips));

	std::cout << endl;
	std::cout << "\t RES EYE R NODE : " << res_eye_R << endl;
	std::cout << "\t RES EYE L NODE : " << res_eye_L << endl;
	std::cout << "\t RES NOSE NODE : " << res_nose << endl;
	std::cout << "\t RES LIPS NODE : " << res_lips << endl;

	std::cout << endl;
	std::cout << "--------------- compareRealnStandardReal fucntion END ---------------" << endl;
	std::cout << endl;

	return res_set;
}
//

//
unordered_map<string, float> compareRealnStandardReal_backup(MyMesh real_mesh, MyMesh std_mesh, MyMesh target_mesh) {
	std::cout << endl;
	std::cout << "--------------- compareRealnStandardReal fucntion START ---------------" << endl;
	std::cout << endl;

	// real mesh vertex 조회
	MyMesh::VertexIter real_t_vIt;
	OpenMesh::Vec3f real_t_v;

	std::vector<Eigen::Vector3f> t_real;
	t_real.resize(real_mesh.n_vertices());

	float *real_x_pos = new float[real_mesh.n_vertices()];
	float *real_y_pos = new float[real_mesh.n_vertices()];

	for (real_t_vIt = real_mesh.vertices_begin(); real_t_vIt != real_mesh.vertices_end(); ++real_t_vIt)
	{
		real_t_v = real_mesh.point(real_t_vIt.handle());
		t_real[real_t_vIt->idx()] = Eigen::Vector3f(real_t_v[0], real_t_v[1], real_t_v[2]);

		// SRC X,Y POS 값 저장
		real_x_pos[real_t_vIt->idx()] = real_t_v[0];
		real_y_pos[real_t_vIt->idx()] = real_t_v[1];
	}
	//

	// standard real mesh vertex 조회
	MyMesh::VertexIter std_t_vIt;
	OpenMesh::Vec3f std_t_v;

	std::vector<Eigen::Vector3f> t_std;
	t_std.resize(std_mesh.n_vertices());

	float *std_x_pos = new float[std_mesh.n_vertices()];
	float *std_y_pos = new float[std_mesh.n_vertices()];

	for (std_t_vIt = std_mesh.vertices_begin(); std_t_vIt != std_mesh.vertices_end(); ++std_t_vIt)
	{
		//std::cout << tg_t_vIt.handle().idx() << endl;

		std_t_v = std_mesh.point(std_t_vIt.handle());
		t_std[std_t_vIt->idx()] = Eigen::Vector3f(std_t_v[0], std_t_v[1], std_t_v[2]);

		// TARGET X,Y POS 값 저장
		std_x_pos[std_t_vIt->idx()] = std_t_v[0];
		std_y_pos[std_t_vIt->idx()] = std_t_v[1];
	}
	//

	// scaled mesh vertex 조회
	MyMesh::VertexIter tg_t_vIt;
	OpenMesh::Vec3f tg_t_v;

	std::vector<Eigen::Vector3f> t_tg;
	t_tg.resize(target_mesh.n_vertices());

	float *tg_x_pos = new float[target_mesh.n_vertices()];
	float *tg_y_pos = new float[target_mesh.n_vertices()];

	for (tg_t_vIt = target_mesh.vertices_begin(); tg_t_vIt != target_mesh.vertices_end(); ++tg_t_vIt)
	{
		//std::cout << tg_t_vIt.handle().idx() << endl;

		tg_t_v = target_mesh.point(tg_t_vIt.handle());
		t_tg[tg_t_vIt->idx()] = Eigen::Vector3f(tg_t_v[0], tg_t_v[1], tg_t_v[2]);

		// TARGET X,Y POS 값 저장
		tg_x_pos[tg_t_vIt->idx()] = tg_t_v[0];
		tg_y_pos[tg_t_vIt->idx()] = tg_t_v[1];
	}
	//

	// calc real & standard mesh eye node
	// eye_R landmark
	const static int eye_landmark_size = 12;
	const static int eye_R_landmark_idx[eye_landmark_size] =
	{ 1355, 1358, 1361, 1364, 1366, 1368, 1340, 445, 1044, 439, 802, 432 };

	// eye_L landmark
	const static int eye_L_landmark_idx[eye_landmark_size] =
	{ 2787, 2790, 2792, 2795, 2798, 2800, 2772, 1877, 2476, 1871, 2234, 1864 };

	float tmp_real_eye_R_Y = 0.0f;
	float tmp_real_eye_L_Y = 0.0f;

	float tmp_std_eye_R_Y = 0.0f;
	float tmp_std_eye_L_Y = 0.0f;

	for (int i = 0; i < eye_landmark_size; i++) {
		tmp_real_eye_R_Y += real_y_pos[eye_R_landmark_idx[i]];
		tmp_real_eye_L_Y += real_y_pos[eye_L_landmark_idx[i]];

		tmp_std_eye_R_Y += std_y_pos[eye_R_landmark_idx[i]];
		tmp_std_eye_L_Y += std_y_pos[eye_L_landmark_idx[i]];
	}

	float real_eye_R_node = tmp_real_eye_R_Y / eye_landmark_size;
	float real_eye_L_node = tmp_real_eye_L_Y / eye_landmark_size;

	float std_eye_R_node = tmp_std_eye_R_Y / eye_landmark_size;
	float std_eye_L_node = tmp_std_eye_L_Y / eye_landmark_size;

	// calc real & standard mesh nose node
	// nose landmark
	const static int nose_landmark_size = 26;
	const static int nose_landmark_idx[nose_landmark_size] =
	{ 41, 80, 70, 58, 36, 35, 34, 704, 553, 1037, 1013, 1001, 1004, 480, 913, 462, 62, 2136, 1985, 2469, 2448, 2438, 2436, 1912, 2344, 2944 };

	float tmp_real_nose_Y = 0.0f;
	float tmp_std_nose_Y = 0.0f;

	for (int i = 0; i < nose_landmark_size; i++) {
		tmp_real_nose_Y += real_y_pos[nose_landmark_idx[i]];
		tmp_std_nose_Y += std_y_pos[nose_landmark_idx[i]];
	}

	float real_nose_node = tmp_real_nose_Y / nose_landmark_size;
	float std_nose_node = tmp_std_nose_Y / nose_landmark_size;

	// calc real & standard mesh lips node
	// lips landmark
	const static int lips_landmark_size = 38;
	const static int lips_landmark_idx[lips_landmark_size] =
	{ 1425, 1445, 1442, 1440, 1438, 1437, 79, 2869, 2870, 2872, 2874, 2877, 2834,
		2885, 2887, 1716, 1714, 1796, 21, 364, 282, 284, 1455, 1453, 1397, 1394, 1391,
		0, 2823, 2826, 2829, 2828, 2841, 2843, 1, 1411, 1409, 1406 };

	float tmp_real_lips_Y = 0.0f;
	float tmp_std_lips_Y = 0.0f;

	for (int i = 0; i < lips_landmark_size; i++) {
		tmp_real_lips_Y += real_y_pos[lips_landmark_idx[i]];
		tmp_std_lips_Y += std_y_pos[lips_landmark_idx[i]];
	}

	// face node Y (IDX 0)
	const static int face_node_idx = 0;
	float real_face_node_Y = real_y_pos[face_node_idx];
	float std_face_node_Y = std_y_pos[face_node_idx];
	float target_face_node_Y = tg_y_pos[face_node_idx];

	std::cout << "\t REAL FACE NODE Y(VERTEX IDX " << face_node_idx << ") (A) : " << real_face_node_Y << endl;
	std::cout << "\t STANDARD FACE NODE Y(VERTEX IDX " << face_node_idx << ") (B) : " << std_face_node_Y << endl;
	std::cout << "\t TARGET(BEAUTY) FACE NODE Y(VERTEX IDX " << face_node_idx << ") (C) : " << target_face_node_Y << endl;

	float real_lips_node = tmp_real_lips_Y / lips_landmark_size;
	float std_lips_node = tmp_std_lips_Y / lips_landmark_size;

	std::cout << endl;
	std::cout << "\t REAL EYE R NODE (A1) : " << real_eye_R_node << endl;
	std::cout << "\t REAL EYE L NODE (A2) : " << real_eye_L_node << endl;
	std::cout << "\t REAL NOSE NODE (A3) : " << real_nose_node << endl;
	std::cout << "\t REAL LIPS NODE (A4) : " << real_lips_node << endl;

	/*
	std::cout << endl;
	std::cout << "\t STANDARD EYE R NODE : " << std_eye_R_node << endl;
	std::cout << "\t STANDARD EYE L NODE : " << std_eye_L_node << endl;
	std::cout << "\t STANDARD NOSE NODE : " << std_nose_node << endl;
	std::cout << "\t STANDARD LIPS NODE : " << std_lips_node << endl;
	*/

	/*
	float real_gap_eye_R_Y = real_eye_R_node - real_face_node_Y;
	float real_gap_eye_L_Y = real_eye_L_node - real_face_node_Y;
	float real_gap_nose_Y = real_nose_node - real_face_node_Y;
	float real_gap_lips_Y = real_lips_node - real_face_node_Y;
	*/

	float real_gap_eye_R_Y = real_eye_R_node - std_face_node_Y;
	float real_gap_eye_L_Y = real_eye_L_node - std_face_node_Y;
	float real_gap_nose_Y = real_nose_node - std_face_node_Y;
	float real_gap_lips_Y = real_lips_node - std_face_node_Y;

	float std_gap_eye_R_Y = std_eye_R_node - std_face_node_Y;
	float std_gap_eye_L_Y = std_eye_L_node - std_face_node_Y;
	float std_gap_nose_Y = std_nose_node - std_face_node_Y;
	float std_gap_lips_Y = std_lips_node - std_face_node_Y;

	std::cout << endl;
	std::cout << "\t GAP of REAL with STANDARD EYE R (A1-B) : " << real_gap_eye_R_Y << endl;
	std::cout << "\t GAP of REAL with STANDARD EYE L (A2-B) : " << real_gap_eye_L_Y << endl;
	std::cout << "\t GAP of REAL with STANDARD NOSE (A3-B) : " << real_gap_nose_Y << endl;
	std::cout << "\t GAP of REAL with STANDARD LIPS (A4-B) : " << real_gap_lips_Y << endl;

	/*
	std::cout << endl;
	std::cout << "\t GAP STANDARD EYE R BY RATE (B1) : " << (real_gap_eye_R_Y * std_face_node_Y) / real_face_node_Y << endl;
	std::cout << "\t GAP STANDARD EYE L BY RATE (B2) : " << (real_gap_eye_L_Y * std_face_node_Y) / real_face_node_Y << endl;
	std::cout << "\t GAP STANDARD NOSE BY RATE (B3) : " << (real_gap_nose_Y * std_face_node_Y) / real_face_node_Y << endl;
	std::cout << "\t GAP STANDARD LIPS BY RATE (B4) : " << (real_gap_lips_Y * std_face_node_Y) / real_face_node_Y << endl;
	*/

	std::cout << endl;
	std::cout << "\t POSITION TARGET EYE R BY RATE (B1) : " << (real_gap_eye_R_Y * target_face_node_Y) / std_face_node_Y << endl;
	std::cout << "\t POSITION TARGET EYE L BY RATE (B2) : " << (real_gap_eye_L_Y * target_face_node_Y) / std_face_node_Y << endl;
	std::cout << "\t POSITION TARGET NOSE BY RATE (B3) : " << (real_gap_nose_Y * target_face_node_Y) / std_face_node_Y << endl;
	std::cout << "\t POSITION TARGET LIPS BY RATE (B4) : " << (real_gap_lips_Y * target_face_node_Y) / std_face_node_Y << endl;

	// set result response map data
	float res_eye_R_Y = (real_gap_eye_R_Y * target_face_node_Y) / std_face_node_Y;
	float res_eye_L_Y = (real_gap_eye_L_Y * target_face_node_Y) / std_face_node_Y;
	float res_nose_Y = (real_gap_nose_Y * target_face_node_Y) / std_face_node_Y;
	float res_lips_Y = (real_gap_lips_Y * target_face_node_Y) / std_face_node_Y;

	unordered_map<string, float> res_set;
	res_set.clear();

	res_set.insert(unordered_map<string, float>::value_type("eye_R_Y", res_eye_R_Y));
	res_set.insert(unordered_map<string, float>::value_type("eye_L_Y", res_eye_L_Y));
	res_set.insert(unordered_map<string, float>::value_type("nose_Y", res_nose_Y));
	res_set.insert(unordered_map<string, float>::value_type("lips_Y", res_lips_Y));

	std::cout << endl;
	std::cout << "--------------- compareRealnStandardReal fucntion END ---------------" << endl;
	std::cout << endl;

	return res_set;
}
//

//
unordered_map<string, float> compareMesh(MyMesh src_mesh, MyMesh target_mesh) {
	std::cout << endl;
	std::cout << "--------------- compareMesh fucntion START ---------------" << endl;
	std::cout << endl;
	// src mesh vertex 조회
	MyMesh::VertexIter src_t_vIt;
	OpenMesh::Vec3f src_t_v;

	std::vector<Eigen::Vector3f> t_src;
	t_src.resize(src_mesh.n_vertices());

	// 
	float *src_x_pos = new float[src_mesh.n_vertices()];
	float *src_y_pos = new float[src_mesh.n_vertices()];

	for (src_t_vIt = src_mesh.vertices_begin(); src_t_vIt != src_mesh.vertices_end(); ++src_t_vIt)
	{
		src_t_v = src_mesh.point(src_t_vIt.handle());
		t_src[src_t_vIt->idx()] = Eigen::Vector3f(src_t_v[0], src_t_v[1], src_t_v[2]);

		// SRC X,Y POS 값 저장
		src_x_pos[src_t_vIt->idx()] = src_t_v[0];
		src_y_pos[src_t_vIt->idx()] = src_t_v[1];
	}

	// target mesh vertex 조회
	MyMesh::VertexIter tg_t_vIt;
	OpenMesh::Vec3f tg_t_v;

	std::vector<Eigen::Vector3f> t_tg;
	t_tg.resize(target_mesh.n_vertices());

	// 
	float *target_x_pos = new float[target_mesh.n_vertices()];
	float *target_y_pos = new float[target_mesh.n_vertices()];

	for (tg_t_vIt = target_mesh.vertices_begin(); tg_t_vIt != target_mesh.vertices_end(); ++tg_t_vIt)
	{
		//std::cout << tg_t_vIt.handle().idx() << endl;

		tg_t_v = target_mesh.point(tg_t_vIt.handle());
		t_src[tg_t_vIt->idx()] = Eigen::Vector3f(tg_t_v[0], tg_t_v[1], tg_t_v[2]);

		// TARGET X,Y POS 값 저장
		target_x_pos[tg_t_vIt->idx()] = tg_t_v[0];
		target_y_pos[tg_t_vIt->idx()] = tg_t_v[1];
	}

	// face, eye, nose, lips mesh index info size 
	const static int face_R_landmark_size = 16;
	const static int face_L_landmark_size = 16;
	const static int face_C_landmark_size = 1;
	const static int face_dis_size = 16;

	// face landmark index info
	const static int face_R_landmark_idx[face_R_landmark_size] =
	{ 610, 1305, 571, 569, 860, 861, 859, 858, 857, 856, 855, 854, 853, 852, 966, 979 };

	const static int face_L_landmark_idx[face_L_landmark_size] =
	{ 2681, 2042, 2737, 2003, 2001, 2292, 2293, 2291, 2290, 2289, 2288, 2287, 2286, 2285, 2284, 2398 };

	const static int face_C_landmark_idx[face_C_landmark_size] =
	{ 2411 };

	const int area = 3;
	float *src_dis = new float[area]; // 0 : eye 1: nose 2: lips
	float *target_dis = new float[area]; // 0 : eye 1: nose 2: lips

	float *src_tmp_dis = new float[face_dis_size];
	float *target_tmp_dis = new float[face_dis_size];

	for (int area_idx = 0; area_idx < area; area_idx++) {
		for (int lm_f_idx = 0; lm_f_idx < face_dis_size; lm_f_idx++) {
			float src_Rx = src_x_pos[face_R_landmark_idx[lm_f_idx]];
			float src_Ry = src_y_pos[face_R_landmark_idx[lm_f_idx]];
			float src_Lx = src_x_pos[face_L_landmark_idx[lm_f_idx]];
			float src_Ly = src_y_pos[face_L_landmark_idx[lm_f_idx]];

			float target_Rx = target_x_pos[face_R_landmark_idx[lm_f_idx]];
			float target_Ry = target_y_pos[face_R_landmark_idx[lm_f_idx]];
			float target_Lx = target_x_pos[face_L_landmark_idx[lm_f_idx]];
			float target_Ly = target_y_pos[face_L_landmark_idx[lm_f_idx]];

			src_tmp_dis[lm_f_idx] = calcDistance(src_Rx, src_Ry, src_Lx, src_Ly);
			target_tmp_dis[lm_f_idx] = calcDistance(target_Rx, target_Ry, target_Lx, target_Ly);
		}
	}

	// 영역별 평균 길이 저장
	for (int area_idx = 0; area_idx < area; area_idx++) {
		if (area_idx == 0) {
			src_dis[area_idx] = (src_tmp_dis[0] + src_tmp_dis[1]) / 2;
			target_dis[area_idx] = (target_tmp_dis[0] + target_tmp_dis[1]) / 2;
		}
		else if (area_idx == 1) {
			src_dis[area_idx] =
				(src_tmp_dis[0] + src_tmp_dis[1] + src_tmp_dis[2] + src_tmp_dis[3] + src_tmp_dis[4] + src_tmp_dis[5] + src_tmp_dis[6]) / 7;
			target_dis[area_idx] =
				(target_tmp_dis[0] + target_tmp_dis[1] + target_tmp_dis[2] + target_tmp_dis[3] + target_tmp_dis[4] + target_tmp_dis[5] + target_tmp_dis[6]) / 7;
		}
		else if (area_idx == 2) {
			src_dis[area_idx] =
				(src_tmp_dis[6] + src_tmp_dis[7] + src_tmp_dis[8] + src_tmp_dis[9]) / 4;
			target_dis[area_idx] =
				(target_tmp_dis[6] + target_tmp_dis[7] + target_tmp_dis[8] + target_tmp_dis[9]) / 4;
		}
		//std::cout << "SRC Distance >>>> " << "AREA : "<< area_idx << " VALUE : " << src_dis[area_idx] << endl;
		//std::cout << "TARGET Distance >>>> " << "AREA : " << area_idx << " VALUE : " << target_dis[area_idx] << endl;
	}

	// eye repositioning to target
	int eye_area_idx = 0;

	// target eye R positioninig by rate
	const static int eye_R_landmark_size = 12;
	const static int eye_L_landmark_size = 12;

	int eye_R_landmark_idx[eye_R_landmark_size] =
	{ 1355, 1358, 1361, 1364, 1366, 1368, 1340, 445, 1044, 439, 802, 432 };

	float *result_eye_R_X = new float[eye_R_landmark_size];
	float *result_eye_R_Y = new float[eye_R_landmark_size];

	float *target_eye_R_X = new float[eye_R_landmark_size];
	float *target_eye_R_Y = new float[eye_R_landmark_size];

	for (int lm_idx = 0; lm_idx < eye_R_landmark_size; lm_idx++) {
		result_eye_R_X[lm_idx] = calcXRating(src_dis[eye_area_idx], target_dis[eye_area_idx], src_x_pos[eye_R_landmark_idx[lm_idx]]);
		result_eye_R_Y[lm_idx] = calcXRating(src_dis[eye_area_idx], target_dis[eye_area_idx], src_y_pos[eye_R_landmark_idx[lm_idx]]);

		target_eye_R_X[lm_idx] = target_x_pos[eye_R_landmark_idx[lm_idx]];
		target_eye_R_Y[lm_idx] = target_y_pos[eye_R_landmark_idx[lm_idx]];
	}

	// target eye L positioninig by rate
	const static int eye_L_landmark_idx[eye_L_landmark_size] =
	{ 2787, 2790, 2792, 2795, 2798, 2800, 2772, 1877, 2476, 1871, 2234, 1864 };

	float *result_eye_L_X = new float[eye_L_landmark_size];
	float *result_eye_L_Y = new float[eye_L_landmark_size];

	float *target_eye_L_X = new float[eye_L_landmark_size];
	float *target_eye_L_Y = new float[eye_L_landmark_size];

	for (int lm_idx = 0; lm_idx < eye_L_landmark_size; lm_idx++) {
		result_eye_L_X[lm_idx] = calcXRating(src_dis[eye_area_idx], target_dis[eye_area_idx], src_x_pos[eye_L_landmark_idx[lm_idx]]);
		result_eye_L_Y[lm_idx] = calcXRating(src_dis[eye_area_idx], target_dis[eye_area_idx], src_y_pos[eye_L_landmark_idx[lm_idx]]);

		target_eye_L_X[lm_idx] = target_x_pos[eye_L_landmark_idx[lm_idx]];
		target_eye_L_Y[lm_idx] = target_y_pos[eye_L_landmark_idx[lm_idx]];
	}

	// nose repositioning to target
	int nose_area_idx = 1;
	const static int nose_landmark_size = 18;
	const static int nose_R_landmark_size = 9;
	const static int nose_L_landmark_size = 9;
	const static int nose_C_landmark_size = 8;

	const static int nose_landmark_idx[nose_landmark_size] =
	{ 704, 553, 1037, 1013, 1001, 1004, 480, 913, 462, 2136, 1985, 2469, 2448, 2438, 2436, 1912, 2344, 2944 };

	float *result_nose_X = new float[nose_landmark_size];
	float *result_nose_Y = new float[nose_landmark_size];

	float *target_nose_X = new float[nose_landmark_size];
	float *target_nose_Y = new float[nose_landmark_size];

	for (int lm_idx = 0; lm_idx < nose_landmark_size; lm_idx++) {
		result_nose_X[lm_idx] = calcXRating(src_dis[nose_area_idx], target_dis[nose_area_idx], src_x_pos[nose_landmark_idx[lm_idx]]);
		result_nose_Y[lm_idx] = calcXRating(src_dis[nose_area_idx], target_dis[nose_area_idx], src_y_pos[nose_landmark_idx[lm_idx]]);

		target_nose_X[lm_idx] = target_x_pos[nose_landmark_idx[lm_idx]];
		target_nose_Y[lm_idx] = target_y_pos[nose_landmark_idx[lm_idx]];
	}

	// lips repositioning to target
	int lips_area_idx = 2;
	const static int lips_landmark_size = 38;
	const static int lips_landmark_idx[lips_landmark_size] =
	{ 1425, 1445, 1442, 1440, 1438, 1437, 79, 2869, 2870, 2872, 2874, 2877, 2834,
		2885, 2887, 1716, 1714, 1796, 21, 364, 282, 284, 1455, 1453, 1397, 1394, 1391,
		0, 2823, 2826, 2829, 2828, 2841, 2843, 1, 1411, 1409, 1406 };

	int res_lips_idx = 9;

	float *result_lips_X = new float[lips_landmark_size];
	float *result_lips_Y = new float[lips_landmark_size];

	float *target_lips_X = new float[lips_landmark_size];
	float *target_lips_Y = new float[lips_landmark_size];

	for (int lm_idx = 0; lm_idx < lips_landmark_size; lm_idx++) {
		result_lips_X[lm_idx] = calcXRating(src_dis[lips_area_idx], target_dis[lips_area_idx], src_x_pos[lips_landmark_idx[lm_idx]]);
		result_lips_Y[lm_idx] = calcXRating(src_dis[lips_area_idx], target_dis[lips_area_idx], src_y_pos[lips_landmark_idx[lm_idx]]);

		target_lips_X[lm_idx] = target_x_pos[lips_landmark_idx[lm_idx]];
		target_lips_Y[lm_idx] = target_y_pos[lips_landmark_idx[lm_idx]];
	}

	float* rate_average_res = new float[8];
	rate_average_res[0] = calcAverage(result_eye_R_X, eye_R_landmark_size);
	rate_average_res[1] = calcAverage(result_eye_R_Y, eye_R_landmark_size);

	rate_average_res[2] = calcAverage(result_eye_L_X, eye_L_landmark_size);
	rate_average_res[3] = calcAverage(result_eye_L_Y, eye_L_landmark_size);

	rate_average_res[4] = calcAverage(result_nose_X, nose_landmark_size);
	rate_average_res[5] = calcAverage(result_nose_Y, nose_landmark_size);

	rate_average_res[6] = calcAverage(result_lips_X, lips_landmark_size);
	rate_average_res[7] = calcAverage(result_lips_Y, lips_landmark_size);

	float *target_obj_average = new float[8];
	target_obj_average[0] = calcAverage(target_eye_R_X, eye_R_landmark_size);
	target_obj_average[1] = calcAverage(target_eye_R_Y, eye_R_landmark_size);

	target_obj_average[2] = calcAverage(target_eye_L_X, eye_L_landmark_size);
	target_obj_average[3] = calcAverage(target_eye_L_Y, eye_L_landmark_size);

	target_obj_average[4] = calcAverage(target_nose_X, nose_landmark_size);
	target_obj_average[5] = calcAverage(target_nose_Y, nose_landmark_size);

	target_obj_average[6] = calcAverage(target_lips_X, lips_landmark_size);
	target_obj_average[7] = calcAverage(target_lips_Y, lips_landmark_size);

	float* result = new float[8];
	for (int i = 0; i < 8; i++) {
		result[i] = target_obj_average[i] - rate_average_res[i];
		//result[i] = rate_average_res[i] - target_obj_average[i];
	}

	unordered_map<string, float> res_set;
	res_set.clear();

	res_set.insert(unordered_map<string, float>::value_type("eye_R_X", result[0]));
	res_set.insert(unordered_map<string, float>::value_type("eye_R_Y", result[1]));

	res_set.insert(unordered_map<string, float>::value_type("eye_L_X", result[2]));
	res_set.insert(unordered_map<string, float>::value_type("eye_L_Y", result[3]));

	res_set.insert(unordered_map<string, float>::value_type("nose_X", result[4]));
	res_set.insert(unordered_map<string, float>::value_type("nose_Y", result[5]));

	res_set.insert(unordered_map<string, float>::value_type("lips_X", result[6]));
	res_set.insert(unordered_map<string, float>::value_type("lips_Y", result[7]));

	// calculator result by rate
	/*
	for (int i = 0; i < eye_L_landmark_size; i++) {
	std::cout << "RESULT EYE R---------------------------------------------------" << endl;
	std::cout << "X: " << result_eye_R_X[i] << " Y: " << result_eye_R_Y[i] << endl;

	std::cout << "RESULT EYE L---------------------------------------------------" << endl;
	std::cout << "X: " << result_eye_L_X[i] << " Y: " << result_eye_L_Y[i] << endl;
	}

	for (int i = 0; i < nose_L_landmark_size; i++) {
	std::cout << "RESULT NOSE R---------------------------------------------------" << endl;
	std::cout << "X: " << result_nose_R_X[i] << " Y: " << result_nose_R_Y[i] << endl;

	std::cout << "RESULT NOSE L---------------------------------------------------" << endl;
	std::cout << "X: " << result_nose_L_X[i] << " Y: " << result_nose_L_Y[i] << endl;
	}

	for (int i = 0; i < lips_landmark_size; i++) {
	std::cout << "RESULT LIPS ----------------------------------------------------" << endl;
	std::cout << "X: " << result_lips_X[i] << " Y: " << result_lips_Y[i] << endl;
	}
	*/
	std::cout << "RESULT START -------------------------------------" << endl;
	unordered_map<string, float>::iterator itr;
	for (itr = res_set.begin(); itr != res_set.end(); itr++) {
		cout << itr->first << " : " << itr->second << endl;
	}
	std::cout << "RESULT END   -------------------------------------" << endl;

	std::cout << endl;
	std::cout << "--------------- compareMesh fucntion END ---------------" << endl;
	std::cout << endl;

	return res_set;
}
//

/*
x1 : 다각형의 x 좌표 중 가장 작은 값
y1 : 다각형의 y 좌표 중 가장 작은 값
x2 : 다각형의 x 좌표 중 가장 큰 값
y2 : 다각형의 y 좌표 중 가장 큰 값
center.x = x1 + ((x2 - x1) / 2);
center.y = y1 + ((y2 - y1) / 2);
*/
float calcCentralYgap_1(const int *src_indices, float *src_x_pos, float *src_y_pos, const int src_size,
	const int *tg_indices, float *tg_x_pos, float *tg_y_pos, const int tg_size) {

	float *src_max_min_X_val = new float[src_size];
	float *src_max_min_Y_val = new float[src_size];
	float *tg_max_min_X_val = new float[tg_size];
	float *tg_max_min_Y_val = new float[tg_size];

	for (int i = 0; i < src_size; i++) {
		src_max_min_X_val[i] = src_x_pos[src_indices[i]];
		src_max_min_Y_val[i] = src_y_pos[src_indices[i]];
	}

	float src_max_X = calcMaxVal(src_max_min_X_val, src_size);
	float src_min_X = calcMinVal(src_max_min_X_val, src_size);
	float src_max_Y = calcMaxVal(src_max_min_Y_val, src_size);
	float src_min_Y = calcMinVal(src_max_min_Y_val, src_size);
	float src_central_node_Y = src_min_Y + ((src_max_Y - src_min_Y) / 2);

	for (int i = 0; i < tg_size; i++) {
		tg_max_min_X_val[i] = tg_x_pos[tg_indices[i]];
		tg_max_min_Y_val[i] = tg_y_pos[tg_indices[i]];
	}

	float tg_max_X = calcMaxVal(tg_max_min_X_val, tg_size);
	float tg_min_X = calcMinVal(tg_max_min_X_val, tg_size);
	float tg_max_Y = calcMaxVal(tg_max_min_Y_val, tg_size);
	float tg_min_Y = calcMinVal(tg_max_min_Y_val, tg_size);
	float tg_central_node_Y = tg_min_Y + ((tg_max_Y - tg_min_Y) / 2);

	return src_central_node_Y - tg_central_node_Y;
}

float calcCentralYgap_1_1(const int *indices, const int size, float *src_x_pos, float *src_y_pos, float *tg_x_pos, float *tg_y_pos) {

	float src_t_y, src_b_y, tg_t_y, tg_b_y;
	for (int i = 0; i < size; i++) {
		if (indices[i] == 1364) src_t_y = src_y_pos[indices[i]]; tg_t_y = src_y_pos[indices[i]];
		if (indices[i] == 439)	src_b_y = src_y_pos[indices[i]]; tg_b_y = src_y_pos[indices[i]];
	}


	float src_central_node_Y = (src_t_y + src_b_y) / 2;
	float tg_central_node_Y = (tg_t_y + tg_b_y) / 2;

	return src_central_node_Y - tg_central_node_Y;
}


float calcCentralYgap_2(const int *indices, const int size, float *src_x_pos, float *src_y_pos, float src_bottom_Y, float *tg_x_pos, float *tg_y_pos, float tg_bottom_Y) {

	float *src_max_min_X_val = new float[size];
	float *src_max_min_Y_val = new float[size];
	float *tg_max_min_X_val = new float[size];
	float *tg_max_min_Y_val = new float[size];

	for (int i = 0; i < size; i++) {
		src_max_min_X_val[i] = src_x_pos[indices[i]];
		src_max_min_Y_val[i] = src_y_pos[indices[i]];
		tg_max_min_X_val[i] = tg_x_pos[indices[i]];
		tg_max_min_Y_val[i] = tg_y_pos[indices[i]];
	}

	float src_max_X = calcMaxVal(src_max_min_X_val, size);
	float src_min_X = calcMinVal(src_max_min_X_val, size);
	float src_max_Y = calcMaxVal(src_max_min_Y_val, size);
	float src_min_Y = calcMinVal(src_max_min_Y_val, size);
	float src_central_node_Y = src_min_Y + ((src_max_Y - src_min_Y) / 2);

	float tg_max_X = calcMaxVal(tg_max_min_X_val, size);
	float tg_min_X = calcMinVal(tg_max_min_X_val, size);
	float tg_max_Y = calcMaxVal(tg_max_min_Y_val, size);
	float tg_min_Y = calcMinVal(tg_max_min_Y_val, size);
	float tg_central_node_Y = tg_min_Y + ((tg_max_Y - tg_min_Y) / 2);

	return (src_central_node_Y - src_bottom_Y) - (tg_central_node_Y - tg_bottom_Y);
}

float calcCentralY(const int *landmark_idx, float *x_pos, float *y_pos, const int size) {
	float *max_min_X_val = new float[size];
	float *max_min_Y_val = new float[size];

	for (int i = 0; i < size; i++) {
		max_min_X_val[i] = x_pos[landmark_idx[i]];
		max_min_Y_val[i] = y_pos[landmark_idx[i]];
	}

	float max_X = calcMaxVal(max_min_X_val, size);
	float min_X = calcMinVal(max_min_X_val, size);

	float max_Y = calcMaxVal(max_min_Y_val, size);
	float min_Y = calcMinVal(max_min_Y_val, size);

	float central_node_Y = min_Y + ((max_Y - min_Y) / 2);
	return central_node_Y;
}

float calcCentralX(const int *landmark_idx, float *x_pos, float *y_pos, const int size) {
	float *max_min_X_val = new float[size];
	float *max_min_Y_val = new float[size];

	for (int i = 0; i < size; i++) {
		max_min_X_val[i] = x_pos[landmark_idx[i]];
		max_min_Y_val[i] = y_pos[landmark_idx[i]];
	}

	float max_X = calcMaxVal(max_min_X_val, size);
	float min_X = calcMinVal(max_min_X_val, size);

	float max_Y = calcMaxVal(max_min_Y_val, size);
	float min_Y = calcMinVal(max_min_Y_val, size);

	float central_node_X = min_X + ((max_X - min_X) / 2);
	return central_node_X;
}

float calcGap(float src, float target) {
	float gap = src - target;
	return gap;
}

float calcCentralY_1003(const int *landmark_idx, float *x_pos, float *y_pos, const int size) {
	float *max_min_X_val = new float[size];
	float *max_min_Y_val = new float[size];

	for (int i = 0; i < size; i++) {
		max_min_X_val[i] = x_pos[landmark_idx[i]];
		max_min_Y_val[i] = y_pos[landmark_idx[i]];
	}

	float max_X = calcMaxVal(max_min_X_val, size);
	float min_X = calcMinVal(max_min_X_val, size);

	float max_Y = calcMaxVal(max_min_Y_val, size);
	float min_Y = calcMinVal(max_min_Y_val, size);

	float central_node_Y = min_Y + ((max_Y - min_Y) / 2);
	return central_node_Y;
}

float calcCentralX_1003(const int *landmark_idx, float *x_pos, float *y_pos, const int size) {
	float *max_min_X_val = new float[size];
	float *max_min_Y_val = new float[size];

	for (int i = 0; i < size; i++) {
		max_min_X_val[i] = x_pos[landmark_idx[i]];
		max_min_Y_val[i] = y_pos[landmark_idx[i]];
	}

	float max_X = calcMaxVal(max_min_X_val, size);
	float min_X = calcMinVal(max_min_X_val, size);

	float max_Y = calcMaxVal(max_min_Y_val, size);
	float min_Y = calcMinVal(max_min_Y_val, size);

	float central_node_X = min_X + ((max_X - min_X) / 2);
	return central_node_X;
}

float calcMaxVal(float *arr, int arr_size) {
	float max = arr[0];
	for (int i = 0; i < arr_size; i++) {
		if (arr[i] > max)	max = arr[i];
	}
	return max;
}

float calcMinVal(float *arr, int arr_size) {
	float min = arr[0];
	for (int i = 0; i < arr_size; i++) {
		if (arr[i] < min)	min = arr[i];
	}
	return min;
}

float calcAverage(float *req, int size) {
	float average;
	float tmp = 0.0f;

	for (int i = 0; i < size; i++) {
		tmp += req[i];
	}
	average = tmp / size;
	return average;
}

float calcDistance(float x1, float y1, float x2, float y2) {
	float distance = sqrt(pow(x1 - x2, 2) + pow(y1 - y2, 2));
	return distance;
}

float calcXRating(float src_dis, float target_dis, float srcX) {
	float calc_result = (target_dis * srcX) / src_dis;
	return calc_result;
}

float calcYRating(float src_dis, float target_dis, float srcY) {
	float calc_result = (target_dis * srcY) / src_dis;
	return calc_result;
}

int eyeType(int received_data) {

	int eye_case = 0;

	unordered_map<int, int> eyeTypeMap;

	///삼각 눈
	eyeTypeMap.insert(make_pair(1, 9)); eyeTypeMap.insert(make_pair(2, 24)); eyeTypeMap.insert(make_pair(3, 73)); eyeTypeMap.insert(make_pair(4, 14)); eyeTypeMap.insert(make_pair(5, 9)); eyeTypeMap.insert(make_pair(6, 24)); eyeTypeMap.insert(make_pair(7, 28)); eyeTypeMap.insert(make_pair(8, 28)); eyeTypeMap.insert(make_pair(9, 47));
	eyeTypeMap.insert(make_pair(10, 11)); eyeTypeMap.insert(make_pair(11, 13)); eyeTypeMap.insert(make_pair(12, 73)); eyeTypeMap.insert(make_pair(13, 17)); eyeTypeMap.insert(make_pair(14, 11)); eyeTypeMap.insert(make_pair(15, 13));	eyeTypeMap.insert(make_pair(16, 20)); eyeTypeMap.insert(make_pair(17, 20)); eyeTypeMap.insert(make_pair(18, 47));
	eyeTypeMap.insert(make_pair(19, 11)); eyeTypeMap.insert(make_pair(20, 13));	eyeTypeMap.insert(make_pair(21, 13)); eyeTypeMap.insert(make_pair(22, 17)); eyeTypeMap.insert(make_pair(23, 11)); eyeTypeMap.insert(make_pair(24, 13)); eyeTypeMap.insert(make_pair(25, 20)); eyeTypeMap.insert(make_pair(26, 20)); eyeTypeMap.insert(make_pair(27, 47));

	//반달 눈
	eyeTypeMap.insert(make_pair(28, 19)); eyeTypeMap.insert(make_pair(29, 39)); eyeTypeMap.insert(make_pair(30, 37)); eyeTypeMap.insert(make_pair(31, 19)); eyeTypeMap.insert(make_pair(32, 19)); eyeTypeMap.insert(make_pair(33, 7)); eyeTypeMap.insert(make_pair(34, 39)); eyeTypeMap.insert(make_pair(35, 39)); eyeTypeMap.insert(make_pair(36, 7));
	eyeTypeMap.insert(make_pair(37, 19)); eyeTypeMap.insert(make_pair(38, 19)); eyeTypeMap.insert(make_pair(39, 7)); eyeTypeMap.insert(make_pair(40, 19)); eyeTypeMap.insert(make_pair(41, 19)); eyeTypeMap.insert(make_pair(42, 7)); eyeTypeMap.insert(make_pair(43, 38)); eyeTypeMap.insert(make_pair(44, 38)); eyeTypeMap.insert(make_pair(45, 7));
	eyeTypeMap.insert(make_pair(46, 29)); eyeTypeMap.insert(make_pair(47, 29)); eyeTypeMap.insert(make_pair(48, 29)); eyeTypeMap.insert(make_pair(49, 29)); eyeTypeMap.insert(make_pair(50, 29)); eyeTypeMap.insert(make_pair(51, 29)); eyeTypeMap.insert(make_pair(52, 29)); eyeTypeMap.insert(make_pair(53, 29)); eyeTypeMap.insert(make_pair(54, 29));

	//둥근 눈
	eyeTypeMap.insert(make_pair(55, 32)); eyeTypeMap.insert(make_pair(56, 44)); eyeTypeMap.insert(make_pair(57, 23)); eyeTypeMap.insert(make_pair(58, 15)); eyeTypeMap.insert(make_pair(59, 10)); eyeTypeMap.insert(make_pair(60, 21));	eyeTypeMap.insert(make_pair(61, 22)); eyeTypeMap.insert(make_pair(62, 22)); eyeTypeMap.insert(make_pair(63, 22));
	eyeTypeMap.insert(make_pair(64, 34)); eyeTypeMap.insert(make_pair(65, 31)); eyeTypeMap.insert(make_pair(66, 1));  eyeTypeMap.insert(make_pair(67, 4)); eyeTypeMap.insert(make_pair(68, 18)); eyeTypeMap.insert(make_pair(69, 12)); eyeTypeMap.insert(make_pair(70, 8)); eyeTypeMap.insert(make_pair(71, 8)); eyeTypeMap.insert(make_pair(72, 42));
	eyeTypeMap.insert(make_pair(73, 62)); eyeTypeMap.insert(make_pair(74, 50)); eyeTypeMap.insert(make_pair(75, 50));  eyeTypeMap.insert(make_pair(76, 43)); eyeTypeMap.insert(make_pair(77, 62)); eyeTypeMap.insert(make_pair(78, 50)); eyeTypeMap.insert(make_pair(79, 8)); eyeTypeMap.insert(make_pair(80, 8));	eyeTypeMap.insert(make_pair(81, 8));

	//보통 눈
	eyeTypeMap.insert(make_pair(82, 32)); eyeTypeMap.insert(make_pair(83, 44)); eyeTypeMap.insert(make_pair(84, 23)); eyeTypeMap.insert(make_pair(85, 15)); eyeTypeMap.insert(make_pair(86, 10)); eyeTypeMap.insert(make_pair(87, 21)); eyeTypeMap.insert(make_pair(88, 22)); eyeTypeMap.insert(make_pair(89, 22)); eyeTypeMap.insert(make_pair(90, 22));
	eyeTypeMap.insert(make_pair(91, 34)); eyeTypeMap.insert(make_pair(92, 31)); eyeTypeMap.insert(make_pair(93, 1)); eyeTypeMap.insert(make_pair(94, 4)); eyeTypeMap.insert(make_pair(95, 18)); eyeTypeMap.insert(make_pair(96, 12)); eyeTypeMap.insert(make_pair(97, 8)); eyeTypeMap.insert(make_pair(98, 8)); eyeTypeMap.insert(make_pair(99, 42));
	eyeTypeMap.insert(make_pair(100, 62)); eyeTypeMap.insert(make_pair(101, 50)); eyeTypeMap.insert(make_pair(102, 50)); eyeTypeMap.insert(make_pair(103, 43)); eyeTypeMap.insert(make_pair(104, 62)); eyeTypeMap.insert(make_pair(105, 50));	eyeTypeMap.insert(make_pair(106, 8)); eyeTypeMap.insert(make_pair(107, 8)); eyeTypeMap.insert(make_pair(108, 8));


	// 사각 눈
	eyeTypeMap.insert(make_pair(109, 27)); eyeTypeMap.insert(make_pair(110, 36)); eyeTypeMap.insert(make_pair(111, 6)); eyeTypeMap.insert(make_pair(112, 16)); eyeTypeMap.insert(make_pair(113, 27)); eyeTypeMap.insert(make_pair(114, 36)); eyeTypeMap.insert(make_pair(115, 26)); eyeTypeMap.insert(make_pair(116, 26)); eyeTypeMap.insert(make_pair(117, 46));
	eyeTypeMap.insert(make_pair(118, 27)); eyeTypeMap.insert(make_pair(119, 6)); eyeTypeMap.insert(make_pair(120, 71)); eyeTypeMap.insert(make_pair(121, 16)); eyeTypeMap.insert(make_pair(122, 27)); eyeTypeMap.insert(make_pair(123, 6)); eyeTypeMap.insert(make_pair(124, 26)); eyeTypeMap.insert(make_pair(125, 26)); eyeTypeMap.insert(make_pair(126, 46));
	eyeTypeMap.insert(make_pair(127, 27)); eyeTypeMap.insert(make_pair(128, 6)); eyeTypeMap.insert(make_pair(129, 6)); eyeTypeMap.insert(make_pair(130, 41)); eyeTypeMap.insert(make_pair(131, 27)); eyeTypeMap.insert(make_pair(132, 6)); eyeTypeMap.insert(make_pair(133, 26)); eyeTypeMap.insert(make_pair(134, 26)); eyeTypeMap.insert(make_pair(135, 46));

	//눈 정보 저장
	eye_case = eyeTypeMap.find(received_data)->second;

	return eye_case;
}

int noseType(int received_data)
{
	int nose_case = 0;
	unordered_map<int, int> noseTypeMap;

	//코 분류
	noseTypeMap.insert(make_pair(1, 2)); noseTypeMap.insert(make_pair(2, 10)); noseTypeMap.insert(make_pair(3, 35)); noseTypeMap.insert(make_pair(4, 13)); noseTypeMap.insert(make_pair(5, 18)); noseTypeMap.insert(make_pair(6, 46)); noseTypeMap.insert(make_pair(7, 5)); noseTypeMap.insert(make_pair(8, 6)); noseTypeMap.insert(make_pair(9, 7));
	noseTypeMap.insert(make_pair(10, 17)); noseTypeMap.insert(make_pair(11, 36)); noseTypeMap.insert(make_pair(12, 25)); noseTypeMap.insert(make_pair(13, 1)); noseTypeMap.insert(make_pair(14, 9)); noseTypeMap.insert(make_pair(15, 4)); noseTypeMap.insert(make_pair(16, 14)); noseTypeMap.insert(make_pair(17, 16)); noseTypeMap.insert(make_pair(18, 34));
	noseTypeMap.insert(make_pair(19, 12)); noseTypeMap.insert(make_pair(20, 15)); noseTypeMap.insert(make_pair(21, 21)); noseTypeMap.insert(make_pair(22, 20)); noseTypeMap.insert(make_pair(23, 8)); noseTypeMap.insert(make_pair(24, 11)); noseTypeMap.insert(make_pair(25, 20)); noseTypeMap.insert(make_pair(26, 8)); noseTypeMap.insert(make_pair(27, 11));

	// 코 정보 저장
	nose_case = noseTypeMap.find(received_data)->second;

	return nose_case;
}

int lipType(int received_data) {

	int lip_case = 0;

	unordered_map<int, int> lipTypeMap;

	///일반 입
	lipTypeMap.insert(make_pair(1, 10)); lipTypeMap.insert(make_pair(2, 10)); lipTypeMap.insert(make_pair(3, 10)); lipTypeMap.insert(make_pair(4, 10)); lipTypeMap.insert(make_pair(5, 10)); lipTypeMap.insert(make_pair(6, 10)); lipTypeMap.insert(make_pair(7, 10)); lipTypeMap.insert(make_pair(8, 10)); lipTypeMap.insert(make_pair(9, 10));
	lipTypeMap.insert(make_pair(10, 10)); lipTypeMap.insert(make_pair(11, 10)); lipTypeMap.insert(make_pair(12, 10)); lipTypeMap.insert(make_pair(13, 10)); lipTypeMap.insert(make_pair(14, 10)); lipTypeMap.insert(make_pair(15, 10)); lipTypeMap.insert(make_pair(16, 10)); lipTypeMap.insert(make_pair(17, 10)); lipTypeMap.insert(make_pair(18, 10));
	lipTypeMap.insert(make_pair(19, 13)); lipTypeMap.insert(make_pair(20, 13)); lipTypeMap.insert(make_pair(21, 13)); lipTypeMap.insert(make_pair(22, 13)); lipTypeMap.insert(make_pair(23, 13)); lipTypeMap.insert(make_pair(24, 13)); lipTypeMap.insert(make_pair(25, 13)); lipTypeMap.insert(make_pair(26, 13)); lipTypeMap.insert(make_pair(27, 13));

	lipTypeMap.insert(make_pair(28, 14)); lipTypeMap.insert(make_pair(29, 14)); lipTypeMap.insert(make_pair(30, 14)); lipTypeMap.insert(make_pair(31, 14)); lipTypeMap.insert(make_pair(32, 14)); lipTypeMap.insert(make_pair(33, 14)); lipTypeMap.insert(make_pair(34, 14)); lipTypeMap.insert(make_pair(35, 14)); lipTypeMap.insert(make_pair(36, 14));
	lipTypeMap.insert(make_pair(37, 21)); lipTypeMap.insert(make_pair(38, 4)); lipTypeMap.insert(make_pair(39, 3)); lipTypeMap.insert(make_pair(40, 1)); lipTypeMap.insert(make_pair(41, 21)); lipTypeMap.insert(make_pair(42, 4)); lipTypeMap.insert(make_pair(43, 12)); lipTypeMap.insert(make_pair(44, 1)); lipTypeMap.insert(make_pair(45, 21));
	lipTypeMap.insert(make_pair(46, 16)); lipTypeMap.insert(make_pair(47, 6)); lipTypeMap.insert(make_pair(48, 3)); lipTypeMap.insert(make_pair(49, 17)); lipTypeMap.insert(make_pair(50, 16));	lipTypeMap.insert(make_pair(51, 6)); lipTypeMap.insert(make_pair(52, 17)); lipTypeMap.insert(make_pair(53, 17)); lipTypeMap.insert(make_pair(54, 16));

	lipTypeMap.insert(make_pair(55, 14)); lipTypeMap.insert(make_pair(56, 7)); lipTypeMap.insert(make_pair(57, 15)); lipTypeMap.insert(make_pair(58, 14)); lipTypeMap.insert(make_pair(59, 14)); lipTypeMap.insert(make_pair(60, 7)); lipTypeMap.insert(make_pair(61, 1)); lipTypeMap.insert(make_pair(62, 1)); lipTypeMap.insert(make_pair(63, 14));
	lipTypeMap.insert(make_pair(64, 2)); lipTypeMap.insert(make_pair(65, 5)); lipTypeMap.insert(make_pair(66, 5)); lipTypeMap.insert(make_pair(67, 2)); lipTypeMap.insert(make_pair(68, 2)); lipTypeMap.insert(make_pair(69, 5)); lipTypeMap.insert(make_pair(70, 2)); lipTypeMap.insert(make_pair(71, 2)); lipTypeMap.insert(make_pair(72, 2));
	lipTypeMap.insert(make_pair(73, 16)); lipTypeMap.insert(make_pair(74, 6)); lipTypeMap.insert(make_pair(75, 3)); lipTypeMap.insert(make_pair(76, 17)); lipTypeMap.insert(make_pair(77, 16)); lipTypeMap.insert(make_pair(78, 6)); lipTypeMap.insert(make_pair(79, 17)); lipTypeMap.insert(make_pair(80, 17)); lipTypeMap.insert(make_pair(81, 16));


	// M형
	lipTypeMap.insert(make_pair(82, 10)); lipTypeMap.insert(make_pair(83, 10)); lipTypeMap.insert(make_pair(84, 10)); lipTypeMap.insert(make_pair(85, 10)); lipTypeMap.insert(make_pair(86, 10)); lipTypeMap.insert(make_pair(87, 10)); lipTypeMap.insert(make_pair(88, 10)); lipTypeMap.insert(make_pair(89, 10)); lipTypeMap.insert(make_pair(90, 10));
	lipTypeMap.insert(make_pair(91, 10)); lipTypeMap.insert(make_pair(92, 10)); lipTypeMap.insert(make_pair(93, 10)); lipTypeMap.insert(make_pair(94, 10)); lipTypeMap.insert(make_pair(95, 10)); lipTypeMap.insert(make_pair(96, 10)); lipTypeMap.insert(make_pair(97, 10)); lipTypeMap.insert(make_pair(98, 10)); lipTypeMap.insert(make_pair(99, 10));
	lipTypeMap.insert(make_pair(100, 13)); lipTypeMap.insert(make_pair(101, 13)); lipTypeMap.insert(make_pair(102, 13)); lipTypeMap.insert(make_pair(103, 13)); lipTypeMap.insert(make_pair(104, 13)); lipTypeMap.insert(make_pair(105, 13)); lipTypeMap.insert(make_pair(106, 13)); lipTypeMap.insert(make_pair(107, 13)); lipTypeMap.insert(make_pair(108, 13));

	lipTypeMap.insert(make_pair(109, 14)); lipTypeMap.insert(make_pair(110, 14)); lipTypeMap.insert(make_pair(111, 14)); lipTypeMap.insert(make_pair(112, 14)); lipTypeMap.insert(make_pair(113, 14)); lipTypeMap.insert(make_pair(114, 14)); lipTypeMap.insert(make_pair(115, 14)); lipTypeMap.insert(make_pair(116, 14)); lipTypeMap.insert(make_pair(117, 14));
	lipTypeMap.insert(make_pair(118, 21)); lipTypeMap.insert(make_pair(119, 4)); lipTypeMap.insert(make_pair(120, 9)); lipTypeMap.insert(make_pair(121, 1)); lipTypeMap.insert(make_pair(122, 20)); lipTypeMap.insert(make_pair(123, 19)); lipTypeMap.insert(make_pair(124, 12)); lipTypeMap.insert(make_pair(125, 1)); lipTypeMap.insert(make_pair(126, 21));
	lipTypeMap.insert(make_pair(127, 16)); lipTypeMap.insert(make_pair(128, 6)); lipTypeMap.insert(make_pair(129, 3)); lipTypeMap.insert(make_pair(130, 17)); lipTypeMap.insert(make_pair(131, 16));	lipTypeMap.insert(make_pair(132, 6)); lipTypeMap.insert(make_pair(133, 17)); lipTypeMap.insert(make_pair(134, 17)); lipTypeMap.insert(make_pair(135, 16));

	lipTypeMap.insert(make_pair(136, 14)); lipTypeMap.insert(make_pair(137, 7)); lipTypeMap.insert(make_pair(138, 15)); lipTypeMap.insert(make_pair(139, 14)); lipTypeMap.insert(make_pair(140, 14)); lipTypeMap.insert(make_pair(141, 7)); lipTypeMap.insert(make_pair(142, 1)); lipTypeMap.insert(make_pair(143, 1)); lipTypeMap.insert(make_pair(144, 14));
	lipTypeMap.insert(make_pair(145, 2)); lipTypeMap.insert(make_pair(146, 5)); lipTypeMap.insert(make_pair(147, 5)); lipTypeMap.insert(make_pair(148, 2)); lipTypeMap.insert(make_pair(149, 2)); lipTypeMap.insert(make_pair(150, 5)); lipTypeMap.insert(make_pair(151, 2)); lipTypeMap.insert(make_pair(152, 2)); lipTypeMap.insert(make_pair(153, 2));
	lipTypeMap.insert(make_pair(154, 16)); lipTypeMap.insert(make_pair(155, 6)); lipTypeMap.insert(make_pair(156, 3)); lipTypeMap.insert(make_pair(157, 17)); lipTypeMap.insert(make_pair(158, 16)); lipTypeMap.insert(make_pair(159, 6)); lipTypeMap.insert(make_pair(160, 17)); lipTypeMap.insert(make_pair(161, 17)); lipTypeMap.insert(make_pair(162, 16));

	lip_case = lipTypeMap.find(received_data)->second;

	return lip_case;
}

